require('dotenv').config()

const path = require('path')

const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const FaviconsWebpackPlugin = require('favicons-webpack-plugin')
const AddAssetHtmlPlugin = require('add-asset-html-webpack-plugin')

const AWS = require('aws-sdk')
const get = require('lodash/get')

const STAGES = {
    DEV: 'Dev',
    BETA: 'Beta',
    PROD: 'Prod',
}
const STAGE = get(process.env, 'STAGE', STAGES.BETA)
const IS_DEV = STAGE === STAGES.DEV

const CONSTANTS = {
    STAGE,
    REGION: get(process.env, 'AWS_REGION', 'us-west-2'),
    STACK: `ForgeInfrastructure-${IS_DEV ? STAGES.BETA : STAGE}`,

    SENTRY_CONFIG_URL: {
        [STAGES.BETA]:
            'https://00f355d347d24e4096f75f2214fcfced@sentry.io/1320029',
        [STAGES.PROD]:
            'https://985aac46a1fa4cddb06515604a9badc0@sentry.io/1314118',
    },

    STACK_OUTPUT_USER_POOL: 'UserPool',
    STACK_OUTPUT_USER_POOL_CLIENT: 'UserPoolClient',
    STACK_OUTPUT_IDENTITY_POOL: 'IdentityPoolID',
    STACK_OUTPUT_USER_SETTINGS_DYNAMODB_TABLE: 'UserSettingsDynamoDBTableName',
    STACK_OUTPUT_FORGE_SETTINGS_DYNAMODB_TABLE:
        'ForgeSettingsDynamoDBTableName',

    CEREBRO_OAUTH_CLIENT_ID: process.env['CEREBRO_OAUTH_CLIENT_ID'],
    CEREBRO_OAUTH_CLIENT_SECRET: process.env['CEREBRO_OAUTH_CLIENT_SECRET'],

    CEREBRO_API_ENDPOINTS: {
        [STAGES.DEV]: get(
            process.env,
            'CEREBRO_ENDPOINT',
            'http://localhost:8000'
        ),
        [STAGES.BETA]: 'https://cerebro.dsi.ninja',
        [STAGES.PROD]: 'https://cerebro.downstreamimpact.com',
    },

    GOOGLE_ANALYTICS_IDS: {
        [STAGES.DEV]: 'UA-113868253-3',
        [STAGES.BETA]: 'UA-113868253-3',
        [STAGES.PROD]: 'UA-113868253-4',
    },

    TERMS_OF_SERVICE: {
        [STAGES.DEV]: 'https://www.dsi.ninja/terms',
        [STAGES.BETA]: 'https://www.dsi.ninja/terms',
        [STAGES.PROD]: 'https://www.downstreamimpact.com/terms',
    },

    REQUEST_A_DEMO: {
        [STAGES.DEV]:
            'https://www.dsi.ninja/request_a_demo/?utm_source=website&utm_medium=forge',
        [STAGES.BETA]:
            'https://www.dsi.ninja/request_a_demo/?utm_source=website&utm_medium=forge',
        [STAGES.PROD]:
            'https://www.downstreamimpact.com/request_a_demo/?utm_source=website&utm_medium=forge',
    },
}

const commonConfigPromise = () => {
    AWS.config.region = CONSTANTS.REGION
    const cloudFormation = new AWS.CloudFormation()
    const params = { StackName: CONSTANTS.STACK }
    let describeStacksPromise = cloudFormation.describeStacks(params).promise()

    return describeStacksPromise
        .then(data => {
            const outputs = get(data, 'Stacks[0].Outputs')
            const resourceIds = {}
            outputs.forEach(value => {
                if (value.OutputKey === CONSTANTS.STACK_OUTPUT_USER_POOL) {
                    resourceIds['USER_POOL_ID'] = value.OutputValue
                } else if (
                    value.OutputKey === CONSTANTS.STACK_OUTPUT_USER_POOL_CLIENT
                ) {
                    resourceIds['USER_POOL_CLIENT_ID'] = value.OutputValue
                } else if (
                    value.OutputKey === CONSTANTS.STACK_OUTPUT_IDENTITY_POOL
                ) {
                    resourceIds['IDENTITY_POOL_ID'] = value.OutputValue
                } else if (
                    value.OutputKey ===
                    CONSTANTS.STACK_OUTPUT_USER_SETTINGS_DYNAMODB_TABLE
                ) {
                    resourceIds['USER_SETTINGS_DYNAMODB_TABLE'] =
                        value.OutputValue
                } else if (
                    value.OutputKey ===
                    CONSTANTS.STACK_OUTPUT_FORGE_SETTINGS_DYNAMODB_TABLE
                ) {
                    resourceIds['FORGE_SETTINGS_DYNAMODB_TABLE'] =
                        value.OutputValue
                }
            })
            return resourceIds
        })
        .then(data => ({
            entry: {
                polyfills: ['@babel/polyfill', 'whatwg-fetch'],
                app: ['./src/index.js'],
            },
            resolve: {
                modules: [
                    path.resolve(__dirname, 'src'),
                    path.resolve(__dirname, 'node_modules'),
                ],
            },
            output: {
                path: path.resolve(__dirname, 'dist/'),

                // Turning off pathinfo in order not to trigger too much Garbage Collection
                // Please reference `Enemy #3: Garbage Collection in the following blog
                // @see https://medium.com/@kenneth_chau/speeding-up-webpack-typescript-incremental-builds-by-7x-3912ba4c1d15
                pathinfo: false,

                filename: '[name]-[hash].js',
                publicPath: '/',
            },
            optimization: {
                runtimeChunk: 'single',

                splitChunks: {
                    cacheGroups: {
                        vendor: {
                            test: /node_modules/,
                            name: 'vendors',
                            chunks: 'all',
                        },
                    },
                },
            },
            module: {
                rules: [
                    {
                        test: /\.jsx?$/,
                        // link for more information: https://github.com/babel/babel-loader/issues/171
                        exclude: /node_modules\/(?!(query-string|qs|strict-uri-encode)\/).*/,
                        use: [
                            'thread-loader',
                            {
                                loader: 'babel-loader',
                                options: {
                                    // This is a feature of `babel-loader` for webpack (not Babel itself).
                                    // It enables caching results in ./node_modules/.cache/babel-loader/
                                    // directory for faster rebuilds.
                                    cacheDirectory: true,
                                    plugins: ['react-hot-loader/babel'],
                                },
                            },
                        ],
                    },

                    {
                        test: /\.(woff|woff2|eot|ttf|otf)$/,
                        use: [
                            {
                                loader: 'file-loader',
                                options: {
                                    name: '[name].[ext]',
                                    outputPath: 'fonts/',
                                },
                            },
                        ],
                    },

                    {
                        test: /\.(png|jpg|svg|gif|jpeg)$/,
                        use: [
                            {
                                loader: 'file-loader',
                                options: {
                                    name: '[name]-[hash].[ext]',
                                    outputPath: 'images/',
                                },
                            },
                        ],
                    },
                ],
            },
            plugins: [
                new webpack.DefinePlugin({
                    STAGE: JSON.stringify(CONSTANTS.STAGE),
                    REGION: JSON.stringify(CONSTANTS.REGION),
                    SENTRY_CONFIG_URL: JSON.stringify(
                        CONSTANTS.SENTRY_CONFIG_URL[CONSTANTS.STAGE]
                    ),
                    USER_POOL_ID: JSON.stringify(data['USER_POOL_ID']),
                    USER_POOL_CLIENT_ID: JSON.stringify(
                        data['USER_POOL_CLIENT_ID']
                    ),
                    IDENTITY_POOL_ID: JSON.stringify(data['IDENTITY_POOL_ID']),
                    USER_SETTINGS_TABLE: JSON.stringify(
                        data['USER_SETTINGS_DYNAMODB_TABLE']
                    ),
                    FORGE_SETTINGS_TABLE: JSON.stringify(
                        data['FORGE_SETTINGS_DYNAMODB_TABLE']
                    ),
                    CEREBRO_OAUTH_CLIENT_ID: JSON.stringify(
                        CONSTANTS.CEREBRO_OAUTH_CLIENT_ID
                    ),
                    CEREBRO_OAUTH_CLIENT_SECRET: JSON.stringify(
                        CONSTANTS.CEREBRO_OAUTH_CLIENT_SECRET
                    ),
                    CEREBRO_API_ENDPOINT: JSON.stringify(
                        CONSTANTS.CEREBRO_API_ENDPOINTS[CONSTANTS.STAGE]
                    ),
                    GOOGLE_ANALYTICS_ID: JSON.stringify(
                        CONSTANTS.GOOGLE_ANALYTICS_IDS[CONSTANTS.STAGE]
                    ),
                    TERMS_OF_SERVICE: JSON.stringify(
                        CONSTANTS.TERMS_OF_SERVICE[CONSTANTS.STAGE]
                    ),
                    REQUEST_A_DEMO: JSON.stringify(
                        CONSTANTS.REQUEST_A_DEMO[CONSTANTS.STAGE]
                    ),
                }),

                new webpack.DllReferencePlugin({
                    context: __dirname,
                    manifest: require('./dist/dll-manifest.json'),
                }),

                new HtmlWebpackPlugin({
                    title: 'Downstream',
                    template: 'src/index.html',
                }),

                new AddAssetHtmlPlugin({
                    filepath: path.resolve(__dirname, './dist/*.dll.js'),
                }),

                new FaviconsWebpackPlugin(
                    path.resolve(__dirname, 'src/images/favicon.png')
                ),
            ],
        }))
        .catch(err => {
            console.error('Unable to initialize: ', err)
        })
}

module.exports = commonConfigPromise
