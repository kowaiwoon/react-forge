module.exports = {
    presets: ['@babel/preset-env', '@babel/preset-react'],
    plugins: [
        ['import', { libraryName: 'antd', style: true }],

        // -----------------------------------------------------------------
        // In Babel 7, stage presets are dropped out, making you
        // fine-tune plugins yourself.
        //
        // @see https://babeljs.io/blog/2018/07/27/removing-babels-stage-presets
        //
        // The followings are plugins that were included in preset-stage-2
        // originally. Please add plugin as needed from now on.
        // ------------------------------------------------------------------

        // Stage 2
        ['@babel/plugin-proposal-decorators', { legacy: true }],
        // '@babel/plugin-proposal-function-sent',
        // '@babel/plugin-proposal-export-namespace-from',
        // '@babel/plugin-proposal-numeric-separator',
        // '@babel/plugin-proposal-throw-expressions',

        // Stage 3
        // '@babel/plugin-syntax-dynamic-import',
        // '@babel/plugin-syntax-import-meta',
        ['@babel/plugin-proposal-class-properties', { loose: false }],
        // '@babel/plugin-proposal-json-strings',
    ],
}
