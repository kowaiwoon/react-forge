const webpack = require('webpack')
const merge = require('webpack-merge')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const commonConfigPromise = require('./webpack.common.js')
const ANT_THEME_OVERRIDES = require('./ant-theme')

const prodConfigPromise = () =>
    commonConfigPromise().then(commonConfig =>
        merge(commonConfig, {
            mode: 'production',
            devtool: 'source-map',
            optimization: {
                minimizer: [
                    new UglifyJsPlugin({
                        sourceMap: true,
                    }),
                    new OptimizeCSSAssetsPlugin({}),
                ],
            },

            module: {
                // Run postCSS plugin (auto-prefixer) to support various browsers
                rules: [
                    {
                        test: /react-date-range\/dist\/styles\.css$/,
                        use: [
                            MiniCssExtractPlugin.loader,
                            {
                                loader: 'css-loader',
                                options: {
                                    importLoaders: 1,
                                },
                            },
                            {
                                loader: 'postcss-loader',
                            },
                        ],
                    },

                    {
                        test: /react-date-range\/dist\/theme\/default\.css$/,
                        use: [
                            MiniCssExtractPlugin.loader,
                            {
                                loader: 'css-loader',
                                options: {
                                    importLoaders: 1,
                                },
                            },
                            {
                                loader: 'postcss-loader',
                            },
                        ],
                    },

                    {
                        test: /\.less$/,
                        use: [
                            MiniCssExtractPlugin.loader,
                            {
                                loader: 'css-loader',
                                options: {
                                    importLoaders: 2,
                                },
                            },
                            'postcss-loader',
                            {
                                loader: 'less-loader',
                                options: {
                                    javascriptEnabled: true,
                                    modifyVars: ANT_THEME_OVERRIDES,
                                },
                            },
                        ],
                    },

                    {
                        test: /\.scss$/,
                        exclude: /node_modules/,
                        use: [
                            MiniCssExtractPlugin.loader,
                            {
                                loader: 'css-loader',
                                options: {
                                    sourceMap: true,
                                    modules: true,
                                    localIdentName: '[local]___[hash:base64:5]',
                                    importLoaders: 2,
                                },
                            },
                            'postcss-loader',
                            'fast-sass-loader',
                        ],
                    },
                ],
            },
            plugins: [
                new webpack.HashedModuleIdsPlugin(),

                new MiniCssExtractPlugin({
                    // Options similar to the same options in webpackOptions.output
                    // both options are optional
                    filename: '[name].[hash].css',
                    chunkFilename: '[id].[hash].css',
                }),
            ],
        })
    )

module.exports = prodConfigPromise
