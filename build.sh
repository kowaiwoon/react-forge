#!/usr/bin/env bash

# Removing the node_modules/ directory from the host ensures the node_modules/
# directory isn't part of the "build context" when the image is built.

# This also ensures a new version of node_modules/ is copied to the host below
rm -rf node_modules/

# Build the docker container image
docker-compose build

# Create a container, copy node_modules/ to host, then remove the container
# See https://github.com/moby/moby/issues/16079#issuecomment-385668818
IMG_ID=$(dd if=/dev/urandom bs=1k count=1 2> /dev/null | LC_CTYPE=C tr -cd "a-z0-9" | cut -c 1-22)
docker create --name ${IMG_ID} forge:latest
docker cp ${IMG_ID}:/Forge/node_modules ./node_modules
docker rm ${IMG_ID}
