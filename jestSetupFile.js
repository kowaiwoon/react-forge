import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

// configure enzyme
configure({ adapter: new Adapter() })

// mock local storage
class LocalStorageMock {
    constructor() {
        this.store = {}
    }

    clear() {
        this.store = {}
    }

    getItem(key) {
        return this.store[key] || null
    }

    setItem(key, value) {
        this.store[key] = value.toString()
    }

    removeItem(key) {
        delete this.store[key]
    }
}

global.localStorage = new LocalStorageMock()
