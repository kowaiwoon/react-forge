const path = require('path')
const webpack = require('webpack')
const HardSourceWebpackPlugin = require('hard-source-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')

const pkg = require('./package.json')

const outputPath = path.join(__dirname, 'dist')

module.exports = {
    entry: {
        vendor: Object.keys(pkg.dependencies),
    },

    output: {
        path: outputPath,
        filename: 'dll-[hash].dll.js',
        library: '[name]_[hash]',
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                use: ['thread-loader', 'babel-loader'],
            },
        ],
    },

    plugins: [
        // DDL build runs first, so clear the entire directory
        new CleanWebpackPlugin(['dist/*'], {
            verbose: true,
            exclude: ['.gitkeep'],
        }),

        new HardSourceWebpackPlugin(),

        // DllPlugin
        new webpack.DllPlugin({
            name: '[name]_[hash]',
            path: path.join(outputPath, 'dll-manifest.json'),
        }),
    ],
}
