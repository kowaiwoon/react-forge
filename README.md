# Forge

<img src="https://img.shields.io/badge/tier-1-brightgreen.svg">

Forge is the source code for the Forge web application.

![Forge](resources/images/forge.jpg?raw=true)

## Coding Conventions

#### Action Creators

Forge leverages the [redux-actions](https://github.com/redux-utilities/redux-actions) to reduce boilerplate
when creating action creators. When creating / using action creators, follow these conventions:

1. In `createAction`, use the default `payloadCreator` (i.e., the `Identity` function) whenever possible. Sometimes
   you'll need to define a custom `payloadCreator` function, but this should be only in special circumstances when the
   payload is to complicated to build when dispatching the action using the action creator.
2. We prefer top level keys in the `payload` object. For example, when calling the action creator function, we prefer
   this `xxxAction({ filters })` instead of this `xxxAction(filters)`.

## Development Setup

### Configure Environment

#### When using Docker, create an `.env` file at the root of the project

```text
AWS_DEFAULT_REGION=us-west-2
AWS_REGION=us-west-2
AWS_ACCESS_KEY_ID=secret
AWS_SECRET_ACCESS_KEY=secret
STAGE=Dev
CEREBRO_ENDPOINT=http://localhost:8000
CEREBRO_OAUTH_CLIENT_ID=rR8Gi6U9ZyAMzjExJcCVgB8z93e8hjFuPQ9nc98A
CEREBRO_OAUTH_CLIENT_SECRET=secret
```

#### When using NPM directly, add the following environment variables to your login file

```bash
# in your .bash_profile (or equivalent)
AWS_DEFAULT_REGION=us-west-2
AWS_REGION=us-west-2
AWS_ACCESS_KEY_ID=secret
AWS_SECRET_ACCESS_KEY=secret
STAGE=Dev
CEREBRO_ENDPOINT=http://localhost:8000
CEREBRO_OAUTH_CLIENT_ID=rR8Gi6U9ZyAMzjExJcCVgB8z93e8hjFuPQ9nc98A
CEREBRO_OAUTH_CLIENT_SECRET=secret
```

#### Notes about environment variables

- `CEREBRO_ENDPOINT` is optional and defaults to `http://localhost:8000` (i.e., assumes you're
  running Cerebro locally). Set this value to `https://cerebro.dsi.ninja` to use the Cerebro API
  deployed to AWS in the Beta Stage.
- Update the `AWS_ACCESS_KEY_ID` with the appropriate value from your AWS IAM User
- Update the `AWS_SECRET_ACCESS_KEY` with the appropriate value your AWS IAM User
- Update the `CEREBRO_OAUTH_CLIENT_SECRET` with the appropriate value from Cerebro

### Start the Development Server

#### Using Docker to run development server

```bash
# build the docker image and install NPM modules
./build.sh

# start the development server
docker-compose up
```

#### Using NPM directly to run development server

```bash
# install dependencies
npm install

# start the development server
npm start
```

## Helpful Development Commands

#### Analyze Webpack Generated Bundles

```bash
# generates webpack-ddl-stats.json and webpack-stats.json files
npm run analyze
```

Visualize the files generated with the command above using the [Webpack Visualizer](https://chrisbateman.github.io/webpack-visualizer).

#### Run ESLint

```bash
# with docker
docker-compose run forge npm run lint

# with npm directly
npm run lint
```

#### Auto-Format Code Using Prettier

```bash
# with docker
docker-compose run forge npm run format

# with npm directly
npm run format
```

#### Run Unit Tests

```bash
# with docker
docker-compose run forge npm run test

# with npm directly
npm run test
```

#### Run Unit Tests in Watch Mode

```bash
# with docker
docker-compose run forge npm run-script test-watch

# with npm directly
npm run test-watch
```

#### Start a Bash Terminal Session in Docker container

```bash
docker-compose run forge bash
```

## Contributing

Feature requests are merged into `development` branch and deployed to `Beta` environment before being merged into `master` and `production` branches and deployed to `Prod` environment.

1. Checkout `development` branch and pull latest changes

```bash
git checkout development && git pull
```

2. Checkout feature branch using Jira ticket number for branch name

```bash
git checkout -b DOW-1193
```

3. Develop on your feature branch making sure to back merge changes in the `development` branch

```bash
git pull origin development
```

5. Format, lint, and run tests once your feature is complete (see above sections for commands to run)

6. Push your branch to Github and open a Pull Request with a descriptive name

```
[DOW-1193] My description of my awesome feature
```

7. Request review in Github and paste link to PR in Slack `#engineering` channel tagging team members with context

## References

- [React Router V4 Tutorial](https://medium.com/@pshrmn/a-simple-react-router-v4-tutorial-7f23ff27adf)
- [Securing React Redux Apps With JWT Tokens](https://medium.com/@rajaraodv/securing-react-redux-apps-with-jwt-tokens-fcfe81356ea0)
- [Configuring JavaScript Libraries in PyCharm](https://www.jetbrains.com/help/pycharm/configuring-javascript-libraries.html)
- [Handling Response Codes with Sagas](https://github.com/redux-saga/redux-saga/issues/110)
- [React Router V4 Migration Guide](https://codeburst.io/react-router-v4-unofficial-migration-guide-5a370b8905a)
- [Configure Prettier and ESLint in Visual Studio Code](https://www.39digits.com/configure-prettier-and-eslint-in-visual-studio-code/)
- [Integrating Prettier with ESlint](https://prettier.io/docs/en/eslint.html)
