import isEmpty from 'lodash/isEmpty'
import get from 'lodash/get'
import has from 'lodash/has'

import { DATES, REGIONS, COUNTRIES } from 'constants/filters'
import { formatCerebroDate, formatCurrency } from 'helpers/formatting'
import { KEYWORD_ACTIVE_BID } from 'constants/resourceTypes'

export const formatFilters = filters => {
    const params = {}

    if (!isEmpty(filters[DATES])) {
        params.history_date_min = formatCerebroDate(filters[DATES][0])
        params.history_date_max = formatCerebroDate(filters[DATES][1])
    }

    if (!isEmpty(filters[REGIONS])) {
        params.region__in = filters[REGIONS].map(item => item.value).join(',')
    }

    if (!isEmpty(filters[COUNTRIES])) {
        params.country_code__in = filters[COUNTRIES].map(
            item => item.value
        ).join(',')
    }

    return params
}

export const formatChangeDescription = change => {
    if (change.object_type === KEYWORD_ACTIVE_BID) {
        if (has(change, ['changes', 'bid'])) {
            const oldBid = get(change, ['changes', 'bid', 'old'])
            const newBid = get(change, ['changes', 'bid', 'new'])
            const currencyCode = get(change, 'currency_code')

            return `Changed keyword bid from ${formatCurrency(oldBid, {
                decimal: true,
                currencyCode,
            })} to ${formatCurrency(newBid, {
                decimal: true,
                currencyCode,
            })}`
        }
    }

    return null
}
