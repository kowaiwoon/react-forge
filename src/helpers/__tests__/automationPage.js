import { KEYWORD_ACTIVE_BID } from 'constants/resourceTypes'
import { formatChangeDescription } from '../ui/automationPage'

describe('[automationPage]', () => {
    describe('formatChangeDescription', () => {
        it('formats a dayparting change description', () => {
            expect(
                formatChangeDescription({
                    object_type: KEYWORD_ACTIVE_BID,
                    currency_code: 'USD',
                    changes: { bid: { old: '5.5', new: '6.05' } },
                })
            ).toBe('Changed keyword bid from $5.50 to $6.05')
        })
    })
})
