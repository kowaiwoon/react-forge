import { matchPath, generatePath } from 'react-router-dom'

import {
    HOME_PAGE,
    BRAND_PAGE,
    BRAND_CAMPAIGN_PAGE,
    BRANDS_SUMMARY_PAGE,
    CAMPAIGN_PAGE,
    PROFILE_PAGE,
    KEYWORDS_SUMMARY_PAGE,
    PRODUCTS_SUMMARY_PAGE,
    BRAND_CAMPAIGN_KEYWORD_PAGE,
    BRAND_CAMPAIGN_PRODUCT_PAGE,
    CAMPAIGNS_SUMMARY_PAGE,
    CAMPAIGN_KEYWORD_PAGE,
    CAMPAIGN_PRODUCT_PAGE,
    AUTH_PAGE,
} from 'constants/pages'

import { getPath, getPage } from '../pages'

describe('[pages]', () => {
    describe('getPath', () => {
        it('can get paths', () => {
            expect(getPath(HOME_PAGE)).toEqual('/')
            expect(getPath(BRANDS_SUMMARY_PAGE)).toEqual('/brands')
            expect(getPath(BRAND_PAGE)).toEqual('/brands/:brandId')
            expect(getPath(BRAND_CAMPAIGN_PAGE)).toEqual(
                '/brands/:brandId/campaigns/:campaignId'
            )
            expect(getPath(AUTH_PAGE)).toEqual('/auth')
        })
        it('can get work with generatePath', () => {
            const campaignPath = getPath(BRAND_CAMPAIGN_PAGE)
            expect(
                generatePath(campaignPath, {
                    brandId: 1234,
                    campaignId: 5678,
                })
            ).toEqual('/brands/1234/campaigns/5678')
        })
    })

    describe('matchPath', () => {
        it('can be matched with matchPath and exact options', () => {
            expect(
                matchPath('/brands/1234', {
                    path: getPath(BRAND_PAGE),
                    exact: false,
                })
            ).toEqual({
                isExact: true,
                params: { brandId: '1234' },
                path: '/brands/:brandId',
                url: '/brands/1234',
            })
        })
        it('can return null with matchPath', () => {
            expect(
                matchPath('/brands/1234/campaigns/4567', {
                    path: getPath(BRAND_PAGE),
                    exact: true,
                })
            ).toBeNull()
        })
        it('can match profile sub routes', () => {
            expect(
                matchPath('/profile/integrations', {
                    path: '/profile',
                    exact: false,
                })
            ).toEqual({
                isExact: false,
                params: {},
                path: '/profile',
                url: '/profile',
            })
        })
    })

    describe('getPageName', () => {
        it('can match brand page', () => {
            expect(getPage('/brands/1234')).toEqual({
                match: {
                    isExact: true,
                    params: { brandId: '1234' },
                    path: '/brands/:brandId',
                    url: '/brands/1234',
                },
                page: BRAND_PAGE,
            })
        })
        it('can match brand page with trailing slash', () => {
            expect(getPage('/brands/1234/')).toEqual({
                match: {
                    isExact: true,
                    params: { brandId: '1234' },
                    path: '/brands/:brandId',
                    url: '/brands/1234/',
                },
                page: BRAND_PAGE,
            })
        })
        it('can match home page', () => {
            expect(getPage('/')).toEqual({
                match: {
                    isExact: true,
                    params: {},
                    path: '/',
                    url: '/',
                },
                page: HOME_PAGE,
            })
        })
        it('can match brands summary page', () => {
            expect(getPage('/brands')).toEqual({
                match: {
                    isExact: true,
                    params: {},
                    path: '/brands',
                    url: '/brands',
                },
                page: BRANDS_SUMMARY_PAGE,
            })
        })
        it('can match brands summary page with trailing slash', () => {
            expect(getPage('/brands/')).toEqual({
                match: {
                    isExact: true,
                    params: {},
                    path: '/brands',
                    url: '/brands/',
                },
                page: BRANDS_SUMMARY_PAGE,
            })
        })
        it('can match brand campaign page', () => {
            expect(getPage('/brands/12345/campaigns/1485')).toEqual({
                match: {
                    isExact: true,
                    params: { brandId: '12345', campaignId: '1485' },
                    path: '/brands/:brandId/campaigns/:campaignId',
                    url: '/brands/12345/campaigns/1485',
                },
                page: BRAND_CAMPAIGN_PAGE,
            })
        })
        it('can match brand campaign keyword page', () => {
            expect(
                getPage('/brands/12345/campaigns/1485/keywords/1234')
            ).toEqual({
                match: {
                    isExact: true,
                    params: {
                        brandId: '12345',
                        campaignId: '1485',
                        keywordId: '1234',
                    },
                    path:
                        '/brands/:brandId/campaigns/:campaignId/keywords/:keywordId',
                    url: '/brands/12345/campaigns/1485/keywords/1234',
                },
                page: BRAND_CAMPAIGN_KEYWORD_PAGE,
            })
        })
        it('can match brand campaign product page', () => {
            expect(
                getPage('/brands/12345/campaigns/1485/products/1234')
            ).toEqual({
                match: {
                    isExact: true,
                    params: {
                        brandId: '12345',
                        campaignId: '1485',
                        productId: '1234',
                    },
                    path:
                        '/brands/:brandId/campaigns/:campaignId/products/:productId',
                    url: '/brands/12345/campaigns/1485/products/1234',
                },
                page: BRAND_CAMPAIGN_PRODUCT_PAGE,
            })
        })
        it('can match campaigns summary page', () => {
            expect(getPage('/campaigns')).toEqual({
                match: {
                    isExact: true,
                    params: {},
                    path: '/campaigns',
                    url: '/campaigns',
                },
                page: CAMPAIGNS_SUMMARY_PAGE,
            })
        })
        it('can match campaign page', () => {
            expect(getPage('/campaigns/1485')).toEqual({
                match: {
                    isExact: true,
                    params: { campaignId: '1485' },
                    path: '/campaigns/:campaignId',
                    url: '/campaigns/1485',
                },
                page: CAMPAIGN_PAGE,
            })
        })
        it('can match campaign keyword page', () => {
            expect(getPage('/campaigns/1485/keywords/1234')).toEqual({
                match: {
                    isExact: true,
                    params: { campaignId: '1485', keywordId: '1234' },
                    path: '/campaigns/:campaignId/keywords/:keywordId',
                    url: '/campaigns/1485/keywords/1234',
                },
                page: CAMPAIGN_KEYWORD_PAGE,
            })
        })
        it('can match campaign product page', () => {
            expect(getPage('/campaigns/1485/products/1234')).toEqual({
                match: {
                    isExact: true,
                    params: { campaignId: '1485', productId: '1234' },
                    path: '/campaigns/:campaignId/products/:productId',
                    url: '/campaigns/1485/products/1234',
                },
                page: CAMPAIGN_PRODUCT_PAGE,
            })
        })
        it('can match profile page', () => {
            expect(getPage('/profile')).toEqual({
                match: {
                    isExact: true,
                    params: {},
                    path: '/profile',
                    url: '/profile',
                },
                page: PROFILE_PAGE,
            })
        })
        it('can match keyword summary page given keyword page path', () => {
            expect(getPage('/keywords/12345', false)).toEqual({
                match: {
                    isExact: false,
                    params: {},
                    path: '/keywords',
                    url: '/keywords',
                },
                page: KEYWORDS_SUMMARY_PAGE,
            })
        })
        it('can match products summary page given products page path', () => {
            expect(getPage('/products/12345', false)).toEqual({
                match: {
                    isExact: false,
                    params: {},
                    path: '/products',
                    url: '/products',
                },
                page: PRODUCTS_SUMMARY_PAGE,
            })
        })
        it('can match brands summary page given campaigns page path', () => {
            expect(getPage('/brands/12345/campaigns/1485', false)).toEqual({
                match: {
                    isExact: false,
                    params: {},
                    path: '/brands',
                    url: '/brands',
                },
                page: BRANDS_SUMMARY_PAGE,
            })
        })
        it('to return null when there is no match', () => {
            expect(getPage('/not_a_route')).toEqual({})
            const { page } = getPage('/not_a_route')
            expect(page).toBeUndefined()
        })
    })
})
