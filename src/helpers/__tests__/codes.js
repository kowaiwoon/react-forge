import map from 'lodash/map'

import { AMS_COUNTRIES, SOV_COUNTRIES } from 'configuration/codes'

import {
    getAmsRegionCountryMap,
    getSovRegionCountryMap,
    getAmsCountryLanguageMap,
    getSovCountryLanguageMap,
    getAmsCountryOptions,
    getSovCountryOptions,
    getAmsRegionOptions,
} from '../codes'

describe('[codes]', () => {
    describe('getAmsRegionCountryMap', () => {
        it('returns the correct data', () => {
            expect(getAmsRegionCountryMap()).toMatchObject({
                EU: ['DE', 'ES', 'FR', 'IN', 'IT', 'UK'],
                NA: ['CA', 'US', 'MX'],
            })
        })

        it('contains all AMS countries', () => {
            const allCountries = new Set()
            map(getAmsRegionCountryMap(), countries => {
                countries.map(country => allCountries.add(country))
            })
            expect(AMS_COUNTRIES).toHaveLength(allCountries.size)
        })
    })

    describe('getSovRegionCountryMap', () => {
        it('returns the correct data', () => {
            expect(getSovRegionCountryMap()).toMatchObject({
                EU: ['DE', 'FR', 'IN', 'IT', 'UK'],
                NA: ['CA', 'US'],
                JP: ['JP'],
            })
        })

        it('contains all SOV countries', () => {
            const allCountries = new Set()
            map(getSovRegionCountryMap(), countries => {
                countries.map(country => allCountries.add(country))
            })
            expect(SOV_COUNTRIES).toHaveLength(allCountries.size)
        })
    })

    describe('getAmsCountryLanguageMap', () => {
        it('returns the correct data', () => {
            expect(getAmsCountryLanguageMap()).toMatchObject({
                CA: ['ENG', 'FRA'],
                DE: ['CES', 'ENG', 'GER', 'NLD', 'POL', 'TUR'],
                ES: ['SPA'],
                FR: ['FRA'],
                IN: ['ENG'],
                IT: ['ITA'],
                MX: ['SPA'],
                UK: ['ENG'],
                US: ['ENG', 'SPA'],
            })
        })

        it('contains all AMS countries', () => {
            const countries = Object.keys(getAmsCountryLanguageMap())
            expect(countries).toHaveLength(AMS_COUNTRIES.length)
        })
    })

    describe('getSovCountryLanguageMap', () => {
        it('returns the correct data', () => {
            expect(getSovCountryLanguageMap()).toMatchObject({
                CA: ['ENG', 'FRA'],
                DE: ['CES', 'ENG', 'GER', 'NLD', 'POL', 'TUR'],
                FR: ['FRA'],
                IN: ['ENG'],
                IT: ['ITA'],
                JP: ['JPN', 'ENG', 'ZHO'],
                UK: ['ENG'],
                US: ['ENG', 'SPA'],
            })
        })

        it('contains all SOV countries', () => {
            const countries = Object.keys(getSovCountryLanguageMap())
            expect(countries).toHaveLength(SOV_COUNTRIES.length)
        })
    })

    describe('getAmsCountryOptions', () => {
        it('returns the correct data', () => {
            expect(getAmsCountryOptions()).toMatchObject([
                { label: 'Canada', value: 'CA' },
                { label: 'Germany', value: 'DE' },
                { label: 'Spain', value: 'ES' },
                { label: 'France', value: 'FR' },
                { label: 'India', value: 'IN' },
                { label: 'Italy', value: 'IT' },
                { label: 'Japan', value: 'JP' },
                { label: 'Mexico', value: 'MX' },
                { label: 'United Kingdom', value: 'UK' },
                { label: 'United States', value: 'US' },
            ])
        })
    })

    describe('getSovCountryOptions', () => {
        it('returns the correct data', () => {
            expect(getSovCountryOptions()).toMatchObject([
                { label: 'Canada', value: 'CA' },
                { label: 'Germany', value: 'DE' },
                { label: 'France', value: 'FR' },
                { label: 'India', value: 'IN' },
                { label: 'Italy', value: 'IT' },
                { label: 'Japan', value: 'JP' },
                { label: 'United Kingdom', value: 'UK' },
                { label: 'United States', value: 'US' },
            ])
        })
    })

    describe('getAmsRegionOptions', () => {
        it('returns the correct data', () => {
            expect(getAmsRegionOptions()).toMatchObject([
                { label: 'Europe', value: 'EU' },
                { label: 'North America', value: 'NA' },
                { label: 'Japan', value: 'JP' },
            ])
        })
    })
})
