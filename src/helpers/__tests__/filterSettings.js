import { isCurrentSchema } from '../filterSettings'

describe('[filterSettings]', () => {
    describe('isCurrentSchema', () => {
        const otherSettings = {
            order: ['countries', 'brands', 'regions'],
            displayState: {
                brands: true,
                countries: false,
                regions: false,
            },
        }

        it('returns true when schema matches current', () => {
            const item = {
                Domain: 'homePage-filtersDisplayState',
                UserId: 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx',
                order: ['regions', 'brands', 'countries'],
                displayState: {
                    brands: false,
                    countries: false,
                    regions: true,
                },
            }

            expect(isCurrentSchema(item, otherSettings)).toBe(true)

            // test to ensure objects are not mutated
            expect(item).toMatchObject({
                Domain: 'homePage-filtersDisplayState',
                UserId: 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx',
                order: ['regions', 'brands', 'countries'],
                displayState: {
                    brands: false,
                    countries: false,
                    regions: true,
                },
            })
            expect(otherSettings).toMatchObject({
                order: ['countries', 'brands', 'regions'],
                displayState: {
                    brands: true,
                    countries: false,
                    regions: false,
                },
            })
        })

        it('returns false when schema does not match current', () => {
            const item = {
                Domain: 'homePage-filtersDisplayState',
                UserId: 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx',
                order: ['brands', 'countries', 'regions', 'anotherFilter'],
                displayState: {
                    brands: false,
                    countries: false,
                    regions: true,
                    anotherFilter: false,
                },
            }
            expect(isCurrentSchema(item, otherSettings)).toBe(false)
        })
    })
})
