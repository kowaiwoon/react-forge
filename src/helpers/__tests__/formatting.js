import { formatUrl } from '../formatting'

describe('[formatting]', () => {
    describe('formatUrl', () => {
        it('appends http:// if no protocol is provided', () => {
            expect(formatUrl('www.test.com')).toBe('http://www.test.com')
        })
        it('appends http:// if no protocol and subdomain are provided', () => {
            expect(formatUrl('test.com')).toBe('http://test.com')
        })
        it('does not append http:// if http:// is provided', () => {
            expect(formatUrl('http://www.test.com')).toBe('http://www.test.com')
        })
        it('does not append http:// if https:// is provided', () => {
            expect(formatUrl('https://www.test.com')).toBe(
                'https://www.test.com'
            )
        })
    })
})
