import { formatSignUpData } from '../params'

describe('[params]', () => {
    describe('formatSignUpData', () => {
        it('can parse minimal signup data', () => {
            expect(
                formatSignUpData({
                    name: 'Salim Hamed',
                    company: 'Downstream Impact',
                    email: 'salim@fake.com',
                    password: 'fake-password',
                    terms_of_service: true,
                })
            ).toEqual({
                attributes: {
                    'custom:company': 'Downstream Impact',
                    'custom:terms_of_service': '1',
                    email: 'salim@fake.com',
                    name: 'Salim Hamed',
                },
                password: 'fake-password',
                username: 'salim@fake.com',
            })
        })

        it('can parse all signup data', () => {
            expect(
                formatSignUpData({
                    name: 'Salim Hamed',
                    company: 'Downstream Impact',
                    website: 'www.downstreamimpact.com',
                    phone: '206-669-2510',
                    how_did_you_hear_other: 'from the internet',
                    email: 'salim@fake.com',
                    password: 'fake-password',
                    terms_of_service: true,
                })
            ).toEqual({
                attributes: {
                    'custom:company': 'Downstream Impact',
                    'custom:terms_of_service': '1',
                    'custom:how_did_you_hear': 'from the internet',
                    email: 'salim@fake.com',
                    name: 'Salim Hamed',
                    phone_number: '+12066692510',
                    website: 'www.downstreamimpact.com',
                },
                password: 'fake-password',
                username: 'salim@fake.com',
            })
        })
    })
})
