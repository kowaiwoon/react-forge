import { changelogResourceUrl } from '../urls'

describe('[urls]', () => {
    describe('changelogResourceUrl', () => {
        it('returns a properly formatted keyword url', () => {
            expect(changelogResourceUrl('keyword', '264402723002080')).toBe(
                `/keywords/264402723002080`
            )
        })
    })
})
