import { isCurrentSchema } from '../columnSettings'

describe('[columnSettings]', () => {
    describe('isCurrentSchema', () => {
        it('returns true when schema matches current', () => {
            const otherSettings = {
                actionColumns: ['actions'],
                order: ['impressions__sum', 'clicks__sum'],
                displayState: {
                    conversion_rate_clicks: false,
                    attributed_conversions_7_day__sum: false,
                },
            }
            const item = {
                Domain: 'campaignPage-keywordsTable',
                UserId: 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx',
                actionColumns: ['actions'],
                order: ['impressions__sum', 'clicks__sum'],
                displayState: {
                    conversion_rate_clicks: false,
                    attributed_conversions_7_day__sum: false,
                },
            }
            expect(isCurrentSchema(item, otherSettings)).toBe(true)
        })

        it('returns false when schema does not match current', () => {
            const otherSettings = {
                actionColumns: ['actions'],
                order: ['impressions__sum', 'clicks__sum'],
                displayState: {
                    conversion_rate_clicks: false,
                    attributed_conversions_7_day__sum: false,
                },
            }
            const item = {
                Domain: 'campaignPage-keywordsTable',
                UserId: 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx',
                order: ['impressions__sum', 'clicks__sum'],
                displayState: {
                    conversion_rate_clicks: false,
                    attributed_conversions_7_day__sum: false,
                },
            }
            expect(isCurrentSchema(item, otherSettings)).toBe(false)
        })

        it('returns false when columns change', () => {
            const otherSettings = {
                actionColumns: [],
                order: [
                    'scheduled_date',
                    'search_time',
                    'country_code',
                    'language_code',
                    'text',
                    'sponsored_result_count',
                    'result_count',
                ],
                displayState: {
                    scheduled_date: true,
                    search_time: true,
                    country_code: true,
                    language_code: true,
                    text: true,
                    sponsored_result_count: true,
                    result_count: true,
                },
            }
            const item = {
                displayState: {
                    country_code: true,
                    language_code: true,
                    created_at: true,
                    text: true,
                    sponsored_result_count: true,
                    scheduled_date: true,
                    result_count: true,
                },
                UserId: '15d1909b-92d9-4c99-8149-6d2cefd9246b',
                order: [
                    'scheduled_date',
                    'created_at',
                    'country_code',
                    'language_code',
                    'text',
                    'sponsored_result_count',
                    'result_count',
                ],
                Domain: 'sovKeywordPage-table',
                actionColumns: [],
            }
            expect(isCurrentSchema(item, otherSettings)).toBe(false)
        })
    })
})
