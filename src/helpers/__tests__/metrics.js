import forEach from 'lodash/forEach'

import { testData } from '../__data__/testData'
import {
    ctrWeightedAvg,
    roasWeightedAvg,
    spcWeightedAvg,
    cpcWeightedAvg,
    cpaWeightedAvg,
    cpmWeightedAvg,
    spmWeightedAvg,
    conversionRateClicksWeightedAvg,
    conversionRateImpressionsWeightedAvg,
    aggregateMetricByDateGroups,
} from '../metrics'

describe('[Utilities] metrics', () => {
    describe('ctrWeightedAvg', () => {
        it('calculates metric correctly', () => {
            expect(ctrWeightedAvg(testData)).toBeCloseTo(0.0039, 4)
        })
    })

    describe('roasWeightedAvg', () => {
        it('calculates metric correctly', () => {
            expect(roasWeightedAvg(testData)).toBeCloseTo(8.95, 4)
        })
    })

    describe('spcWeightedAvg', () => {
        it('calculates metric correctly', () => {
            expect(spcWeightedAvg(testData)).toBeCloseTo(3.8388, 4)
        })
    })

    describe('cpcWeightedAvg', () => {
        it('calculates metric correctly', () => {
            expect(cpcWeightedAvg(testData)).toBeCloseTo(0.4289, 4)
        })
    })

    describe('cpaWeightedAvg', () => {
        it('calculates metric correctly', () => {
            expect(cpaWeightedAvg(testData)).toBeCloseTo(4.4448, 4)
        })
    })

    describe('cpmWeightedAvg', () => {
        it('calculates metric correctly', () => {
            expect(cpmWeightedAvg(testData)).toBeCloseTo(1.6891, 4)
        })
    })

    describe('spmWeightedAvg', () => {
        it('calculates metric correctly', () => {
            expect(spmWeightedAvg(testData)).toBeCloseTo(15.1174, 4)
        })
    })

    describe('conversionRateClicksWeightedAvg', () => {
        it('calculates metric correctly', () => {
            expect(conversionRateClicksWeightedAvg(testData)).toBeCloseTo(
                0.0965,
                4
            )
        })
    })

    describe('conversionRateImpressionsWeightedAvg', () => {
        it('calculates metric correctly', () => {
            expect(conversionRateImpressionsWeightedAvg(testData)).toBeCloseTo(
                0.0004,
                4
            )
        })
    })
    describe('aggregateMetricByDateGroups', () => {
        it('aggregates clicks by month correctly', () => {
            expect(
                aggregateMetricByDateGroups({
                    data: testData,
                    metric: 'clicks__sum',
                    aggregate: 'month',
                    order: 'desc',
                })
            ).toEqual([
                {
                    clicks__sum: 21392,
                    period_end: '2018-06-30T23:59:59.999Z',
                    period_start: '2018-06-01T00:00:00.000Z',
                },
                {
                    clicks__sum: 306902,
                    period_end: '2018-05-31T23:59:59.999Z',
                    period_start: '2018-05-01T00:00:00.000Z',
                },
                {
                    clicks__sum: 414179,
                    period_end: '2018-04-30T23:59:59.999Z',
                    period_start: '2018-04-01T00:00:00.000Z',
                },
                {
                    clicks__sum: 299884,
                    period_end: '2018-03-31T23:59:59.999Z',
                    period_start: '2018-03-01T00:00:00.000Z',
                },
                {
                    clicks__sum: 154539,
                    period_end: '2018-02-28T23:59:59.999Z',
                    period_start: '2018-02-01T00:00:00.000Z',
                },
            ])
        })

        it('aggregates cost by month correctly', () => {
            const expectedData = [
                {
                    period_start: '2018-06-01T00:00:00.000Z',
                    period_end: '2018-06-30T23:59:59.999Z',
                    cost__sum: 10658.36,
                },
                {
                    period_start: '2018-05-01T00:00:00.000Z',
                    period_end: '2018-05-31T23:59:59.999Z',
                    cost__sum: 142668.76,
                },
                {
                    period_start: '2018-04-01T00:00:00.000Z',
                    period_end: '2018-04-30T23:59:59.999Z',
                    cost__sum: 182450.98,
                },
                {
                    period_start: '2018-03-01T00:00:00.000Z',
                    period_end: '2018-03-31T23:59:59.999Z',
                    cost__sum: 116176.71,
                },
                {
                    period_start: '2018-02-01T00:00:00.000Z',
                    period_end: '2018-02-28T23:59:59.999Z',
                    cost__sum: 61415.09,
                },
            ]

            forEach(
                aggregateMetricByDateGroups({
                    data: testData,
                    metric: 'cost__sum',
                    aggregate: 'month',
                    order: 'desc',
                }),
                (value, index) => {
                    expect(expectedData[index].cost__sum).toBeCloseTo(
                        value.cost__sum,
                        2
                    )
                    expect(expectedData[index].period_start).toEqual(
                        value.period_start
                    )
                    expect(expectedData[index].period_end).toEqual(
                        value.period_end
                    )
                }
            )
        })
    })
})
