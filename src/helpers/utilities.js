import isArray from 'lodash/isArray'
import union from 'lodash/union'

export const mergeArrayWithUnion = (objValue, srcValue) => {
    if (isArray(objValue)) {
        return union(objValue, srcValue)
    }

    return undefined
}

export const mergeArrayTakeLatest = (objValue, srcValue) => {
    if (isArray(objValue)) {
        return srcValue
    }

    return undefined
}
