/**
 * Define utility functions that format params to be sent to the api
 */

import { PhoneNumberFormat, PhoneNumberUtil } from 'google-libphonenumber'
import isEmpty from 'lodash/isEmpty'
import isNull from 'lodash/isNull'
import has from 'lodash/has'
import set from 'lodash/set'

import {
    BRANDS,
    CAMPAIGN_BUDGET,
    CAMPAIGN_DAYPARTINGS,
    CAMPAIGN_NAME,
    CAMPAIGN_STATES,
    CAMPAIGN_TARGETING_TYPES,
    CAMPAIGNS,
    COUNTRIES,
    DATES,
    EQUALS,
    KEYWORD_MATCH_TYPES,
    KEYWORDS,
    PRODUCT_ASINS,
    PRODUCT_TITLES,
    REGIONS,
    SOV_COUNTRIES,
    SOV_FOLDS,
    SOV_KEYWORD_CATEGORIES,
    SOV_KEYWORDS,
    SOV_LANGUAGES,
    SOV_RESULT_TYPES,
    SOV_SEARCH_TIMES,
    SOV_STATES,
} from 'constants/filters'

import { formatCerebroDate } from './formatting'

/**
 * Format filters into query params using DRF api style
 *
 * @param {object} filters Filters to apply on api request
 */
export const formatFilters = filters => {
    const params = {}

    if (!isEmpty(filters[DATES])) {
        params.report_date_min = formatCerebroDate(filters[DATES][0])
        params.report_date_max = formatCerebroDate(filters[DATES][1])
    }

    if (!isEmpty(filters[SOV_SEARCH_TIMES])) {
        params.search_time_min = formatCerebroDate(filters[SOV_SEARCH_TIMES][0])
        params.search_time_max = formatCerebroDate(filters[SOV_SEARCH_TIMES][1])
    }

    if (!isEmpty(filters[REGIONS])) {
        params.profile__region__in = filters[REGIONS].map(
            item => item.value
        ).join(',')
    }

    if (!isEmpty(filters[COUNTRIES])) {
        const countries = filters[COUNTRIES].map(item => item.value).join(',')
        if (has(filters, [SOV_KEYWORDS])) {
            params.country_code__in = countries
        } else {
            params.profile__country_code__in = countries
        }
    }

    if (!isEmpty(filters[SOV_COUNTRIES])) {
        params.country_code__in = filters[SOV_COUNTRIES].map(
            item => item.value
        ).join(',')
    }

    if (!isEmpty(filters[BRANDS])) {
        params.profile__in = filters[BRANDS].map(item => item.value).join(',')
    }

    if (!isEmpty(filters[CAMPAIGNS])) {
        params.campaign__in = filters[CAMPAIGNS].map(item => item.value).join(
            ','
        )
    }

    if (!isEmpty(filters[CAMPAIGN_BUDGET])) {
        const { operator, value } = filters[CAMPAIGN_BUDGET]
        let key = 'campaign__budget'
        if (operator !== EQUALS) {
            key += `__${operator}`
        }
        params[key] = value
    }

    if (!isEmpty(filters[CAMPAIGN_NAME])) {
        params.campaign__name__icontains = filters[CAMPAIGN_NAME]
    }

    if (!isEmpty(filters[CAMPAIGN_STATES])) {
        params.campaign__state__in = filters[CAMPAIGN_STATES].map(
            item => item.value
        ).join(',')
    }

    if (!isEmpty(filters[CAMPAIGN_TARGETING_TYPES])) {
        params.campaign__targeting_type__in = filters[
            CAMPAIGN_TARGETING_TYPES
        ].map(item => item.value).join(',')
    }

    if (!isEmpty(filters[CAMPAIGN_DAYPARTINGS])) {
        params.campaign__dayparting_enabled = filters[CAMPAIGN_DAYPARTINGS].map(
            item => item.value
        ).join(',')
    }

    if (!isEmpty(filters[KEYWORDS])) {
        params.keyword__in = filters[KEYWORDS].map(item => item.value).join(',')
    }

    if (!isEmpty(filters[KEYWORD_MATCH_TYPES])) {
        params.keyword__match_type__in = filters[KEYWORD_MATCH_TYPES].map(
            item => item.value
        ).join(',')
    }

    if (!isEmpty(filters[PRODUCT_ASINS])) {
        params.product_ad__asin__in = filters[PRODUCT_ASINS].map(
            item => item.value
        ).join(',')
    }

    if (!isEmpty(filters[PRODUCT_TITLES])) {
        params.product_ad__metadata__id__in = filters[PRODUCT_TITLES].map(
            item => item.value
        ).join(',')
    }

    if (!isEmpty(filters[SOV_KEYWORDS])) {
        params.id__in = filters[SOV_KEYWORDS].map(item => item.value).join(',')
    }

    if (!isEmpty(filters[SOV_KEYWORD_CATEGORIES])) {
        params.category__in = filters[SOV_KEYWORD_CATEGORIES].map(
            item => item.value
        ).join(',')
    }

    if (!isEmpty(filters[SOV_LANGUAGES])) {
        params.language_code__in = filters[SOV_LANGUAGES].map(
            item => item.value
        ).join(',')
    }

    if (!isEmpty(filters[SOV_STATES])) {
        params.state__in = filters[SOV_STATES].map(item => item.value).join(',')
    }

    if (!isEmpty(filters[SOV_RESULT_TYPES])) {
        params.result_type__in = filters[SOV_RESULT_TYPES].map(
            item => item.value
        ).join(',')
    }

    return params
}

/**
 * Format SOV filters into query params using DRF api style
 *
 * @param {object} filters Filters to apply on api request
 */
const formatSovAggregateFilters = filters => {
    const params = {}

    if (!isEmpty(filters[SOV_SEARCH_TIMES])) {
        params.search_time_min = formatCerebroDate(filters[SOV_SEARCH_TIMES][0])
        params.search_time_max = formatCerebroDate(filters[SOV_SEARCH_TIMES][1])
    }

    if (!isEmpty(filters[SOV_COUNTRIES])) {
        params.keyword__country_code__in = filters[SOV_COUNTRIES].map(
            item => item.value
        ).join(',')
    }

    if (!isEmpty(filters[SOV_KEYWORDS])) {
        params.keyword__id__in = filters[SOV_KEYWORDS].map(
            item => item.value
        ).join(',')
    } else {
        params.auto_filter = 'top_aggregate_filter'
        params.auto_filter_dimension = 'metadata__brand'
        params.auto_filter_limit = 20
        params.auto_filter_metric = 'rank_weight__sum'
    }

    if (!isEmpty(filters[SOV_LANGUAGES])) {
        params.keyword__language_code__in = filters[SOV_LANGUAGES].map(
            item => item.value
        ).join(',')
    }

    if (!isEmpty(filters[SOV_STATES])) {
        params.keyword__state__in = filters[SOV_STATES].map(
            item => item.value
        ).join(',')
    }

    if (!isEmpty(filters[SOV_RESULT_TYPES])) {
        params.result_type__in = filters[SOV_RESULT_TYPES].map(
            item => item.value
        ).join(',')
    }

    if (!isEmpty(filters[SOV_KEYWORD_CATEGORIES])) {
        params.keyword__category__in = filters[SOV_KEYWORD_CATEGORIES].map(
            item => item.value
        ).join(',')
    }

    if (!isEmpty(filters[SOV_FOLDS])) {
        const { operator, value } = filters[SOV_FOLDS]
        let key = 'fold'
        if (operator !== EQUALS) {
            key += `__${operator}`
        }
        params[key] = value - 1 // folds are 0 indexed
    }

    return params
}

/**
 * Convert pagination into query params using DRF's LimitOffsetPagination format
 *
 * @param {object} pagination pagination object
 */
export const formatPagination = pagination => ({
    limit: pagination.pageSize,
    offset: (pagination.current - 1) * pagination.pageSize,
})

/**
 * Convert sorter into query params using DRF's OrderingFilter format
 *
 * @param {object} sorter sorter object
 */
export const formatSorter = sorter => {
    const { field, order, filterPositiveValues } = sorter
    const params = {}

    if (!field) {
        return params
    }

    // Cerebro uses '__' where our JS datasets use '.' for nesting separators
    const formattedField = field.replace(/\./g, '__')

    params.ordering =
        order === 'descend' ? `-${formattedField}` : formattedField

    // Filter only positive values if `filterPositiveValues` is true
    if (filterPositiveValues) {
        params[`${formattedField}__gt`] = 0
    }

    return params
}

const formatMetrics = metrics =>
    isEmpty(metrics) ? {} : { metrics: metrics.join(',') }

const formatCurrency = currency => (currency ? { currency } : {})

const formatGroupBy = (groups, aggregation) => ({
    group_by: [...groups, aggregation].join(','),
})

/**
 * Format all params
 *
 * @param filters
 * @param pagination
 * @param sorter
 * @param metrics
 * @param currency
 * @returns {{limit, offset, currency: *}}
 */
export const formatAllParams = (
    filters,
    pagination,
    sorter,
    metrics = [],
    currency = null
) => ({
    ...formatFilters(filters),
    ...formatPagination(pagination),
    ...formatSorter(sorter),
    ...formatMetrics(metrics),
    ...formatCurrency(currency),
})

/**
 * Format query parameters for SOV Fact Aggregate endpoint
 *
 * @param keywordId
 * @param filters
 * @param pagination
 * @param sorter
 * @param aggregation
 * @param groups
 * @returns {{keyword__id: *, limit, offset}}
 */
export const formatSovFactAggregateParams = ({
    sovKeywordId = null,
    filters,
    pagination,
    sorter,
    aggregation,
    groups,
}) => ({
    ...(isNull(sovKeywordId) ? {} : { keyword__id: sovKeywordId }),
    ...formatSovAggregateFilters(filters),
    ...formatPagination(pagination),
    ...formatSorter(sorter),
    ...formatGroupBy(groups, aggregation),
})

/**
 * Format sign up form values
 */
export const formatSignUpData = ({
    name,
    company,
    website,
    phone,
    how_did_you_hear,
    how_did_you_hear_other,
    email,
    password,
    terms_of_service,
}) => {
    const data = {
        username: email, // using email as username, per Cognito settings
        password,
        attributes: {
            email,
            name,
            'custom:company': company,
            'custom:terms_of_service': terms_of_service ? '1' : '0',
        },
    }

    if (website) {
        set(data, 'attributes.website', website)
    }

    if (phone) {
        const phoneUtil = PhoneNumberUtil.getInstance()
        const phoneParsed = phoneUtil.parseAndKeepRawInput(phone, 'US')
        set(
            data,
            'attributes.phone_number',
            phoneUtil.format(phoneParsed, PhoneNumberFormat.E164)
        )
    }

    if (how_did_you_hear_other) {
        set(data, 'attributes.custom:how_did_you_hear', how_did_you_hear_other)
    } else if (how_did_you_hear && how_did_you_hear !== 'other') {
        set(data, 'attributes.custom:how_did_you_hear', how_did_you_hear)
    }

    return data
}
