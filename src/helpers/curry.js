/**
 * Currys action creator to inject pageName in payload
 *
 * @param {function} action - Action creator
 */
export const curryActionForPage = action => pageName => data =>
    action({
        pageName,
        data,
    })
