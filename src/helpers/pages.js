import { matchPath } from 'react-router-dom'
import find from 'lodash/find'
import isNull from 'lodash/isNull'
import get from 'lodash/get'

import { PAGES_AND_PATHS } from 'configuration/pages'

export const getPath = page => get(find(PAGES_AND_PATHS, { page }), 'path')

export const getPage = (path, exact = true) => {
    let match = null
    const pageAndPath = find(PAGES_AND_PATHS, item => {
        match = matchPath(path, { path: item.path, exact })
        return !isNull(match)
    })
    if (pageAndPath) {
        const { page } = pageAndPath
        return { page, match }
    }
    return {}
}
