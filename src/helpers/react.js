/**
 * Returns the properties that are defined on a React component
 *
 * Note, the `type` namespace of a React component contains the properties that
 * were defined on the function or class.
 *
 * @param component A React component
 * @returns {*}
 */
export const getComponentProperties = component => component.type
