/**
 * REDUCER KEYS USED IN ALL REDUCERS
 */

// Normalization
export const BY_ID = 'byId'
export const ALL_IDS = 'allIds'
export const BY_ORG_ID = 'byOrgId'
export const BY_ORG_GROUP_ID = 'byOrgGroupId'

// Page Filters
export const FILTERS = 'filters'
export const FILTER_SETTINGS = 'filtersSettings'
export const DATES = 'dates'

// Charts
export const CHARTS = 'charts'
export const SOV_CHART = 'sovChart'

export const AGG_UNIT = {
    DAY: 'day',
    WEEK: 'week',
    MONTH: 'month',
}

export const SOV_AGGREGATION_UNIT = {
    DAY: 'search_time_day',
    WEEK: 'search_time_week',
    MONTH: 'search_time_month',
}
