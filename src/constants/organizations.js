// organization group resource types
export const RESOURCE_TYPE_ALL = 'all'
export const RESOURCE_TYPE_REGIONS = 'regions'
export const RESOURCE_TYPE_COUNTRIES = 'countries'
export const RESOURCE_TYPE_BRANDS = 'brands'
export const RESOURCE_TYPE_LABELS = {
    [RESOURCE_TYPE_ALL]: 'All Resource Types',
    [RESOURCE_TYPE_REGIONS]: 'Regions',
    [RESOURCE_TYPE_COUNTRIES]: 'Countries',
    [RESOURCE_TYPE_BRANDS]: 'Brands',
}
