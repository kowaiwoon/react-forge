/**
 * Page Names
 */
export const AUTH_PAGE = 'authPage'
export const AUTH_SIGNUP_PAGE = 'authSignupPage'
export const AUTH_FORGOT_PASSWORD_PAGE = 'authForgotPasswordPage'
export const BRAND_PAGE = 'brandPage'
export const BRAND_CAMPAIGN_PAGE = 'brandCampaignPage'
export const BRAND_CAMPAIGN_KEYWORD_PAGE = 'brandCampaignKeywordPage'
export const BRAND_CAMPAIGN_PRODUCT_PAGE = 'brandCampaignProductPage'
export const BRANDS_SUMMARY_PAGE = 'brandsSummaryPage'
export const CAMPAIGNS_SUMMARY_PAGE = 'campaignsSummaryPage'
export const CAMPAIGN_PAGE = 'campaignPage'
export const CAMPAIGN_KEYWORD_PAGE = 'campaignKeywordPage'
export const CAMPAIGN_PRODUCT_PAGE = 'campaignProductPage'
export const HOME_PAGE = 'homePage'
export const LABELS_SUMMARY_PAGE = 'labelsSummaryPage'
export const KEYWORD_PAGE = 'keywordPage'
export const KEYWORDS_SUMMARY_PAGE = 'keywordsSummaryPage'
export const PRODUCT_PAGE = 'productPage'
export const PRODUCTS_SUMMARY_PAGE = 'productsSummaryPage'
export const SOV_KEYWORD_PAGE = 'sovKeywordPage'
export const SOV_KEYWORD_SEARCH_RESULT_PAGE = 'sovKeywordSearchResultPage'
export const SOV_KEYWORDS_SUMMARY_PAGE = 'sovKeywordsSummaryPage'
export const PROFILE_PAGE = 'profilePage'
export const ORGANIZATION_PAGE = 'organizationPage'
export const ORGANIZATION_GROUP_PAGE = 'organizationGroupPage'
export const AUTOMATION_PAGE = 'automationPage'
