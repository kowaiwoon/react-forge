export const DIMENSIONS = {
    userId: 'userId',
}

export const CUSTOM_DIMENSIONS = {
    customerId: 'dimension1', // see google analytics console for more information
}

export const UNAUTHENTICATED_USER_ID = 'unauthenticated-user-id'

export const UNAUTHENTICATED_CUSTOMER_ID = 'unauthenticated-customer-id'
