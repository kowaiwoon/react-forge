export const DAYPARTING = 'feature_dayparting'
export const SOV_READ = 'feature_sov_read'
export const SOV_WRITE = 'feature_sov_write'
export const ASSUME_ANY_ORGANIZATION_GROUP =
    'feature_assume_any_organization_group'
export const ORGANIZATION_ADMIN = 'feature_org_admin'
export const AUTOMATION = 'feature_automation'

export const FEATURE_PERMISSIONS_KEYS = [
    DAYPARTING,
    SOV_READ,
    SOV_WRITE,
    ASSUME_ANY_ORGANIZATION_GROUP,
    ORGANIZATION_ADMIN,
    AUTOMATION,
]
