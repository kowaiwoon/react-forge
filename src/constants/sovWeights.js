export const NON_WEIGHTED = 'count'
export const FOLD_AND_AREA_WEIGHTED = 'fold_weight__sum'
export const POSITION_WEIGHTED = 'rank_weight__sum'
