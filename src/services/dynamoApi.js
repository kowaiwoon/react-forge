/*
global
	USER_SETTINGS_TABLE,
	FORGE_SETTINGS_TABLE
*/

import { Auth } from 'aws-amplify'
import AWS from 'aws-sdk'

const getDocumentClient = () =>
    Auth.currentCredentials().then(
        credentials =>
            new AWS.DynamoDB.DocumentClient({
                credentials: Auth.essentialCredentials(credentials),
            })
    )

export const putItemToUserSettingsTable = Item => {
    const params = {
        TableName: USER_SETTINGS_TABLE,
        Item,
    }
    return getDocumentClient().then(client => client.put(params).promise())
}

export const getItemFromUserSettingsTable = Key => {
    const params = {
        TableName: USER_SETTINGS_TABLE,
        Key,
    }
    return getDocumentClient().then(client => client.get(params).promise())
}

export const getItemFromForgeSettingsTable = Key => {
    const params = {
        TableName: FORGE_SETTINGS_TABLE,
        Key,
    }
    return getDocumentClient().then(client => client.get(params).promise())
}
