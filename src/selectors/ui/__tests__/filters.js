import { HOME_PAGE } from 'constants/pages'
import reducer from 'reducers'
import { updatePageFilter } from 'actions/ui'

import { selectPageFilters } from '../filters'

describe('[Selectors] ui/filters', () => {
    let state

    describe('selectPageFilters', () => {
        const pageName = HOME_PAGE

        beforeEach(() => {
            state = reducer(
                undefined,
                updatePageFilter({
                    pageName: HOME_PAGE,
                    data: {
                        key: 'countries',
                        value: ['US', 'CA'],
                    },
                })
            )
        })

        it('selects page filters by page name', () => {
            expect(selectPageFilters(state, pageName).countries).toEqual([
                'US',
                'CA',
            ])
        })

        it('memoizes page filters by page name', () => {
            const selector = selectPageFilters.getMatchingSelector(
                state,
                pageName
            )

            // Reset computations
            selector.resetRecomputations()

            // Repeat computations without state change
            selectPageFilters(state, pageName)
            selectPageFilters(state, pageName)
            expect(selector.recomputations()).toEqual(1)

            // Update state
            state = reducer(
                state,
                updatePageFilter({
                    pageName: HOME_PAGE,
                    data: {
                        key: 'regions',
                        value: ['NA'],
                    },
                })
            )

            // Trigger computation after state change
            selectPageFilters(state, pageName)
            expect(selector.recomputations()).toEqual(2)
        })
    })
})
