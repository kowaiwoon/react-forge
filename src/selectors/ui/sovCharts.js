import createCachedSelector from 're-reselect'
import groupBy from 'lodash/groupBy'
import map from 'lodash/map'
import sortBy from 'lodash/sortBy'
import orderBy from 'lodash/orderBy'
import last from 'lodash/last'

import moment from 'utilities/moment'
import { SOV_CHART } from 'constants/reducerKeys'
import { SOV_AGGREGATION } from 'constants/filters'
import { UNDEFINED_BRAND } from 'constants/formatting'

import { selectDomainValue } from './ui'
import { selectPageFilters } from './filters'

const selectPageSovChartData = (state, pageName) =>
    selectDomainValue(state, [pageName, SOV_CHART, 'data'])

export const selectPageSovChartWeight = (state, pageName) =>
    selectDomainValue(state, [pageName, SOV_CHART, 'weight'])

export const selectDataForSovChart = createCachedSelector(
    selectPageSovChartData,
    selectPageSovChartWeight,
    selectPageFilters,
    (data, weightKey, filters) => {
        // group data by brands
        const brandGroups = groupBy(
            data,
            result => result.metadata__brand || UNDEFINED_BRAND
        )

        // used to calculate group with most data points
        let maxDataPoints = 0

        // format data as a highcharts series
        const aggregate = filters[SOV_AGGREGATION]
        let series = map(brandGroups, (brandData, brandName) => {
            const brandDataForChart = brandData.map(result => {
                const xAxis = moment(result[aggregate]).valueOf()
                const yAxis = result[weightKey]
                return [xAxis, yAxis]
            })

            maxDataPoints = Math.max(maxDataPoints, brandDataForChart.length)

            return {
                name: brandName,
                // eslint-disable-next-line
                data: sortBy(brandDataForChart, ([x, y]) => x),
            }
        })

        // sort in desc order of y-axis, using most recent data point
        series = orderBy(
            series,
            [({ data: _data }) => last(_data)[1]],
            ['desc']
        )

        return { series, maxDataPoints }
    }
)((state, pageName) => pageName)

export const selectLoadingForSovChart = (state, pageName) =>
    selectDomainValue(state, [pageName, SOV_CHART, 'loading'])
