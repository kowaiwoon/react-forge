import { createAction } from 'redux-actions'

// sov keyword
export const fetchSovKeywordSuccess = createAction('FETCH_SOV_KEYWORD_SUCCESS')
export const attachSovKeywordSuccess = createAction(
    'ATTACH_SOV_KEYWORD_SUCCESS'
)
export const updateSovKeywordSuccess = createAction(
    'UPDATE_SOV_KEYWORD_SUCCESS'
)
export const deleteSovKeywordSuccess = createAction(
    'DELETE_SOV_KEYWORD_SUCCESS'
)
