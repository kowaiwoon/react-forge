import { createAction } from 'redux-actions'

// brand
export const fetchBrandSuccess = createAction('FETCH_BRAND_SUCCESS')
