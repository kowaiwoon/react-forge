import { createAction } from 'redux-actions'

import { curryActionForPage } from 'helpers/curry'

// Filters
export const updatePageFilter = createAction('UPDATE_PAGE_FILTER')
export const resetPageFilters = createAction('RESET_PAGE_FILTERS')
export const updatePageFilterSettings = createAction(
    'UPDATE_PAGE_FILTER_SETTINGS'
)
export const fetchPageFilterSettingsSuccess = createAction(
    'FETCH_PAGE_FILTER_SETTINGS_SUCCESS'
)

export const updatePageFilterForPage = curryActionForPage(updatePageFilter)
export const resetPageFiltersForPage = curryActionForPage(resetPageFilters)
export const updatePageFilterSettingsForPage = curryActionForPage(
    updatePageFilterSettings
)

// Chart
export const updateChartMetrics = createAction('UPDATE_CHART_METRICS')

// Table
export const updateTableSorter = createAction('UPDATE_TABLE_SORTER')

// Treemap
export const updateTreemapSorter = createAction('UPDATE_TREEMAP_SORTER')
