import { createAction } from 'redux-actions'

// mounting
export const mountSovKeywordsSummaryPageRequest = createAction(
    'MOUNT_SOV_KEYWORDS_SUMMARY_PAGE_REQUEST'
)
export const mountSovKeywordsSummaryPageSuccess = createAction(
    'MOUNT_SOV_KEYWORDS_SUMMARY_PAGE_SUCCESS'
)
export const mountSovKeywordsSummaryPageFailure = createAction(
    'MOUNT_SOV_KEYWORDS_SUMMARY_PAGE_FAILURE'
)
export const unmountSovKeywordsSummaryPage = createAction(
    'UNMOUNT_SOV_KEYWORDS_SUMMARY_PAGE'
)

// page data
export const fetchSovKeywordsSummaryPageDataRequest = createAction(
    'FETCH_SOV_KEYWORDS_SUMMARY_PAGE_DATA_REQUEST'
)
export const fetchSovKeywordsSummaryPageDataSuccess = createAction(
    'FETCH_SOV_KEYWORDS_SUMMARY_PAGE_DATA_SUCCESS'
)
export const fetchSovKeywordsSummaryPageDataFailure = createAction(
    'FETCH_SOV_KEYWORDS_SUMMARY_PAGE_DATA_FAILURE'
)

// table data
export const fetchSovKeywordsSummaryPageTableRequest = createAction(
    'FETCH_SOV_KEYWORDS_SUMMARY_PAGE_TABLE_REQUEST'
)
export const fetchSovKeywordsSummaryPageTableSuccess = createAction(
    'FETCH_SOV_KEYWORDS_SUMMARY_PAGE_TABLE_SUCCESS'
)
export const fetchSovKeywordsSummaryPageTableFailure = createAction(
    'FETCH_SOV_KEYWORDS_SUMMARY_PAGE_TABLE_FAILURE'
)

// table controls
export const updateSovKeywordsSummaryPageTablePagination = createAction(
    'UPDATE_SOV_KEYWORDS_SUMMARY_PAGE_TABLE_PAGINATION'
)
export const updateSovKeywordsSummaryPageTableSorter = createAction(
    'UPDATE_SOV_KEYWORDS_SUMMARY_PAGE_TABLE_SORTER'
)

// table settings
export const updateSovKeywordsSummaryPageTableSettings = createAction(
    'UPDATE_SOV_KEYWORDS_SUMMARY_PAGE_TABLE_SETTINGS'
)
export const fetchSovKeywordsSummaryPageTableSettingsSuccess = createAction(
    'FETCH_SOV_KEYWORDS_SUMMARY_PAGE_TABLE_SETTINGS_SUCCESS'
)

// table download
export const downloadSovKeywordsSummaryPageTableRequest = createAction(
    'DOWNLOAD_SOV_KEYWORDS_SUMMARY_PAGE_TABLE_REQUEST'
)
export const downloadSovKeywordsSummaryPageTableSuccess = createAction(
    'DOWNLOAD_SOV_KEYWORDS_SUMMARY_PAGE_TABLE_SUCCESS'
)
export const downloadSovKeywordsSummaryPageTableFailure = createAction(
    'DOWNLOAD_SOV_KEYWORDS_SUMMARY_PAGE_TABLE_FAILURE'
)

// table attach keyword
export const attachSovKeywordsSummaryPageTableKeywordsRequest = createAction(
    'ATTACH_SOV_KEYWORDS_SUMMARY_PAGE_TABLE_KEYWORDS_REQUEST'
)
export const attachSovKeywordsSummaryPageTableKeywordsSuccess = createAction(
    'ATTACH_SOV_KEYWORDS_SUMMARY_PAGE_TABLE_KEYWORDS_SUCCESS'
)
export const attachSovKeywordsSummaryPageTableKeywordsFailure = createAction(
    'ATTACH_SOV_KEYWORDS_SUMMARY_PAGE_TABLE_KEYWORDS_FAILURE'
)

// table update keyword
export const updateSovKeywordsSummaryPageTableKeywordRequest = createAction(
    'UPDATE_SOV_KEYWORDS_SUMMARY_PAGE_TABLE_KEYWORD_REQUEST'
)
export const updateSovKeywordsSummaryPageTableKeywordSuccess = createAction(
    'UPDATE_SOV_KEYWORDS_SUMMARY_PAGE_TABLE_KEYWORD_SUCCESS'
)
export const updateSovKeywordsSummaryPageTableKeywordFailure = createAction(
    'UPDATE_SOV_KEYWORDS_SUMMARY_PAGE_TABLE_KEYWORD_FAILURE'
)

// table delete keyword
export const deleteSovKeywordsSummaryPageTableKeywordRequest = createAction(
    'DELETE_SOV_KEYWORDS_SUMMARY_PAGE_TABLE_KEYWORD_REQUEST'
)
export const deleteSovKeywordsSummaryPageTableKeywordSuccess = createAction(
    'DELETE_SOV_KEYWORDS_SUMMARY_PAGE_TABLE_KEYWORD_SUCCESS'
)
export const deleteSovKeywordsSummaryPageTableKeywordFailure = createAction(
    'DELETE_SOV_KEYWORDS_SUMMARY_PAGE_TABLE_KEYWORD_FAILURE'
)

// search brands
export const searchSovBrandsRequest = createAction('SEARCH_SOV_BRANDS_REQUEST')
export const searchSovBrandsSuccess = createAction('SEARCH_SOV_BRANDS_SUCCESS')
export const searchSovBrandsFailure = createAction('SEARCH_SOV_BRANDS_FAILURE')
export const changeSovBrandsFilterInput = createAction(
    'CHANGE_SOV_BRANDS_FILTER_INPUT'
)

// search keywords
export const searchSovKeywordsRequest = createAction(
    'SEARCH_SOV_KEYWORDS_REQUEST'
)
export const searchSovKeywordsSuccess = createAction(
    'SEARCH_SOV_KEYWORDS_SUCCESS'
)
export const searchSovKeywordsFailure = createAction(
    'SEARCH_SOV_KEYWORDS_FAILURE'
)
export const changeSovKeywordsFilterInput = createAction(
    'CHANGE_SOV_KEYWORDS_FILTER_INPUT'
)

// search categories
export const searchSovKeywordCategoriesRequest = createAction(
    'SEARCH_SOV_KEYWORD_CATEGORIES_REQUEST'
)
export const searchSovKeywordCategoriesSuccess = createAction(
    'SEARCH_SOV_KEYWORD_CATEGORIES_SUCCESS'
)
export const searchSovKeywordCategoriesFailure = createAction(
    'SEARCH_SOV_KEYWORD_CATEGORIES_FAILURE'
)
export const changeSovKeywordCategoriesFilterInput = createAction(
    'CHANGE_SOV_KEYWORD_CATEGORIES_FILTER_INPUT'
)

// sov chart
export const fetchSovKeywordsSummaryPageSovChartSuccess = createAction(
    'FETCH_SOV_KEYWORDS_SUMMARY_PAGE_SOV_CHART_SUCCESS'
)

export const updateSovKeywordsSummaryPageSovChartWeight = createAction(
    'UPDATE_SOV_KEYWORDS_SUMMARY_PAGE_SOV_CHART_WEIGHT'
)
