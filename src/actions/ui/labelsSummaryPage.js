import { createAction } from 'redux-actions'

// mounting
export const mountLabelsSummaryPageRequest = createAction(
    'MOUNT_LABELS_SUMMARY_PAGE_REQUEST'
)
export const mountLabelsSummaryPageSuccess = createAction(
    'MOUNT_LABELS_SUMMARY_PAGE_SUCCESS'
)
export const mountLabelsSummaryPageFailure = createAction(
    'MOUNT_LABELS_SUMMARY_PAGE_FAILURE'
)
export const unmountLabelsSummaryPage = createAction(
    'UNMOUNT_LABELS_SUMMARY_PAGE'
)

// page data
export const fetchLabelsSummaryPageDataRequest = createAction(
    'FETCH_LABELS_SUMMARY_PAGE_DATA_REQUEST'
)
export const fetchLabelsSummaryPageDataSuccess = createAction(
    'FETCH_LABELS_SUMMARY_PAGE_DATA_SUCCESS'
)
export const fetchLabelsSummaryPageDataFailure = createAction(
    'FETCH_LABELS_SUMMARY_PAGE_DATA_FAILURE'
)

// table data
export const fetchLabelsSummaryPageTableRequest = createAction(
    'FETCH_LABELS_SUMMARY_PAGE_TABLE_REQUEST'
)
export const fetchLabelsSummaryPageTableSuccess = createAction(
    'FETCH_LABELS_SUMMARY_PAGE_TABLE_SUCCESS'
)
export const fetchLabelsSummaryPageTableFailure = createAction(
    'FETCH_LABELS_SUMMARY_PAGE_TABLE_FAILURE'
)

// table controls
export const updateLabelsSummaryPageTablePagination = createAction(
    'UPDATE_LABELS_SUMMARY_PAGE_TABLE_PAGINATION'
)
export const updateLabelsSummaryPageTableSorter = createAction(
    'UPDATE_LABELS_SUMMARY_PAGE_TABLE_SORTER'
)

// table settings
export const updateLabelsSummaryPageTableSettings = createAction(
    'UPDATE_LABELS_SUMMARY_PAGE_TABLE_SETTINGS'
)
export const fetchLabelsSummaryPageTableSettingsSuccess = createAction(
    'FETCH_LABELS_SUMMARY_PAGE_TABLE_SETTINGS_SUCCESS'
)

// create label
export const createLabelsSummaryPageLabelRequest = createAction(
    'CREATE_LABELS_SUMMARY_PAGE_LABEL_REQUEST'
)
export const createLabelsSummaryPageLabelSuccess = createAction(
    'CREATE_LABELS_SUMMARY_PAGE_LABEL_SUCCESS'
)
export const createLabelsSummaryPageLabelFailure = createAction(
    'CREATE_LABELS_SUMMARY_PAGE_LABEL_FAILURE'
)
