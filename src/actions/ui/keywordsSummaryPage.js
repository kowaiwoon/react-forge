import { createAction } from 'redux-actions'

// mounting
export const mountKeywordsSummaryPageRequest = createAction(
    'MOUNT_KEYWORDS_SUMMARY_PAGE_REQUEST'
)
export const mountKeywordsSummaryPageSuccess = createAction(
    'MOUNT_KEYWORDS_SUMMARY_PAGE_SUCCESS'
)
export const mountKeywordsSummaryPageFailure = createAction(
    'MOUNT_KEYWORDS_SUMMARY_PAGE_FAILURE'
)
export const unmountKeywordsPageSummary = createAction(
    'UNMOUNT_KEYWORDS_PAGE_SUMMARY'
)

// page data
export const fetchKeywordsSummaryPageDataRequest = createAction(
    'FETCH_KEYWORDS_SUMMARY_PAGE_DATA_REQUEST'
)
export const fetchKeywordsSummaryPageDataSuccess = createAction(
    'FETCH_KEYWORDS_SUMMARY_PAGE_DATA_SUCCESS'
)
export const fetchKeywordsSummaryPageDataFailure = createAction(
    'FETCH_KEYWORDS_SUMMARY_PAGE_DATA_FAILURE'
)

// treemap data
export const fetchKeywordsSummaryPageTreemapRequest = createAction(
    'FETCH_KEYWORDS_SUMMARY_PAGE_TREEMAP_REQUEST'
)
export const fetchKeywordsSummaryPageTreemapSuccess = createAction(
    'FETCH_KEYWORDS_SUMMARY_PAGE_TREEMAP_SUCCESS'
)
export const fetchKeywordsSummaryPageTreemapFailure = createAction(
    'FETCH_KEYWORDS_SUMMARY_PAGE_TREEMAP_FAILURE'
)

// treemap controls
export const updateKeywordsSummaryPageTreemapPagination = createAction(
    'UPDATE_KEYWORDS_SUMMARY_PAGE_TREEMAP_PAGINATION'
)
export const updateKeywordsSummaryPageTreemapSorter = createAction(
    'UPDATE_KEYWORDS_SUMMARY_PAGE_TREEMAP_SORTER'
)

// table data
export const fetchKeywordsSummaryPageTableRequest = createAction(
    'FETCH_KEYWORDS_SUMMARY_PAGE_TABLE_REQUEST'
)
export const fetchKeywordsSummaryPageTableSuccess = createAction(
    'FETCH_KEYWORDS_SUMMARY_PAGE_TABLE_SUCCESS'
)
export const fetchKeywordsSummaryPageTableFailure = createAction(
    'FETCH_KEYWORDS_SUMMARY_PAGE_TABLE_FAILURE'
)

// table controls
export const updateKeywordsSummaryPageTablePagination = createAction(
    'UPDATE_KEYWORDS_SUMMARY_PAGE_TABLE_PAGINATION'
)
export const updateKeywordsSummaryPageTableSorter = createAction(
    'UPDATE_KEYWORDS_SUMMARY_PAGE_TABLE_SORTER'
)

// table settings
export const updateKeywordsSummaryPageTableSettings = createAction(
    'UPDATE_KEYWORDS_SUMMARY_PAGE_TABLE_SETTINGS'
)
export const fetchKeywordsSummaryPageTableSettingsSuccess = createAction(
    'FETCH_KEYWORDS_SUMMARY_PAGE_TABLE_SETTINGS_SUCCESS'
)

// table download
export const downloadKeywordsSummaryPageTableRequest = createAction(
    'DOWNLOAD_KEYWORDS_SUMMARY_PAGE_TABLE_REQUEST'
)
export const downloadKeywordsSummaryPageTableSuccess = createAction(
    'DOWNLOAD_KEYWORDS_SUMMARY_PAGE_TABLE_SUCCESS'
)
export const downloadKeywordsSummaryPageTableFailure = createAction(
    'DOWNLOAD_KEYWORDS_SUMMARY_PAGE_TABLE_FAILURE'
)

// search
export const searchKeywordsRequest = createAction('SEARCH_KEYWORDS_REQUEST')
export const searchKeywordsSuccess = createAction('SEARCH_KEYWORDS_SUCCESS')
export const searchKeywordsFailure = createAction('SEARCH_KEYWORDS_FAILURE')
export const changeKeywordsFilterInput = createAction(
    'CHANGE_KEYWORDS_FILTER_INPUT'
)
