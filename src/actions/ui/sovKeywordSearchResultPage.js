import { createAction } from 'redux-actions'

// mounting
export const mountSovKeywordSearchResultPageRequest = createAction(
    'MOUNT_SOV_KEYWORD_SEARCH_RESULT_PAGE_REQUEST'
)
export const mountSovKeywordSearchResultPageSuccess = createAction(
    'MOUNT_SOV_KEYWORD_SEARCH_RESULT_PAGE_SUCCESS'
)
export const mountSovKeywordSearchResultPageFailure = createAction(
    'MOUNT_SOV_KEYWORD_SEARCH_RESULT_PAGE_FAILURE'
)
export const unmountSovKeywordSearchResultPage = createAction(
    'UNMOUNT_SOV_KEYWORD_SEARCH_RESULT_PAGE'
)

// page data
export const fetchSovKeywordSearchResultPageDataRequest = createAction(
    'FETCH_SOV_KEYWORD_SEARCH_RESULT_PAGE_DATA_REQUEST'
)
export const fetchSovKeywordSearchResultPageDataSuccess = createAction(
    'FETCH_SOV_KEYWORD_SEARCH_RESULT_PAGE_DATA_SUCCESS'
)
export const fetchSovKeywordSearchResultPageDataFailure = createAction(
    'FETCH_SOV_KEYWORD_SEARCH_RESULT_PAGE_DATA_FAILURE'
)

// table data
export const fetchSovKeywordSearchResultPageTableRequest = createAction(
    'FETCH_SOV_KEYWORD_SEARCH_RESULT_PAGE_TABLE_REQUEST'
)
export const fetchSovKeywordSearchResultPageTableSuccess = createAction(
    'FETCH_SOV_KEYWORD_SEARCH_RESULT_PAGE_TABLE_SUCCESS'
)
export const fetchSovKeywordSearchResultPageTableFailure = createAction(
    'FETCH_SOV_KEYWORD_SEARCH_RESULT_PAGE_TABLE_FAILURE'
)

// table controls
export const updateSovKeywordSearchResultPageTablePagination = createAction(
    'UPDATE_SOV_KEYWORD_SEARCH_RESULT_PAGE_TABLE_PAGINATION'
)
export const updateSovKeywordSearchResultPageTableSorter = createAction(
    'UPDATE_SOV_KEYWORD_SEARCH_RESULT_PAGE_TABLE_SORTER'
)

// table settings
export const updateSovKeywordSearchResultPageTableSettings = createAction(
    'UPDATE_SOV_KEYWORD_SEARCH_RESULT_PAGE_TABLE_SETTINGS'
)
export const fetchSovKeywordSearchResultPageTableSettingsSuccess = createAction(
    'FETCH_SOV_KEYWORD_SEARCH_RESULT_PAGE_TABLE_SETTINGS_SUCCESS'
)

// table download
export const downloadSovKeywordSearchResultPageTableRequest = createAction(
    'DOWNLOAD_SOV_KEYWORD_SEARCH_RESULT_PAGE_TABLE_REQUEST'
)
export const downloadSovKeywordSearchResultPageTableSuccess = createAction(
    'DOWNLOAD_SOV_KEYWORD_SEARCH_RESULT_PAGE_TABLE_SUCCESS'
)
export const downloadSovKeywordSearchResultPageTableFailure = createAction(
    'DOWNLOAD_SOV_KEYWORD_SEARCH_RESULT_PAGE_TABLE_FAILURE'
)

// screenshot
export const fetchSovKeywordSearchResultPageScreenshotSuccess = createAction(
    'FETCH_SOV_KEYWORD_SEARCH_RESULT_PAGE_SCREENSHOT_SUCCESS'
)
