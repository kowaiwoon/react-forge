import { createAction } from 'redux-actions'

// mounting
export const mountProductsSummaryPageRequest = createAction(
    'MOUNT_PRODUCTS_SUMMARY_PAGE_REQUEST'
)
export const mountProductsSummaryPageSuccess = createAction(
    'MOUNT_PRODUCTS_SUMMARY_PAGE_SUCCESS'
)
export const mountProductsSummaryPageFailure = createAction(
    'MOUNT_PRODUCTS_SUMMARY_PAGE_FAILURE'
)
export const unmountProductsSummary = createAction('UNMOUNT_PRODUCTS_SUMMARY')

// page data
export const fetchProductsSummaryPageDataRequest = createAction(
    'FETCH_PRODUCTS_SUMMARY_PAGE_DATA_REQUEST'
)
export const fetchProductsSummaryPageDataSuccess = createAction(
    'FETCH_PRODUCTS_SUMMARY_PAGE_DATA_SUCCESS'
)
export const fetchProductsSummaryPageDataFailure = createAction(
    'FETCH_PRODUCTS_SUMMARY_PAGE_DATA_FAILURE'
)

// treemap data
export const fetchProductsSummaryPageTreemapRequest = createAction(
    'FETCH_PRODUCTS_SUMMARY_PAGE_TREEMAP_REQUEST'
)
export const fetchProductsSummaryPageTreemapSuccess = createAction(
    'FETCH_PRODUCTS_SUMMARY_PAGE_TREEMAP_SUCCESS'
)
export const fetchProductsSummaryPageTreemapFailure = createAction(
    'FETCH_PRODUCTS_SUMMARY_PAGE_TREEMAP_FAILURE'
)

// treemap controls
export const updateProductsSummaryPageTreemapPagination = createAction(
    'UPDATE_PRODUCTS_SUMMARY_PAGE_TREEMAP_PAGINATION'
)
export const updateProductsSummaryPageTreemapSorter = createAction(
    'UPDATE_PRODUCTS_SUMMARY_PAGE_TREEMAP_SORTER'
)

// table data
export const fetchProductsSummaryPageTableRequest = createAction(
    'FETCH_PRODUCTS_SUMMARY_PAGE_TABLE_REQUEST'
)
export const fetchProductsSummaryPageTableSuccess = createAction(
    'FETCH_PRODUCTS_SUMMARY_PAGE_TABLE_SUCCESS'
)
export const fetchProductsSummaryPageTableFailure = createAction(
    'FETCH_PRODUCTS_SUMMARY_PAGE_TABLE_FAILURE'
)

// table controls
export const updateProductsSummaryPageTablePagination = createAction(
    'UPDATE_PRODUCTS_SUMMARY_PAGE_TABLE_PAGINATION'
)
export const updateProductsSummaryPageTableSorter = createAction(
    'UPDATE_PRODUCTS_SUMMARY_PAGE_TABLE_SORTER'
)

// table settings
export const updateProductsSummaryPageTableSettingsRequest = createAction(
    'UPDATE_PRODUCTS_SUMMARY_PAGE_TABLE_SETTINGS_REQUEST'
)
export const fetchProductsSummaryPageTableSettingsSuccess = createAction(
    'FETCH_PRODUCTS_SUMMARY_PAGE_TABLE_SETTINGS_SUCCESS'
)

// table download
export const downloadProductsSummaryPageTableRequest = createAction(
    'DOWNLOAD_PRODUCTS_SUMMARY_PAGE_TABLE_REQUEST'
)
export const downloadProductsSummaryPageTableSuccess = createAction(
    'DOWNLOAD_PRODUCTS_SUMMARY_PAGE_TABLE_SUCCESS'
)
export const downloadProductsSummaryPageTableFailure = createAction(
    'DOWNLOAD_PRODUCTS_SUMMARY_PAGE_TABLE_FAILURE'
)

// search
export const searchProductAsinsRequest = createAction(
    'SEARCH_PRODUCT_ASINS_REQUEST'
)
export const searchProductAsinsSuccess = createAction(
    'SEARCH_PRODUCT_ASINS_SUCCESS'
)
export const searchProductAsinsFailure = createAction(
    'SEARCH_PRODUCT_ASINS_FAILURE'
)
export const changeProductAsinsFilterInput = createAction(
    'CHANGE_PRODUCT_ASINS_FILTER_INPUT'
)

export const searchProductTitlesRequest = createAction(
    'SEARCH_PRODUCT_TITLES_REQUEST'
)
export const searchProductTitlesSuccess = createAction(
    'SEARCH_PRODUCT_TITLES_SUCCESS'
)
export const searchProductTitlesFailure = createAction(
    'SEARCH_PRODUCT_TITLES_FAILURE'
)
export const changeProductTitlesFilterInput = createAction(
    'CHANGE_PRODUCT_TITLES_FILTER_INPUT'
)
