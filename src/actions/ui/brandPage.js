import { createAction } from 'redux-actions'

// mounting
export const mountBrandPageRequest = createAction('MOUNT_BRAND_PAGE_REQUEST')
export const mountBrandPageSuccess = createAction('MOUNT_BRAND_PAGE_SUCCESS')
export const mountBrandPageFailure = createAction('MOUNT_BRAND_PAGE_FAILURE')
export const unmountBrandPage = createAction('UNMOUNT_BRAND_PAGE')

// page data
export const fetchBrandPageDataRequest = createAction(
    'FETCH_BRAND_PAGE_DATA_REQUEST'
)
export const fetchBrandPageDataSuccess = createAction(
    'FETCH_BRAND_PAGE_DATA_SUCCESS'
)
export const fetchBrandPageDataFailure = createAction(
    'FETCH_BRAND_PAGE_DATA_FAILURE'
)

// toggle brand details
export const toggleBrandPageDetails = createAction('TOGGLE_BRAND_PAGE_DETAILS')

// aggregate data
export const fetchBrandPageAggregateSuccess = createAction(
    'FETCH_BRAND_PAGE_AGGREGATE_SUCCESS'
)

// timeseries data
export const fetchBrandPageSponsoredProductTimeseriesSuccess = createAction(
    'FETCH_BRAND_PAGE_SPONSORED_PRODUCT_TIMESERIES_SUCCESS'
)
export const fetchBrandPageHeadlineSearchTimeseriesSuccess = createAction(
    'FETCH_BRAND_PAGE_HEADLINE_SEARCH_TIMESERIES_SUCCESS'
)

// timeseries download
export const downloadBrandPageTimeseriesRequest = createAction(
    'DOWNLOAD_BRAND_PAGE_TIMESERIES_REQUEST'
)
export const downloadBrandPageTimeseriesSuccess = createAction(
    'DOWNLOAD_BRAND_PAGE_TIMESERIES_SUCCESS'
)
export const downloadBrandPageTimeseriesFailure = createAction(
    'DOWNLOAD_BRAND_PAGE_TIMESERIES_FAILURE'
)

// treemap data
export const fetchBrandPageTreemapRequest = createAction(
    'FETCH_BRAND_PAGE_TREEMAP_REQUEST'
)
export const fetchBrandPageTreemapSuccess = createAction(
    'FETCH_BRAND_PAGE_TREEMAP_SUCCESS'
)
export const fetchBrandPageTreemapFailure = createAction(
    'FETCH_BRAND_PAGE_TREEMAP_FAILURE'
)

// treemap controls
export const updateBrandPageTreemapPagination = createAction(
    'UPDATE_BRAND_PAGE_TREEMAP_PAGINATION'
)
export const updateBrandPageTreemapSorter = createAction(
    'UPDATE_BRAND_PAGE_TREEMAP_SORTER'
)

// table data
export const fetchBrandPageTableRequest = createAction(
    'FETCH_BRAND_PAGE_TABLE_REQUEST'
)
export const fetchBrandPageTableSuccess = createAction(
    'FETCH_BRAND_PAGE_TABLE_SUCCESS'
)
export const fetchBrandPageTableFailure = createAction(
    'FETCH_BRAND_PAGE_TABLE_FAILURE'
)

// table controls
export const updateBrandPageTablePagination = createAction(
    'UPDATE_BRAND_PAGE_TABLE_PAGINATION'
)
export const updateBrandPageTableSorter = createAction(
    'UPDATE_BRAND_PAGE_TABLE_SORTER'
)

// table settings
export const updateBrandPageTableSettings = createAction(
    'UPDATE_BRAND_PAGE_TABLE_SETTINGS'
)
export const fetchBrandPageTableSettingsSuccess = createAction(
    'FETCH_BRAND_PAGE_TABLE_SETTINGS_SUCCESS'
)

// table download
export const downloadBrandPageTableRequest = createAction(
    'DOWNLOAD_BRAND_PAGE_TABLE_REQUEST'
)
export const downloadBrandPageTableSuccess = createAction(
    'DOWNLOAD_BRAND_PAGE_TABLE_SUCCESS'
)
export const downloadBrandPageTableFailure = createAction(
    'DOWNLOAD_BRAND_PAGE_TABLE_FAILURE'
)

// table update
export const updateBrandPageTableRequest = createAction(
    'UPDATE_BRAND_PAGE_TABLE_REQUEST'
)
export const updateBrandPageTableSuccess = createAction(
    'UPDATE_BRAND_PAGE_TABLE_SUCCESS'
)
export const updateBrandPageTableFailure = createAction(
    'UPDATE_BRAND_PAGE_TABLE_FAILURE'
)

// table delete
export const deleteBrandPageTableRequest = createAction(
    'DELETE_BRAND_PAGE_TABLE_REQUEST'
)
export const deleteBrandPageTableSuccess = createAction(
    'DELETE_BRAND_PAGE_TABLE_SUCCESS'
)
export const deleteBrandPageTableFailure = createAction(
    'DELETE_BRAND_PAGE_TABLE_FAILURE'
)
