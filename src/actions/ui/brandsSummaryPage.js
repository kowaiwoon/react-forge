import { createAction } from 'redux-actions'

// mounting
export const mountBrandsSummaryPageRequest = createAction(
    'MOUNT_BRANDS_SUMMARY_PAGE_REQUEST'
)
export const mountBrandsSummaryPageSuccess = createAction(
    'MOUNT_BRANDS_SUMMARY_PAGE_SUCCESS'
)
export const mountBrandsSummaryPageFailure = createAction(
    'MOUNT_BRANDS_SUMMARY_PAGE_FAILURE'
)
export const unmountBrandsSummaryPage = createAction(
    'UNMOUNT_BRANDS_SUMMARY_PAGE'
)

// page data
export const fetchBrandsSummaryPageDataRequest = createAction(
    'FETCH_BRANDS_SUMMARY_PAGE_DATA_REQUEST'
)
export const fetchBrandsSummaryPageDataSuccess = createAction(
    'FETCH_BRANDS_SUMMARY_PAGE_DATA_SUCCESS'
)
export const fetchBrandsSummaryPageDataFailure = createAction(
    'FETCH_BRANDS_SUMMARY_PAGE_DATA_FAILURE'
)

// treemap data
export const fetchBrandsSummaryPageTreemapRequest = createAction(
    'FETCH_BRANDS_SUMMARY_PAGE_TREEMAP_REQUEST'
)
export const fetchBrandsSummaryPageTreemapSuccess = createAction(
    'FETCH_BRANDS_SUMMARY_PAGE_TREEMAP_SUCCESS'
)
export const fetchBrandsSummaryPageTreemapFailure = createAction(
    'FETCH_BRANDS_SUMMARY_PAGE_TREEMAP_FAILURE'
)

// treemap controls
export const updateBrandsSummaryPageTreemapPagination = createAction(
    'UPDATE_BRANDS_SUMMARY_PAGE_TREEMAP_PAGINATION'
)
export const updateBrandsSummaryPageTreemapSorter = createAction(
    'UPDATE_BRANDS_SUMMARY_PAGE_TREEMAP_SORTER'
)

// table data
export const fetchBrandsSummaryPageTableRequest = createAction(
    'FETCH_BRANDS_SUMMARY_PAGE_TABLE_REQUEST'
)
export const fetchBrandsSummaryPageTableSuccess = createAction(
    'FETCH_BRANDS_SUMMARY_PAGE_TABLE_SUCCESS'
)
export const fetchBrandsSummaryPageTableFailure = createAction(
    'FETCH_BRANDS_SUMMARY_PAGE_TABLE_FAILURE'
)

// table controls
export const updateBrandsSummaryPageTablePagination = createAction(
    'UPDATE_BRANDS_SUMMARY_PAGE_TABLE_PAGINATION'
)
export const updateBrandsSummaryPageTableSorter = createAction(
    'UPDATE_BRANDS_SUMMARY_PAGE_TABLE_SORTER'
)

// table settings
export const updateBrandsSummaryPageTableSettings = createAction(
    'UPDATE_BRANDS_SUMMARY_PAGE_TABLE_SETTINGS'
)
export const fetchBrandsSummaryPageTableSettingsSuccess = createAction(
    'FETCH_BRANDS_SUMMARY_PAGE_TABLE_SETTINGS_SUCCESS'
)

// table download
export const downloadBrandsSummaryPageTableRequest = createAction(
    'DOWNLOAD_BRANDS_SUMMARY_PAGE_TABLE_REQUEST'
)
export const downloadBrandsSummaryPageTableSuccess = createAction(
    'DOWNLOAD_BRANDS_SUMMARY_PAGE_TABLE_SUCCESS'
)
export const downloadBrandsSummaryPageTableFailure = createAction(
    'DOWNLOAD_BRANDS_SUMMARY_PAGE_TABLE_FAILURE'
)
