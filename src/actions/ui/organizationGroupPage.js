import { createAction } from 'redux-actions'

// mounting
export const mountOrganizationGroupPageRequest = createAction(
    'MOUNT_ORGANIZATION_GROUP_PAGE_REQUEST'
)
export const mountOrganizationGroupPageSuccess = createAction(
    'MOUNT_ORGANIZATION_GROUP_PAGE_SUCCESS'
)
export const mountOrganizationGroupPageFailure = createAction(
    'MOUNT_ORGANIZATION_GROUP_PAGE_FAILURE'
)
export const unmountOrganizationGroupPage = createAction(
    'UNMOUNT_ORGANIZATION_GROUP_PAGE'
)

// page data
export const fetchOrganizationGroupPageDataRequest = createAction(
    'FETCH_ORGANIZATION_GROUP_PAGE_DATA_REQUEST'
)
export const fetchOrganizationGroupPageDataSuccess = createAction(
    'FETCH_ORGANIZATION_GROUP_PAGE_DATA_SUCCESS'
)
export const fetchOrganizationGroupPageDataFailure = createAction(
    'FETCH_ORGANIZATION_GROUP_PAGE_DATA_FAILURE'
)

// update name
export const updateNameOrganizationGroupPageRequest = createAction(
    'UPDATE_NAME_ORGANIZATION_GROUP_PAGE_REQUEST'
)
export const updateNameOrganizationGroupPageSuccess = createAction(
    'UPDATE_NAME_ORGANIZATION_GROUP_PAGE_SUCCESS'
)
export const updateNameOrganizationGroupPageFailure = createAction(
    'UPDATE_NAME_ORGANIZATION_GROUP_PAGE_FAILURE'
)

// add member form
export const addMemberOrganizationGroupPageRequest = createAction(
    'ADD_MEMBER_ORGANIZATION_GROUP_PAGE_REQUEST'
)
export const addMemberOrganizationGroupPageSuccess = createAction(
    'ADD_MEMBER_ORGANIZATION_GROUP_PAGE_SUCCESS'
)
export const addMemberOrganizationGroupPageFailure = createAction(
    'ADD_MEMBER_ORGANIZATION_GROUP_PAGE_FAILURE'
)

// remove member
export const removeMemberOrganizationGroupPageRequest = createAction(
    'REMOVE_MEMBER_ORGANIZATION_GROUP_PAGE_REQUEST'
)
export const removeMemberOrganizationGroupPageSuccess = createAction(
    'REMOVE_MEMBER_ORGANIZATION_GROUP_PAGE_SUCCESS'
)
export const removeMemberOrganizationGroupPageFailure = createAction(
    'REMOVE_MEMBER_ORGANIZATION_GROUP_PAGE_FAILURE'
)

// update resources
export const updateResourcesOrganizationGroupPageRequest = createAction(
    'UPDATE_RESOURCES_ORGANIZATION_GROUP_PAGE_REQUEST'
)
export const updateResourcesOrganizationGroupPageSuccess = createAction(
    'UPDATE_RESOURCES_ORGANIZATION_GROUP_PAGE_SUCCESS'
)
export const updateResourcesOrganizationGroupPageFailure = createAction(
    'UPDATE_RESOURCES_ORGANIZATION_GROUP_PAGE_FAILURE'
)

// update permissions
export const updatePermissionsOrganizationGroupPageRequest = createAction(
    'UPDATE_PERMISSIONS_ORGANIZATION_GROUP_PAGE_REQUEST'
)
export const updatePermissionsOrganizationGroupPageSuccess = createAction(
    'UPDATE_PERMISSIONS_ORGANIZATION_GROUP_PAGE_SUCCESS'
)
export const updatePermissionsOrganizationGroupPageFailure = createAction(
    'UPDATE_PERMISSIONS_ORGANIZATION_GROUP_PAGE_FAILURE'
)

// search brands
export const changeOrganizationGroupPageBrandsSearchInput = createAction(
    'CHANGE_ORGANIZATION_GROUP_PAGE_BRANDS_SEARCH_INPUT'
)
export const searchOrganizationGroupPageBrandsRequest = createAction(
    'SEARCH_ORGANIZATION_GROUP_PAGE_BRANDS_REQUEST'
)
export const searchOrganizationGroupPageBrandsSuccess = createAction(
    'SEARCH_ORGANIZATION_GROUP_PAGE_BRANDS_SUCCESS'
)
export const searchOrganizationGroupPageBrandsFailure = createAction(
    'SEARCH_ORGANIZATION_GROUP_PAGE_BRANDS_FAILURE'
)
