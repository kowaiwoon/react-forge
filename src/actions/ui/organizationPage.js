import { createAction } from 'redux-actions'

// mounting
export const mountOrganizationPageRequest = createAction(
    'MOUNT_ORGANIZATION_PAGE_REQUEST'
)
export const mountOrganizationPageSuccess = createAction(
    'MOUNT_ORGANIZATION_PAGE_SUCCESS'
)
export const mountOrganizationPageFailure = createAction(
    'MOUNT_ORGANIZATION_PAGE_FAILURE'
)
export const unmountOrganizationPage = createAction('UNMOUNT_ORGANIZATION_PAGE')

// page data
export const fetchOrganizationPageDataRequest = createAction(
    'FETCH_ORGANIZATION_PAGE_DATA_REQUEST'
)
export const fetchOrganizationPageDataSuccess = createAction(
    'FETCH_ORGANIZATION_PAGE_DATA_SUCCESS'
)
export const fetchOrganizationPageDataFailure = createAction(
    'FETCH_ORGANIZATION_PAGE_DATA_FAILURE'
)

// organization
export const fetchOrganizationPageOrganizationSucess = createAction(
    'FETCH_ORGANIZATION_PAGE_ORGANIZATION_SUCCESS'
)

// toggle details
export const toggleOrganizationPageDetails = createAction(
    'TOGGLE_ORGANIZATION_PAGE_DETAILS'
)

// groups table data
export const fetchOrganizationPageGroupsTableRequest = createAction(
    'FETCH_ORGANIZATION_PAGE_GROUPS_TABLE_REQUEST'
)
export const fetchOrganizationPageGroupsTableSuccess = createAction(
    'FETCH_ORGANIZATION_PAGE_GROUPS_TABLE_SUCCESS'
)
export const fetchOrganizationPageGroupsTableFailure = createAction(
    'FETCH_ORGANIZATION_PAGE_GROUPS_TABLE_FAILURE'
)

// groups table controls
export const updateOrganizationPageGroupsTablePagination = createAction(
    'UPDATE_ORGANIZATION_PAGE_GROUPS_TABLE_PAGINATION'
)

// groups table create
export const createOrganizationPageGroupRequest = createAction(
    'CREATE_ORGANIZATION_PAGE_GROUP_REQUEST'
)
export const createOrganizationPageGroupSuccess = createAction(
    'CREATE_ORGANIZATION_PAGE_GROUP_SUCCESS'
)
export const createOrganizationPageGroupFailure = createAction(
    'CREATE_ORGANIZATION_PAGE_GROUP_FAILURE'
)

// members table data
export const fetchOrganizationPageMembersTableRequest = createAction(
    'FETCH_ORGANIZATION_PAGE_MEMBERS_TABLE_REQUEST'
)
export const fetchOrganizationPageMembersTableSuccess = createAction(
    'FETCH_ORGANIZATION_PAGE_MEMBERS_TABLE_SUCCESS'
)
export const fetchOrganizationPageMembersTableFailure = createAction(
    'FETCH_ORGANIZATION_PAGE_MEMBERS_TABLE_FAILURE'
)

// members table controls
export const updateOrganizationPageMembersTablePagination = createAction(
    'UPDATE_ORGANIZATION_PAGE_MEMBERS_TABLE_PAGINATION'
)

// remove member
export const removeMemberOrganizationPageRequest = createAction(
    'REMOVE_MEMBER_ORGANIZATION_PAGE_REQUEST'
)
export const removeMemberOrganizationPageSuccess = createAction(
    'REMOVE_MEMBER_ORGANIZATION_PAGE_SUCCESS'
)
export const removeMemberOrganizationPageFailure = createAction(
    'REMOVE_MEMBER_ORGANIZATION_PAGE_FAILURE'
)

// invitations table invite
export const inviteMemberOrganizationPageRequest = createAction(
    'INVITE_MEMBER_ORGANIZATION_PAGE_REQUEST'
)
export const inviteMemberOrganizationPageSuccess = createAction(
    'INVITE_MEMBER_ORGANIZATION_PAGE_SUCCESS'
)
export const inviteMemberOrganizationPageFailure = createAction(
    'INVITE_MEMBER_ORGANIZATION_PAGE_FAILURE'
)

// invitations table data
export const fetchOrganizationPageInvitationsTableRequest = createAction(
    'FETCH_ORGANIZATION_PAGE_INVITATIONS_TABLE_REQUEST'
)
export const fetchOrganizationPageInvitationsTableSuccess = createAction(
    'FETCH_ORGANIZATION_PAGE_INVITATIONS_TABLE_SUCCESS'
)
export const fetchOrganizationPageInvitationsTableFailure = createAction(
    'FETCH_ORGANIZATION_PAGE_INVITATIONS_TABLE_FAILURE'
)

// invitations table controls
export const updateOrganizationPageInvitationsTablePagination = createAction(
    'UPDATE_ORGANIZATION_PAGE_INVITATIONS_TABLE_PAGINATION'
)
export const updateOrganizationPageInvitationsTableSorter = createAction(
    'UPDATE_ORGANIZATION_PAGE_INVITATIONS_TABLE_SORTER'
)

// integrations table data
export const fetchOrganizationPageIntegrationsTableRequest = createAction(
    'FETCH_ORGANIZATION_PAGE_INTEGRATIONS_TABLE_REQUEST'
)
export const fetchOrganizationPageIntegrationsTableSuccess = createAction(
    'FETCH_ORGANIZATION_PAGE_INTEGRATIONS_TABLE_SUCCESS'
)
export const fetchOrganizationPageIntegrationsTableFailure = createAction(
    'FETCH_ORGANIZATION_PAGE_INTEGRATIONS_TABLE_FAILURE'
)

// integrations table controls
export const updateOrganizationPageIntegrationsTablePagination = createAction(
    'UPDATE_ORGANIZATION_PAGE_INTEGRATIONS_TABLE_PAGINATION'
)

// integrations table create
export const createOrganizationPageIntegrationRequest = createAction(
    'CREATE_ORGANIZATION_PAGE_INTEGRATION_REQUEST'
)
export const createOrganizationPageIntegrationSuccess = createAction(
    'CREATE_ORGANIZATION_PAGE_INTEGRATION_SUCCESS'
)
export const createOrganizationPageIntegrationFailure = createAction(
    'CREATE_ORGANIZATION_PAGE_INTEGRATION_FAILURE'
)

// search brands
export const changeOrganizationPageBrandsSearchInput = createAction(
    'CHANGE_ORGANIZATION_PAGE_BRANDS_SEARCH_INPUT'
)
export const searchOrganizationPageBrandsRequest = createAction(
    'SEARCH_ORGANIZATION_PAGE_BRANDS_REQUEST'
)
export const searchOrganizationPageBrandsSuccess = createAction(
    'SEARCH_ORGANIZATION_PAGE_BRANDS_SUCCESS'
)
export const searchOrganizationPageBrandsFailure = createAction(
    'SEARCH_ORGANIZATION_PAGE_BRANDS_FAILURE'
)
