import { createAction } from 'redux-actions'

// mounting
export const mountAutomationPageRequest = createAction(
    'MOUNT_AUTOMATION_PAGE_REQUEST'
)
export const mountAutomationPageSuccess = createAction(
    'MOUNT_AUTOMATION_PAGE_SUCCESS'
)
export const mountAutomationPageFailure = createAction(
    'MOUNT_AUTOMATION_PAGE_FAILURE'
)
export const unmountAutomationPage = createAction('UNMOUNT_AUTOMATION_PAGE')

// page data
export const fetchAutomationPageDataRequest = createAction(
    'FETCH_AUTOMATION_PAGE_DATA_REQUEST'
)
export const fetchAutomationPageDataSuccess = createAction(
    'FETCH_AUTOMATION_PAGE_DATA_SUCCESS'
)
export const fetchAutomationPageDataFailure = createAction(
    'FETCH_AUTOMATION_PAGE_DATA_FAILURE'
)

// automation description
export const fetchAutomationPageDescriptionSuccess = createAction(
    'FETCH_AUTOMATION_PAGE_DESCRIPTION_SUCCESS'
)

// table data
export const fetchAutomationPageTableRequest = createAction(
    'FETCH_AUTOMATION_PAGE_TABLE_REQUEST'
)
export const fetchAutomationPageTableSuccess = createAction(
    'FETCH_AUTOMATION_PAGE_TABLE_SUCCESS'
)
export const fetchAutomationPageTableFailure = createAction(
    'FETCH_AUTOMATION_PAGE_TABLE_FAILURE'
)

// table controls
export const updateAutomationPageTablePagination = createAction(
    'UPDATE_AUTOMATION_PAGE_TABLE_PAGINATION'
)
export const updateAutomationPageTableSorter = createAction(
    'UPDATE_AUTOMATION_PAGE_TABLE_SORTER'
)

// table settings
export const updateAutomationPageTableSettings = createAction(
    'UPDATE_AUTOMATION_PAGE_TABLE_SETTINGS'
)
export const fetchAutomationPageTableSettingsSuccess = createAction(
    'FETCH_AUTOMATION_PAGE_TABLE_SETTINGS_SUCCESS'
)
