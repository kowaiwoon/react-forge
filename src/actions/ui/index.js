import {
    HOME_PAGE,
    BRANDS_SUMMARY_PAGE,
    BRAND_PAGE,
    CAMPAIGNS_SUMMARY_PAGE,
    CAMPAIGN_PAGE,
    PRODUCTS_SUMMARY_PAGE,
    PRODUCT_PAGE,
    KEYWORDS_SUMMARY_PAGE,
    KEYWORD_PAGE,
    SOV_KEYWORDS_SUMMARY_PAGE,
    SOV_KEYWORD_PAGE,
    SOV_KEYWORD_SEARCH_RESULT_PAGE,
    ORGANIZATION_PAGE,
    ORGANIZATION_GROUP_PAGE,
    LABELS_SUMMARY_PAGE,
    AUTOMATION_PAGE,
} from 'constants/pages'

import { fetchHomePageDataRequest } from './homePage'
import {
    fetchBrandsSummaryPageDataRequest,
    updateBrandsSummaryPageTablePagination,
    updateBrandsSummaryPageTreemapPagination,
} from './brandsSummaryPage'
import {
    fetchBrandPageDataRequest,
    updateBrandPageTablePagination,
    updateBrandPageTreemapPagination,
} from './brandPage'
import {
    fetchCampaignsSummaryPageDataRequest,
    updateCampaignsSummaryPageTablePagination,
    updateCampaignsSummaryPageTreemapPagination,
} from './campaignsSummaryPage'
import {
    fetchCampaignPageDataRequest,
    updateCampaignPageKeywordsTablePagination,
    updateCampaignPageProductsTablePagination,
} from './campaignPage'
import {
    fetchProductsSummaryPageDataRequest,
    updateProductsSummaryPageTablePagination,
    updateProductsSummaryPageTreemapPagination,
} from './productSummaryPage'
import { fetchProductPageDataRequest } from './productPage'
import {
    fetchKeywordsSummaryPageDataRequest,
    updateKeywordsSummaryPageTablePagination,
    updateKeywordsSummaryPageTreemapPagination,
} from './keywordsSummaryPage'
import { fetchKeywordPageDataRequest } from './keywordPage'
import {
    fetchSovKeywordsSummaryPageDataRequest,
    updateSovKeywordsSummaryPageTablePagination,
} from './sovKeywordsSummaryPage'
import {
    fetchSovKeywordPageDataRequest,
    updateSovKeywordPageTablePagination,
} from './sovKeywordPage'
import {
    fetchSovKeywordSearchResultPageDataRequest,
    updateSovKeywordSearchResultPageTablePagination,
} from './sovKeywordSearchResultPage'
import {
    fetchOrganizationPageDataRequest,
    updateOrganizationPageGroupsTablePagination,
    updateOrganizationPageIntegrationsTablePagination,
    updateOrganizationPageInvitationsTablePagination,
    updateOrganizationPageMembersTablePagination,
} from './organizationPage'
import { fetchOrganizationGroupPageDataRequest } from './organizationGroupPage'
import {
    fetchLabelsSummaryPageDataRequest,
    updateLabelsSummaryPageTablePagination,
} from './labelsSummaryPage'
import { fetchAutomationPageDataRequest } from './automationPage'

export * from './app'
export * from './brandPage'
export * from './brandsSummaryPage'
export * from './campaignsSummaryPage'
export * from './campaignPage'
export * from './homePage'
export * from './keywordPage'
export * from './keywordsSummaryPage'
export * from './labelsSummaryPage'
export * from './productPage'
export * from './productSummaryPage'
export * from './sovKeywordsSummaryPage'
export * from './sovKeywordPage'
export * from './sovKeywordSearchResultPage'
export * from './shared'
export * from './profilePage'
export * from './organizationPage'
export * from './organizationGroupPage'
export * from './automationPage'

export const fetchPageDataActions = {
    [HOME_PAGE]: fetchHomePageDataRequest,
    [BRANDS_SUMMARY_PAGE]: fetchBrandsSummaryPageDataRequest,
    [BRAND_PAGE]: fetchBrandPageDataRequest,
    [CAMPAIGNS_SUMMARY_PAGE]: fetchCampaignsSummaryPageDataRequest,
    [CAMPAIGN_PAGE]: fetchCampaignPageDataRequest,
    [PRODUCTS_SUMMARY_PAGE]: fetchProductsSummaryPageDataRequest,
    [PRODUCT_PAGE]: fetchProductPageDataRequest,
    [KEYWORDS_SUMMARY_PAGE]: fetchKeywordsSummaryPageDataRequest,
    [KEYWORD_PAGE]: fetchKeywordPageDataRequest,
    [SOV_KEYWORDS_SUMMARY_PAGE]: fetchSovKeywordsSummaryPageDataRequest,
    [SOV_KEYWORD_PAGE]: fetchSovKeywordPageDataRequest,
    [SOV_KEYWORD_SEARCH_RESULT_PAGE]: fetchSovKeywordSearchResultPageDataRequest,
    [ORGANIZATION_PAGE]: fetchOrganizationPageDataRequest,
    [ORGANIZATION_GROUP_PAGE]: fetchOrganizationGroupPageDataRequest,
    [LABELS_SUMMARY_PAGE]: fetchLabelsSummaryPageDataRequest,
    [AUTOMATION_PAGE]: fetchAutomationPageDataRequest,
}

export const updatePaginationActions = {
    [BRANDS_SUMMARY_PAGE]: [
        updateBrandsSummaryPageTablePagination,
        updateBrandsSummaryPageTreemapPagination,
    ],
    [BRAND_PAGE]: [
        updateBrandPageTablePagination,
        updateBrandPageTreemapPagination,
    ],
    [CAMPAIGNS_SUMMARY_PAGE]: [
        updateCampaignsSummaryPageTablePagination,
        updateCampaignsSummaryPageTreemapPagination,
    ],
    [CAMPAIGN_PAGE]: [
        updateCampaignPageKeywordsTablePagination,
        updateCampaignPageProductsTablePagination,
    ],
    [PRODUCTS_SUMMARY_PAGE]: [
        updateProductsSummaryPageTablePagination,
        updateProductsSummaryPageTreemapPagination,
    ],
    [KEYWORDS_SUMMARY_PAGE]: [
        updateKeywordsSummaryPageTablePagination,
        updateKeywordsSummaryPageTreemapPagination,
    ],
    [SOV_KEYWORDS_SUMMARY_PAGE]: [updateSovKeywordsSummaryPageTablePagination],
    [SOV_KEYWORD_PAGE]: [updateSovKeywordPageTablePagination],
    [SOV_KEYWORD_SEARCH_RESULT_PAGE]: [
        updateSovKeywordSearchResultPageTablePagination,
    ],
    [ORGANIZATION_PAGE]: [
        updateOrganizationPageGroupsTablePagination,
        updateOrganizationPageIntegrationsTablePagination,
        updateOrganizationPageInvitationsTablePagination,
        updateOrganizationPageMembersTablePagination,
    ],
    [LABELS_SUMMARY_PAGE]: [updateLabelsSummaryPageTablePagination],
}
