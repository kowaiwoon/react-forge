import { createAction } from 'redux-actions'

// mounting
export const mountProductPageRequest = createAction(
    'MOUNT_PRODUCT_PAGE_REQUEST'
)
export const mountProductPageSuccess = createAction(
    'MOUNT_PRODUCT_PAGE_SUCCESS'
)
export const mountProductPageFailure = createAction(
    'MOUNT_PRODUCT_PAGE_FAILURE'
)
export const unmountProductPage = createAction('UNMOUNT_PRODUCT_PAGE')

// page data
export const fetchProductPageDataRequest = createAction(
    'FETCH_PRODUCT_PAGE_DATA_REQUEST'
)
export const fetchProductPageDataSuccess = createAction(
    'FETCH_PRODUCT_PAGE_DATA_SUCCESS'
)
export const fetchProductPageDataFailure = createAction(
    'FETCH_PRODUCT_PAGE_DATA_FAILURE'
)

// toggle product details
export const toggleProductPageDetails = createAction(
    'TOGGLE_PRODUCT_PAGE_DETAILS'
)

// update product details
export const updateProductPageProductDetailsRequest = createAction(
    'UPDATE_PRODUCT_PAGE_PRODUCT_DETAILS_REQUEST'
)
export const updateProductPageProductDetailsSuccess = createAction(
    'UPDATE_PRODUCT_PAGE_PRODUCT_DETAILS_SUCCESS'
)
export const updateProductPageProductDetailsFailure = createAction(
    'UPDATE_PRODUCT_PAGE_PRODUCT_DETAILS_FAILURE'
)

// aggregate data
export const fetchProductPageAggregateSuccess = createAction(
    'FETCH_PRODUCT_PAGE_AGGREGATE_SUCCESS'
)

// timeseries data
export const fetchProductPageSponsoredProductTimeseriesSuccess = createAction(
    'FETCH_PRODUCT_PAGE_SPONSORED_PRODUCT_TIMESERIES_SUCCESS'
)
export const fetchProductPageHeadlineSearchTimeseriesSuccess = createAction(
    'FETCH_PRODUCT_PAGE_HEADLINE_SEARCH_TIMESERIES_SUCCESS'
)

// timeseries download
export const downloadProductPageTimeseriesRequest = createAction(
    'DOWNLOAD_PRODUCT_PAGE_TIMESERIES_REQUEST'
)
export const downloadProductPageTimeseriesSuccess = createAction(
    'DOWNLOAD_PRODUCT_PAGE_TIMESERIES_SUCCESS'
)
export const downloadProductPageTimeseriesFailure = createAction(
    'DOWNLOAD_PRODUCT_PAGE_TIMESERIES_FAILURE'
)
