import { createAction } from 'redux-actions'

// mounting
export const mountSovKeywordPageRequest = createAction(
    'MOUNT_SOV_KEYWORD_PAGE_REQUEST'
)
export const mountSovKeywordPageSuccess = createAction(
    'MOUNT_SOV_KEYWORD_PAGE_SUCCESS'
)
export const mountSovKeywordPageFailure = createAction(
    'MOUNT_SOV_KEYWORD_PAGE_FAILURE'
)
export const unmountSovKeywordPage = createAction('UNMOUNT_SOV_KEYWORD_PAGE')

// page data
export const fetchSovKeywordPageDataRequest = createAction(
    'FETCH_SOV_KEYWORD_PAGE_DATA_REQUEST'
)
export const fetchSovKeywordPageDataSuccess = createAction(
    'FETCH_SOV_KEYWORD_PAGE_DATA_SUCCESS'
)
export const fetchSovKeywordPageDataFailure = createAction(
    'FETCH_SOV_KEYWORD_PAGE_DATA_FAILURE'
)

// toggle keyword details
export const toggleSovKeywordPageDetails = createAction(
    'TOGGLE_SOV_KEYWORD_PAGE_DETAILS'
)

// update keyword details
export const updateSovKeywordPageKeywordDetailsRequest = createAction(
    'UPDATE_SOV_KEYWORD_PAGE_KEYWORD_DETAILS_REQUEST'
)
export const updateSovKeywordPageKeywordDetailsSuccess = createAction(
    'UPDATE_SOV_KEYWORD_PAGE_KEYWORD_DETAILS_SUCCESS'
)
export const updateSovKeywordPageKeywordDetailsFailure = createAction(
    'UPDATE_SOV_KEYWORD_PAGE_KEYWORD_DETAILS_FAILURE'
)

// table data
export const fetchSovKeywordPageTableRequest = createAction(
    'FETCH_SOV_KEYWORD_PAGE_TABLE_REQUEST'
)
export const fetchSovKeywordPageTableSuccess = createAction(
    'FETCH_SOV_KEYWORD_PAGE_TABLE_SUCCESS'
)
export const fetchSovKeywordPageTableFailure = createAction(
    'FETCH_SOV_KEYWORD_PAGE_TABLE_FAILURE'
)

// table controls
export const updateSovKeywordPageTablePagination = createAction(
    'UPDATE_SOV_KEYWORD_PAGE_TABLE_PAGINATION'
)
export const updateSovKeywordPageTableSorter = createAction(
    'UPDATE_SOV_KEYWORD_PAGE_TABLE_SORTER'
)

// table settings
export const updateSovKeywordPageTableSettings = createAction(
    'UPDATE_SOV_KEYWORD_PAGE_TABLE_SETTINGS'
)
export const fetchSovKeywordPageTableSettingsSuccess = createAction(
    'FETCH_SOV_KEYWORD_PAGE_TABLE_SETTINGS_SUCCESS'
)

// table download
export const downloadSovKeywordPageTableRequest = createAction(
    'DOWNLOAD_SOV_KEYWORD_PAGE_TABLE_REQUEST'
)
export const downloadSovKeywordPageTableSuccess = createAction(
    'DOWNLOAD_SOV_KEYWORD_PAGE_TABLE_SUCCESS'
)
export const downloadSovKeywordPageTableFailure = createAction(
    'DOWNLOAD_SOV_KEYWORD_PAGE_TABLE_FAILURE'
)

// sov chart
export const fetchSovKeywordPageSovChartSuccess = createAction(
    'FETCH_SOV_KEYWORD_PAGE_SOV_CHART_SUCCESS'
)

export const updateSovKeywordPageSovChartWeight = createAction(
    'UPDATE_SOV_KEYWORD_PAGE_SOV_CHART_WEIGHT'
)
