import { createAction } from 'redux-actions'

// mounting
export const mountCampaignPageRequest = createAction(
    'MOUNT_CAMPAIGN_PAGE_REQUEST'
)
export const mountCampaignPageSuccess = createAction(
    'MOUNT_CAMPAIGN_PAGE_SUCCESS'
)
export const mountCampaignPageFailure = createAction(
    'MOUNT_CAMPAIGN_PAGE_FAILURE'
)
export const unmountCampaignPage = createAction('UNMOUNT_CAMPAIGN_PAGE')

// page data
export const fetchCampaignPageDataRequest = createAction(
    'FETCH_CAMPAIGN_PAGE_DATA_REQUEST'
)
export const fetchCampaignPageDataSuccess = createAction(
    'FETCH_CAMPAIGN_PAGE_DATA_SUCCESS'
)
export const fetchCampaignPageDataFailure = createAction(
    'FETCH_CAMPAIGN_PAGE_DATA_FAILURE'
)

// fetch dayparting
export const fetchCampaignPageHourlyMultipliersRequest = createAction(
    'FETCH_CAMPAIGN_PAGE_HOURLY_MULTIPLIERS_REQUEST'
)
export const fetchCampaignPageHourlyMultipliersSuccess = createAction(
    'FETCH_CAMPAIGN_PAGE_HOURLY_MULTIPLIERS_SUCCESS'
)
export const fetchCampaignPageHourlyMultipliersFailure = createAction(
    'FETCH_CAMPAIGN_PAGE_HOURLY_MULTIPLIERS_FAILURE'
)

// toggle campaign details
export const toggleCampaignPageDetails = createAction(
    'TOGGLE_CAMPAIGN_PAGE_DETAILS'
)

// update campaign details
export const updateCampaignPageCampaignDetailsRequest = createAction(
    'UPDATE_CAMPAIGN_PAGE_CAMPAIGN_DETAILS_REQUEST'
)
export const updateCampaignPageCampaignDetailsSuccess = createAction(
    'UPDATE_CAMPAIGN_PAGE_CAMPAIGN_DETAILS_SUCCESS'
)
export const updateCampaignPageCampaignDetailsFailure = createAction(
    'UPDATE_CAMPAIGN_PAGE_CAMPAIGN_DETAILS_FAILURE'
)

// update dayparting multipliers
export const updateCampaignPageHourlyMultipliersRequest = createAction(
    'UPDATE_CAMPAIGN_PAGE_HOURLY_MULTIPLIERS_REQUEST'
)
export const updateCampaignPageHourlyMultipliersSuccess = createAction(
    'UPDATE_CAMPAIGN_PAGE_HOURLY_MULTIPLIERS_SUCCESS'
)
export const updateCampaignPageHourlyMultipliersFailure = createAction(
    'UPDATE_CAMPAIGN_PAGE_HOURLY_MULTIPLIERS_FAILURE'
)

// aggregate data
export const fetchCampaignPageAggregateSuccess = createAction(
    'FETCH_CAMPAIGN_PAGE_AGGREGATE_SUCCESS'
)

// timeseries data
export const fetchCampaignPageSponsoredProductTimeseriesSuccess = createAction(
    'FETCH_CAMPAIGN_PAGE_SPONSORED_PRODUCT_TIMESERIES_SUCCESS'
)
export const fetchCampaignPageHeadlineSearchTimeseriesSuccess = createAction(
    'FETCH_CAMPAIGN_PAGE_HEADLINE_SEARCH_TIMESERIES_SUCCESS'
)

// timeseries download
export const downloadCampaignPageTimeseriesRequest = createAction(
    'DOWNLOAD_CAMPAIGN_PAGE_TIMESERIES_REQUEST'
)
export const downloadCampaignPageTimeseriesSuccess = createAction(
    'DOWNLOAD_CAMPAIGN_PAGE_TIMESERIES_SUCCESS'
)
export const downloadCampaignPageTimeseriesFailure = createAction(
    'DOWNLOAD_CAMPAIGN_PAGE_TIMESERIES_FAILURE'
)

// keyword table data
export const fetchCampaignPageKeywordsTableRequest = createAction(
    'FETCH_CAMPAIGN_PAGE_KEYWORDS_TABLE_REQUEST'
)
export const fetchCampaignPageKeywordsTableSuccess = createAction(
    'FETCH_CAMPAIGN_PAGE_KEYWORDS_TABLE_SUCCESS'
)
export const fetchCampaignPageKeywordsTableFailure = createAction(
    'FETCH_CAMPAIGN_PAGE_KEYWORDS_TABLE_FAILURE'
)

// keywords table controls
export const updateCampaignPageKeywordsTablePagination = createAction(
    'UPDATE_CAMPAIGN_PAGE_KEYWORDS_TABLE_PAGINATION'
)
export const updateCampaignPageKeywordsTableSorter = createAction(
    'UPDATE_CAMPAIGN_PAGE_KEYWORDS_TABLE_SORTER'
)

// keyword table settings
export const updateCampaignPageKeywordsTableSettings = createAction(
    'UPDATE_CAMPAIGN_PAGE_KEYWORDS_TABLE_SETTINGS'
)
export const fetchCampaignPageKeywordsTableSettingsSuccess = createAction(
    'FETCH_CAMPAIGN_PAGE_KEYWORDS_TABLE_SETTINGS_SUCCESS'
)

// keywords table download
export const downloadCampaignPageKeywordsTableRequest = createAction(
    'DOWNLOAD_CAMPAIGN_PAGE_KEYWORDS_TABLE_REQUEST'
)
export const downloadCampaignPageKeywordsTableSuccess = createAction(
    'DOWNLOAD_CAMPAIGN_PAGE_KEYWORDS_TABLE_SUCCESS'
)
export const downloadCampaignPageKeywordsTableFailure = createAction(
    'DOWNLOAD_CAMPAIGN_PAGE_KEYWORDS_TABLE_FAILURE'
)

// keywords table attach
export const attachCampaignPageKeywordsTableKeywordsRequest = createAction(
    'ATTACH_CAMPAIGN_PAGE_KEYWORDS_TABLE_KEYWORDS_REQUEST'
)
export const attachCampaignPageKeywordsTableKeywordsSuccess = createAction(
    'ATTACH_CAMPAIGN_PAGE_KEYWORDS_TABLE_KEYWORDS_SUCCESS'
)
export const attachCampaignPageKeywordsTableKeywordsFailure = createAction(
    'ATTACH_CAMPAIGN_PAGE_KEYWORDS_TABLE_KEYWORDS_FAILURE'
)

// keywords table update
export const updateCampaignPageKeywordsTableKeywordRequest = createAction(
    'UPDATE_CAMPAIGN_PAGE_KEYWORDS_TABLE_KEYWORD_REQUEST'
)
export const updateCampaignPageKeywordsTableKeywordSuccess = createAction(
    'UPDATE_CAMPAIGN_PAGE_KEYWORDS_TABLE_KEYWORD_SUCCESS'
)
export const updateCampaignPageKeywordsTableKeywordFailure = createAction(
    'UPDATE_CAMPAIGN_PAGE_KEYWORDS_TABLE_KEYWORD_FAILURE'
)

// keywords table delete
export const deleteCampaignPageKeywordsTableKeywordRequest = createAction(
    'DELETE_CAMPAIGN_PAGE_KEYWORDS_TABLE_KEYWORD_REQUEST'
)
export const deleteCampaignPageKeywordsTableKeywordSuccess = createAction(
    'DELETE_CAMPAIGN_PAGE_KEYWORDS_TABLE_KEYWORD_SUCCESS'
)
export const deleteCampaignPageKeywordsTableKeywordFailure = createAction(
    'DELETE_CAMPAIGN_PAGE_KEYWORDS_TABLE_KEYWORD_FAILURE'
)

// products table data
export const fetchCampaignPageProductsTableRequest = createAction(
    'FETCH_CAMPAIGN_PAGE_PRODUCTS_TABLE_REQUEST'
)
export const fetchCampaignPageProductsTableSuccess = createAction(
    'FETCH_CAMPAIGN_PAGE_PRODUCTS_TABLE_SUCCESS'
)
export const fetchCampaignPageProductsTableFailure = createAction(
    'FETCH_CAMPAIGN_PAGE_PRODUCTS_TABLE_FAILURE'
)

// products table controls
export const updateCampaignPageProductsTablePagination = createAction(
    'UPDATE_CAMPAIGN_PAGE_PRODUCTS_TABLE_PAGINATION'
)
export const updateCampaignPageProductsTableSorter = createAction(
    'UPDATE_CAMPAIGN_PAGE_PRODUCTS_TABLE_SORTER'
)

// products table settings
export const updateCampaignPageProductsTableSettings = createAction(
    'UPDATE_CAMPAIGN_PAGE_PRODUCTS_TABLE_SETTINGS'
)
export const fetchCampaignPageProductsTableSettingsSuccess = createAction(
    'FETCH_CAMPAIGN_PAGE_PRODUCTS_TABLE_SETTINGS_SUCCESS'
)

// products table download
export const downloadCampaignPageProductsTableRequest = createAction(
    'DOWNLOAD_CAMPAIGN_PAGE_PRODUCTS_TABLE_REQUEST'
)
export const downloadCampaignPageProductsTableSuccess = createAction(
    'DOWNLOAD_CAMPAIGN_PAGE_PRODUCTS_TABLE_SUCCESS'
)
export const downloadCampaignPageProductsTableFailure = createAction(
    'DOWNLOAD_CAMPAIGN_PAGE_PRODUCTS_TABLE_FAILURE'
)

// products table attach
export const attachCampaignPageProductsTableProductsRequest = createAction(
    'ATTACH_CAMPAIGN_PAGE_PRODUCTS_TABLE_PRODUCTS_REQUEST'
)
export const attachCampaignPageProductsTableProductsSuccess = createAction(
    'ATTACH_CAMPAIGN_PAGE_PRODUCTS_TABLE_PRODUCTS_SUCCESS'
)
export const attachCampaignPageProductsTableProductsFailure = createAction(
    'ATTACH_CAMPAIGN_PAGE_PRODUCTS_TABLE_PRODUCTS_FAILURE'
)

// products table update
export const updateCampaignPageProductsTableProductRequest = createAction(
    'UPDATE_CAMPAIGN_PAGE_PRODUCTS_TABLE_PRODUCT_REQUEST'
)
export const updateCampaignPageProductsTableProductSuccess = createAction(
    'UPDATE_CAMPAIGN_PAGE_PRODUCTS_TABLE_PRODUCT_SUCCESS'
)
export const updateCampaignPageProductsTableProductFailure = createAction(
    'UPDATE_CAMPAIGN_PAGE_PRODUCTS_TABLE_PRODUCT_FAILURE'
)

// products table delete
export const deleteCampaignPageProductsTableProductRequest = createAction(
    'DELETE_CAMPAIGN_PAGE_PRODUCTS_TABLE_PRODUCT_REQUEST'
)
export const deleteCampaignPageProductsTableProductSuccess = createAction(
    'DELETE_CAMPAIGN_PAGE_PRODUCTS_TABLE_PRODUCT_SUCCESS'
)
export const deleteCampaignPageProductsTableProductFailure = createAction(
    'DELETE_CAMPAIGN_PAGE_PRODUCTS_TABLE_PRODUCT_FAILURE'
)

// sync campaign
export const syncCampaignRequest = createAction('SYNC_CAMPAIGN_REQUEST')
export const syncCampaignSuccess = createAction('SYNC_CAMPAIGN_SUCCESS')
export const syncCampaignFailure = createAction('SYNC_CAMPAIGN_FAILURE')

// feature permissions
export const fetchCampaignPageFeaturePermissionsSuccess = createAction(
    'FETCH_CAMPAIGN_PAGE_FEATURE_PERMISSIONS_SUCCESS'
)
