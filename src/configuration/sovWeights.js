import {
    NON_WEIGHTED,
    FOLD_AND_AREA_WEIGHTED,
    POSITION_WEIGHTED,
} from 'constants/sovWeights'

export const SOV_WEIGHT_LABELS = {
    [NON_WEIGHTED]: 'Non Weighted',
    [FOLD_AND_AREA_WEIGHTED]: 'Fold and Area Weighted',
    [POSITION_WEIGHTED]: 'Position Weighted',
}

export const ALL_SOV_WEIGHTS = [
    NON_WEIGHTED,
    FOLD_AND_AREA_WEIGHTED,
    POSITION_WEIGHTED,
]
