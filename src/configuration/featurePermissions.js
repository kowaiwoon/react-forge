import {
    DAYPARTING,
    SOV_WRITE,
    SOV_READ,
    ASSUME_ANY_ORGANIZATION_GROUP,
    ORGANIZATION_ADMIN,
    AUTOMATION,
} from 'constants/featurePermissions'

export const FEATURE_METADATA = {
    [DAYPARTING]: {
        title: 'Hourly Keyword Multipliers',
        description:
            'Ability to apply different keyword bids during each hour of the day (also referred to as "day-parting").',
        condition: () => true,
    },
    [SOV_READ]: {
        title: 'Share of Voice (Read Access)',
        description:
            'Ability to track and analyze all search results for particular keywords.',
        condition: () => true,
    },
    [SOV_WRITE]: {
        title: 'Share of Voice (Write Access)',
        description:
            'Ability to track and analyze all search results for particular keywords.',
        condition: () => true,
    },
    [ASSUME_ANY_ORGANIZATION_GROUP]: {
        title: 'Downstream Customer Service',
        description:
            'Ability to assume organization groups of Downstream users.',
        condition: userPermissions =>
            userPermissions.includes(ASSUME_ANY_ORGANIZATION_GROUP),
    },
    [ORGANIZATION_ADMIN]: {
        title: 'Organization Admin',
        description:
            'Ability to manage an organization and its groups, members, and integrations.',
        condition: () => true,
    },
    [AUTOMATION]: {
        title: 'Automation',
        description:
            'Ability to view organization goals and Downstream automation activities.',
        condition: () => true,
    },
}
