import { formatNumber } from 'helpers/formatting'
import { UNDEFINED_VALUE } from 'constants/formatting'
import { metrics } from 'configuration/metrics'

export const METRIC_COLUMNS = {
    clicks__sum: {
        title: metrics.clicks__sum.short_name,
        dataIndex: 'clicks__sum',
        sorter: true,
        align: 'right',
        render: value =>
            value === undefined || value === null
                ? UNDEFINED_VALUE
                : formatNumber(value, metrics.clicks__sum.format),
    },
    cost__sum: {
        title: metrics.cost__sum.short_name,
        dataIndex: 'cost__sum',
        sorter: true,
        align: 'right',
        render: value =>
            value === undefined || value === null
                ? UNDEFINED_VALUE
                : formatNumber(value, metrics.cost__sum.format),
    },
    impressions__sum: {
        title: metrics.impressions__sum.short_name,
        dataIndex: 'impressions__sum',
        sorter: true,
        align: 'right',
        render: value =>
            value === undefined || value === null
                ? UNDEFINED_VALUE
                : formatNumber(value, metrics.impressions__sum.format),
    },
    attributed_conversions_14_day__sum: {
        title: metrics.attributed_conversions_14_day__sum.short_name,
        dataIndex: 'attributed_conversions_14_day__sum',
        sorter: true,
        align: 'right',
        render: value =>
            value === undefined || value === null
                ? UNDEFINED_VALUE
                : formatNumber(
                      value,
                      metrics.attributed_conversions_14_day__sum.format
                  ),
    },
    attributed_conversions_14_day_same_sku__sum: {
        title: metrics.attributed_conversions_14_day_same_sku__sum.short_name,
        dataIndex: 'attributed_conversions_14_day_same_sku__sum',
        sorter: true,
        align: 'right',
        render: value =>
            value === undefined || value === null
                ? UNDEFINED_VALUE
                : formatNumber(
                      value,
                      metrics.attributed_conversions_14_day_same_sku__sum.format
                  ),
    },
    attributed_sales_14_day__sum: {
        title: metrics.attributed_sales_14_day__sum.short_name,
        dataIndex: 'attributed_sales_14_day__sum',
        sorter: true,
        align: 'right',
        render: value =>
            value === undefined || value === null
                ? UNDEFINED_VALUE
                : formatNumber(
                      value,
                      metrics.attributed_sales_14_day__sum.format
                  ),
    },
    attributed_sales_14_day_same_sku__sum: {
        title: metrics.attributed_sales_14_day_same_sku__sum.short_name,
        dataIndex: 'attributed_sales_14_day_same_sku__sum',
        sorter: true,
        align: 'right',
        render: value =>
            value === undefined || value === null
                ? UNDEFINED_VALUE
                : formatNumber(
                      value,
                      metrics.attributed_sales_14_day_same_sku__sum.format
                  ),
    },
    roas: {
        title: metrics.roas.short_name,
        dataIndex: 'roas',
        sorter: true,
        align: 'right',
        render: value =>
            value === undefined || value === null
                ? UNDEFINED_VALUE
                : formatNumber(value, metrics.roas.format),
    },
    acos: {
        title: metrics.acos.short_name,
        dataIndex: 'acos',
        sorter: true,
        align: 'right',
        render: value =>
            value === undefined || value === null
                ? UNDEFINED_VALUE
                : formatNumber(value, metrics.acos.format),
    },
    ctr: {
        title: metrics.ctr.short_name,
        dataIndex: 'ctr',
        sorter: true,
        align: 'right',
        render: value =>
            value === undefined || value === null
                ? UNDEFINED_VALUE
                : formatNumber(value, metrics.ctr.format),
    },
    spc: {
        title: metrics.spc.short_name,
        dataIndex: 'spc',
        sorter: true,
        align: 'right',
        render: value =>
            value === undefined || value === null
                ? UNDEFINED_VALUE
                : formatNumber(value, metrics.spc.format),
    },
    cpc: {
        title: metrics.cpc.short_name,
        dataIndex: 'cpc',
        sorter: true,
        align: 'right',
        render: value =>
            value === undefined || value === null
                ? UNDEFINED_VALUE
                : formatNumber(value, metrics.cpc.format),
    },
    cpa: {
        title: metrics.cpa.short_name,
        dataIndex: 'cpa',
        sorter: true,
        align: 'right',
        render: value =>
            value === undefined || value === null
                ? UNDEFINED_VALUE
                : formatNumber(value, metrics.cpa.format),
    },
    cpm: {
        title: metrics.cpm.short_name,
        dataIndex: 'cpm',
        sorter: true,
        align: 'right',
        render: value =>
            value === undefined || value === null
                ? UNDEFINED_VALUE
                : formatNumber(value, metrics.cpm.format),
    },
    spm: {
        title: metrics.spm.short_name,
        dataIndex: 'spm',
        sorter: true,
        align: 'right',
        render: value =>
            value === undefined || value === null
                ? UNDEFINED_VALUE
                : formatNumber(value, metrics.spm.format),
    },
    aov: {
        title: metrics.aov.short_name,
        dataIndex: 'aov',
        sorter: true,
        align: 'right',
        render: value =>
            value === undefined || value === null
                ? UNDEFINED_VALUE
                : formatNumber(value, metrics.aov.format),
    },
    conversion_rate_clicks: {
        title: metrics.conversion_rate_clicks.short_name,
        dataIndex: 'conversion_rate_clicks',
        sorter: true,
        align: 'right',
        render: value =>
            value === undefined || value === null
                ? UNDEFINED_VALUE
                : formatNumber(value, metrics.conversion_rate_clicks.format),
    },
    conversion_rate_impressions: {
        title: metrics.conversion_rate_impressions.short_name,
        dataIndex: 'conversion_rate_impressions',
        sorter: true,
        align: 'right',
        render: value =>
            value === undefined || value === null
                ? UNDEFINED_VALUE
                : formatNumber(
                      value,
                      metrics.conversion_rate_impressions.format
                  ),
    },
    attributed_conversions_1_day__sum: {
        title: metrics.attributed_conversions_1_day__sum.short_name,
        dataIndex: 'attributed_conversions_1_day__sum',
        sorter: true,
        align: 'right',
        render: value =>
            value === undefined || value === null
                ? UNDEFINED_VALUE
                : formatNumber(
                      value,
                      metrics.attributed_conversions_1_day__sum.format
                  ),
    },
    attributed_conversions_1_day_same_sku__sum: {
        title: metrics.attributed_conversions_1_day_same_sku__sum.short_name,
        dataIndex: 'attributed_conversions_1_day_same_sku__sum',
        sorter: true,
        align: 'right',
        render: value =>
            value === undefined || value === null
                ? UNDEFINED_VALUE
                : formatNumber(
                      value,
                      metrics.attributed_conversions_1_day_same_sku__sum.format
                  ),
    },
    attributed_conversions_7_day__sum: {
        title: metrics.attributed_conversions_7_day__sum.short_name,
        dataIndex: 'attributed_conversions_7_day__sum',
        sorter: true,
        align: 'right',
        render: value =>
            value === undefined || value === null
                ? UNDEFINED_VALUE
                : formatNumber(
                      value,
                      metrics.attributed_conversions_7_day__sum.format
                  ),
    },
    attributed_conversions_7_day_same_sku__sum: {
        title: metrics.attributed_conversions_7_day_same_sku__sum.short_name,
        dataIndex: 'attributed_conversions_7_day_same_sku__sum',
        sorter: true,
        align: 'right',
        render: value =>
            value === undefined || value === null
                ? UNDEFINED_VALUE
                : formatNumber(
                      value,
                      metrics.attributed_conversions_7_day_same_sku__sum.format
                  ),
    },
    attributed_conversions_30_day__sum: {
        title: metrics.attributed_conversions_30_day__sum.short_name,
        dataIndex: 'attributed_conversions_30_day__sum',
        sorter: true,
        align: 'right',
        render: value =>
            value === undefined || value === null
                ? UNDEFINED_VALUE
                : formatNumber(
                      value,
                      metrics.attributed_conversions_30_day__sum.format
                  ),
    },
    attributed_conversions_30_day_same_sku__sum: {
        title: metrics.attributed_conversions_30_day_same_sku__sum.short_name,
        dataIndex: 'attributed_conversions_30_day_same_sku__sum',
        sorter: true,
        align: 'right',
        render: value =>
            value === undefined || value === null
                ? UNDEFINED_VALUE
                : formatNumber(
                      value,
                      metrics.attributed_conversions_30_day_same_sku__sum.format
                  ),
    },
    attributed_sales_1_day__sum: {
        title: metrics.attributed_sales_1_day__sum.short_name,
        dataIndex: 'attributed_sales_1_day__sum',
        sorter: true,
        align: 'right',
        render: value =>
            value === undefined || value === null
                ? UNDEFINED_VALUE
                : formatNumber(
                      value,
                      metrics.attributed_sales_1_day__sum.format
                  ),
    },
    attributed_sales_1_day_same_sku__sum: {
        title: metrics.attributed_sales_1_day_same_sku__sum.short_name,
        dataIndex: 'attributed_sales_1_day_same_sku__sum',
        sorter: true,
        align: 'right',
        render: value =>
            value === undefined || value === null
                ? UNDEFINED_VALUE
                : formatNumber(
                      value,
                      metrics.attributed_sales_1_day_same_sku__sum.format
                  ),
    },
    attributed_sales_7_day__sum: {
        title: metrics.attributed_sales_7_day__sum.short_name,
        dataIndex: 'attributed_sales_7_day__sum',
        sorter: true,
        align: 'right',
        render: value =>
            value === undefined || value === null
                ? UNDEFINED_VALUE
                : formatNumber(
                      value,
                      metrics.attributed_sales_7_day__sum.format
                  ),
    },
    attributed_sales_7_day_same_sku__sum: {
        title: metrics.attributed_sales_7_day_same_sku__sum.short_name,
        dataIndex: 'attributed_sales_7_day_same_sku__sum',
        sorter: true,
        align: 'right',
        render: value =>
            value === undefined || value === null
                ? UNDEFINED_VALUE
                : formatNumber(
                      value,
                      metrics.attributed_sales_7_day_same_sku__sum.format
                  ),
    },
    attributed_sales_30_day__sum: {
        title: metrics.attributed_sales_30_day__sum.short_name,
        dataIndex: 'attributed_sales_30_day__sum',
        sorter: true,
        align: 'right',
        render: value =>
            value === undefined || value === null
                ? UNDEFINED_VALUE
                : formatNumber(
                      value,
                      metrics.attributed_sales_30_day__sum.format
                  ),
    },
    attributed_sales_30_day_same_sku__sum: {
        title: metrics.attributed_sales_30_day_same_sku__sum.short_name,
        dataIndex: 'attributed_sales_30_day_same_sku__sum',
        sorter: true,
        align: 'right',
        render: value =>
            value === undefined || value === null
                ? UNDEFINED_VALUE
                : formatNumber(
                      value,
                      metrics.attributed_sales_30_day_same_sku__sum.format
                  ),
    },
    attributed_units_ordered_1_day__sum: {
        title: metrics.attributed_units_ordered_1_day__sum.short_name,
        dataIndex: 'attributed_units_ordered_1_day__sum',
        sorter: true,
        align: 'right',
        render: value =>
            value === undefined || value === null
                ? UNDEFINED_VALUE
                : formatNumber(
                      value,
                      metrics.attributed_units_ordered_1_day__sum.format
                  ),
    },
    attributed_units_ordered_7_day__sum: {
        title: metrics.attributed_units_ordered_7_day__sum.short_name,
        dataIndex: 'attributed_units_ordered_7_day__sum',
        sorter: true,
        align: 'right',
        render: value =>
            value === undefined || value === null
                ? UNDEFINED_VALUE
                : formatNumber(
                      value,
                      metrics.attributed_units_ordered_7_day__sum.format
                  ),
    },
    attributed_units_ordered_14_day__sum: {
        title: metrics.attributed_units_ordered_14_day__sum.short_name,
        dataIndex: 'attributed_units_ordered_14_day__sum',
        sorter: true,
        align: 'right',
        render: value =>
            value === undefined || value === null
                ? UNDEFINED_VALUE
                : formatNumber(
                      value,
                      metrics.attributed_units_ordered_14_day__sum.format
                  ),
    },
    attributed_units_ordered_30_day__sum: {
        title: metrics.attributed_units_ordered_30_day__sum.short_name,
        dataIndex: 'attributed_units_ordered_30_day__sum',
        sorter: true,
        align: 'right',
        render: value =>
            value === undefined || value === null
                ? UNDEFINED_VALUE
                : formatNumber(
                      value,
                      metrics.attributed_units_ordered_30_day__sum.format
                  ),
    },
}

export const METRIC_COLUMNS_ORDER = [
    'impressions__sum',
    'clicks__sum',
    'ctr',
    'cost__sum',
    'attributed_sales_14_day__sum',
    'attributed_conversions_14_day__sum',
    'attributed_conversions_14_day_same_sku__sum',
    'attributed_sales_14_day_same_sku__sum',
    'roas',
    'acos',
    'spc',
    'cpc',
    'cpa',
    'cpm',
    'spm',
    'aov',
    'conversion_rate_clicks',
    'conversion_rate_impressions',
    'attributed_conversions_1_day__sum',
    'attributed_conversions_1_day_same_sku__sum',
    'attributed_conversions_7_day__sum',
    'attributed_conversions_7_day_same_sku__sum',
    'attributed_conversions_30_day__sum',
    'attributed_conversions_30_day_same_sku__sum',
    'attributed_sales_1_day__sum',
    'attributed_sales_1_day_same_sku__sum',
    'attributed_sales_7_day__sum',
    'attributed_sales_7_day_same_sku__sum',
    'attributed_sales_30_day__sum',
    'attributed_sales_30_day_same_sku__sum',
    'attributed_units_ordered_1_day__sum',
    'attributed_units_ordered_7_day__sum',
    'attributed_units_ordered_14_day__sum',
    'attributed_units_ordered_30_day__sum',
]

export const METRIC_COLUMNS_STATE = {
    impressions__sum: true,
    clicks__sum: true,
    ctr: true,
    cost__sum: true,
    attributed_sales_14_day__sum: true,
    attributed_conversions_14_day__sum: false,
    attributed_conversions_14_day_same_sku__sum: false,
    attributed_sales_14_day_same_sku__sum: false,
    roas: false,
    acos: false,
    spc: false,
    cpc: false,
    cpa: false,
    cpm: false,
    spm: false,
    aov: false,
    conversion_rate_clicks: false,
    conversion_rate_impressions: false,
    attributed_conversions_1_day__sum: false,
    attributed_conversions_1_day_same_sku__sum: false,
    attributed_conversions_7_day__sum: false,
    attributed_conversions_7_day_same_sku__sum: false,
    attributed_conversions_30_day__sum: false,
    attributed_conversions_30_day_same_sku__sum: false,
    attributed_sales_1_day__sum: false,
    attributed_sales_1_day_same_sku__sum: false,
    attributed_sales_7_day__sum: false,
    attributed_sales_7_day_same_sku__sum: false,
    attributed_sales_30_day__sum: false,
    attributed_sales_30_day_same_sku__sum: false,
    attributed_units_ordered_1_day__sum: false,
    attributed_units_ordered_7_day__sum: false,
    attributed_units_ordered_14_day__sum: false,
    attributed_units_ordered_30_day__sum: false,
}
