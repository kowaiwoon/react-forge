export const brandAttributeTooltips = {
    country_code: [
        {
            description:
                'The geographic code that represents a specific country or area.',
        },
    ],
    currency_code: [
        {
            description:
                'A three-letter country specific code that represents a unit of currency assigned by the International Organization for Standardization (ISO) standards.',
        },
    ],
    region: [
        {
            description:
                'A two-letter code that represents the geographic region of the world.',
        },
    ],
    timezone: [
        {
            description:
                'The uniform standard for time of a geographic region.',
        },
    ],
}

export const campaignAttributeTooltips = {
    campaign_type: [
        {
            description:
                'What ad type a campaign falls underneath (Sponsored Products or Headline Search).',
        },
    ],
    state: [
        {
            title: 'Paused',
            description:
                'Campaign is not currently running but may be re-enabled.',
        },
        {
            title: 'Enabled',
            description: 'Campaign is active and being served to customer.',
        },
        {
            title: 'Terminated',
            description:
                'Campaign has ran for the designated amount of time and has concluded or has been ended by a user. Reporting on historic performance is available but this campaign cannot be restarted again.',
        },
        {
            title: 'Daily Budge Spent',
            description:
                'The daily dollar amount allocated to a particular campaign has been spent before the day is over.',
        },
    ],
    start_date: [
        {
            description: 'The first date included in a custom date range.',
        },
    ],
    end_date: [
        {
            description: 'The last date included in a custom date range.',
        },
    ],
    budget_type: [
        {
            title: 'Daily',
            description:
                'The budget amount represents the average daily budget for each day the campaign is active.',
        },
        {
            title: 'Lifetime',
            description:
                'The budget amount represents the the budget for the entire lifetime of the campaign. "Lifetime" budgets are only applicable for "Headline Search" campaigns.',
        },
    ],
    budget: [
        {
            description: 'Average desired spend for a particular campaign.',
        },
    ],
    premium_bid_adjustment: [
        {
            description:
                'Bid+ will raise your bids by up to 50% for clicks at the top of search results. This metric will provide insight into how much additional spend is required to serve your ad at the top of Amazon’s search engine results page.',
        },
    ],
    targeting_type: [
        {
            title: 'Auto Targeting',
            description:
                'This type of targeting allows Amazon’s algorithms to determine relevant keywords for a specified campaign based on your product information.',
        },
        {
            title: 'Manual Targeting',
            description:
                'An advertiser will determine what keywords a campaign is allowed to serve to customers.',
        },
    ],
    dayparting_enabled: [
        {
            description:
                'Enabling Hourly Keyword Bid Multipliers (also know as "Day Parting") allows you to adjust your bids dynamically throughout the day.',
        },
    ],
}

export const productAttributeTooltips = {
    campaign_type: [
        {
            description:
                'What ad type a campaign falls underneath (Sponsored Products or Headline Search).',
        },
    ],
    asin: [
        {
            description:
                'Amazon Standard Item Number (ASIN) is a 10-character unique identifier created by Amazon to reference catalog data, track inventory, and index catalog pages for search and browse.',
        },
    ],
    sku: [
        {
            description:
                'A stock keeping unit (SKU) is the merchants unique product item number or the Amazon Standard Item Number (ASIN).',
        },
    ],
    state: [
        {
            description:
                'Refers to the current status of advertised products (i.e., enabled, paused, archived).',
        },
        {
            title: 'Enabled',
            description:
                'The state of an advertised product that is actively surfaced in ads displayed to shoppers.',
        },
        {
            title: 'Paused',
            description:
                'The state of an advertised product that is not currently being surfaced in ads displayed to shoppers. Note that when your product is out of stock or a 3rd party seller is winning the buy box your ad will be systematically paused.',
        },
        {
            title: 'Archived',
            description:
                'The state of an advertised product in a campaign that has concluded or has been ended by users. Archived ASINs cannot be made active again.',
        },
    ],
    price: [
        {
            description:
                'Amazon\'s "List Price" or the suggested retail price of a product as provided by a manufacturer, supplier, or seller. This does not reflect the most recent price displayed on the item\'s product detail page.',
        },
    ],
    brand: [
        {
            description:
                'The registered brand owner of the selling ASIN listed.',
        },
    ],
}

export const keywordAttributeTooltips = {
    campaign_type: [
        {
            description:
                'What ad type a campaign falls underneath (Sponsored Products or Headline Search).',
        },
    ],
    text: [
        {
            description:
                'A single word or phrase used to target and display your ads to potential customers.',
        },
    ],
    match_type: [
        {
            description:
                'Match types control how relevant shopper search queries are and determine when your ads are displayed.',
        },
        {
            title: 'Broad Match',
            description:
                'Match type that provides the widest traffic exposure. Your ad may appear when a shopper searches for all your keyword terms in any order, including close variations (plural and singular variations allowed).',
        },
        {
            title: 'Exact Match',
            description:
                'The most restrictive match type that generates the most relevant traffic. Your ad may appear only when a shopper’s search term match your keyword exactly (plural and singular variations allowed).',
        },
        {
            title: 'Negative Phrase and Exact Match',
            description:
                'Match type that prevents your ads from showing when a customer searches for certain terms (word or phrase).',
        },
    ],
    bid: [
        {
            title: 'Bid',
            description:
                'The maximum amount you are willing to spend for a search keyword click.',
        },
    ],
    state: [
        {
            title: 'Enabled',
            description:
                'The state of a keyword actively being targeted to display ads.',
        },
        {
            title: 'Paused',
            description:
                'The state of a keyword that is not currently targeted to display ads. Note that when your product is out of stock or a 3rd party seller is winning the buy box your ad will be systematically paused.',
        },
        {
            title: 'Archived',
            description:
                'The state of a keyword in a campaign that has concluded or has been ended users. Archived keywords cannot be made active again.',
        },
    ],
}

export const sovKeywordAttributeTooltips = {
    text: [
        {
            description:
                'A single word or phrase used to target and display your ads to potential customers.',
        },
    ],
    frequency_in_hours: [
        {
            title: 'Frequency',
            description: 'The number of search (in hours).',
        },
    ],
    state: [
        {
            title: 'Enabled',
            description:
                'The state of a keyword actively being targeted to display ads.',
        },
        {
            title: 'Paused',
            description:
                'The state of a keyword that is not currently targeted to display ads. Note that when your product is out of stock or a 3rd party seller is winning the buy box your ad will be systematically paused.',
        },
        {
            title: 'Archived',
            description:
                'The state of a keyword in a campaign that has concluded or has been ended users. Archived keywords cannot be made active again.',
        },
    ],
}
