import {
    aovWeightedAvg,
    ctrWeightedAvg,
    roasWeightedAvg,
    acosWeightedAvg,
    spcWeightedAvg,
    cpcWeightedAvg,
    cpaWeightedAvg,
    cpmWeightedAvg,
    spmWeightedAvg,
    conversionRateClicksWeightedAvg,
    conversionRateImpressionsWeightedAvg,
} from 'helpers/metrics'

export const categories = {
    roi: [
        'cost__sum',
        'attributed_sales_14_day__sum',
        'roas',
        'spc',
        'cpc',
        'spm',
    ],
    reach: ['impressions__sum', 'clicks__sum', 'ctr'],
    conversions: [
        'impressions__sum',
        'clicks__sum',
        'attributed_conversions_14_day__sum',
        'conversion_rate_clicks',
        'conversion_rate_impressions',
    ],
}

export const headlineSearchMetrics = [
    'clicks__sum',
    'cost__sum',
    'impressions__sum',
    'attributed_conversions_14_day__sum',
    'attributed_conversions_14_day_same_sku__sum',
    'attributed_sales_14_day__sum',
    'attributed_sales_14_day_same_sku__sum',
    'ctr',
    'roas',
    'acos',
    'spc',
    'cpc',
    'cpa',
    'cpm',
    'spm',
    'aov',
    'conversion_rate_clicks',
    'conversion_rate_impressions',
]

export const metrics = {
    ctr: {
        name: 'Click Through Rate',
        short_name: 'CTR',
        format: '0.00%',
        short_format: '0.00%',
        summary_calc: ctrWeightedAvg,
        helptext:
            'The percentage of shoppers who click on your ad when shown. CTR is calculated by dividing the total number of clicks by total number of impressions.',
    },
    roas: {
        name: 'Return on Ad Spend',
        short_name: 'RoAS',
        format: '$0,0.00',
        short_format: '$0.00a',
        summary_calc: roasWeightedAvg,
        helptext:
            'The average revenue earned for each unit spent on advertising. RoAS is calculated by dividing total attributed sales by total cost of the ad. Note: Amazon uses a default sales attribution based on a 14-day last click window. Revenue is based on sales price to end customer.',
    },
    acos: {
        name: 'Advertising Cost of Sale',
        short_name: 'ACoS',
        format: '0.00%',
        short_format: '0.00%',
        summary_calc: acosWeightedAvg,
        helptext:
            'Advertising cost as a percent of total sales. ACoS is calculated by dividing advertising cost by attributed sales. Note: Amazon uses a default sales attribution based on a 14-day last click window. Revenue is based on sales price to end customer.',
    },
    spc: {
        name: 'Sales per Click',
        short_name: 'SPC',
        format: '$0,0.00',
        short_format: '$0.00a',
        summary_calc: spcWeightedAvg,
        helptext:
            'The average revenue generated for each click on your ad. SPC is calculated by dividing total attributed sales by total number of clicks. Note: Amazon uses a default sales attribution based on a 14-day last click window. Revenue is based on sales price to end customer.',
    },
    cpc: {
        name: 'Cost per Click',
        short_name: 'CPC',
        format: '$0,0.00',
        short_format: '$0.00a',
        summary_calc: cpcWeightedAvg,
        helptext:
            'The average amount you pay for each click on your ad. CPC is calculated by dividing the total cost of the ad by total number of clicks.',
    },
    cpa: {
        name: 'Cost per Acquisition',
        short_name: 'CPA',
        format: '$0,0.00',
        short_format: '$0.00a',
        summary_calc: cpaWeightedAvg,
        helptext:
            'The average amount you pay to acquire a conversion. CPA is calculated by dividing the total cost of the ad by total number of conversions.',
    },
    cpm: {
        name: 'Cost per Thousand',
        short_name: 'CPM',
        format: '$0,0.00',
        short_format: '$0.00a',
        summary_calc: cpmWeightedAvg,
        helptext:
            'The average amount you pay for a thousand ad impressions. CPM is calculated by dividing total cost of the ad by total number of impressions multiplied by 1,000.',
    },
    spm: {
        name: 'Sales per Thousand',
        short_name: 'SPM',
        format: '$0,0.00',
        short_format: '$0.00a',
        summary_calc: spmWeightedAvg,
        helptext:
            'The average revenue generated for a thousand ad impressions. SPM is calculated by dividing total attributed sales by total number of impressions multiplied by 1,000. Note: Amazon uses a default sales attribution based on a 14-day last click window.',
    },
    conversion_rate_clicks: {
        name: 'Conversion Rate (Clicks)',
        short_name: 'CVR (Clicks)',
        format: '0.00%',
        short_format: '0.00%',
        summary_calc: conversionRateClicksWeightedAvg,
        helptext:
            'The percentage of shoppers who click on your ad and complete a purchase. CVR is calculated by dividing the total number of conversions by total number of clicks.',
    },
    conversion_rate_impressions: {
        name: 'Conversion Rate (Impressions)',
        short_name: 'CVR (Impressions)',
        format: '0.00%',
        short_format: '0.00%',
        summary_calc: conversionRateImpressionsWeightedAvg,
        helptext:
            'The percentage of shoppers who view your ad and complete a purchase. CVR is calculated by dividing the total number of conversions by total number of impressions.',
    },
    aov: {
        name: 'Average Order Value',
        short_name: 'AOV',
        format: '$0,0.00',
        short_format: '$0.00a',
        summary_calc: aovWeightedAvg,
        helptext:
            'The average sales value of customer orders. AOV is calculated by dividing total attributed sales by total attributed conversions.',
    },
    clicks__sum: {
        name: 'Clicks',
        short_name: 'Clicks',
        format: '0,0',
        short_format: '0.00a',
        helptext:
            'A click occurs when a shopper clicks on your ad and measures the total number of times shoppers clicked on an ad.',
    },
    cost__sum: {
        name: 'Cost',
        short_name: 'Cost',
        format: '$0,0.00',
        short_format: '$0.0a',
        helptext:
            'The total cost value of clicks generated by a campaign or keyword.',
    },
    impressions__sum: {
        name: 'Impressions',
        short_name: 'Impressions',
        format: '0,0',
        short_format: '0.00a',
        helptext:
            'An impression occurs whenever your ad is displayed and measures the total number of times your ad is shown. Note: Impressions are counted when ads load on a page, even if below the fold and not initially visible by a customer.',
    },
    attributed_conversions_1_day__sum: {
        name: 'Attributed Conversions (1 Day)',
        short_name: 'Conversions (1 Day)',
        format: '0,0',
        short_format: '0.00a',
        helptext:
            'The number of times a shopper purchases any of your brand’s products within 1 day of clicking on your ad. Note: If a shopper purchases multiple units, this will still count as one conversion.',
    },
    attributed_conversions_1_day_same_sku__sum: {
        name: 'Attributed Conversions (1 Day Same SKU)',
        short_name: 'Conversions (1 Day Same SKU)',
        format: '0,0',
        short_format: '0.00a',
        helptext:
            'The number of times a shopper purchases the advertised product within 1 day of clicking on your ad. Note: If a shopper purchases multiple units, this will still count as one conversion.',
    },
    attributed_conversions_7_day__sum: {
        name: 'Attributed Conversions (7 Day)',
        short_name: 'Conversions (7 Day)',
        format: '0,0',
        short_format: '0.00a',
        helptext:
            'The number of times a shopper purchases any of your brand’s products within 7 days of clicking on your ad. Note: If a shopper purchases multiple units, this will still count as one conversion.',
    },
    attributed_conversions_7_day_same_sku__sum: {
        name: 'Attributed Conversions (7 Day Same SKU)',
        short_name: 'Conversions (7 Day Same SKU)',
        format: '0,0',
        short_format: '0.00a',
        helptext:
            'The number of times a shopper purchases the advertised product within 7 days of clicking on your ad. Note: If a shopper purchases multiple units, this will still count as one conversion.',
    },
    attributed_conversions_14_day__sum: {
        name: 'Attributed Conversions (14 Day)',
        short_name: 'Conversions (14 Day)',
        format: '0,0',
        short_format: '0.00a',
        helptext:
            'The number of times a shopper purchases any of your brand’s products within 14 days of clicking on your ad. Note: If a shopper purchases multiple units, this will still count as one conversion.',
    },
    attributed_conversions_14_day_same_sku__sum: {
        name: 'Attributed Conversions (14 Day Same SKU)',
        short_name: 'Conversions (14 Day Same SKU)',
        format: '0,0',
        short_format: '0.00a',
        helptext:
            'The number of times a shopper purchases the advertised product within 14 days of clicking on your ad. Note: If a shopper purchases multiple units, this will still count as one conversion.',
    },
    attributed_conversions_30_day__sum: {
        name: 'Attributed Conversions (30 Day)',
        short_name: 'Conversions (30 Day)',
        format: '0,0',
        short_format: '0.00a',
        helptext:
            'The number of times a shopper purchases any of your brand’s products within 30 days of clicking on your ad. Note: If a shopper purchases multiple units, this will still count as one conversion.',
    },
    attributed_conversions_30_day_same_sku__sum: {
        name: 'Attributed Conversions (30 Day Same SKU)',
        short_name: 'Conversions (30 Day Same SKU)',
        format: '0,0',
        short_format: '0.00a',
        helptext:
            'The number of times a shopper purchases the advertised product within 30 days of clicking on your ad. Note: If a shopper purchases multiple units, this will still count as one conversion.',
    },
    attributed_sales_1_day__sum: {
        name: 'Attributed Sales (1 Day)',
        short_name: 'Sales (1 Day)',
        format: '$0,0.00',
        short_format: '$0,0a',
        helptext:
            'The aggregate value of sales generated whenever a shopper purchases any of your brand’s products within 1 day of clicking on your ad. Note: Sales are attributed to the last ad clicked to avoid double-counting sales and may take up to 72 hours to display after a purchase has been made. Sales value is based on sales price to the end customer, not to Amazon.',
    },
    attributed_sales_1_day_same_sku__sum: {
        name: 'Attributed Sales (1 Day Same SKU)',
        short_name: 'Sales (1 Day Same SKU)',
        format: '$0,0.00',
        short_format: '$0.0a',
        helptext:
            'The aggregate value of sales generated whenever a shopper purchases the advertised product within 1 day of clicking on your ad. Note: Sales are attributed to the last ad clicked to avoid double-counting sales and may take up to 72 hours to display after a purchase has been made. Sales value is based on sales price to the end customer, not to Amazon.',
    },
    attributed_sales_7_day__sum: {
        name: 'Attributed Sales (7 Day)',
        short_name: 'Sales (7 Day)',
        format: '$0,0.00',
        short_format: '$0.0a',
        helptext:
            'The aggregate value of sales generated whenever a shopper purchases any of your brand’s products within 7 days of clicking on your ad. Note: Sales are attributed to the last ad clicked to avoid double-counting sales and may take up to 72 hours to display after a purchase has been made. Sales value is based on sales price to the end customer, not to Amazon.',
    },
    attributed_sales_7_day_same_sku__sum: {
        name: 'Attributed Sales (7 Day Same SKU)',
        short_name: 'Sales (7 Day Same SKU)',
        format: '$0,0.00',
        short_format: '$0.0a',
        helptext:
            'The aggregate value of sales generated whenever a shopper purchases the advertised product within 7 days of clicking on your ad. Note: Sales are attributed to the last ad clicked to avoid double-counting sales and may take up to 72 hours to display after a purchase has been made. Sales value is based on sales price to the end customer, not to Amazon.',
    },
    attributed_sales_14_day__sum: {
        name: 'Attributed Sales (14 Day)',
        short_name: 'Sales (14 Day)',
        format: '$0,0.00',
        short_format: '$0.0a',
        helptext:
            'The aggregate value of sales generated whenever a shopper purchases any of your brand’s products within 14 days of clicking on your ad. Note: Sales are attributed to the last ad clicked to avoid double-counting sales and may take up to 72 hours to display after a purchase has been made. Sales value is based on sales price to the end customer, not to Amazon.',
    },
    attributed_sales_14_day_same_sku__sum: {
        name: 'Attributed Sales (14 Day Same SKU)',
        short_name: 'Sales (14 Day Same SKU)',
        format: '$0,0.00',
        short_format: '$0.0a',
        helptext:
            'The aggregate value of sales generated whenever a shopper purchases the advertised product within 14 days of clicking on your ad. Note: Sales are attributed to the last ad clicked to avoid double-counting sales and may take up to 72 hours to display after a purchase has been made. Sales value is based on sales price to the end customer, not to Amazon.',
    },
    attributed_sales_30_day__sum: {
        name: 'Attributed Sales (30 Day)',
        short_name: 'Sales (30 Day)',
        format: '$0,0.00',
        short_format: '$0.0a',
        helptext:
            'The aggregate value of sales generated whenever a shopper purchases any of your brand’s products within 30 days of clicking on your ad. Note: Sales are attributed to the last ad clicked to avoid double-counting sales and may take up to 72 hours to display after a purchase has been made. Sales value is based on sales price to the end customer, not to Amazon.',
    },
    attributed_sales_30_day_same_sku__sum: {
        name: 'Attributed Sales (30 Day Same SKU)',
        short_name: 'Sales (30 Day Same SKU)',
        format: '$0,0.00',
        short_format: '$0.0a',
        helptext:
            'The aggregate value of sales generated whenever a shopper purchases the advertised product within 30 days of clicking on your ad. Note: Sales are attributed to the last ad clicked to avoid double-counting sales and may take up to 72 hours to display after a purchase has been made. Sales value is based on sales price to the end customer, not to Amazon.',
    },
    attributed_units_ordered_1_day__sum: {
        name: 'Attributed Units Ordered (1 day)',
        short_name: 'Units Ordered (1 day)',
        format: '0,0',
        short_format: '0,0',
        helptext:
            'The number of units a shopper purchases from your brand within 1 day of clicking on your ad.',
    },
    attributed_units_ordered_7_day__sum: {
        name: 'Attributed Units Ordered (7 day)',
        short_name: 'Units Ordered (7 day)',
        format: '0,0',
        short_format: '0,0',
        helptext:
            'The number of units a shopper purchases from your brand within 7 days of clicking on your ad.',
    },
    attributed_units_ordered_14_day__sum: {
        name: 'Attributed Units Ordered (14 day)',
        short_name: 'Units Ordered (14 day)',
        format: '0,0',
        short_format: '0,0',
        helptext:
            'The number of units a shopper purchases from your brand within 14 days of clicking on your ad.',
    },
    attributed_units_ordered_30_day__sum: {
        name: 'Attributed Units Ordered (30 day)',
        short_name: 'Units Ordered (30 day)',
        format: '0,0',
        short_format: '0,0',
        helptext:
            'The number of units a shopper purchases from your brand within 30 days of clicking on your ad.',
    },
}
