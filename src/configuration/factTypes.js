import { SPONSORED_PRODUCT, HEADLINE_SEARCH } from 'constants/factTypes'

export const FACT_TYPE_LABELS = {
    [SPONSORED_PRODUCT]: 'Sponsored Products',
    [HEADLINE_SEARCH]: 'Headline Search',
}
