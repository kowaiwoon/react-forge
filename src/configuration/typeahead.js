/**
 * The number of search results that are returned for a given Typeahead query
 *
 * @type {number}
 */
export const SEARCH_RESULTS_PER_QUERY = 50
