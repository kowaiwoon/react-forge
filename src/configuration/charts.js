import {
    ROI,
    REACH,
    CONVERSIONS,
    LEFT_AXIS_FIRST,
    LEFT_AXIS_SECOND,
    RIGHT_AXIS,
} from 'constants/charts'

export const CHART_AXIS_TITLES = {
    [LEFT_AXIS_FIRST]: {
        title: 'First Metric for Left Axis',
        helpText: 'Select the first metric that is plotted on the left axis.',
    },

    [LEFT_AXIS_SECOND]: {
        title: 'Second Metric for Left Axis',
        helpText: 'Select the second metric that is plotted on the left axis.',
    },

    [RIGHT_AXIS]: {
        title: 'Metric for Right Axis',
        helpText: 'Select the metric that is plotted on the right axis.',
    },
}

export const AMS_TIMESERIES_CHARTS = {
    [ROI]: {
        title: 'Return on Investment',
        titleHelpText: 'The returns on your advertising investments.',
        metricOptions: {
            [LEFT_AXIS_FIRST]: [
                'attributed_sales_1_day__sum',
                'attributed_sales_1_day_same_sku__sum',
                'attributed_sales_7_day__sum',
                'attributed_sales_7_day_same_sku__sum',
                'attributed_sales_14_day__sum',
                'attributed_sales_14_day_same_sku__sum',
                'attributed_sales_30_day__sum',
                'attributed_sales_30_day_same_sku__sum',
            ],
            [LEFT_AXIS_SECOND]: ['cost__sum'],
            [RIGHT_AXIS]: ['roas', 'spc', 'cpc', 'spm'],
        },
        defaultMetrics: {
            [LEFT_AXIS_FIRST]: 'attributed_sales_14_day__sum',
            [LEFT_AXIS_SECOND]: 'cost__sum',
            [RIGHT_AXIS]: 'roas',
        },
    },
    [REACH]: {
        title: 'Reach and Engagement',
        titleHelpText:
            'The number of potential customers you are reaching with your ads and their level of engagement.',
        metricOptions: {
            [LEFT_AXIS_FIRST]: ['impressions__sum'],
            [LEFT_AXIS_SECOND]: ['clicks__sum'],
            [RIGHT_AXIS]: ['ctr', 'spc', 'cpc', 'spm', 'cpm'],
        },
        defaultMetrics: {
            [LEFT_AXIS_FIRST]: 'impressions__sum',
            [LEFT_AXIS_SECOND]: 'clicks__sum',
            [RIGHT_AXIS]: 'ctr',
        },
    },
    [CONVERSIONS]: {
        title: 'Conversions',
        titleHelpText: 'How often customers are converting on your ads.',
        metricOptions: {
            [LEFT_AXIS_FIRST]: [
                'attributed_conversions_1_day__sum',
                'attributed_conversions_1_day_same_sku__sum',
                'attributed_conversions_7_day__sum',
                'attributed_conversions_7_day_same_sku__sum',
                'attributed_conversions_14_day__sum',
                'attributed_conversions_14_day_same_sku__sum',
                'attributed_conversions_30_day__sum',
                'attributed_conversions_30_day_same_sku__sum',
            ],
            [LEFT_AXIS_SECOND]: ['clicks__sum', 'impressions__sum'],
            [RIGHT_AXIS]: [
                'conversion_rate_clicks',
                'conversion_rate_impressions',
                'aov',
            ],
        },
        defaultMetrics: {
            [LEFT_AXIS_FIRST]: 'attributed_conversions_14_day__sum',
            [LEFT_AXIS_SECOND]: 'clicks__sum',
            [RIGHT_AXIS]: 'conversion_rate_clicks',
        },
    },
}
