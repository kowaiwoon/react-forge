export const ASIN_DOMAINS = {
    US: 'www.amazon.com',
    CA: 'www.amazon.ca',
    UK: 'www.amazon.co.uk',
    DE: 'www.amazon.de',
    FR: 'www.amazon.fr',
    ES: 'www.amazon.es',
    IT: 'www.amazon.it',
}
