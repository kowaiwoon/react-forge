import { handleActions, combineActions } from 'redux-actions'
import { normalize } from 'normalizr'
import set from 'lodash/fp/set'
import mergeWith from 'lodash/fp/mergeWith'

import { mergeArrayWithUnion } from 'helpers/utilities'
import { BY_ID, ALL_IDS } from 'constants/reducerKeys'
import {
    // brands
    fetchBrandSuccess,

    // campaigns
    fetchCampaignSuccess,
    updateCampaignSuccess,
    deleteCampaignSuccess,

    // products
    fetchProductSuccess,
    updateProductSuccess,
    deleteProductSuccess,

    // keywords
    fetchKeywordSuccess,
    updateKeywordSuccess,
    deleteKeywordSuccess,

    // sov keywords
    fetchSovKeywordSuccess,
    updateSovKeywordSuccess,
    deleteSovKeywordSuccess,
} from 'actions/entities'
import {
    searchBrandsSuccess,
    searchCampaignsSuccess,
    searchKeywordsSuccess,
    searchProductAsinsSuccess,
    searchProductTitlesSuccess,
    searchSovBrandsSuccess,
    searchSovKeywordsSuccess,
    searchSovKeywordCategoriesSuccess,
    searchOrganizationPageBrandsSuccess,
    searchOrganizationGroupPageBrandsSuccess,
} from 'actions/ui'
import {
    brandListSchema,
    keywordSchema,
    productSchema,
    campaignListSchema,
    campaignSchema,
    keywordListSchema,
    productListSchema,
    sovKeywordListSchema,
} from 'schemas'
import { signOutSuccess } from 'actions/auth'

const defaultState = {
    brands: {
        [BY_ID]: {},
        [ALL_IDS]: [],
    },
    campaigns: {
        [BY_ID]: {},
        [ALL_IDS]: [],
    },
    products: {
        [BY_ID]: {},
        [ALL_IDS]: [],
    },
    keywords: {
        [BY_ID]: {},
        [ALL_IDS]: [],
    },
    sovBrands: [],
    sovKeywords: {
        [BY_ID]: {},
        [ALL_IDS]: [],
    },
    sovKeywordCategories: {
        [ALL_IDS]: [],
    },
    productMetadata: {
        [BY_ID]: {},
        [ALL_IDS]: [],
    },
}

export default handleActions(
    {
        // brands
        [combineActions(
            searchBrandsSuccess,
            searchOrganizationPageBrandsSuccess,
            searchOrganizationGroupPageBrandsSuccess
        )](state, action) {
            const { results } = action.payload
            const {
                entities: { brands },
                result: brandIds,
            } = normalize(results, brandListSchema)

            return mergeWith(mergeArrayWithUnion, state, {
                brands: {
                    [BY_ID]: brands,
                    [ALL_IDS]: brandIds,
                },
            })
        },

        // brand
        [fetchBrandSuccess](state, action) {
            const brand = action.payload
            return mergeWith(mergeArrayWithUnion, state, {
                brands: {
                    [BY_ID]: {
                        [brand.id]: brand,
                    },
                    [ALL_IDS]: [brand.id],
                },
            })
        },

        // campaigns
        [combineActions(searchCampaignsSuccess)](state, action) {
            const {
                entities: { campaigns, brands },
                result,
            } = normalize(action.payload.results, campaignListSchema)
            return mergeWith(mergeArrayWithUnion, state, {
                campaigns: {
                    [BY_ID]: campaigns,
                    [ALL_IDS]: result,
                },
                ...(brands && {
                    brands: {
                        [BY_ID]: brands,
                        [ALL_IDS]: Object.keys(brands),
                    },
                }),
            })
        },

        // campaign
        [combineActions(
            fetchCampaignSuccess,
            updateCampaignSuccess,
            deleteCampaignSuccess
        )](state, action) {
            const {
                entities: { campaigns, brands },
                result: campaignId,
            } = normalize(action.payload, campaignSchema)
            return mergeWith(mergeArrayWithUnion, state, {
                campaigns: {
                    [BY_ID]: campaigns,
                    [ALL_IDS]: [campaignId],
                },
                brands: {
                    [BY_ID]: brands,
                    [ALL_IDS]: [campaigns[campaignId].profile],
                },
            })
        },

        // products
        [combineActions(searchProductAsinsSuccess, searchProductTitlesSuccess)](
            state,
            action
        ) {
            const {
                entities: { brands, campaigns, products, product_metadata },
                result: productIds,
            } = normalize(action.payload.results, productListSchema)
            return mergeWith(mergeArrayWithUnion, state, {
                products: {
                    [BY_ID]: products,
                    [ALL_IDS]: productIds,
                },
                ...(product_metadata && {
                    productMetadata: {
                        [BY_ID]: product_metadata,
                        [ALL_IDS]: Object.keys(product_metadata),
                    },
                }),
                ...(brands && {
                    brands: {
                        [BY_ID]: brands,
                        [ALL_IDS]: Object.keys(brands),
                    },
                }),
                ...(campaigns && {
                    campaigns: {
                        [BY_ID]: campaigns,
                        [ALL_IDS]: Object.keys(campaigns),
                    },
                }),
            })
        },

        // product
        [combineActions(
            fetchProductSuccess,
            updateProductSuccess,
            deleteProductSuccess
        )](state, action) {
            const productData = action.payload
            const {
                entities: { campaigns, products, brands, product_metadata },
                result: productId,
            } = normalize(productData, productSchema)
            return mergeWith(mergeArrayWithUnion, state, {
                products: {
                    [BY_ID]: products,
                    [ALL_IDS]: [productId],
                },
                campaigns: {
                    [BY_ID]: campaigns,
                    [ALL_IDS]: [products[productId].campaign],
                },
                brands: {
                    [BY_ID]: brands,
                    [ALL_IDS]: [products[productId].profile],
                },
                ...(product_metadata && {
                    productMetadata: {
                        [BY_ID]: product_metadata,
                        [ALL_IDS]: [products[productId].product_metadata],
                    },
                }),
            })
        },

        // keywords
        [combineActions(searchKeywordsSuccess)](state, action) {
            const {
                entities: { keywords, campaigns, brands },
                result: keywordIds,
            } = normalize(action.payload.results, keywordListSchema)
            return mergeWith(mergeArrayWithUnion, state, {
                keywords: {
                    [BY_ID]: keywords,
                    [ALL_IDS]: keywordIds,
                },
                campaigns: {
                    [BY_ID]: campaigns,
                    [ALL_IDS]: Object.keys(campaigns),
                },
                brands: {
                    [BY_ID]: brands,
                    [ALL_IDS]: Object.keys(brands),
                },
            })
        },

        // keyword
        [combineActions(
            fetchKeywordSuccess,
            updateKeywordSuccess,
            deleteKeywordSuccess
        )](state, action) {
            const keywordData = action.payload
            const {
                entities: {
                    brands: brand,
                    campaigns: campaign,
                    keywords: keyword,
                },
                result: keywordId,
            } = normalize(keywordData, keywordSchema)
            return mergeWith(mergeArrayWithUnion, state, {
                keywords: {
                    [BY_ID]: keyword,
                    [ALL_IDS]: [keywordId],
                },
                brands: {
                    [BY_ID]: brand,
                    [ALL_IDS]: [keyword[keywordId].profile],
                },
                campaigns: {
                    [BY_ID]: campaign,
                    [ALL_IDS]: [keyword[keywordId].campaign],
                },
            })
        },

        // sov brand
        [searchSovBrandsSuccess](state, action) {
            const { results } = action.payload

            return set(['sovBrands'], results, state)
        },

        // sov keyword
        [searchSovKeywordsSuccess](state, action) {
            const {
                entities: { sovKeywords },
                result: keywordIds,
            } = normalize(action.payload.results, sovKeywordListSchema)
            return mergeWith(mergeArrayWithUnion, state, {
                sovKeywords: {
                    [BY_ID]: sovKeywords,
                    [ALL_IDS]: keywordIds,
                },
            })
        },
        [combineActions(
            fetchSovKeywordSuccess,
            updateSovKeywordSuccess,
            deleteSovKeywordSuccess
        )](state, action) {
            const sovKeyword = action.payload

            return mergeWith(mergeArrayWithUnion, state, {
                sovKeywords: {
                    [BY_ID]: {
                        [sovKeyword.id]: sovKeyword,
                    },
                    [ALL_IDS]: [sovKeyword.id],
                },
            })
        },

        // sov keyword categories
        [searchSovKeywordCategoriesSuccess](state, action) {
            const categories = action.payload
            return mergeWith(mergeArrayWithUnion, state, {
                sovKeywordCategories: {
                    [ALL_IDS]: categories,
                },
            })
        },

        // sign out
        [signOutSuccess]() {
            return defaultState
        },
    },
    defaultState
)
