import { handleActions, combineActions } from 'redux-actions'
import flow from 'lodash/fp/flow'
import merge from 'lodash/fp/merge'
import set from 'lodash/fp/set'
import cloneDeep from 'lodash/cloneDeep'

import { FILTERS } from 'constants/reducerKeys'
import {
    // mounting
    mountSovKeywordSearchResultPageRequest,
    mountSovKeywordSearchResultPageSuccess,
    mountSovKeywordSearchResultPageFailure,
    unmountSovKeywordSearchResultPage,

    // page data
    fetchSovKeywordSearchResultPageDataRequest,
    fetchSovKeywordSearchResultPageDataSuccess,
    fetchSovKeywordSearchResultPageDataFailure,

    // table data
    fetchSovKeywordSearchResultPageTableRequest,
    fetchSovKeywordSearchResultPageTableSuccess,
    fetchSovKeywordSearchResultPageTableFailure,

    // table controls
    updateSovKeywordSearchResultPageTablePagination,
    updateSovKeywordSearchResultPageTableSorter,

    // table settings
    updateSovKeywordSearchResultPageTableSettings,
    fetchSovKeywordSearchResultPageTableSettingsSuccess,

    // table download
    downloadSovKeywordSearchResultPageTableRequest,
    downloadSovKeywordSearchResultPageTableSuccess,
    downloadSovKeywordSearchResultPageTableFailure,

    // screenshot
    fetchSovKeywordSearchResultPageScreenshotSuccess,
} from 'actions/ui'

import { getDefaultTable } from '../defaults'

const defaultTable = {
    ...getDefaultTable({
        actionColumns: ['actions'],
        order: [
            'rank',
            'result_type',
            'text',
            'product_metadata.brand',
            'asin',
            'product_metadata.title',
        ],
        displayState: {
            rank: true,
            result_type: true,
            text: true,
            'product_metadata.brand': true,
            asin: true,
            'product_metadata.title': true,
        },
    }),
    sorter: {
        // Order by rank in ascending order by default
        field: 'rank',
        order: 'ascend',
        filterPositiveValues: true,
    },
}

export const defaultState = {
    [FILTERS]: {},
    table: defaultTable,
    sovKeywordId: null,
    scheduledDate: null,
    screenshotUrl: null,

    mounting: true,
    error: null,
    downloading: false,
}

export default handleActions(
    {
        // mounting
        [mountSovKeywordSearchResultPageRequest](state, action) {
            const { sovKeywordId, scheduledDate } = action.payload
            return flow(
                set(['sovKeywordId'], sovKeywordId),
                set(['scheduledDate'], scheduledDate),
                set(['mounting'], true)
            )(state)
        },
        [mountSovKeywordSearchResultPageSuccess](state) {
            return set(['mounting'], false, state)
        },
        [mountSovKeywordSearchResultPageFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['mounting'], false),
                set(['error'], message)
            )(state)
        },
        [unmountSovKeywordSearchResultPage](state) {
            return flow(
                set(['sovKeywordId'], null),
                set(['scheduledDate'], null),
                set(['table'], defaultTable)
            )(state)
        },

        // page data
        [fetchSovKeywordSearchResultPageDataRequest](state) {
            return set(['table', 'loading'], true, state)
        },
        [fetchSovKeywordSearchResultPageDataSuccess](state) {
            return set(['table', 'loading'], false, state)
        },
        [fetchSovKeywordSearchResultPageDataFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['table', 'loading'], false),
                set(['error'], message)
            )(state)
        },

        // table data
        [fetchSovKeywordSearchResultPageTableRequest](state) {
            return set(['table', 'loading'], true, state)
        },
        [fetchSovKeywordSearchResultPageTableSuccess](state, action) {
            const { count, results } = action.payload
            return flow(
                set(['table', 'loading'], false),
                set(['table', 'data'], results),
                set(['table', 'pagination', 'total'], count)
            )(state)
        },
        [fetchSovKeywordSearchResultPageTableFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['table', 'loading'], false),
                set(['error'], message)
            )(state)
        },

        // table controls
        [updateSovKeywordSearchResultPageTablePagination](state, action) {
            const pagination = action.payload
            return merge(state, {
                table: {
                    pagination,
                },
            })
        },
        [updateSovKeywordSearchResultPageTableSorter](state, action) {
            const sorter = action.payload
            return set(['table', 'sorter'], sorter, state)
        },

        // table settings
        [combineActions(
            updateSovKeywordSearchResultPageTableSettings,
            fetchSovKeywordSearchResultPageTableSettingsSuccess
        )](state, action) {
            const columnSettings = action.payload
            return set(['table', 'columnSettings'], columnSettings, state)
        },

        // table download
        [downloadSovKeywordSearchResultPageTableRequest](state) {
            return set(['downloading'], true, state)
        },
        [downloadSovKeywordSearchResultPageTableSuccess](state) {
            return set(['downloading'], false, state)
        },
        [downloadSovKeywordSearchResultPageTableFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['downloading'], false),
                set(['error'], message)
            )(state)
        },

        // screenshot
        [fetchSovKeywordSearchResultPageScreenshotSuccess](state, action) {
            return set(['screenshotUrl'], action.payload, state)
        },
    },
    cloneDeep(defaultState)
)
