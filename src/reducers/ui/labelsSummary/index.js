import { handleActions, combineActions } from 'redux-actions'
import flow from 'lodash/fp/flow'
import merge from 'lodash/fp/merge'
import set from 'lodash/fp/set'
import cloneDeep from 'lodash/cloneDeep'

import {
    // mounting
    mountLabelsSummaryPageFailure,
    mountLabelsSummaryPageRequest,
    mountLabelsSummaryPageSuccess,
    unmountLabelsSummaryPage,

    // page data
    fetchLabelsSummaryPageDataRequest,
    fetchLabelsSummaryPageDataSuccess,
    fetchLabelsSummaryPageDataFailure,

    // table data
    fetchLabelsSummaryPageTableSuccess,
    fetchLabelsSummaryPageTableFailure,
    fetchLabelsSummaryPageTableRequest,

    // table controls
    updateLabelsSummaryPageTablePagination,
    updateLabelsSummaryPageTableSorter,

    // table settings
    updateLabelsSummaryPageTableSettings,
    fetchLabelsSummaryPageTableSettingsSuccess,

    // create label
    createLabelsSummaryPageLabelRequest,
    createLabelsSummaryPageLabelSuccess,
    createLabelsSummaryPageLabelFailure,
} from 'actions/ui'

import { getDefaultTable } from '../defaults'

const defaultTable = getDefaultTable({
    actionColumns: [],
    order: ['name', 'description', 'color'],
    displayState: { name: true, description: true, color: true },
})

export const defaultState = {
    table: {
        ...defaultTable,
        sorter: { field: 'name', order: 'ascend' },
    },
    mounting: true,
    creating: false,
    error: null,
}

export default handleActions(
    {
        // mounting
        [mountLabelsSummaryPageRequest](state) {
            return set(['mounting'], true, state)
        },
        [mountLabelsSummaryPageSuccess](state) {
            return set(['mounting'], false, state)
        },
        [mountLabelsSummaryPageFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['mounting'], false),
                set(['error'], message)
            )(state)
        },
        [unmountLabelsSummaryPage](state) {
            return set(['table'], defaultTable, state)
        },

        // page data
        [fetchLabelsSummaryPageDataRequest](state) {
            return set(['table', 'loading'], true, state)
        },
        [fetchLabelsSummaryPageDataSuccess](state) {
            return set(['table', 'loading'], false, state)
        },
        [fetchLabelsSummaryPageDataFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['table', 'loading'], false),
                set(['error'], message)
            )(state)
        },

        // table data
        [fetchLabelsSummaryPageTableRequest](state) {
            return set(['table', 'loading'], true, state)
        },
        [fetchLabelsSummaryPageTableSuccess](state, action) {
            const { count, results } = action.payload
            return flow(
                set(['table', 'loading'], false),
                set(['table', 'data'], results),
                set(['table', 'pagination', 'total'], count)
            )(state)
        },
        [fetchLabelsSummaryPageTableFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['table', 'loading'], false),
                set(['error'], message)
            )(state)
        },

        // table controls
        [updateLabelsSummaryPageTablePagination](state, action) {
            const pagination = action.payload
            return merge(state, {
                table: {
                    pagination,
                },
            })
        },
        [updateLabelsSummaryPageTableSorter](state, action) {
            const sorter = action.payload
            return set(['table', 'sorter'], sorter, state)
        },

        // table settings
        [combineActions(
            updateLabelsSummaryPageTableSettings,
            fetchLabelsSummaryPageTableSettingsSuccess
        )](state, action) {
            return set(['table', 'columnSettings'], action.payload, state)
        },

        // create label
        [createLabelsSummaryPageLabelRequest](state) {
            return set(['creating'], true, state)
        },
        [createLabelsSummaryPageLabelSuccess](state) {
            return set(['creating'], false, state)
        },
        [createLabelsSummaryPageLabelFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['creating'], false),
                set(['error'], message)
            )(state)
        },
    },
    cloneDeep(defaultState) // create clone, so the defaultState is not mutated
)
