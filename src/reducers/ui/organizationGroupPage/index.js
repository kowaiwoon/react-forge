import { handleActions } from 'redux-actions'
import flow from 'lodash/fp/flow'
import set from 'lodash/fp/set'
import cloneDeep from 'lodash/cloneDeep'

import {
    // mounting
    mountOrganizationGroupPageRequest,
    mountOrganizationGroupPageSuccess,
    mountOrganizationGroupPageFailure,
    unmountOrganizationGroupPage,
    addMemberOrganizationGroupPageRequest,
    addMemberOrganizationGroupPageSuccess,
    addMemberOrganizationGroupPageFailure,
    removeMemberOrganizationGroupPageRequest,
    removeMemberOrganizationGroupPageSuccess,
    removeMemberOrganizationGroupPageFailure,
    updateNameOrganizationGroupPageRequest,
    updateNameOrganizationGroupPageSuccess,
    updateNameOrganizationGroupPageFailure,
    updatePermissionsOrganizationGroupPageRequest,
    updatePermissionsOrganizationGroupPageSuccess,
    updatePermissionsOrganizationGroupPageFailure,
    updateResourcesOrganizationGroupPageRequest,
    updateResourcesOrganizationGroupPageSuccess,
    updateResourcesOrganizationGroupPageFailure,
    searchOrganizationGroupPageBrandsRequest,
    searchOrganizationGroupPageBrandsSuccess,
    searchOrganizationGroupPageBrandsFailure,
} from 'actions/ui'

export const defaultState = {
    mounting: true,
    error: null,

    organizationId: null,
    organizationGroupId: null,

    brandSearchLoading: false,

    addingMember: false,
    addMemberError: null,

    removingMemberId: null,
    removeMemberError: null,

    updatingPermissions: false,
    updatePermissionsError: null,

    updatingResources: false,
    updatingResourcesError: null,

    updatingName: false,
    updatingNameError: null,
}

export default handleActions(
    {
        // mounting
        [mountOrganizationGroupPageRequest](state, action) {
            const { organizationId, organizationGroupId } = action.payload
            return flow(
                set(['organizationId'], organizationId),
                set(['organizationGroupId'], organizationGroupId),
                set(['mounting'], true)
            )(state)
        },
        [mountOrganizationGroupPageSuccess](state) {
            return set(['mounting'], false, state)
        },
        [mountOrganizationGroupPageFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['mounting'], false),
                set(['error'], message)
            )(state)
        },
        [unmountOrganizationGroupPage]() {
            return defaultState
        },

        // update name
        [updateNameOrganizationGroupPageRequest](state) {
            return set(['updatingName'], true, state)
        },
        [updateNameOrganizationGroupPageSuccess](state) {
            return flow(
                set(['updatingName'], false),
                set(['updatingNameError'], null)
            )(state)
        },
        [updateNameOrganizationGroupPageFailure](state, action) {
            return flow(
                set(['updatingName'], false),
                set(['updatingNameError'], action.payload.message)
            )(state)
        },

        // add member
        [addMemberOrganizationGroupPageRequest](state) {
            return set(['addingMember'], true, state)
        },
        [addMemberOrganizationGroupPageSuccess](state) {
            return flow(
                set(['addingMember'], false),
                set(['addMemberError'], null)
            )(state)
        },
        [addMemberOrganizationGroupPageFailure](state, action) {
            return flow(
                set(['addingMember'], false),
                set(['addMemberError'], action.payload.message)
            )(state)
        },

        // remove member
        [removeMemberOrganizationGroupPageRequest](state, action) {
            return set(['removingMemberId'], action.payload.memberId, state)
        },
        [removeMemberOrganizationGroupPageSuccess](state) {
            return flow(
                set(['removeMemberError'], null),
                set(['removingMemberId'], null)
            )(state)
        },
        [removeMemberOrganizationGroupPageFailure](state, action) {
            return flow(
                set(['removeMemberError'], action.payload.message),
                set(['removingMemberId'], null)
            )(state)
        },

        // update permissions
        [updatePermissionsOrganizationGroupPageRequest](state) {
            return set(['updatingPermissions'], true, state)
        },
        [updatePermissionsOrganizationGroupPageSuccess](state) {
            return flow(
                set(['updatingPermissions'], false),
                set(['updatePermissionsError'], null)
            )(state)
        },
        [updatePermissionsOrganizationGroupPageFailure](state, action) {
            return flow(
                set(['updatingPermissions'], false),
                set(['updatePermissionsError'], action.payload.message)
            )(state)
        },

        // update resources
        [updateResourcesOrganizationGroupPageRequest](state) {
            return set(['updatingResources'], true, state)
        },
        [updateResourcesOrganizationGroupPageSuccess](state) {
            return flow(
                set(['updatingResources'], false),
                set(['updatingResourcesError'], null)
            )(state)
        },
        [updateResourcesOrganizationGroupPageFailure](state, action) {
            return flow(
                set(['updatingResources'], false),
                set(['updatingResourcesError'], action.payload.message)
            )(state)
        },

        // search brands
        [searchOrganizationGroupPageBrandsRequest](state) {
            return set(['brandSearchLoading'], true, state)
        },
        [searchOrganizationGroupPageBrandsSuccess](state) {
            return set(['brandSearchLoading'], false, state)
        },
        [searchOrganizationGroupPageBrandsFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['brandSearchLoading'], false),
                set(['error'], message)
            )(state)
        },
    },
    cloneDeep(defaultState)
)
