import { handleActions, combineActions } from 'redux-actions'
import flow from 'lodash/fp/flow'
import set from 'lodash/fp/set'
import get from 'lodash/get'
import merge from 'lodash/fp/merge'
import cloneDeep from 'lodash/cloneDeep'

import {
    mountAutomationPageRequest,
    mountAutomationPageSuccess,
    mountAutomationPageFailure,
    fetchAutomationPageDescriptionSuccess,
    fetchAutomationPageDataRequest,
    fetchAutomationPageDataSuccess,
    fetchAutomationPageDataFailure,
    fetchAutomationPageTableRequest,
    fetchAutomationPageTableSuccess,
    fetchAutomationPageTableFailure,
    updateAutomationPageTablePagination,
    updateAutomationPageTableSorter,
    updateAutomationPageTableSettings,
    fetchAutomationPageTableSettingsSuccess,
} from 'actions/ui'
import { FILTERS, FILTER_SETTINGS } from 'constants/reducerKeys'
import { DATES, REGIONS, COUNTRIES } from 'constants/filters'
import moment from 'utilities/moment'

import { getDefaultTable } from '../defaults'

export const defaultState = {
    mounting: true,
    error: null,
    automationDescription: null,
    descriptionLoading: false,
    [FILTERS]: {
        [DATES]: [moment().subtract(7, 'days'), moment()],
        [REGIONS]: [],
        [COUNTRIES]: [],
    },
    [FILTER_SETTINGS]: {
        order: [DATES, REGIONS, COUNTRIES],
        displayState: {
            [DATES]: true,
            [REGIONS]: true,
            [COUNTRIES]: true,
        },
    },
    table: {
        ...getDefaultTable({
            actionColumns: [],
            order: [
                'resource_name',
                'resource_type',
                'change_description',
                'change_reason',
                'change_date',
                'region',
                'country',
            ],
            displayState: {
                resource_name: true,
                resource_type: true,
                change_description: true,
                change_reason: true,
                change_date: true,
                region: true,
                country: true,
            },
        }),
        sorter: { field: 'history_date', order: 'descend' },
    },
}

export default handleActions(
    {
        // mounting
        [mountAutomationPageRequest](state) {
            return set(['mounting'], true, state)
        },
        [mountAutomationPageSuccess](state) {
            return set(['mounting'], false, state)
        },
        [mountAutomationPageFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['mounting'], false),
                set(['error'], message)
            )(state)
        },

        // page data
        [fetchAutomationPageDataRequest](state) {
            return flow(
                set(['descriptionLoading'], true),
                set(['table', 'loading'], true),
                set(['automationDescription'], null)
            )(state)
        },
        [fetchAutomationPageDataSuccess](state) {
            return flow(
                set(['descriptionLoading'], false),
                set(['table', 'loading'], false)
            )(state)
        },
        [fetchAutomationPageDataFailure](state, action) {
            return flow(
                set(['descriptionLoading'], false),
                set(['table', 'loading'], false),
                set(['error'], action.payload)
            )(state)
        },

        // automation description
        [fetchAutomationPageDescriptionSuccess](state, action) {
            return set(
                ['automationDescription'],
                get(action, ['payload', 'automation_description']),
                state
            )
        },

        // table data
        [fetchAutomationPageTableRequest](state) {
            return set(['table', 'loading'], true, state)
        },
        [fetchAutomationPageTableSuccess](state, action) {
            const { count, results } = action.payload
            return flow(
                set(['table', 'loading'], false),
                set(['table', 'data'], results),
                set(['table', 'pagination', 'total'], count)
            )(state)
        },
        [fetchAutomationPageTableFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['table', 'loading'], false),
                set(['error'], message)
            )(state)
        },

        // table controls
        [updateAutomationPageTablePagination](state, action) {
            const pagination = action.payload
            return merge(state, {
                table: {
                    pagination,
                },
            })
        },
        [updateAutomationPageTableSorter](state, action) {
            const sorter = action.payload
            return set(['table', 'sorter'], sorter, state)
        },

        // table settings
        [combineActions(
            updateAutomationPageTableSettings,
            fetchAutomationPageTableSettingsSuccess
        )](state, action) {
            const columnSettings = action.payload
            return set(['table', 'columnSettings'], columnSettings, state)
        },
    },
    cloneDeep(defaultState)
)
