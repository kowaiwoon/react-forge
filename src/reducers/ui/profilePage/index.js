import { handleActions } from 'redux-actions'
import flow from 'lodash/fp/flow'
import set from 'lodash/fp/set'
import cloneDeep from 'lodash/cloneDeep'

import {
    toggleProfilePageDetails,
    openProfilePageOrganizationForm,
    closeProfilePageOrganizationForm,
    createProfilePageOrganizationRequest,
    createProfilePageOrganizationSuccess,
    createProfilePageOrganizationFailure,
    mountProfilePageRequest,
    mountProfilePageSuccess,
    mountProfilePageFailure,
    unmountProfilePage,
    fetchInvitationsProfilePageSuccess,
    acceptInvitationProfilePageRequest,
    acceptInvitationProfilePageSuccess,
    acceptInvitationProfilePageFailure,
} from 'actions/ui'

export const defaultState = {
    showDetails: true,
    organizationFormVisible: false,
    creatingOrganization: false,
    error: null,
    mounting: true,
    invitations: [],
    acceptingInvite: false,
}

export default handleActions(
    {
        // mount profile page
        [mountProfilePageRequest](state) {
            return set(['mounting'], true, state)
        },
        [mountProfilePageSuccess](state) {
            return set(['mounting'], false, state)
        },
        [mountProfilePageFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['mounting'], false),
                set(['error'], message)
            )(state)
        },
        [unmountProfilePage]() {
            return defaultState
        },

        // toggle profile details
        [toggleProfilePageDetails](state) {
            return set(['showDetails'], !state.showDetails, state)
        },

        // org form
        [openProfilePageOrganizationForm](state) {
            return set(['organizationFormVisible'], true, state)
        },
        [closeProfilePageOrganizationForm](state) {
            return set(['organizationFormVisible'], false, state)
        },

        // create org
        [createProfilePageOrganizationRequest](state) {
            return set(['creatingOrganization'], true, state)
        },
        [createProfilePageOrganizationSuccess](state) {
            return flow(
                set(['creatingOrganization'], false),
                set(['organizationFormVisible'], false)
            )(state)
        },
        [createProfilePageOrganizationFailure](state, action) {
            return flow(
                set(['creatingOrganization'], false),
                set(['organizationFormVisible'], false),
                set(['error'], action.payload.message)
            )(state)
        },

        // invitations
        [fetchInvitationsProfilePageSuccess](state, action) {
            return set(['invitations'], action.payload.results, state)
        },
        [acceptInvitationProfilePageRequest](state) {
            return set(['acceptingInvite'], true, state)
        },
        [acceptInvitationProfilePageSuccess](state) {
            return set(['acceptingInvite'], false, state)
        },
        [acceptInvitationProfilePageFailure](state, action) {
            return flow(
                set(['acceptingInvite'], false),
                set(['error'], action.payload.message)
            )(state)
        },
    },
    cloneDeep(defaultState)
)
