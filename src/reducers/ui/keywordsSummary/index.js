import { handleActions, combineActions } from 'redux-actions'
import flow from 'lodash/fp/flow'
import merge from 'lodash/fp/merge'
import set from 'lodash/fp/set'
import cloneDeep from 'lodash/cloneDeep'

import { FILTERS, FILTER_SETTINGS } from 'constants/reducerKeys'
import {
    DATES,
    COUNTRIES,
    REGIONS,
    BRANDS,
    CAMPAIGNS,
    CAMPAIGN_TARGETING_TYPES,
    KEYWORDS,
    KEYWORD_MATCH_TYPES,
    FACT_TYPES,
} from 'constants/filters'
import {
    METRIC_COLUMNS_STATE,
    METRIC_COLUMNS_ORDER,
} from 'configuration/tables'
import {
    // mounting
    mountKeywordsSummaryPageRequest,
    mountKeywordsSummaryPageSuccess,
    mountKeywordsSummaryPageFailure,
    unmountKeywordsPageSummary,

    // page data
    fetchKeywordsSummaryPageDataRequest,
    fetchKeywordsSummaryPageDataSuccess,
    fetchKeywordsSummaryPageDataFailure,

    // treemap data
    fetchKeywordsSummaryPageTreemapRequest,
    fetchKeywordsSummaryPageTreemapSuccess,
    fetchKeywordsSummaryPageTreemapFailure,

    // treemap controls
    updateKeywordsSummaryPageTreemapPagination,
    updateKeywordsSummaryPageTreemapSorter,

    // table data
    fetchKeywordsSummaryPageTableRequest,
    fetchKeywordsSummaryPageTableSuccess,
    fetchKeywordsSummaryPageTableFailure,

    // table controls
    updateKeywordsSummaryPageTablePagination,
    updateKeywordsSummaryPageTableSorter,

    // table settings
    updateKeywordsSummaryPageTableSettings,
    fetchKeywordsSummaryPageTableSettingsSuccess,

    // table download
    downloadKeywordsSummaryPageTableSuccess,
    downloadKeywordsSummaryPageTableRequest,
    downloadKeywordsSummaryPageTableFailure,

    // search
    searchKeywordsRequest,
    searchKeywordsSuccess,
    searchKeywordsFailure,
} from 'actions/ui'

import {
    defaultTreemap,
    defaultDatesFilter,
    getDefaultTable,
    defaultFactTypes,
} from '../defaults'

const defaultTable = getDefaultTable({
    actionColumns: [],
    order: [
        'keyword.text',
        'keyword.match_type',
        'keyword.bid',
        'keyword.state',
        'profile.brand_name',
        'profile.country_code',
        'profile.currency_code',
        'profile.region',
        'profile.timezone',
        'campaign.name',
        'campaign.budget',
        'campaign.budget_type',
        'campaign.campaign_type',
        'campaign.dayparting_enabled',
        'campaign.start_date',
        'campaign.end_date',
        'campaign.state',
        'campaign.targeting_type',
        ...METRIC_COLUMNS_ORDER,
    ],
    displayState: {
        'keyword.text': true,
        'keyword.match_type': true,
        'keyword.bid': true,
        'keyword.state': true,
        'profile.brand_name': true,
        'profile.country_code': true,
        'profile.currency_code': false,
        'profile.region': true,
        'profile.timezone': false,
        'campaign.name': true,
        'campaign.budget': false,
        'campaign.budget_type': false,
        'campaign.campaign_type': true,
        'campaign.dayparting_enabled': false,
        'campaign.start_date': false,
        'campaign.end_date': false,
        'campaign.state': false,
        'campaign.targeting_type': true,
        ...METRIC_COLUMNS_STATE,
    },
})

export const defaultState = {
    [FILTERS]: {
        [DATES]: defaultDatesFilter,
        [FACT_TYPES]: defaultFactTypes,
        [REGIONS]: [],
        [COUNTRIES]: [],
        [BRANDS]: [],
        [CAMPAIGNS]: [],
        [CAMPAIGN_TARGETING_TYPES]: [],
        [KEYWORDS]: [],
        [KEYWORD_MATCH_TYPES]: [],
    },
    [FILTER_SETTINGS]: {
        order: [
            DATES,
            FACT_TYPES,
            REGIONS,
            COUNTRIES,
            BRANDS,
            CAMPAIGNS,
            CAMPAIGN_TARGETING_TYPES,
            KEYWORDS,
            KEYWORD_MATCH_TYPES,
        ],
        displayState: {
            [DATES]: true,
            [FACT_TYPES]: true,
            [REGIONS]: true,
            [COUNTRIES]: true,
            [BRANDS]: false,
            [CAMPAIGNS]: false,
            [CAMPAIGN_TARGETING_TYPES]: false,
            [KEYWORDS]: true,
            [KEYWORD_MATCH_TYPES]: false,
        },
    },

    treemap: defaultTreemap,
    table: defaultTable,
    mounting: true,
    error: null,
    downloading: false,
    keywordsFilterLoading: false,
}

export default handleActions(
    {
        // mounting
        [mountKeywordsSummaryPageRequest](state) {
            return set(['mounting'], true, state)
        },
        [mountKeywordsSummaryPageSuccess](state) {
            return set(['mounting'], false, state)
        },
        [mountKeywordsSummaryPageFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['mounting'], false),
                set(['error'], message)
            )(state)
        },
        [unmountKeywordsPageSummary](state) {
            return flow(
                set(['treemap'], defaultTreemap),
                set(['table'], defaultTable)
            )(state)
        },

        // page data
        [fetchKeywordsSummaryPageDataRequest](state) {
            return flow(
                set(['treemap', 'loading'], true),
                set(['table', 'loading'], true)
            )(state)
        },
        [fetchKeywordsSummaryPageDataSuccess](state) {
            return flow(
                set(['treemap', 'loading'], false),
                set(['table', 'loading'], false)
            )(state)
        },
        [fetchKeywordsSummaryPageDataFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['treemap', 'loading'], false),
                set(['table', 'loading'], false),
                set(['error'], message)
            )(state)
        },

        // treemap data
        [fetchKeywordsSummaryPageTreemapRequest](state) {
            return set(['treemap', 'loading'], true, state)
        },
        [fetchKeywordsSummaryPageTreemapSuccess](state, action) {
            const { count, results } = action.payload
            return flow(
                set(['treemap', 'loading'], false),
                set(['treemap', 'data'], results),
                set(['treemap', 'pagination', 'total'], count)
            )(state)
        },
        [fetchKeywordsSummaryPageTreemapFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['treemap', 'loading'], false),
                set(['error'], message)
            )(state)
        },

        // treemap controls
        [updateKeywordsSummaryPageTreemapPagination](state, action) {
            const pagination = action.payload
            return merge(state, {
                treemap: {
                    pagination,
                },
            })
        },
        [updateKeywordsSummaryPageTreemapSorter](state, action) {
            const sorter = action.payload
            return set(['treemap', 'sorter'], sorter, state)
        },

        // table data
        [fetchKeywordsSummaryPageTableRequest](state) {
            return set(['table', 'loading'], true, state)
        },
        [fetchKeywordsSummaryPageTableSuccess](state, action) {
            const { count, results } = action.payload
            return flow(
                set(['table', 'loading'], false),
                set(['table', 'data'], results),
                set(['table', 'pagination', 'total'], count)
            )(state)
        },
        [fetchKeywordsSummaryPageTableFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['table', 'loading'], false),
                set(['error'], message)
            )
        },

        // table controls
        [updateKeywordsSummaryPageTablePagination](state, action) {
            const pagination = action.payload
            return merge(state, {
                table: {
                    pagination,
                },
            })
        },
        [updateKeywordsSummaryPageTableSorter](state, action) {
            const sorter = action.payload
            return set(['table', 'sorter'], sorter, state)
        },

        // table settings
        [combineActions(
            updateKeywordsSummaryPageTableSettings,
            fetchKeywordsSummaryPageTableSettingsSuccess
        )](state, action) {
            const columnSettings = action.payload
            return set(['table', 'columnSettings'], columnSettings, state)
        },

        // table download
        [downloadKeywordsSummaryPageTableSuccess](state) {
            return set(['downloading'], false, state)
        },
        [downloadKeywordsSummaryPageTableRequest](state) {
            return set(['downloading'], true, state)
        },
        [downloadKeywordsSummaryPageTableFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['downloading'], false),
                set(['error'], message)
            )(state)
        },

        // search
        [searchKeywordsRequest](state) {
            return set(['keywordsFilterLoading'], true, state)
        },
        [combineActions(searchKeywordsSuccess, searchKeywordsFailure)](state) {
            return set(['keywordsFilterLoading'], false, state)
        },
    },
    cloneDeep(defaultState) // create clone, so the defaultState is not mutated
)
