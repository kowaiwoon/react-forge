import { handleActions } from 'redux-actions'
import flow from 'lodash/fp/flow'
import set from 'lodash/fp/set'
import update from 'lodash/fp/update'
import pick from 'lodash/pick'

import { FILTERS, FILTER_SETTINGS, CHARTS } from 'constants/reducerKeys'
import {
    updatePageFilter,
    resetPageFilters,
    updatePageFilterSettings,
    fetchPageFilterSettingsSuccess,
    updateChartMetrics,
    updateTableSorter,
    updateTreemapSorter,
} from 'actions/ui'
import { signOutSuccess } from 'actions/auth'

import { defaultState as uiDefaultState } from '../ui'

const defaultState = {}

export default handleActions(
    {
        // Filter
        [updatePageFilter](state, action) {
            const {
                pageName,
                data: { key, value },
            } = action.payload
            return set([pageName, FILTERS, key], value, state)
        },
        [resetPageFilters](state, action) {
            const { pageName } = action.payload
            const pageDefaultState = uiDefaultState[pageName]

            return flow(
                set([pageName, FILTERS], pageDefaultState[FILTERS]),
                set(
                    [pageName, FILTER_SETTINGS],
                    pageDefaultState[FILTER_SETTINGS]
                )
            )(state)
        },

        // Filter Settings
        [updatePageFilterSettings](state, action) {
            const {
                pageName,
                data: { displayState, order },
            } = action.payload
            const defaultFilters = uiDefaultState[pageName][FILTERS]

            return update(
                [pageName],
                pageState =>
                    flow(
                        set([FILTER_SETTINGS], {
                            displayState,
                            order,
                        }),
                        update([FILTERS], filters =>
                            Object.keys(filters).reduce(
                                (newFilters, key) => ({
                                    ...newFilters,
                                    [key]: displayState[key]
                                        ? filters[key]
                                        : defaultFilters[key],
                                }),
                                {}
                            )
                        )
                    )(pageState),
                state
            )
        },
        [fetchPageFilterSettingsSuccess](state, action) {
            const { pageName, filterSettings } = action.payload

            return set(
                [pageName, FILTER_SETTINGS],
                pick(filterSettings, ['displayState', 'order']),
                state
            )
        },

        // Chart
        [updateChartMetrics](state, action) {
            const { pageName, chartName, key, value } = action.payload
            return set([pageName, CHARTS, chartName, key], value, state)
        },

        // Table
        [updateTableSorter](state, action) {
            const { pageName, sorter } = action.payload
            return set([pageName, 'table', 'sorter'], sorter, state)
        },

        // Treemap
        [updateTreemapSorter](state, action) {
            const { pageName, sorter } = action.payload
            return set([pageName, 'treemap', 'sorter'], sorter, state)
        },

        // sign out
        [signOutSuccess]() {
            return uiDefaultState
        },
    },
    defaultState
)
