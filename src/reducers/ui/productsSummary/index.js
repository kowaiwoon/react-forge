import { handleActions, combineActions } from 'redux-actions'
import flow from 'lodash/fp/flow'
import merge from 'lodash/fp/merge'
import set from 'lodash/fp/set'
import cloneDeep from 'lodash/cloneDeep'

import { SPONSORED_PRODUCT } from 'constants/factTypes'
import { getFactTypeObject } from 'helpers/factTypes'
import { FILTERS, FILTER_SETTINGS } from 'constants/reducerKeys'
import {
    DATES,
    FACT_TYPES,
    REGIONS,
    COUNTRIES,
    BRANDS,
    CAMPAIGNS,
    CAMPAIGN_TARGETING_TYPES,
    PRODUCT_ASINS,
    PRODUCT_TITLES,
} from 'constants/filters'
import {
    METRIC_COLUMNS_ORDER,
    METRIC_COLUMNS_STATE,
} from 'configuration/tables'
import {
    // mounting
    mountProductsSummaryPageRequest,
    mountProductsSummaryPageSuccess,
    mountProductsSummaryPageFailure,
    unmountProductsSummary,

    // page data
    fetchProductsSummaryPageDataRequest,
    fetchProductsSummaryPageDataSuccess,
    fetchProductsSummaryPageDataFailure,

    // treemap data
    fetchProductsSummaryPageTreemapRequest,
    fetchProductsSummaryPageTreemapSuccess,
    fetchProductsSummaryPageTreemapFailure,

    // treemap controls
    updateProductsSummaryPageTreemapPagination,
    updateProductsSummaryPageTreemapSorter,

    // table data
    fetchProductsSummaryPageTableRequest,
    fetchProductsSummaryPageTableSuccess,
    fetchProductsSummaryPageTableFailure,

    // table controls
    updateProductsSummaryPageTablePagination,
    updateProductsSummaryPageTableSorter,

    // table settings
    updateProductsSummaryPageTableSettingsRequest,
    fetchProductsSummaryPageTableSettingsSuccess,

    // table download
    downloadProductsSummaryPageTableSuccess,
    downloadProductsSummaryPageTableRequest,
    downloadProductsSummaryPageTableFailure,

    // search product asins
    searchProductAsinsRequest,
    searchProductAsinsSuccess,
    searchProductAsinsFailure,

    // search product titles
    searchProductTitlesRequest,
    searchProductTitlesSuccess,
    searchProductTitlesFailure,
} from 'actions/ui'
import {
    defaultTreemap,
    defaultDatesFilter,
    getDefaultTable,
} from '../defaults'

const defaultTable = getDefaultTable({
    actionColumns: [],
    order: [
        'product_metadata.title',
        'product_metadata.price',
        'product_ad.asin',
        'product_ad.sku',
        'product_ad.state',
        'profile.brand_name',
        'profile.country_code',
        'profile.currency_code',
        'profile.region',
        'profile.timezone',
        'campaign.name',
        'campaign.budget',
        'campaign.budget_type',
        'campaign.campaign_type',
        'campaign.dayparting_enabled',
        'campaign.start_date',
        'campaign.end_date',
        'campaign.state',
        'campaign.targeting_type',
        ...METRIC_COLUMNS_ORDER,
    ],
    displayState: {
        'product_metadata.title': true,
        'product_metadata.price': true,
        'product_ad.asin': true,
        'product_ad.sku': true,
        'product_ad.state': true,
        'profile.brand_name': true,
        'profile.country_code': true,
        'profile.currency_code': false,
        'profile.region': true,
        'profile.timezone': false,
        'campaign.name': true,
        'campaign.budget': false,
        'campaign.budget_type': false,
        'campaign.campaign_type': true,
        'campaign.dayparting_enabled': false,
        'campaign.start_date': false,
        'campaign.end_date': false,
        'campaign.state': false,
        'campaign.targeting_type': true,
        ...METRIC_COLUMNS_STATE,
    },
})

export const defaultState = {
    [FILTERS]: {
        [DATES]: defaultDatesFilter,
        [FACT_TYPES]: [getFactTypeObject(SPONSORED_PRODUCT)],
        [REGIONS]: [],
        [COUNTRIES]: [],
        [BRANDS]: [],
        [CAMPAIGNS]: [],
        [CAMPAIGN_TARGETING_TYPES]: [],
        [PRODUCT_ASINS]: [],
        [PRODUCT_TITLES]: [],
    },
    [FILTER_SETTINGS]: {
        order: [
            DATES,
            FACT_TYPES,
            REGIONS,
            COUNTRIES,
            BRANDS,
            CAMPAIGNS,
            CAMPAIGN_TARGETING_TYPES,
            PRODUCT_ASINS,
            PRODUCT_TITLES,
        ],
        displayState: {
            [DATES]: true,
            [FACT_TYPES]: true,
            [REGIONS]: true,
            [COUNTRIES]: true,
            [BRANDS]: false,
            [CAMPAIGNS]: false,
            [CAMPAIGN_TARGETING_TYPES]: false,
            [PRODUCT_ASINS]: true,
            [PRODUCT_TITLES]: true,
        },
    },

    treemap: defaultTreemap,
    table: defaultTable,
    mounting: true,
    error: null,
    downloading: false,
    productAsinsFilterLoading: false,
    productTitlesFilterLoading: false,
}

export default handleActions(
    {
        // mounting
        [mountProductsSummaryPageRequest](state) {
            return set(['mounting'], true, state)
        },
        [mountProductsSummaryPageSuccess](state) {
            return set(['mounting'], false, state)
        },
        [mountProductsSummaryPageFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['mounting'], false),
                set(['error'], message)
            )(state)
        },
        [unmountProductsSummary](state) {
            return flow(
                set(['treemap'], defaultTreemap),
                set(['table'], defaultTable)
            )(state)
        },

        // page data
        [fetchProductsSummaryPageDataRequest](state) {
            return flow(
                set(['treemap', 'loading'], true),
                set(['table', 'loading'], true)
            )(state)
        },
        [fetchProductsSummaryPageDataSuccess](state) {
            return flow(
                set(['treemap', 'loading'], false),
                set(['table', 'loading'], false)
            )(state)
        },
        [fetchProductsSummaryPageDataFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['treemap', 'loading'], false),
                set(['table', 'loading'], false),
                set(['error'], message)
            )(state)
        },

        // treemap data
        [fetchProductsSummaryPageTreemapRequest](state) {
            return set(['treemap', 'loading'], true, state)
        },
        [fetchProductsSummaryPageTreemapSuccess](state, action) {
            const { count, results } = action.payload
            return flow(
                set(['treemap', 'loading'], false),
                set(['treemap', 'data'], results),
                set(['treemap', 'pagination', 'total'], count)
            )(state)
        },
        [fetchProductsSummaryPageTreemapFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['treemap', 'loading'], false),
                set(['error'], message)
            )(state)
        },

        // treemap controls
        [updateProductsSummaryPageTreemapPagination](state, action) {
            const pagination = action.payload
            return merge(state, {
                treemap: {
                    pagination,
                },
            })
        },
        [updateProductsSummaryPageTreemapSorter](state, action) {
            const sorter = action.payload
            return set(['treemap', 'sorter'], sorter, state)
        },

        // table data
        [fetchProductsSummaryPageTableRequest](state) {
            return set(['table', 'loading'], true, state)
        },
        [fetchProductsSummaryPageTableSuccess](state, action) {
            const { count, results } = action.payload
            return flow(
                set(['table', 'loading'], false),
                set(['table', 'data'], results),
                set(['table', 'pagination', 'total'], count)
            )(state)
        },
        [fetchProductsSummaryPageTableFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['table', 'loading'], false),
                set(['error'], message)
            )(state)
        },

        // table controls
        [updateProductsSummaryPageTablePagination](state, action) {
            const pagination = action.payload
            return merge(state, {
                table: {
                    pagination,
                },
            })
        },
        [updateProductsSummaryPageTableSorter](state, action) {
            const sorter = action.payload
            return set(['table', 'sorter'], sorter, state)
        },

        // table settings
        [combineActions(
            updateProductsSummaryPageTableSettingsRequest,
            fetchProductsSummaryPageTableSettingsSuccess
        )](state, action) {
            const columnSettings = action.payload
            return set(['table', 'columnSettings'], columnSettings, state)
        },

        // table download
        [downloadProductsSummaryPageTableSuccess](state) {
            return set(['downloading'], false, state)
        },
        [downloadProductsSummaryPageTableRequest](state) {
            return set(['downloading'], true, state)
        },
        [downloadProductsSummaryPageTableFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['downloading'], false),
                set(['error'], message)
            )(state)
        },

        // search product asins
        [searchProductAsinsRequest](state) {
            return set(['productAsinsFilterLoading'], true, state)
        },
        [combineActions(searchProductAsinsSuccess, searchProductAsinsFailure)](
            state
        ) {
            return set(['productAsinsFilterLoading'], false, state)
        },

        // search product titles
        [searchProductTitlesRequest](state) {
            return set(['productTitlesFilterLoading'], true, state)
        },
        [combineActions(
            searchProductTitlesSuccess,
            searchProductTitlesFailure
        )](state) {
            return set(['productTitlesFilterLoading'], false, state)
        },
    },
    cloneDeep(defaultState) // create clone, so the defaultState is not mutated
)
