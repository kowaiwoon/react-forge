import { handleActions } from 'redux-actions'
import flow from 'lodash/fp/flow'
import set from 'lodash/fp/set'
import cloneDeep from 'lodash/cloneDeep'

import { SPONSORED_PRODUCT, HEADLINE_SEARCH } from 'constants/factTypes'
import {
    FILTERS,
    CHARTS,
    AGG_UNIT,
    FILTER_SETTINGS,
} from 'constants/reducerKeys'
import {
    // mounting
    mountProductPageRequest,
    mountProductPageSuccess,
    mountProductPageFailure,
    unmountProductPage,

    // page data
    fetchProductPageDataRequest,
    fetchProductPageDataSuccess,
    fetchProductPageDataFailure,

    // toggle details
    toggleProductPageDetails,

    // update product details
    updateProductPageProductDetailsRequest,
    updateProductPageProductDetailsSuccess,
    updateProductPageProductDetailsFailure,

    // aggregate data
    fetchProductPageAggregateSuccess,

    // timeseries data
    fetchProductPageSponsoredProductTimeseriesSuccess,
    fetchProductPageHeadlineSearchTimeseriesSuccess,

    // timeseries download
    downloadProductPageTimeseriesSuccess,
    downloadProductPageTimeseriesRequest,
    downloadProductPageTimeseriesFailure,
} from 'actions/ui'
import { DATES, AGGREGATION } from 'constants/filters'

import {
    defaultAmsCharts,
    defaultAggregate,
    defaultTimeseries,
    defaultDatesFilter,
} from '../defaults'

export const defaultState = {
    [FILTERS]: {
        [AGGREGATION]: AGG_UNIT.DAY,
        [DATES]: defaultDatesFilter,
    },
    [FILTER_SETTINGS]: {
        order: [AGGREGATION, DATES],
        displayState: {
            [AGGREGATION]: true,
            [DATES]: true,
        },
    },

    productId: null,
    [CHARTS]: defaultAmsCharts,
    showDetails: true,
    aggregate: defaultAggregate,
    timeseries: defaultTimeseries,
    mounting: true,
    error: null,
    downloading: false,
    productUpdating: false,
}

export default handleActions(
    {
        // mounting
        [mountProductPageRequest](state, action) {
            const { productId } = action.payload

            return flow(
                set(['productId'], productId),
                set(['mounting'], true)
            )(state)
        },
        [mountProductPageSuccess](state) {
            return set(['mounting'], false, state)
        },
        [mountProductPageFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['mounting'], false),
                set(['error'], message)
            )(state)
        },
        [unmountProductPage](state) {
            return flow(
                set(['productId'], null),
                set(['aggregate'], defaultAggregate),
                set(['timeseries'], defaultTimeseries)
            )(state)
        },

        // page data
        [fetchProductPageDataRequest](state) {
            return flow(
                set(['aggregate', 'loading'], true),
                set(['timeseries', 'loading'], true)
            )(state)
        },
        [fetchProductPageDataSuccess](state) {
            return flow(
                set(['aggregate', 'loading'], false),
                set(['timeseries', 'loading'], false)
            )(state)
        },
        [fetchProductPageDataFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['aggregate', 'loading'], false),
                set(['timeseries', 'loading'], false),
                set(['error'], message)
            )(state)
        },

        // toggle product details
        [toggleProductPageDetails](state) {
            return set(['showDetails'], !state.showDetails, state)
        },

        // product details
        [updateProductPageProductDetailsRequest](state) {
            return set(['productUpdating'], true, state)
        },
        [updateProductPageProductDetailsSuccess](state) {
            return set(['productUpdating'], false, state)
        },
        [updateProductPageProductDetailsFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['productUpdating'], false),
                set(['error'], message)
            )(state)
        },

        // aggregate
        [fetchProductPageAggregateSuccess](state, action) {
            const results = action.payload
            return set(['aggregate', 'data'], results, state)
        },

        // timeseries data
        [fetchProductPageSponsoredProductTimeseriesSuccess](state, action) {
            const results = action.payload
            return set(['timeseries', SPONSORED_PRODUCT], results, state)
        },
        [fetchProductPageHeadlineSearchTimeseriesSuccess](state, action) {
            const results = action.payload
            return set(['timeseries', HEADLINE_SEARCH], results, state)
        },

        // timeseries download
        [downloadProductPageTimeseriesSuccess](state) {
            return set(['downloading'], false, state)
        },
        [downloadProductPageTimeseriesRequest](state) {
            return set(['downloading'], true, state)
        },
        [downloadProductPageTimeseriesFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['downloading'], false),
                set(['error'], message)
            )(state)
        },
    },
    cloneDeep(defaultState) // create clone, so the defaultState is not mutated
)
