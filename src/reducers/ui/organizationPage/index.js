import { handleActions, combineActions } from 'redux-actions'
import set from 'lodash/fp/set'
import flow from 'lodash/fp/flow'
import merge from 'lodash/fp/merge'
import cloneDeep from 'lodash/cloneDeep'

import {
    // mounting
    mountOrganizationPageRequest,
    mountOrganizationPageSuccess,
    mountOrganizationPageFailure,
    unmountOrganizationPage,

    // toggle details
    toggleOrganizationPageDetails,

    // groups table
    fetchOrganizationPageGroupsTableRequest,
    fetchOrganizationPageGroupsTableSuccess,
    createOrganizationPageGroupRequest,
    createOrganizationPageGroupSuccess,
    createOrganizationPageGroupFailure,
    updateOrganizationPageGroupsTablePagination,

    // members table
    fetchOrganizationPageMembersTableSuccess,
    inviteMemberOrganizationPageRequest,
    inviteMemberOrganizationPageSuccess,
    inviteMemberOrganizationPageFailure,
    fetchOrganizationPageMembersTableRequest,
    updateOrganizationPageMembersTablePagination,
    removeMemberOrganizationPageRequest,

    // invitations table
    fetchOrganizationPageInvitationsTableRequest,
    fetchOrganizationPageInvitationsTableSuccess,
    updateOrganizationPageInvitationsTablePagination,
    updateOrganizationPageInvitationsTableSorter,

    // integrations table
    fetchOrganizationPageIntegrationsTableRequest,
    fetchOrganizationPageIntegrationsTableSuccess,
    updateOrganizationPageIntegrationsTablePagination,
    createOrganizationPageIntegrationRequest,
    createOrganizationPageIntegrationSuccess,
    createOrganizationPageIntegrationFailure,

    // brands search
    searchOrganizationPageBrandsRequest,
    searchOrganizationPageBrandsSuccess,
    searchOrganizationPageBrandsFailure,
    removeMemberOrganizationPageSuccess,
    removeMemberOrganizationPageFailure,
} from 'actions/ui'

import { getDefaultTable } from '../defaults'

export const defaultState = {
    mounting: true,
    error: null,
    inviteMemberError: null,
    createIntegrationError: null,
    organizationId: null,
    showDetails: true,
    brandSearchLoading: false,
    groupsTable: {
        ...getDefaultTable({
            order: ['name', 'resources', 'memberCount'],
            displayState: { name: true, resources: true, memberCount: true },
        }),
        sorter: { field: 'created_date', order: 'descend' },
    },
    membersTable: {
        ...getDefaultTable({
            order: ['email'],
            displayState: { email: true },
            actionColumns: ['actions'],
        }),
        sorter: { field: 'date_joined', order: 'descend' },
        deletingMemberId: null,
    },
    invitationsTable: {
        ...getDefaultTable({
            order: ['email', 'status', 'inviteDate'],
            displayState: { email: true, status: true, inviteDate: true },
        }),
        sorter: { field: 'created_date', order: 'descend' },
    },
    integrationsTable: {
        ...getDefaultTable({
            order: ['description', 'state', 'created'],
            displayState: { description: true, state: true, created: true },
        }),
        sorter: { field: 'created_at', order: 'descend' },
    },
}

export default handleActions(
    {
        // mounting
        [mountOrganizationPageRequest](state, action) {
            const { organizationId } = action.payload
            return flow(
                set(['organizationId'], organizationId),
                set(['mounting'], true)
            )(state)
        },
        [mountOrganizationPageSuccess](state) {
            return set(['mounting'], false, state)
        },
        [mountOrganizationPageFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['mounting'], false),
                set(['error'], message)
            )(state)
        },
        [unmountOrganizationPage]() {
            return defaultState
        },

        // toggle details
        [toggleOrganizationPageDetails](state) {
            return set(['showDetails'], !state.showDetails, state)
        },

        // groups table data
        [fetchOrganizationPageGroupsTableRequest](state) {
            return set(['groupsTable', 'loading'], true, state)
        },
        [fetchOrganizationPageGroupsTableSuccess](state, action) {
            const { count, results } = action.payload
            return flow(
                set(['groupsTable', 'loading'], false),
                set(['groupsTable', 'pagination', 'total'], count),
                set(['groupsTable', 'data'], results)
            )(state)
        },

        // groups table controls
        [updateOrganizationPageGroupsTablePagination](state, action) {
            const pagination = action.payload
            return merge(state, {
                groupsTable: {
                    pagination,
                },
            })
        },

        // groups table create
        [createOrganizationPageGroupRequest](state) {
            return set(['groupsTable', 'attaching'], true, state)
        },
        [createOrganizationPageGroupSuccess](state) {
            return set(['groupsTable', 'attaching'], false, state)
        },
        [createOrganizationPageGroupFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['groupsTable', 'attaching'], false),
                set(['error'], message)
            )(state)
        },

        // members table data
        [fetchOrganizationPageMembersTableRequest](state) {
            return set(['membersTable', 'loading'], true, state)
        },
        [fetchOrganizationPageMembersTableSuccess](state, action) {
            const { count, results } = action.payload
            return flow(
                set(['membersTable', 'loading'], false),
                set(['membersTable', 'pagination', 'total'], count),
                set(['membersTable', 'data'], results)
            )(state)
        },

        // members table controls
        [updateOrganizationPageMembersTablePagination](state, action) {
            const pagination = action.payload
            return merge(state, {
                membersTable: {
                    pagination,
                },
            })
        },

        // remove member
        [removeMemberOrganizationPageRequest](state, action) {
            return flow(
                set(['membersTable', 'deleting'], true),
                set(
                    ['membersTable', 'deletingMemberId'],
                    action.payload.memberId
                )
            )(state)
        },
        [combineActions(
            removeMemberOrganizationPageSuccess,
            removeMemberOrganizationPageFailure
        )](state) {
            return flow(
                set(['membersTable', 'deleting'], false),
                set(['membersTable', 'deletingMemberId'], null)
            )(state)
        },

        // invitations table invite
        [inviteMemberOrganizationPageRequest](state) {
            return set(['membersTable', 'attaching'], true, state)
        },
        [inviteMemberOrganizationPageSuccess](state) {
            return set(['membersTable', 'attaching'], false, state)
        },
        [inviteMemberOrganizationPageFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['membersTable', 'attaching'], false),
                set(['inviteMemberError'], message)
            )(state)
        },

        // invitations table data
        [fetchOrganizationPageInvitationsTableRequest](state) {
            return set(['invitationsTable', 'loading'], true, state)
        },
        [fetchOrganizationPageInvitationsTableSuccess](state, action) {
            const { count, results } = action.payload
            return flow(
                set(['invitationsTable', 'loading'], false),
                set(['invitationsTable', 'pagination', 'total'], count),
                set(['invitationsTable', 'data'], results)
            )(state)
        },

        // invitations table controls
        [updateOrganizationPageInvitationsTablePagination](state, action) {
            const pagination = action.payload
            return merge(state, {
                invitationsTable: {
                    pagination,
                },
            })
        },
        [updateOrganizationPageInvitationsTableSorter](state, action) {
            const sorter = action.payload
            return set(['invitationsTable', 'sorter'], sorter, state)
        },

        // integrations table
        [fetchOrganizationPageIntegrationsTableRequest](state) {
            return set(['integrationsTable', 'loading'], true, state)
        },
        [fetchOrganizationPageIntegrationsTableSuccess](state, action) {
            const { count, results } = action.payload
            return flow(
                set(['integrationsTable', 'loading'], false),
                set(['integrationsTable', 'pagination', 'total'], count),
                set(['integrationsTable', 'data'], results)
            )(state)
        },

        // integrations table controls
        [updateOrganizationPageIntegrationsTablePagination](state, action) {
            const pagination = action.payload
            return merge(state, {
                integrationsTable: {
                    pagination,
                },
            })
        },

        // integrations table create
        [createOrganizationPageIntegrationRequest](state) {
            return set(['integrationsTable', 'attaching'], true, state)
        },
        [createOrganizationPageIntegrationSuccess](state) {
            return set(['integrationsTable', 'attaching'], false, state)
        },
        [createOrganizationPageIntegrationFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['integrationsTable', 'attaching'], false),
                set(['createIntegrationError'], message)
            )(state)
        },

        // search brands
        [searchOrganizationPageBrandsRequest](state) {
            return set(['brandSearchLoading'], true, state)
        },
        [searchOrganizationPageBrandsSuccess](state) {
            return set(['brandSearchLoading'], false, state)
        },
        [searchOrganizationPageBrandsFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['brandSearchLoading'], false),
                set(['error'], message)
            )(state)
        },
    },
    cloneDeep(defaultState)
)
