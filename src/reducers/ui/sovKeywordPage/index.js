import { handleActions, combineActions } from 'redux-actions'
import flow from 'lodash/fp/flow'
import merge from 'lodash/fp/merge'
import set from 'lodash/fp/set'
import cloneDeep from 'lodash/cloneDeep'

import { getResultTypeObject } from 'helpers/resultTypes'
import { HEADLINE_SECTION, SPONSORED, ORGANIC } from 'constants/resultTypes'
import {
    SOV_SEARCH_TIMES,
    SOV_RESULT_TYPES,
    SOV_FOLDS,
    SOV_AGGREGATION,
} from 'constants/filters'
import {
    SOV_CHART,
    FILTERS,
    FILTER_SETTINGS,
    SOV_AGGREGATION_UNIT,
} from 'constants/reducerKeys'
import {
    // mounting
    mountSovKeywordPageRequest,
    mountSovKeywordPageSuccess,
    mountSovKeywordPageFailure,
    unmountSovKeywordPage,

    // page data
    fetchSovKeywordPageDataRequest,
    fetchSovKeywordPageDataSuccess,
    fetchSovKeywordPageDataFailure,

    // toggle details
    toggleSovKeywordPageDetails,

    // update keyword details
    updateSovKeywordPageKeywordDetailsRequest,
    updateSovKeywordPageKeywordDetailsSuccess,
    updateSovKeywordPageKeywordDetailsFailure,

    // table data
    fetchSovKeywordPageTableRequest,
    fetchSovKeywordPageTableSuccess,
    fetchSovKeywordPageTableFailure,

    // table controls
    updateSovKeywordPageTablePagination,
    updateSovKeywordPageTableSorter,

    // table settings
    updateSovKeywordPageTableSettings,
    fetchSovKeywordPageTableSettingsSuccess,

    // table download
    downloadSovKeywordPageTableRequest,
    downloadSovKeywordPageTableSuccess,
    downloadSovKeywordPageTableFailure,

    // sov chart
    fetchSovKeywordPageSovChartSuccess,
    updateSovKeywordPageSovChartWeight,
} from 'actions/ui'

import {
    getDefaultTable,
    defaultSearchTimesFilter,
    defaultSovChart,
} from '../defaults'

const defaultTable = {
    ...getDefaultTable({
        actionColumns: [],
        order: [
            'scheduled_date',
            'search_time',
            'country_code',
            'language_code',
            'text',
            'sponsored_result_count',
            'result_count',
        ],
        displayState: {
            scheduled_date: true,
            search_time: true,
            country_code: true,
            language_code: true,
            text: true,
            sponsored_result_count: true,
            result_count: true,
        },
    }),
    sorter: {
        // Order by scheduled_date in descending order by default
        field: 'scheduled_date',
        order: 'descend',
        filterPositiveValues: true,
    },
}

export const defaultState = {
    [FILTERS]: {
        [SOV_AGGREGATION]: SOV_AGGREGATION_UNIT.DAY,
        [SOV_SEARCH_TIMES]: defaultSearchTimesFilter,
        [SOV_FOLDS]: {},
        // default to all filters except HEADLINE because the charts should never include HEADLINE results
        [SOV_RESULT_TYPES]: [
            getResultTypeObject(HEADLINE_SECTION),
            getResultTypeObject(SPONSORED),
            getResultTypeObject(ORGANIC),
        ],
    },
    [FILTER_SETTINGS]: {
        order: [SOV_AGGREGATION, SOV_SEARCH_TIMES, SOV_FOLDS, SOV_RESULT_TYPES],
        displayState: {
            [SOV_AGGREGATION]: true,
            [SOV_SEARCH_TIMES]: true,
            [SOV_RESULT_TYPES]: true,
            [SOV_FOLDS]: true,
        },
    },

    sovKeywordId: null,
    table: defaultTable,
    [SOV_CHART]: defaultSovChart,
    mounting: true,
    error: null,
    downloading: false,
    showDetails: true,
    keywordUpdating: false,
}

export default handleActions(
    {
        // mounting
        [mountSovKeywordPageRequest](state, action) {
            const { sovKeywordId } = action.payload
            return flow(
                set(['sovKeywordId'], sovKeywordId),
                set(['mounting'], true)
            )(state)
        },
        [mountSovKeywordPageSuccess](state) {
            return set(['mounting'], false, state)
        },
        [mountSovKeywordPageFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['mounting'], false),
                set(['error'], message)
            )(state)
        },
        [unmountSovKeywordPage](state) {
            return flow(
                set(['sovKeywordId'], null),
                set(['table'], defaultTable),
                set([SOV_CHART], defaultSovChart)
            )(state)
        },

        // page data
        [fetchSovKeywordPageDataRequest](state) {
            return flow(
                set(['table', 'loading'], true),
                set([SOV_CHART, 'loading'], true)
            )(state)
        },
        [fetchSovKeywordPageDataSuccess](state) {
            return flow(
                set(['table', 'loading'], false),
                set([SOV_CHART, 'loading'], false)
            )(state)
        },
        [fetchSovKeywordPageDataFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['table', 'loading'], false),
                set([SOV_CHART, 'loading'], false),
                set(['error'], message)
            )(state)
        },

        // toggle keyword details
        [toggleSovKeywordPageDetails](state) {
            return set(['showDetails'], !state.showDetails, state)
        },

        // keyword details
        [updateSovKeywordPageKeywordDetailsRequest](state) {
            return set(['keywordUpdating'], true, state)
        },
        [updateSovKeywordPageKeywordDetailsSuccess](state) {
            return set(['keywordUpdating'], false, state)
        },
        [updateSovKeywordPageKeywordDetailsFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['keywordUpdating'], false),
                set(['error'], message)
            )(state)
        },

        // table data
        [fetchSovKeywordPageTableRequest](state) {
            return set(['table', 'loading'], true, state)
        },
        [fetchSovKeywordPageTableSuccess](state, action) {
            const { count, results } = action.payload
            return flow(
                set(['table', 'loading'], false),
                set(['table', 'data'], results),
                set(['table', 'pagination', 'total'], count)
            )(state)
        },
        [fetchSovKeywordPageTableFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['table', 'loading'], false),
                set(['error'], message)
            )(state)
        },

        // table controls
        [updateSovKeywordPageTablePagination](state, action) {
            const pagination = action.payload
            return merge(state, {
                table: {
                    pagination,
                },
            })
        },
        [updateSovKeywordPageTableSorter](state, action) {
            const sorter = action.payload
            return set(['table', 'sorter'], sorter, state)
        },

        // table settings
        [combineActions(
            updateSovKeywordPageTableSettings,
            fetchSovKeywordPageTableSettingsSuccess
        )](state, action) {
            const columnSettings = action.payload
            return set(['table', 'columnSettings'], columnSettings, state)
        },

        // table download
        [downloadSovKeywordPageTableRequest](state) {
            return set(['downloading'], true, state)
        },
        [downloadSovKeywordPageTableSuccess](state) {
            return set(['downloading'], false, state)
        },
        [downloadSovKeywordPageTableFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['downloading'], false),
                set(['error'], message)
            )(state)
        },

        // sov chart
        [fetchSovKeywordPageSovChartSuccess](state, action) {
            const { results, count } = action.payload
            return flow(
                set([SOV_CHART, 'data'], results),
                set([SOV_CHART, 'pagination', 'total'], count)
            )(state)
        },
        [updateSovKeywordPageSovChartWeight](state, action) {
            const weight = action.payload
            return set([SOV_CHART, 'weight'], weight, state)
        },
    },
    cloneDeep(defaultState) // create clone, so the defaultState is not mutated
)
