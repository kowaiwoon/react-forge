import { handleActions, combineActions } from 'redux-actions'
import flow from 'lodash/fp/flow'
import merge from 'lodash/fp/merge'
import set from 'lodash/fp/set'
import cloneDeep from 'lodash/cloneDeep'

import { getResultTypeObject } from 'helpers/resultTypes'
import { SPONSORED } from 'constants/resultTypes'
import {
    FILTERS,
    FILTER_SETTINGS,
    SOV_AGGREGATION_UNIT,
    SOV_CHART,
} from 'constants/reducerKeys'
import {
    LESS_THAN_OR_EQUAL,
    SOV_AGGREGATION,
    SOV_COUNTRIES,
    SOV_FOLDS,
    SOV_KEYWORDS,
    SOV_KEYWORD_CATEGORIES,
    SOV_LANGUAGES,
    SOV_RESULT_TYPES,
    SOV_SEARCH_TIMES,
    SOV_STATES,
} from 'constants/filters'
import {
    // mounting
    mountSovKeywordsSummaryPageRequest,
    mountSovKeywordsSummaryPageSuccess,
    mountSovKeywordsSummaryPageFailure,
    unmountSovKeywordsSummaryPage,

    // page data
    fetchSovKeywordsSummaryPageDataRequest,
    fetchSovKeywordsSummaryPageDataSuccess,
    fetchSovKeywordsSummaryPageDataFailure,

    // table data
    fetchSovKeywordsSummaryPageTableRequest,
    fetchSovKeywordsSummaryPageTableSuccess,
    fetchSovKeywordsSummaryPageTableFailure,

    // table controls
    updateSovKeywordsSummaryPageTablePagination,
    updateSovKeywordsSummaryPageTableSorter,

    // table settings
    updateSovKeywordsSummaryPageTableSettings,
    fetchSovKeywordsSummaryPageTableSettingsSuccess,

    // table download
    downloadSovKeywordsSummaryPageTableRequest,
    downloadSovKeywordsSummaryPageTableSuccess,
    downloadSovKeywordsSummaryPageTableFailure,

    // table attach keyword
    attachSovKeywordsSummaryPageTableKeywordsRequest,
    attachSovKeywordsSummaryPageTableKeywordsSuccess,
    attachSovKeywordsSummaryPageTableKeywordsFailure,

    // table update keyword
    updateSovKeywordsSummaryPageTableKeywordRequest,
    updateSovKeywordsSummaryPageTableKeywordSuccess,
    updateSovKeywordsSummaryPageTableKeywordFailure,

    // table delete keyword
    deleteSovKeywordsSummaryPageTableKeywordRequest,
    deleteSovKeywordsSummaryPageTableKeywordSuccess,
    deleteSovKeywordsSummaryPageTableKeywordFailure,

    // search brands
    searchSovBrandsRequest,
    searchSovBrandsSuccess,
    searchSovBrandsFailure,

    // search keywords
    searchSovKeywordsRequest,
    searchSovKeywordsSuccess,
    searchSovKeywordsFailure,

    // search keyword categories
    searchSovKeywordCategoriesRequest,
    searchSovKeywordCategoriesSuccess,
    searchSovKeywordCategoriesFailure,

    // sov chart
    fetchSovKeywordsSummaryPageSovChartSuccess,
    updateSovKeywordsSummaryPageSovChartWeight,
} from 'actions/ui'

import {
    getDefaultTable,
    defaultSearchTimesFilter,
    defaultSovChart,
} from '../defaults'

const defaultTable = {
    ...getDefaultTable({
        actionColumns: ['actions'],
        order: [
            'text',
            'country_code',
            'language_code',
            'frequency_in_hours',
            'created_at',
            'state',
            'category',
        ],
        displayState: {
            text: true,
            country_code: true,
            language_code: true,
            frequency_in_hours: true,
            created_at: true,
            state: true,
            category: false,
        },
    }),
    sorter: {
        // Order by created_at in descending order by default
        field: 'created_at',
        order: 'descend',
        filterPositiveValues: true,
    },
}

export const defaultState = {
    [FILTERS]: {
        [SOV_AGGREGATION]: SOV_AGGREGATION_UNIT.DAY,
        [SOV_SEARCH_TIMES]: defaultSearchTimesFilter,
        [SOV_FOLDS]: {
            operator: LESS_THAN_OR_EQUAL,
            value: 2,
        },
        [SOV_RESULT_TYPES]: [getResultTypeObject(SPONSORED)],
        [SOV_COUNTRIES]: [],
        [SOV_KEYWORDS]: [],
        [SOV_KEYWORD_CATEGORIES]: [],
        [SOV_LANGUAGES]: [],
        [SOV_STATES]: [],
    },
    [FILTER_SETTINGS]: {
        order: [
            SOV_AGGREGATION,
            SOV_SEARCH_TIMES,
            SOV_FOLDS,
            SOV_RESULT_TYPES,
            SOV_COUNTRIES,
            SOV_KEYWORDS,
            SOV_KEYWORD_CATEGORIES,
            SOV_LANGUAGES,
            SOV_STATES,
        ],
        displayState: {
            [SOV_AGGREGATION]: true,
            [SOV_SEARCH_TIMES]: true,
            [SOV_FOLDS]: true,
            [SOV_RESULT_TYPES]: true,
            [SOV_COUNTRIES]: true,
            [SOV_KEYWORDS]: false,
            [SOV_KEYWORD_CATEGORIES]: false,
            [SOV_LANGUAGES]: false,
            [SOV_STATES]: false,
        },
    },

    table: defaultTable,
    [SOV_CHART]: defaultSovChart,
    mounting: true,
    error: null,
    downloading: false,
    sovBrandsFilterLoading: false,
    sovKeywordsFilterLoading: false,
    sovKeywordCategoriesFilterLoading: false,
}

export default handleActions(
    {
        // mounting
        [mountSovKeywordsSummaryPageRequest](state) {
            return set(['mounting'], true, state)
        },
        [mountSovKeywordsSummaryPageSuccess](state) {
            return set(['mounting'], false, state)
        },
        [mountSovKeywordsSummaryPageFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['mounting'], false),
                set(['error'], message)
            )(state)
        },
        [unmountSovKeywordsSummaryPage](state) {
            return flow(
                set(['table'], defaultTable),
                set([SOV_CHART], defaultSovChart)
            )(state)
        },

        // page data
        [fetchSovKeywordsSummaryPageDataRequest](state) {
            return flow(
                set(['table', 'loading'], true),
                set([SOV_CHART, 'loading'], true)
            )(state)
        },
        [fetchSovKeywordsSummaryPageDataSuccess](state) {
            return flow(
                set(['table', 'loading'], false),
                set([SOV_CHART, 'loading'], false)
            )(state)
        },
        [fetchSovKeywordsSummaryPageDataFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['table', 'loading'], false),
                set([SOV_CHART, 'loading'], false),
                set(['error'], message)
            )(state)
        },

        // table data
        [fetchSovKeywordsSummaryPageTableRequest](state) {
            return set(['table', 'loading'], true, state)
        },
        [fetchSovKeywordsSummaryPageTableSuccess](state, action) {
            const { count, results } = action.payload
            return flow(
                set(['table', 'loading'], false),
                set(['table', 'data'], results),
                set(['table', 'pagination', 'total'], count)
            )(state)
        },
        [fetchSovKeywordsSummaryPageTableFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['table', 'loading'], false),
                set(['error'], message)
            )(state)
        },

        // table controls
        [updateSovKeywordsSummaryPageTablePagination](state, action) {
            const pagination = action.payload
            return merge(state, {
                table: {
                    pagination,
                },
            })
        },
        [updateSovKeywordsSummaryPageTableSorter](state, action) {
            const sorter = action.payload
            return set(['table', 'sorter'], sorter, state)
        },

        // table settings
        [combineActions(
            updateSovKeywordsSummaryPageTableSettings,
            fetchSovKeywordsSummaryPageTableSettingsSuccess
        )](state, action) {
            const columnSettings = action.payload
            return set(['table', 'columnSettings'], columnSettings, state)
        },

        // table download
        [downloadSovKeywordsSummaryPageTableRequest](state) {
            return set(['downloading'], true, state)
        },
        [downloadSovKeywordsSummaryPageTableSuccess](state) {
            return set(['downloading'], false, state)
        },
        [downloadSovKeywordsSummaryPageTableFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['downloading'], false),
                set(['error'], message)
            )(state)
        },

        // table attach keyword
        [attachSovKeywordsSummaryPageTableKeywordsRequest](state) {
            return set(['table', 'attaching'], true, state)
        },
        [attachSovKeywordsSummaryPageTableKeywordsSuccess](state) {
            return set(['table', 'attaching'], false, state)
        },
        [attachSovKeywordsSummaryPageTableKeywordsFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['table', 'attaching'], false),
                set(['error'], message)
            )(state)
        },

        // table update keyword
        [updateSovKeywordsSummaryPageTableKeywordRequest](state) {
            return set(['table', 'updating'], true, state)
        },
        [updateSovKeywordsSummaryPageTableKeywordSuccess](state) {
            return set(['table', 'updating'], false, state)
        },
        [updateSovKeywordsSummaryPageTableKeywordFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['table', 'updating'], false),
                set(['error'], message)
            )(state)
        },

        // table delete keyword
        [deleteSovKeywordsSummaryPageTableKeywordRequest](state) {
            return set(['table', 'deleting'], true, state)
        },
        [deleteSovKeywordsSummaryPageTableKeywordSuccess](state) {
            return set(['table', 'deleting'], false, state)
        },
        [deleteSovKeywordsSummaryPageTableKeywordFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['table', 'deleting'], false),
                set(['error'], message)
            )(state)
        },

        // search sov brands
        [searchSovBrandsRequest](state) {
            return set(['sovBrandsFilterLoading'], true, state)
        },
        [combineActions(searchSovBrandsSuccess, searchSovBrandsFailure)](
            state
        ) {
            return set(['sovBrandsFilterLoading'], false, state)
        },

        // search sov keywords
        [searchSovKeywordsRequest](state) {
            return set(['sovKeywordsFilterLoading'], true, state)
        },
        [combineActions(searchSovKeywordsSuccess, searchSovKeywordsFailure)](
            state
        ) {
            return set(['sovKeywordsFilterLoading'], false, state)
        },

        // search sov keyword categories
        [searchSovKeywordCategoriesRequest](state) {
            return set(['sovKeywordCategoriesFilterLoading'], true, state)
        },
        [combineActions(
            searchSovKeywordCategoriesSuccess,
            searchSovKeywordCategoriesFailure
        )](state) {
            return set(['sovKeywordCategoriesFilterLoading'], false, state)
        },

        // sov chart
        [fetchSovKeywordsSummaryPageSovChartSuccess](state, action) {
            const { results, count } = action.payload
            return flow(
                set([SOV_CHART, 'data'], results),
                set([SOV_CHART, 'pagination', 'total'], count)
            )(state)
        },
        [updateSovKeywordsSummaryPageSovChartWeight](state, action) {
            const weight = action.payload
            return set([SOV_CHART, 'weight'], weight, state)
        },
    },
    cloneDeep(defaultState) // create clone, so the defaultState is not mutated
)
