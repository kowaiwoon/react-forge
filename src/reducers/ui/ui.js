import { combineReducers } from 'redux'

import {
    HOME_PAGE,
    BRANDS_SUMMARY_PAGE,
    BRAND_PAGE,
    CAMPAIGNS_SUMMARY_PAGE,
    CAMPAIGN_PAGE,
    PRODUCTS_SUMMARY_PAGE,
    PRODUCT_PAGE,
    KEYWORDS_SUMMARY_PAGE,
    KEYWORD_PAGE,
    LABELS_SUMMARY_PAGE,
    SOV_KEYWORDS_SUMMARY_PAGE,
    SOV_KEYWORD_PAGE,
    SOV_KEYWORD_SEARCH_RESULT_PAGE,
    PROFILE_PAGE,
    ORGANIZATION_PAGE,
    ORGANIZATION_GROUP_PAGE,
    AUTOMATION_PAGE,
} from 'constants/pages'

// Common UI reducer
import app from './app'

// Page specific reducers
import homePage from './homePage'
import brandsSummary from './brandsSummary'
import brandPage from './brandPage'
import campaignPage from './campaignPage'
import campaignsSummary from './campaignsSummary'
import productsSummary from './productsSummary'
import productPage from './productPage'
import keywordsSummary from './keywordsSummary'
import keywordPage from './keywordPage'
import labelsSummaryPage from './labelsSummary'
import sovKeywordsSummary from './sovKeywordsSummary'
import sovKeywordPage from './sovKeywordPage'
import sovKeywordSearchResultPage from './sovKeywordSearchResultPage'
import profilePage from './profilePage'
import organizationPage from './organizationPage'
import organizationGroupPage from './organizationGroupPage'
import automationPage from './automationPage'

const reducer = combineReducers({
    app,
    [HOME_PAGE]: homePage,
    [BRANDS_SUMMARY_PAGE]: brandsSummary,
    [BRAND_PAGE]: brandPage,
    [CAMPAIGNS_SUMMARY_PAGE]: campaignsSummary,
    [CAMPAIGN_PAGE]: campaignPage,
    [PRODUCTS_SUMMARY_PAGE]: productsSummary,
    [PRODUCT_PAGE]: productPage,
    [KEYWORDS_SUMMARY_PAGE]: keywordsSummary,
    [KEYWORD_PAGE]: keywordPage,
    [LABELS_SUMMARY_PAGE]: labelsSummaryPage,
    [SOV_KEYWORDS_SUMMARY_PAGE]: sovKeywordsSummary,
    [SOV_KEYWORD_PAGE]: sovKeywordPage,
    [SOV_KEYWORD_SEARCH_RESULT_PAGE]: sovKeywordSearchResultPage,
    [PROFILE_PAGE]: profilePage,
    [ORGANIZATION_PAGE]: organizationPage,
    [ORGANIZATION_GROUP_PAGE]: organizationGroupPage,
    [AUTOMATION_PAGE]: automationPage,
})

export const defaultState = reducer(undefined, {})

export default reducer
