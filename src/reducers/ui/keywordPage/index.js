import { handleActions } from 'redux-actions'
import flow from 'lodash/fp/flow'
import set from 'lodash/fp/set'
import cloneDeep from 'lodash/cloneDeep'

import {
    FILTERS,
    CHARTS,
    FILTER_SETTINGS,
    AGG_UNIT,
} from 'constants/reducerKeys'
import { SPONSORED_PRODUCT, HEADLINE_SEARCH } from 'constants/factTypes'
import { DATES, AGGREGATION } from 'constants/filters'
import {
    // mounting
    mountKeywordPageRequest,
    mountKeywordPageSuccess,
    mountKeywordPageFailure,
    unmountKeywordPage,

    // page data
    fetchKeywordPageDataRequest,
    fetchKeywordPageDataSuccess,
    fetchKeywordPageDataFailure,

    // toggle details
    toggleKeywordPageDetails,

    // update keyword details
    updateKeywordPageKeywordDetailsRequest,
    updateKeywordPageKeywordDetailsSuccess,
    updateKeywordPageKeywordDetailsFailure,

    // aggregate data
    fetchKeywordPageAggregateSuccess,

    // timeseries data
    fetchKeywordPageSponsoredProductTimeseriesSuccess,
    fetchKeywordPageHeadlineSearchTimeseriesSuccess,

    // timeseries download
    downloadKeywordPageTimeseriesRequest,
    downloadKeywordPageTimeseriesSuccess,
    downloadKeywordPageTimeseriesFailure,
} from 'actions/ui'

import {
    defaultAmsCharts,
    defaultAggregate,
    defaultTimeseries,
    defaultDatesFilter,
} from '../defaults'

export const defaultState = {
    [FILTERS]: {
        [AGGREGATION]: AGG_UNIT.DAY,
        [DATES]: defaultDatesFilter,
    },
    [FILTER_SETTINGS]: {
        order: [AGGREGATION, DATES],
        displayState: {
            [AGGREGATION]: true,
            [DATES]: true,
        },
    },

    keywordId: null,
    [CHARTS]: defaultAmsCharts,
    showDetails: true,
    aggregate: defaultAggregate,
    timeseries: defaultTimeseries,
    mounting: true,
    error: null,
    downloading: false,
    keywordUpdating: false,
}

export default handleActions(
    {
        // mounting
        [mountKeywordPageRequest](state, action) {
            const { keywordId } = action.payload
            return flow(
                set(['keywordId'], keywordId),
                set(['mounting'], true)
            )(state)
        },
        [mountKeywordPageSuccess](state) {
            return set(['mounting'], false, state)
        },
        [mountKeywordPageFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['mounting'], false),
                set(['error'], message)
            )(state)
        },
        [unmountKeywordPage](state) {
            return flow(
                set(['keywordId'], null),
                set(['aggregate'], defaultAggregate),
                set(['timeseries'], defaultTimeseries)
            )(state)
        },

        // page data
        [fetchKeywordPageDataRequest](state) {
            return flow(
                set(['aggregate', 'loading'], true),
                set(['timeseries', 'loading'], true)
            )(state)
        },
        [fetchKeywordPageDataSuccess](state) {
            return flow(
                set(['aggregate', 'loading'], false),
                set(['timeseries', 'loading'], false)
            )(state)
        },
        [fetchKeywordPageDataFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['aggregate', 'loading'], false),
                set(['timeseries', 'loading'], false),
                set(['error'], message)
            )(state)
        },

        // toggle keyword details
        [toggleKeywordPageDetails](state) {
            return set(['showDetails'], !state.showDetails, state)
        },

        // keyword details
        [updateKeywordPageKeywordDetailsRequest](state) {
            return set(['keywordUpdating'], true, state)
        },
        [updateKeywordPageKeywordDetailsSuccess](state) {
            return set(['keywordUpdating'], false, state)
        },
        [updateKeywordPageKeywordDetailsFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['keywordUpdating'], false),
                set(['error'], message)
            )(state)
        },

        // aggregate
        [fetchKeywordPageAggregateSuccess](state, action) {
            const results = action.payload
            return set(['aggregate', 'data'], results, state)
        },

        // timeseries data
        [fetchKeywordPageSponsoredProductTimeseriesSuccess](state, action) {
            const results = action.payload
            return set(['timeseries', SPONSORED_PRODUCT], results, state)
        },
        [fetchKeywordPageHeadlineSearchTimeseriesSuccess](state, action) {
            const results = action.payload
            return set(['timeseries', HEADLINE_SEARCH], results, state)
        },

        // timeseries download
        [downloadKeywordPageTimeseriesSuccess](state) {
            return set(['downloading'], false, state)
        },
        [downloadKeywordPageTimeseriesRequest](state) {
            return set(['downloading'], true, state)
        },
        [downloadKeywordPageTimeseriesFailure](state, action) {
            const { message } = action.payload
            return flow(
                set(['downloading'], false),
                set(['error'], message)
            )(state)
        },
    },
    cloneDeep(defaultState) // create clone, so the defaultState is not mutated
)
