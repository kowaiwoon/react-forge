import { call, put, select } from 'redux-saga/effects'

import cerebroApiSaga from 'sagas/common/cerebroApi'
import { fetchUserPermissionsSuccess } from 'actions/orgs'
import { getUserFeaturePermissionsForOrg } from 'services/cerebroApi'
import { selectOrganizationUserPermissions } from 'selectors/orgs'

export function* fetchUserPermissionsSaga(organizationId) {
    const userPermissions = yield select(
        selectOrganizationUserPermissions,
        organizationId
    )

    if (!userPermissions) {
        const { data: permissions } = yield call(
            cerebroApiSaga,
            null,
            getUserFeaturePermissionsForOrg,
            organizationId
        )
        yield put(fetchUserPermissionsSuccess({ permissions, organizationId }))
    }
}
