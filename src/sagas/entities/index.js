import { call, select } from 'redux-saga/effects'

import {
    getBrand,
    getCampaign,
    updateCampaign,
    deleteCampaign,
    getProduct,
    attachProduct,
    updateProduct,
    deleteProduct,
    getKeyword,
    attachKeyword,
    updateKeyword,
    deleteKeyword,
    getSovKeyword,
    attachSovKeyword,
    updateSovKeyword,
    deleteSovKeyword,
} from 'services/cerebroApi'
import {
    // Brand
    fetchBrandSuccess,

    // Campaign
    fetchCampaignSuccess,
    updateCampaignSuccess,
    deleteCampaignSuccess,

    // Product
    fetchProductSuccess,
    attachProductSuccess,
    updateProductSuccess,
    deleteProductSuccess,

    // Keyword
    fetchKeywordSuccess,
    attachKeywordSuccess,
    updateKeywordSuccess,
    deleteKeywordSuccess,

    // SOV Keyword
    fetchSovKeywordSuccess,
    attachSovKeywordSuccess,
    updateSovKeywordSuccess,
    deleteSovKeywordSuccess,
} from 'actions/entities'
import {
    selectBrand,
    selectCampaign,
    selectKeyword,
    selectProduct,
    selectSovKeyword,
} from 'selectors/entities'
import cerebroApiSaga from 'sagas/common/cerebroApi'

export function* fetchBrandSaga(brandId) {
    const brand = yield select(selectBrand, brandId)

    // Fetch brand if not fetched
    if (!brand) {
        yield call(cerebroApiSaga, fetchBrandSuccess, getBrand, brandId)
    }
}

export function* fetchCampaignSaga(campaignId) {
    const campaign = yield select(selectCampaign, campaignId)

    // Fetch campaign if not fetched
    if (!campaign) {
        yield call(
            cerebroApiSaga,
            fetchCampaignSuccess,
            getCampaign,
            campaignId
        )
    }
}

export function* updateCampaignSaga(campaignId, data) {
    yield call(
        cerebroApiSaga,
        updateCampaignSuccess,
        updateCampaign,
        campaignId,
        data
    )
}

export function* deleteCampaignSaga(campaignId) {
    yield call(
        cerebroApiSaga,
        deleteCampaignSuccess,
        deleteCampaign,
        campaignId
    )
}

export function* fetchProductSaga(productId) {
    const product = yield select(selectProduct, productId)

    // Fetch product if not fetched
    if (!product) {
        yield call(cerebroApiSaga, fetchProductSuccess, getProduct, productId)
    }
}

export function* attachProductSaga(data) {
    yield call(cerebroApiSaga, attachProductSuccess, attachProduct, data)
}

export function* updateProductSaga(productId, data) {
    yield call(
        cerebroApiSaga,
        updateProductSuccess,
        updateProduct,
        productId,
        data
    )
}

export function* deleteProductSaga(productId) {
    yield call(cerebroApiSaga, deleteProductSuccess, deleteProduct, productId)
}

export function* fetchKeywordSaga(keywordId) {
    const keyword = yield select(selectKeyword, keywordId)

    // Fetch keyword if not fetched
    if (!keyword) {
        yield call(cerebroApiSaga, fetchKeywordSuccess, getKeyword, keywordId)
    }
}

export function* attachKeywordSaga(data) {
    yield call(cerebroApiSaga, attachKeywordSuccess, attachKeyword, data)
}

export function* updateKeywordSaga(keywordId, data) {
    yield call(
        cerebroApiSaga,
        updateKeywordSuccess,
        updateKeyword,
        keywordId,
        data
    )
}

export function* deleteKeywordSaga(keywordId) {
    yield call(cerebroApiSaga, deleteKeywordSuccess, deleteKeyword, keywordId)
}

export function* fetchSovKeywordSaga(sovKeywordId) {
    const sovKeyword = yield select(selectSovKeyword, sovKeywordId)

    // Fetch keyword if not fetched
    if (!sovKeyword) {
        yield call(
            cerebroApiSaga,
            fetchSovKeywordSuccess,
            getSovKeyword,
            sovKeywordId
        )
    }
}

export function* attachSovKeywordSaga(data) {
    yield call(cerebroApiSaga, attachSovKeywordSuccess, attachSovKeyword, data)
}

export function* updateSovKeywordSaga(sovKeywordId, data) {
    yield call(
        cerebroApiSaga,
        updateSovKeywordSuccess,
        updateSovKeyword,
        sovKeywordId,
        data
    )
}

export function* deleteSovKeywordSaga(sovKeywordId) {
    yield call(
        cerebroApiSaga,
        deleteSovKeywordSuccess,
        deleteSovKeyword,
        sovKeywordId
    )
}
