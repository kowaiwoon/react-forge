import { all } from 'redux-saga/effects'

import appWorker from './app'

// Pages
import sharedWorker from './shared'
import homePageWorker from './home'
import brandsSummaryPageWorker from './brandsSummary'
import brandPageWorker from './brand'
import campaignsSummaryPageWorker from './campaignsSummary'
import campaignPageWorker from './campaign'
import productsSummaryPageWorker from './productsSummary'
import productPageWorker from './product'
import keywordsSummaryPageWorker from './keywordsSummary'
import keywordPageWorker from './keyword'
import labelsSummaryWorker from './labelsSummary'
import sovKeywordsSummaryPageWorker from './sovKeywordsSummary'
import sovKeywordPageWorker from './sovKeyword'
import sovKeywordSearchResultPageWorker from './sovKeywordSearchResult'
import organizationPageWorker from './organization'
import organizationGroupPageWorker from './organizationGroup'
import profilePageWorker from './profile'
import automationPageWorker from './automation'

export default function* uiSaga() {
    yield all([
        appWorker(),

        // Pages
        sharedWorker(),
        homePageWorker(),
        brandsSummaryPageWorker(),
        brandPageWorker(),
        campaignsSummaryPageWorker(),
        campaignPageWorker(),
        productsSummaryPageWorker(),
        productPageWorker(),
        keywordsSummaryPageWorker(),
        keywordPageWorker(),
        labelsSummaryWorker(),
        sovKeywordsSummaryPageWorker(),
        sovKeywordPageWorker(),
        sovKeywordSearchResultPageWorker(),
        organizationPageWorker(),
        organizationGroupPageWorker(),
        profilePageWorker(),
        automationPageWorker(),
    ])
}
