import { all, takeLatest } from 'redux-saga/effects'

import {
    updatePageFilter,
    resetPageFilters,
    updatePageFilterSettings,
} from 'actions/ui'

import {
    updatePageFilterWorker,
    updatePageFilterSettingsWorker,
} from './workers'

export default function* sharedWorker() {
    yield all([
        takeLatest(updatePageFilter.toString(), updatePageFilterWorker),

        takeLatest(
            [resetPageFilters.toString(), updatePageFilterSettings.toString()],
            updatePageFilterSettingsWorker
        ),
    ])
}
