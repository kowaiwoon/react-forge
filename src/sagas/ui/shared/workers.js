import { call, all, put, select } from 'redux-saga/effects'
import some from 'lodash/some'
import get from 'lodash/get'
import isEmpty from 'lodash/isEmpty'

import { HEADLINE_SEARCH } from 'constants/factTypes'
import { FACT_TYPES } from 'constants/filters'
import { FILTER_SETTINGS, CHARTS } from 'constants/reducerKeys'

import { headlineSearchMetrics } from 'configuration/metrics'

import { getChartDefaultMetrics } from 'helpers/charts'
import {
    isCurrentSchema as isCurrentFilterSettingsSchema,
    createFilterSettingsItem,
    createFilterSettingsKey,
} from 'helpers/filterSettings'

import {
    fetchPageDataActions,
    updatePaginationActions,
    fetchPageFilterSettingsSuccess,
    updateChartMetrics,
    updateTableSorter,
    updateTreemapSorter,
} from 'actions/ui'

import { selectDomainValue as selectAuthDomainValue } from 'selectors/auth'
import {
    selectPageFactTypes,
    selectDomainValue as selectUiDomainValue,
} from 'selectors/ui'

import {
    getItemFromUserSettingsTable,
    putItemToUserSettingsTable,
} from 'services/dynamoApi'

function* fetchPageDataSaga(action) {
    const { pageName } = action.payload
    const fetchingAction = fetchPageDataActions[pageName]

    if (fetchingAction) {
        yield put(fetchingAction())
    }
}

/**
 * Persist current filter settings of given page to DynamoDB
 *
 * @param {string} pageName
 */
function* updatePageFilterSettingsSaga(pageName) {
    const userId = yield select(selectAuthDomainValue, ['username'])
    const { order, displayState } = yield select(selectUiDomainValue, [
        pageName,
        FILTER_SETTINGS,
    ])

    const item = createFilterSettingsItem({
        userId,
        page: pageName,
        order,
        displayState,
    })

    yield call(putItemToUserSettingsTable, item)
}

/**
 * Fetches filter settings of given page from DynamoDB
 *
 * @param {string} pageName
 */
export function* fetchPageFilterSettingsSaga(pageName) {
    const userId = yield select(selectAuthDomainValue, ['username'])
    const currentFilterSettings = yield select(selectUiDomainValue, [
        pageName,
        FILTER_SETTINGS,
    ])

    const key = createFilterSettingsKey({
        userId,
        page: pageName,
    })

    const { Item } = yield call(getItemFromUserSettingsTable, key)

    if (Item) {
        // If filter settings schema didn't change, accept
        // Otherwise, persist the current filter settings
        if (isCurrentFilterSettingsSchema(Item, currentFilterSettings)) {
            yield put(
                fetchPageFilterSettingsSuccess({
                    pageName,
                    filterSettings: Item,
                })
            )
        } else {
            yield call(updatePageFilterSettingsSaga, pageName)
        }
    }
}

function* resetPagination(action) {
    const { pageName } = action.payload

    const paginationActions = get(updatePaginationActions, pageName, []).map(
        paginationAction => put(paginationAction({ current: 1 }))
    )
    if (!isEmpty(paginationActions)) {
        yield all(paginationActions)
    }
}

/**
 * This saga resets chart metrics to default
 * if currently selected ones are unavailable in headline search
 */
function* resetPageSelectedMetricsSaga(action) {
    const { pageName } = action.payload
    const factTypes = yield select(selectPageFactTypes, pageName)

    if (!some(factTypes, ['value', HEADLINE_SEARCH])) {
        return
    }

    const charts = yield select(selectUiDomainValue, [pageName, CHARTS])
    const table = yield select(selectUiDomainValue, [pageName, 'table'])
    const treemap = yield select(selectUiDomainValue, [pageName, 'treemap'])

    // reset selected chart metrics, which are only available for headline search, back to default values
    if (charts) {
        yield all(
            Object.keys(charts).map(chartName => {
                const chartMetrics = charts[chartName]
                const defaultChartMetrics = getChartDefaultMetrics(chartName)

                return all(
                    Object.keys(chartMetrics)
                        .filter(
                            chartMetricKey =>
                                !headlineSearchMetrics.includes(
                                    chartMetrics[chartMetricKey]
                                )
                        )
                        .map(chartMetricKey =>
                            put(
                                updateChartMetrics({
                                    pageName,
                                    chartName,
                                    key: chartMetricKey,
                                    value: defaultChartMetrics[chartMetricKey],
                                })
                            )
                        )
                )
            })
        )
    }

    // reset page sorter if sorting on a metric that's not available for headline search
    if (table && !headlineSearchMetrics.includes(table.sorter.field)) {
        const { sorter } = table

        yield put(
            updateTableSorter({
                pageName,
                sorter: {
                    ...sorter,
                    field: 'attributed_sales_14_day__sum',
                },
            })
        )
    }

    // reset selected treemap metric if it's not available for headline search
    if (treemap && !headlineSearchMetrics.includes(treemap.sorter.field)) {
        const { sorter } = treemap

        yield put(
            updateTreemapSorter({
                pageName,
                sorter: {
                    ...sorter,
                    field: 'attributed_sales_14_day__sum',
                },
            })
        )
    }
}

/**
 * Reload data for page when global page filters are updated
 */
export function* updatePageFilterWorker(action) {
    const { reload = true, key } = action.payload.data

    if (reload) {
        const sagas = [
            // reset selected metrics when the user changes fact types
            ...(key === FACT_TYPES
                ? [call(resetPageSelectedMetricsSaga, action)]
                : []),

            // reset pagination when filters change
            call(resetPagination, action),

            // reload page data
            call(fetchPageDataSaga, action),
        ]

        yield all(sagas)
    }
}

/**
 * When filter settings are updated
 *
 * - Refreshes data for page
 * - Puts new filter settings to DynamoDB
 */
export function* updatePageFilterSettingsWorker(action) {
    const { pageName } = action.payload

    yield all([
        call(updatePageFilterSettingsSaga, pageName),
        call(fetchPageDataSaga, action),
    ])
}
