import { all, takeLatest } from 'redux-saga/effects'

import {
    mountProductsSummaryPageRequest,
    fetchProductsSummaryPageDataRequest,
    updateProductsSummaryPageTableSettingsRequest,
    fetchProductsSummaryPageTreemapRequest,
    fetchProductsSummaryPageTableRequest,
    downloadProductsSummaryPageTableRequest,
    changeProductAsinsFilterInput,
    changeProductTitlesFilterInput,
} from 'actions/ui'
import {
    mountProductsSummaryPageWorker,
    fetchProductsSummaryPageDataWorker,
    fetchProductsSummaryPageTreemapWorker,
    fetchProductsSummaryPageTableWorker,
    downloadProductsSummaryPageTableWorker,
    updateProductsSummaryPageTableSettingsWorker,
    changeProductAsinsFilterInputWorker,
    changeProductTitlesFilterInputWorker,
} from './workers'

export default function* productsSummaryPageWorker() {
    yield all([
        takeLatest(
            mountProductsSummaryPageRequest.toString(),
            mountProductsSummaryPageWorker
        ),

        takeLatest(
            fetchProductsSummaryPageDataRequest.toString(),
            fetchProductsSummaryPageDataWorker
        ),

        takeLatest(
            fetchProductsSummaryPageTreemapRequest.toString(),
            fetchProductsSummaryPageTreemapWorker
        ),

        takeLatest(
            fetchProductsSummaryPageTableRequest.toString(),
            fetchProductsSummaryPageTableWorker
        ),

        takeLatest(
            downloadProductsSummaryPageTableRequest.toString(),
            downloadProductsSummaryPageTableWorker
        ),

        takeLatest(
            updateProductsSummaryPageTableSettingsRequest.toString(),
            updateProductsSummaryPageTableSettingsWorker
        ),

        takeLatest(
            changeProductAsinsFilterInput.toString(),
            changeProductAsinsFilterInputWorker
        ),

        takeLatest(
            changeProductTitlesFilterInput.toString(),
            changeProductTitlesFilterInputWorker
        ),
    ])
}
