import { all, call, select, put } from 'redux-saga/effects'
import { delay } from 'redux-saga'

import { PRODUCTS_SUMMARY_PAGE } from 'constants/pages'
import { SPONSORED_PRODUCT, HEADLINE_SEARCH } from 'constants/factTypes'
import { SEARCH_RESULTS_PER_QUERY } from 'configuration/typeahead'
import { formatAllParams, formatFilters } from 'helpers/params'
import { downloadCsv } from 'helpers/downloads'
import {
    getProductsFactAggregates,
    getProductsSponsoredProductFactAggregates,
    getProductsHeadlineSearchFactAggregates,
    getProductsFactAggregatesExport,
    getProducts,
} from 'services/cerebroApi'
import {
    putItemToUserSettingsTable,
    getItemFromUserSettingsTable,
} from 'services/dynamoApi'
import {
    isCurrentSchema as isCurrentColumnSettingsSchema,
    createColumnSettingsItem,
    createColumnSettingsKey,
} from 'helpers/columnSettings'
import {
    // mounting
    mountProductsSummaryPageSuccess,
    mountProductsSummaryPageFailure,

    // page data
    fetchProductsSummaryPageDataSuccess,
    fetchProductsSummaryPageDataFailure,

    // treemap data
    fetchProductsSummaryPageTreemapSuccess,
    fetchProductsSummaryPageTreemapFailure,

    // table data
    fetchProductsSummaryPageTableSuccess,
    fetchProductsSummaryPageTableFailure,

    // table settings
    fetchProductsSummaryPageTableSettingsSuccess,

    // table download
    downloadProductsSummaryPageTableSuccess,
    downloadProductsSummaryPageTableFailure,

    // search product asins
    searchProductAsinsRequest,
    searchProductAsinsSuccess,
    searchProductAsinsFailure,

    // search product titles
    searchProductTitlesRequest,
    searchProductTitlesSuccess,
    searchProductTitlesFailure,
} from 'actions/ui'
import {
    selectDomainValue as selectUiDomainValue,
    selectPageFilters,
    selectPageFactTypes,
    selectTreemapSelectedMetric,
    selectTableSelectedMetrics,
    selectCurrencyCode,
} from 'selectors/ui'
import { selectDomainValue as selectAuthDomainValue } from 'selectors/auth'
import cerebroApiSaga from 'sagas/common/cerebroApi'

import { fetchPageFilterSettingsSaga } from '../shared/workers'

const getAggregatesApi = factTypes => {
    if (factTypes.length === 2) {
        return getProductsFactAggregates
    }

    if (factTypes[0].value === SPONSORED_PRODUCT) {
        return getProductsSponsoredProductFactAggregates
    }

    if (factTypes[0].value === HEADLINE_SEARCH) {
        return getProductsHeadlineSearchFactAggregates
    }

    return null
}

function* fetchProductsSummaryPageTreemapSaga() {
    const filters = yield select(selectPageFilters, PRODUCTS_SUMMARY_PAGE)
    const factTypes = yield select(selectPageFactTypes, PRODUCTS_SUMMARY_PAGE)
    const { pagination, sorter } = yield select(selectUiDomainValue, [
        PRODUCTS_SUMMARY_PAGE,
        'treemap',
    ])
    const metric = yield select(
        selectTreemapSelectedMetric,
        PRODUCTS_SUMMARY_PAGE,
        'treemap'
    )
    const currency = yield select(selectCurrencyCode)
    const params = formatAllParams(
        filters,
        pagination,
        sorter,
        [metric],
        currency
    )
    const aggregatesApi = getAggregatesApi(factTypes)

    yield call(
        cerebroApiSaga,
        fetchProductsSummaryPageTreemapSuccess,
        aggregatesApi,
        params
    )
}

function* fetchProductsSummaryPageTableSaga() {
    const filters = yield select(selectPageFilters, PRODUCTS_SUMMARY_PAGE)
    const factTypes = yield select(selectPageFactTypes, PRODUCTS_SUMMARY_PAGE)
    const { pagination, sorter } = yield select(selectUiDomainValue, [
        PRODUCTS_SUMMARY_PAGE,
        'table',
    ])
    const metrics = yield select(
        selectTableSelectedMetrics,
        PRODUCTS_SUMMARY_PAGE,
        'table'
    )
    const currency = yield select(selectCurrencyCode)
    const params = formatAllParams(
        filters,
        pagination,
        sorter,
        metrics,
        currency
    )
    const aggregatesApi = getAggregatesApi(factTypes)

    yield call(
        cerebroApiSaga,
        fetchProductsSummaryPageTableSuccess,
        aggregatesApi,
        params
    )
}

function* downloadProductsSummaryPageTableSaga() {
    const filters = yield select(selectPageFilters, PRODUCTS_SUMMARY_PAGE)
    const currency = yield select(selectCurrencyCode)
    const params = {
        ...formatFilters(filters),
        currency,
    }

    const response = yield call(
        cerebroApiSaga,
        downloadProductsSummaryPageTableSuccess,
        getProductsFactAggregatesExport,
        params
    )

    yield call(downloadCsv, response.data, 'products')
}

function* updateProductsSummaryPageTableSettingsSaga() {
    const userId = yield select(selectAuthDomainValue, ['username'])
    const { actionColumns, order, displayState } = yield select(
        selectUiDomainValue,
        [PRODUCTS_SUMMARY_PAGE, 'table', 'columnSettings']
    )

    const item = createColumnSettingsItem({
        userId,
        page: PRODUCTS_SUMMARY_PAGE,
        tableName: 'table',
        actionColumns,
        order,
        displayState,
    })

    yield call(putItemToUserSettingsTable, item)
}

function* fetchProductsTableColumnSettingsSaga() {
    const userId = yield select(selectAuthDomainValue, ['username'])
    const columnSettings = yield select(selectUiDomainValue, [
        PRODUCTS_SUMMARY_PAGE,
        'table',
        'columnSettings',
    ])

    const key = createColumnSettingsKey({
        userId,
        page: PRODUCTS_SUMMARY_PAGE,
        tableName: 'table',
    })

    const { Item } = yield call(getItemFromUserSettingsTable, key)

    if (Item) {
        if (isCurrentColumnSettingsSchema(Item, columnSettings)) {
            yield put(fetchProductsSummaryPageTableSettingsSuccess(Item))
        } else {
            yield call(updateProductsSummaryPageTableSettingsSaga)
        }
    }
}

function* fetchProductsSummaryPageDataSaga() {
    yield all([
        call(fetchProductsSummaryPageTreemapSaga),
        call(fetchProductsSummaryPageTableSaga),
    ])
}

function* searchProductAsinsSaga(query) {
    if (query.length > 1) {
        yield put(searchProductAsinsRequest())
        yield call(cerebroApiSaga, searchProductAsinsSuccess, getProducts, {
            limit: SEARCH_RESULTS_PER_QUERY,
            asin__istartswith: query,
        })
    }
}

function* searchProductTitlesSaga(query) {
    if (query.length > 1) {
        yield put(searchProductTitlesRequest())
        yield call(cerebroApiSaga, searchProductTitlesSuccess, getProducts, {
            limit: SEARCH_RESULTS_PER_QUERY,
            metadata__title__icontains: query,
        })
    }
}

/**
 * Puts new table settings to DynamoDB
 */
export function* updateProductsSummaryPageTableSettingsWorker() {
    yield call(updateProductsSummaryPageTableSettingsSaga)
}

/**
 * Downloads data for the Products Summary Page table
 */
export function* downloadProductsSummaryPageTableWorker() {
    try {
        yield call(downloadProductsSummaryPageTableSaga)
    } catch (error) {
        yield put(downloadProductsSummaryPageTableFailure(error))
    }
}

/**
 * Fetches data for Products Summary Page table
 */
export function* fetchProductsSummaryPageTableWorker() {
    try {
        yield call(fetchProductsSummaryPageTableSaga)
    } catch (error) {
        yield put(fetchProductsSummaryPageTableFailure(error))
    }
}

/**
 * Fetches data for Products Summary Page Treemap
 */
export function* fetchProductsSummaryPageTreemapWorker() {
    try {
        yield call(fetchProductsSummaryPageTreemapSaga)
    } catch (error) {
        yield put(fetchProductsSummaryPageTreemapFailure(error))
    }
}

/**
 * Fetch data for Products Summary Page
 */
export function* fetchProductsSummaryPageDataWorker() {
    try {
        yield call(fetchProductsSummaryPageDataSaga)
        yield put(fetchProductsSummaryPageDataSuccess())
    } catch (error) {
        yield put(fetchProductsSummaryPageDataFailure(error))
    }
}

/**
 * Mounts the Products Summary Page and fetches data
 */
export function* mountProductsSummaryPageWorker() {
    try {
        yield call(fetchPageFilterSettingsSaga, PRODUCTS_SUMMARY_PAGE)
        yield call(fetchProductsTableColumnSettingsSaga)
        yield call(fetchProductsSummaryPageDataSaga)
        yield put(mountProductsSummaryPageSuccess())
    } catch (error) {
        yield put(mountProductsSummaryPageFailure(error))
    }
}

export function* changeProductAsinsFilterInputWorker(action) {
    // debounce by 500ms
    yield delay(500)
    try {
        yield call(searchProductAsinsSaga, action.payload)
    } catch (error) {
        yield put(searchProductAsinsFailure(error))
    }
}

export function* changeProductTitlesFilterInputWorker(action) {
    // debounce by 500ms
    yield delay(500)
    try {
        yield call(searchProductTitlesSaga, action.payload)
    } catch (error) {
        yield put(searchProductTitlesFailure(error))
    }
}
