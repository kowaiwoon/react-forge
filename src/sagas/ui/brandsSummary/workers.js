import { all, call, select, put } from 'redux-saga/effects'

import { BRANDS_SUMMARY_PAGE } from 'constants/pages'
import { SPONSORED_PRODUCT, HEADLINE_SEARCH } from 'constants/factTypes'
import { formatAllParams, formatFilters } from 'helpers/params'
import { downloadCsv } from 'helpers/downloads'
import {
    getBrandsFactAggregates,
    getBrandsSponsoredProductFactAggregates,
    getBrandsHeadlineSearchFactAggregates,
    getBrandsFactAggregatesExport,
} from 'services/cerebroApi'
import {
    putItemToUserSettingsTable,
    getItemFromUserSettingsTable,
} from 'services/dynamoApi'
import {
    isCurrentSchema as isCurrentColumnSettingsSchema,
    createColumnSettingsItem,
    createColumnSettingsKey,
} from 'helpers/columnSettings'
import {
    // mounting
    mountBrandsSummaryPageSuccess,
    mountBrandsSummaryPageFailure,

    // page data
    fetchBrandsSummaryPageDataSuccess,
    fetchBrandsSummaryPageDataFailure,

    // treemap data
    fetchBrandsSummaryPageTreemapSuccess,
    fetchBrandsSummaryPageTreemapFailure,

    // table data
    fetchBrandsSummaryPageTableSuccess,
    fetchBrandsSummaryPageTableFailure,

    // table settings
    fetchBrandsSummaryPageTableSettingsSuccess,

    // table download
    downloadBrandsSummaryPageTableSuccess,
    downloadBrandsSummaryPageTableFailure,
} from 'actions/ui'
import {
    selectDomainValue as selectUiDomainValue,
    selectPageFilters,
    selectPageFactTypes,
    selectTreemapSelectedMetric,
    selectTableSelectedMetrics,
    selectCurrencyCode,
} from 'selectors/ui'
import { selectDomainValue as selectAuthDomainValue } from 'selectors/auth'

import cerebroApiSaga from 'sagas/common/cerebroApi'
import { fetchPageFilterSettingsSaga } from '../shared/workers'

const getAggregatesApi = factTypes => {
    if (factTypes.length === 2) {
        return getBrandsFactAggregates
    }

    if (factTypes[0].value === SPONSORED_PRODUCT) {
        return getBrandsSponsoredProductFactAggregates
    }

    if (factTypes[0].value === HEADLINE_SEARCH) {
        return getBrandsHeadlineSearchFactAggregates
    }

    return null
}

function* fetchBrandsSummaryPageTreemapSaga() {
    const filters = yield select(selectPageFilters, BRANDS_SUMMARY_PAGE)
    const factTypes = yield select(selectPageFactTypes, BRANDS_SUMMARY_PAGE)
    const { pagination, sorter } = yield select(selectUiDomainValue, [
        BRANDS_SUMMARY_PAGE,
        'treemap',
    ])
    const metric = yield select(
        selectTreemapSelectedMetric,
        BRANDS_SUMMARY_PAGE,
        'treemap'
    )
    const currency = yield select(selectCurrencyCode)
    const params = formatAllParams(
        filters,
        pagination,
        sorter,
        [metric],
        currency
    )
    const aggregatesApi = getAggregatesApi(factTypes)

    yield call(
        cerebroApiSaga,
        fetchBrandsSummaryPageTreemapSuccess,
        aggregatesApi,
        params
    )
}

function* fetchBrandsSummaryPageTableSaga() {
    const filters = yield select(selectPageFilters, BRANDS_SUMMARY_PAGE)
    const factTypes = yield select(selectPageFactTypes, BRANDS_SUMMARY_PAGE)
    const { pagination, sorter } = yield select(selectUiDomainValue, [
        BRANDS_SUMMARY_PAGE,
        'table',
    ])
    const metrics = yield select(
        selectTableSelectedMetrics,
        BRANDS_SUMMARY_PAGE,
        'table'
    )
    const currency = yield select(selectCurrencyCode)
    const params = formatAllParams(
        filters,
        pagination,
        sorter,
        metrics,
        currency
    )
    const aggregatesApi = getAggregatesApi(factTypes)

    yield call(
        cerebroApiSaga,
        fetchBrandsSummaryPageTableSuccess,
        aggregatesApi,
        params
    )
}

function* updateBrandsTableColumnSettingsSaga() {
    const userId = yield select(selectAuthDomainValue, ['username'])
    const { actionColumns, order, displayState } = yield select(
        selectUiDomainValue,
        [BRANDS_SUMMARY_PAGE, 'table', 'columnSettings']
    )

    const item = createColumnSettingsItem({
        userId,
        page: BRANDS_SUMMARY_PAGE,
        tableName: 'table',
        actionColumns,
        order,
        displayState,
    })

    yield call(putItemToUserSettingsTable, item)
}

function* fetchBrandsSummaryPageTableSettingsSaga() {
    const userId = yield select(selectAuthDomainValue, ['username'])
    const columnSettings = yield select(selectUiDomainValue, [
        BRANDS_SUMMARY_PAGE,
        'table',
        'columnSettings',
    ])

    const key = createColumnSettingsKey({
        userId,
        page: BRANDS_SUMMARY_PAGE,
        tableName: 'table',
    })

    const { Item } = yield call(getItemFromUserSettingsTable, key)

    if (Item) {
        if (isCurrentColumnSettingsSchema(Item, columnSettings)) {
            yield put(fetchBrandsSummaryPageTableSettingsSuccess(Item))
        } else {
            yield call(updateBrandsTableColumnSettingsSaga)
        }
    }
}

function* downloadBrandsSummaryPageTableSaga() {
    const filters = yield select(selectPageFilters, BRANDS_SUMMARY_PAGE)
    const currency = yield select(selectCurrencyCode)
    const params = {
        ...formatFilters(filters),
        currency,
    }

    const response = yield call(
        cerebroApiSaga,
        downloadBrandsSummaryPageTableSuccess,
        getBrandsFactAggregatesExport,
        params
    )

    yield call(downloadCsv, response.data, 'brands')
}

function* fetchBrandsSummaryPageDataSaga() {
    yield all([
        call(fetchBrandsSummaryPageTreemapSaga),
        call(fetchBrandsSummaryPageTableSaga),
    ])
}

/**
 * Puts new table settings to DynamoDB
 */
export function* updateBrandsSummaryPageTableSettingsWorker() {
    yield call(updateBrandsTableColumnSettingsSaga)
}

/**
 * Fetches data for Brands Summary Page Table
 */
export function* fetchBrandsSummaryPageTableWorker() {
    try {
        yield call(fetchBrandsSummaryPageTableSaga)
    } catch (error) {
        yield put(fetchBrandsSummaryPageTableFailure(error))
    }
}

/**
 * Downloads data for the Brands Summary Page
 */
export function* downloadBrandsSummaryPageTableWorker() {
    try {
        yield call(downloadBrandsSummaryPageTableSaga)
    } catch (error) {
        yield put(downloadBrandsSummaryPageTableFailure(error))
    }
}

/**
 * Fetches data for Brands Summary Page Treemap
 */
export function* fetchBrandsSummaryPageTreemapWorker() {
    try {
        yield call(fetchBrandsSummaryPageTreemapSaga)
    } catch (error) {
        yield put(fetchBrandsSummaryPageTreemapFailure(error))
    }
}

/**
 * Fetches all data required for the Brands Summary Page
 */
export function* fetchBrandsSummaryPageDataWorker() {
    try {
        yield call(fetchBrandsSummaryPageDataSaga)
        yield put(fetchBrandsSummaryPageDataSuccess())
    } catch (error) {
        yield put(fetchBrandsSummaryPageDataFailure(error))
    }
}

/**
 * Mounts the Brands Summary Page and fetches data
 */
export function* mountBrandsSummaryPageWorker() {
    try {
        yield call(fetchPageFilterSettingsSaga, BRANDS_SUMMARY_PAGE)
        yield call(fetchBrandsSummaryPageTableSettingsSaga)
        yield call(fetchBrandsSummaryPageDataSaga)
        yield put(mountBrandsSummaryPageSuccess())
    } catch (error) {
        yield put(mountBrandsSummaryPageFailure(error))
    }
}
