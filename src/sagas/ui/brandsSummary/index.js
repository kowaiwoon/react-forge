import { all, takeLatest } from 'redux-saga/effects'

import {
    mountBrandsSummaryPageRequest,
    fetchBrandsSummaryPageDataRequest,
    updateBrandsSummaryPageTableSettings,
    fetchBrandsSummaryPageTreemapRequest,
    fetchBrandsSummaryPageTableRequest,
    downloadBrandsSummaryPageTableRequest,
} from 'actions/ui'

import {
    mountBrandsSummaryPageWorker,
    fetchBrandsSummaryPageDataWorker,
    fetchBrandsSummaryPageTreemapWorker,
    fetchBrandsSummaryPageTableWorker,
    downloadBrandsSummaryPageTableWorker,
    updateBrandsSummaryPageTableSettingsWorker,
} from './workers'

export default function* brandsSummaryPageWorker() {
    yield all([
        takeLatest(
            mountBrandsSummaryPageRequest.toString(),
            mountBrandsSummaryPageWorker
        ),

        takeLatest(
            fetchBrandsSummaryPageDataRequest.toString(),
            fetchBrandsSummaryPageDataWorker
        ),

        takeLatest(
            fetchBrandsSummaryPageTreemapRequest.toString(),
            fetchBrandsSummaryPageTreemapWorker
        ),

        takeLatest(
            fetchBrandsSummaryPageTableRequest.toString(),
            fetchBrandsSummaryPageTableWorker
        ),

        takeLatest(
            updateBrandsSummaryPageTableSettings.toString(),
            updateBrandsSummaryPageTableSettingsWorker
        ),

        takeLatest(
            downloadBrandsSummaryPageTableRequest.toString(),
            downloadBrandsSummaryPageTableWorker
        ),
    ])
}
