import { all, call, select, put } from 'redux-saga/effects'
import { delay } from 'redux-saga'

import { selectDomainValue } from 'selectors/ui'
import { ORGANIZATION_PAGE } from 'constants/pages'
import { SEARCH_RESULTS_PER_QUERY } from 'configuration/typeahead'
import {
    mountOrganizationPageSuccess,
    mountOrganizationPageFailure,
    fetchOrganizationPageMembersTableSuccess,
    fetchOrganizationPageIntegrationsTableSuccess,
    createOrganizationPageGroupSuccess,
    createOrganizationPageGroupFailure,
    createOrganizationPageIntegrationSuccess,
    createOrganizationPageIntegrationFailure,
    searchOrganizationPageBrandsRequest,
    searchOrganizationPageBrandsSuccess,
    searchOrganizationPageBrandsFailure,
    inviteMemberOrganizationPageSuccess,
    inviteMemberOrganizationPageFailure,
    fetchOrganizationPageGroupsTableSuccess,
    fetchOrganizationPageGroupsTableFailure,
    fetchOrganizationPageIntegrationsTableFailure,
    fetchOrganizationPageMembersTableFailure,
    fetchOrganizationPageInvitationsTableFailure,
    fetchOrganizationPageInvitationsTableSuccess,
    fetchOrganizationPageMembersTableRequest,
    removeMemberOrganizationPageFailure,
    removeMemberOrganizationPageSuccess,
    fetchOrganizationPageInvitationsTableRequest,
    fetchOrganizationPageGroupsTableRequest,
} from 'actions/ui'
import cerebroApiSaga from 'sagas/common/cerebroApi'
import {
    getOrganizationIntegrations,
    getBrandsForOrg,
    createOrganizationGroup,
    loginWithAmazon,
    inviteMember,
    getInvitations,
    getOrganizationGroups,
    getOrganizationMembers,
    removeMemberFromOrganization,
} from 'services/cerebroApi'
import { fetchUserPermissionsSaga } from 'sagas/orgs/userPermissions'
import { fetchOrganizationSaga } from 'sagas/orgs/organizations'
import { fetchOrganizationGroupsSaga } from 'sagas/orgs/groups'
import { formatPagination, formatSorter } from 'helpers/params'

function* fetchOrganizationPageGroupsSaga() {
    const organizationId = yield select(selectDomainValue, [
        ORGANIZATION_PAGE,
        'organizationId',
    ])
    yield call(fetchOrganizationGroupsSaga, organizationId)
}

function* fetchOrganizationPageGroupsTableSaga() {
    const organizationId = yield select(selectDomainValue, [
        ORGANIZATION_PAGE,
        'organizationId',
    ])

    const { pagination, sorter } = yield select(selectDomainValue, [
        ORGANIZATION_PAGE,
        'groupsTable',
    ])

    yield call(
        cerebroApiSaga,
        fetchOrganizationPageGroupsTableSuccess,
        getOrganizationGroups,
        organizationId,
        {
            ...formatPagination(pagination),
            ...formatSorter(sorter),
        }
    )
}

function* fetchOrganizationPageOrganizationSaga() {
    const organizationId = yield select(selectDomainValue, [
        ORGANIZATION_PAGE,
        'organizationId',
    ])

    yield call(fetchOrganizationSaga, organizationId)
}

function* fetchOrganizationPageInvitationsTableSaga() {
    const organizationId = yield select(selectDomainValue, [
        ORGANIZATION_PAGE,
        'organizationId',
    ])

    const { pagination, sorter } = yield select(selectDomainValue, [
        ORGANIZATION_PAGE,
        'invitationsTable',
    ])

    yield call(
        cerebroApiSaga,
        fetchOrganizationPageInvitationsTableSuccess,
        getInvitations,
        {
            ...formatPagination(pagination),
            ...formatSorter(sorter),
            organization: organizationId,
        }
    )
}

function* fetchOrganizationPageMembersTableSaga() {
    const organizationId = yield select(selectDomainValue, [
        ORGANIZATION_PAGE,
        'organizationId',
    ])

    const { pagination, sorter } = yield select(selectDomainValue, [
        ORGANIZATION_PAGE,
        'membersTable',
    ])

    yield call(
        cerebroApiSaga,
        fetchOrganizationPageMembersTableSuccess,
        getOrganizationMembers,
        organizationId,
        {
            ...formatPagination(pagination),
            ...formatSorter(sorter),
        }
    )
}

function* fetchOrganizationPageIntegrationsTableSaga() {
    const organizationId = yield select(selectDomainValue, [
        ORGANIZATION_PAGE,
        'organizationId',
    ])

    const { pagination, sorter } = yield select(selectDomainValue, [
        ORGANIZATION_PAGE,
        'integrationsTable',
    ])

    yield call(
        cerebroApiSaga,
        fetchOrganizationPageIntegrationsTableSuccess,
        getOrganizationIntegrations,
        organizationId,
        {
            ...formatPagination(pagination),
            ...formatSorter(sorter),
        }
    )
}

function* fetchOrganizationPageFeaturePermissionsSaga() {
    const organizationId = yield select(selectDomainValue, [
        ORGANIZATION_PAGE,
        'organizationId',
    ])

    yield call(fetchUserPermissionsSaga, organizationId)
}

function* fetchOrganizationPageDataSaga() {
    yield all([
        call(fetchOrganizationPageOrganizationSaga),
        call(fetchOrganizationPageGroupsSaga),
        call(fetchOrganizationPageGroupsTableSaga),
        call(fetchOrganizationPageMembersTableSaga),
        call(fetchOrganizationPageInvitationsTableSaga),
        call(fetchOrganizationPageIntegrationsTableSaga),
        call(fetchOrganizationPageFeaturePermissionsSaga),
    ])
}

function* createOrganizationPageGroupSaga(action) {
    const organizationId = yield select(selectDomainValue, [
        ORGANIZATION_PAGE,
        'organizationId',
    ])

    yield call(
        cerebroApiSaga,
        createOrganizationPageGroupSuccess,
        createOrganizationGroup,
        {
            ...action.payload,
            organization_id: organizationId,
        }
    )
}

function* searchBrandsSaga(query) {
    if (query.length > 1) {
        const organizationId = yield select(selectDomainValue, [
            ORGANIZATION_PAGE,
            'organizationId',
        ])
        yield put(searchOrganizationPageBrandsRequest())
        const {
            data: { results },
        } = yield call(cerebroApiSaga, null, getBrandsForOrg, organizationId, {
            limit: SEARCH_RESULTS_PER_QUERY,
            brand_name__icontains: query,
        })
        yield put(
            searchOrganizationPageBrandsSuccess({ results, organizationId })
        )
    }
}

function* inviteMemberOrganizationPageSaga(data) {
    const organizationId = yield select(selectDomainValue, [
        ORGANIZATION_PAGE,
        'organizationId',
    ])

    yield call(
        cerebroApiSaga,
        inviteMemberOrganizationPageSuccess,
        inviteMember,
        {
            ...data,
            organization_id: organizationId,
        }
    )
}

function* createIntegrationOrganizationPageSaga(data) {
    const response = yield call(
        cerebroApiSaga,
        createOrganizationPageIntegrationSuccess,
        loginWithAmazon,
        data
    )
    return response.data.lwa_uri
}

function* removeOrganizationPageMemberSaga(action) {
    const organizationId = yield select(selectDomainValue, [
        ORGANIZATION_PAGE,
        'organizationId',
    ])

    yield call(
        cerebroApiSaga,
        removeMemberOrganizationPageSuccess,
        removeMemberFromOrganization,
        organizationId,
        action.payload.memberId
    )
}

/**
 * Fetches typeahead data for brands search
 *
 * @param action
 * @returns {IterableIterator<*>}
 */
export function* changeOrganizationPageBrandsSearchInputWorker(action) {
    // debounce by 500ms
    yield delay(500)
    try {
        yield call(searchBrandsSaga, action.payload)
    } catch (error) {
        yield put(searchOrganizationPageBrandsFailure(error))
    }
}

/**
 * Create an organization group
 */
export function* createOrganizationPageGroupWorker(action) {
    try {
        yield call(createOrganizationPageGroupSaga, action)
        // repopulate all group options for inviting a member
        yield call(fetchOrganizationPageGroupsSaga)
        // reload the paginated/sorted table
        yield put(fetchOrganizationPageGroupsTableRequest())
    } catch (error) {
        yield put(createOrganizationPageGroupFailure(error))
    }
}

/**
 * Invite a member to an organization
 */
export function* inviteMemberOrganizationPageWorker(action) {
    try {
        yield call(inviteMemberOrganizationPageSaga, action.payload)
        // reload the paginated/sorted table
        yield put(fetchOrganizationPageInvitationsTableRequest())
    } catch (error) {
        yield put(inviteMemberOrganizationPageFailure(error))
    }
}

/**
 * Create a new integration for an organization
 */
export function* createOrganizationPageIntegrationWorker(action) {
    try {
        window.location.href = yield call(
            createIntegrationOrganizationPageSaga,
            action.payload
        )
    } catch (error) {
        yield put(createOrganizationPageIntegrationFailure(error))
    }
}

/**
 * Fetches data for organization groups table
 */
export function* fetchOrganizationPageGroupsTableWorker() {
    try {
        yield call(fetchOrganizationPageGroupsTableSaga)
    } catch (error) {
        yield put(fetchOrganizationPageGroupsTableFailure(error))
    }
}

/**
 * Fetches data for organization members table
 */
export function* fetchOrganizationPageMembersTableWorker() {
    try {
        yield call(fetchOrganizationPageMembersTableSaga)
    } catch (error) {
        yield put(fetchOrganizationPageMembersTableFailure(error))
    }
}

/**
 * Fetches data for organization invitations table
 */
export function* fetchOrganizationPageInvitationsTableWorker() {
    try {
        yield call(fetchOrganizationPageInvitationsTableSaga)
    } catch (error) {
        yield put(fetchOrganizationPageInvitationsTableFailure(error))
    }
}

/**
 * Fetches data for organization integrations table
 */
export function* fetchOrganizationPageIntegrationsTableWorker() {
    try {
        yield call(fetchOrganizationPageIntegrationsTableSaga)
    } catch (error) {
        yield put(fetchOrganizationPageIntegrationsTableFailure(error))
    }
}

/**
 * Remove a member from an organization
 */
export function* removeMemberOrganizationPageWorker(action) {
    try {
        yield call(removeOrganizationPageMemberSaga, action)
        // reload the paginated/sorted table
        yield put(fetchOrganizationPageMembersTableRequest())
    } catch (error) {
        yield put(removeMemberOrganizationPageFailure(error))
    }
}

/**
 * Mounts the Organization Page and fetches data
 */
export function* mountOrganizationPageWorker() {
    try {
        yield call(fetchOrganizationPageDataSaga)
        yield put(mountOrganizationPageSuccess())
    } catch (error) {
        yield put(mountOrganizationPageFailure(error))
    }
}
