import { all, takeLatest } from 'redux-saga/effects'

import {
    mountOrganizationPageRequest,
    createOrganizationPageGroupRequest,
    createOrganizationPageIntegrationRequest,
    changeOrganizationPageBrandsSearchInput,
    inviteMemberOrganizationPageRequest,
    fetchOrganizationPageGroupsTableRequest,
    fetchOrganizationPageIntegrationsTableRequest,
    fetchOrganizationPageInvitationsTableRequest,
    removeMemberOrganizationPageRequest,
    fetchOrganizationPageMembersTableRequest,
} from 'actions/ui'

import {
    mountOrganizationPageWorker,
    createOrganizationPageGroupWorker,
    changeOrganizationPageBrandsSearchInputWorker,
    inviteMemberOrganizationPageWorker,
    createOrganizationPageIntegrationWorker,
    fetchOrganizationPageGroupsTableWorker,
    fetchOrganizationPageIntegrationsTableWorker,
    fetchOrganizationPageInvitationsTableWorker,
    removeMemberOrganizationPageWorker,
    fetchOrganizationPageMembersTableWorker,
} from './workers'

export default function* organizationPageWorker() {
    yield all([
        takeLatest(
            mountOrganizationPageRequest.toString(),
            mountOrganizationPageWorker
        ),
        takeLatest(
            createOrganizationPageGroupRequest.toString(),
            createOrganizationPageGroupWorker
        ),
        takeLatest(
            changeOrganizationPageBrandsSearchInput.toString(),
            changeOrganizationPageBrandsSearchInputWorker
        ),
        takeLatest(
            inviteMemberOrganizationPageRequest.toString(),
            inviteMemberOrganizationPageWorker
        ),
        takeLatest(
            createOrganizationPageIntegrationRequest.toString(),
            createOrganizationPageIntegrationWorker
        ),
        takeLatest(
            fetchOrganizationPageGroupsTableRequest.toString(),
            fetchOrganizationPageGroupsTableWorker
        ),
        takeLatest(
            fetchOrganizationPageIntegrationsTableRequest.toString(),
            fetchOrganizationPageIntegrationsTableWorker
        ),
        takeLatest(
            fetchOrganizationPageInvitationsTableRequest.toString(),
            fetchOrganizationPageInvitationsTableWorker
        ),
        takeLatest(
            fetchOrganizationPageMembersTableRequest.toString(),
            fetchOrganizationPageMembersTableWorker
        ),
        takeLatest(
            removeMemberOrganizationPageRequest.toString(),
            removeMemberOrganizationPageWorker
        ),
    ])
}
