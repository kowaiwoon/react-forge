import { all, takeLatest } from 'redux-saga/effects'

import {
    mountOrganizationGroupPageRequest,
    addMemberOrganizationGroupPageRequest,
    removeMemberOrganizationGroupPageRequest,
    changeOrganizationGroupPageBrandsSearchInput,
    updateNameOrganizationGroupPageRequest,
    updatePermissionsOrganizationGroupPageRequest,
    updateResourcesOrganizationGroupPageRequest,
} from 'actions/ui'

import {
    mountOrganizationGroupPageWorker,
    addMemberOrganizationGroupPageWorker,
    removeMemberOrganizationGroupPageWorker,
    updateNameOrganizationGroupPageWorker,
    updatePermissionsOrganizationGroupPageWorker,
    changeOrganizationGroupPageBrandsSearchInputWorker,
    updateResourcesOrganizationGroupPageWorker,
} from './workers'

export default function* organizationGroupPageWorker() {
    yield all([
        takeLatest(
            mountOrganizationGroupPageRequest.toString(),
            mountOrganizationGroupPageWorker
        ),
        takeLatest(
            updateNameOrganizationGroupPageRequest.toString(),
            updateNameOrganizationGroupPageWorker
        ),
        takeLatest(
            addMemberOrganizationGroupPageRequest.toString(),
            addMemberOrganizationGroupPageWorker
        ),
        takeLatest(
            removeMemberOrganizationGroupPageRequest.toString(),
            removeMemberOrganizationGroupPageWorker
        ),
        takeLatest(
            updatePermissionsOrganizationGroupPageRequest.toString(),
            updatePermissionsOrganizationGroupPageWorker
        ),
        takeLatest(
            updateResourcesOrganizationGroupPageRequest.toString(),
            updateResourcesOrganizationGroupPageWorker
        ),
        takeLatest(
            changeOrganizationGroupPageBrandsSearchInput.toString(),
            changeOrganizationGroupPageBrandsSearchInputWorker
        ),
    ])
}
