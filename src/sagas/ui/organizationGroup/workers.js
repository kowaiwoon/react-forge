import { call, all, select, put } from 'redux-saga/effects'
import { delay } from 'redux-saga'

import { selectDomainValue } from 'selectors/ui'
import { ORGANIZATION_GROUP_PAGE } from 'constants/pages'
import { SEARCH_RESULTS_PER_QUERY } from 'configuration/typeahead'
import {
    mountOrganizationGroupPageSuccess,
    mountOrganizationGroupPageFailure,
    addMemberOrganizationGroupPageSuccess,
    removeMemberOrganizationGroupPageSuccess,
    addMemberOrganizationGroupPageFailure,
    removeMemberOrganizationGroupPageFailure,
    updateNameOrganizationGroupPageSuccess,
    updateNameOrganizationGroupPageFailure,
    updatePermissionsOrganizationGroupPageSuccess,
    updatePermissionsOrganizationGroupPageFailure,
    updateResourcesOrganizationGroupPageSuccess,
    updateResourcesOrganizationGroupPageFailure,
    searchOrganizationGroupPageBrandsRequest,
    searchOrganizationGroupPageBrandsSuccess,
    searchOrganizationGroupPageBrandsFailure,
} from 'actions/ui'
import { fetchOrganizationGroupSaga } from 'sagas/orgs/groups'
import {
    fetchOrganizationMembersSaga,
    fetchOrganizationGroupMembersSaga,
} from 'sagas/orgs/members'
import cerebroApiSaga from 'sagas/common/cerebroApi'
import {
    addMemberToOrganizationGroup,
    removeMemberFromOrganizationGroup,
    patchOrganizationGroup,
    getBrandsForOrg,
} from 'services/cerebroApi'
import { fetchUserPermissionsSaga } from 'sagas/orgs/userPermissions'

function* fetchOrganizationGroupPageDataSaga() {
    const organizationId = yield select(selectDomainValue, [
        ORGANIZATION_GROUP_PAGE,
        'organizationId',
    ])
    const organizationGroupId = yield select(selectDomainValue, [
        ORGANIZATION_GROUP_PAGE,
        'organizationGroupId',
    ])
    yield all([
        call(fetchOrganizationGroupSaga, organizationGroupId),
        call(fetchOrganizationGroupMembersSaga, organizationGroupId),
        call(fetchOrganizationMembersSaga, organizationId),
        call(fetchUserPermissionsSaga, organizationId),
    ])
}

function* addMemberSaga(action) {
    const organizationGroupId = yield select(selectDomainValue, [
        ORGANIZATION_GROUP_PAGE,
        'organizationGroupId',
    ])

    yield call(
        cerebroApiSaga,
        addMemberOrganizationGroupPageSuccess,
        addMemberToOrganizationGroup,
        organizationGroupId,
        action.payload
    )
}

function* removeMemberSaga(action) {
    const organizationGroupId = yield select(selectDomainValue, [
        ORGANIZATION_GROUP_PAGE,
        'organizationGroupId',
    ])
    const { memberId } = action.payload

    yield call(
        cerebroApiSaga,
        null,
        removeMemberFromOrganizationGroup,
        organizationGroupId,
        memberId
    )

    yield put(
        removeMemberOrganizationGroupPageSuccess({
            organizationGroupId,
            memberId,
        })
    )
}

function* updateNameSaga(action) {
    const organizationGroupId = yield select(selectDomainValue, [
        ORGANIZATION_GROUP_PAGE,
        'organizationGroupId',
    ])

    yield call(
        cerebroApiSaga,
        updateNameOrganizationGroupPageSuccess,
        patchOrganizationGroup,
        organizationGroupId,
        action.payload
    )
}

function* updatePermissionsSaga(action) {
    const organizationGroupId = yield select(selectDomainValue, [
        ORGANIZATION_GROUP_PAGE,
        'organizationGroupId',
    ])

    yield call(
        cerebroApiSaga,
        updatePermissionsOrganizationGroupPageSuccess,
        patchOrganizationGroup,
        organizationGroupId,
        action.payload
    )
}

function* updateResourcesSaga(action) {
    const organizationGroupId = yield select(selectDomainValue, [
        ORGANIZATION_GROUP_PAGE,
        'organizationGroupId',
    ])

    yield call(
        cerebroApiSaga,
        updateResourcesOrganizationGroupPageSuccess,
        patchOrganizationGroup,
        organizationGroupId,
        action.payload
    )
}

function* searchBrandsSaga(query) {
    if (query.length > 1) {
        const organizationId = yield select(selectDomainValue, [
            ORGANIZATION_GROUP_PAGE,
            'organizationId',
        ])
        yield put(searchOrganizationGroupPageBrandsRequest())
        const {
            data: { results },
        } = yield call(cerebroApiSaga, null, getBrandsForOrg, organizationId, {
            limit: SEARCH_RESULTS_PER_QUERY,
            brand_name__icontains: query,
        })
        yield put(
            searchOrganizationGroupPageBrandsSuccess({
                results,
                organizationId,
            })
        )
    }
}

/**
 * Fetches typeahead data for brands search
 *
 * @param action
 * @returns {IterableIterator<*>}
 */
export function* changeOrganizationGroupPageBrandsSearchInputWorker(action) {
    // debounce by 500ms
    yield delay(500)
    try {
        yield call(searchBrandsSaga, action.payload)
    } catch (error) {
        yield put(searchOrganizationGroupPageBrandsFailure(error))
    }
}

/**
 * Update an organization group's name
 */
export function* updateNameOrganizationGroupPageWorker(action) {
    try {
        yield call(updateNameSaga, action)
    } catch (error) {
        yield put(updateNameOrganizationGroupPageFailure(error))
    }
}

/**
 * Update an organization group's permissions
 */
export function* updatePermissionsOrganizationGroupPageWorker(action) {
    try {
        yield call(updatePermissionsSaga, action)
    } catch (error) {
        yield put(updatePermissionsOrganizationGroupPageFailure(error))
    }
}

/**
 * Update an organization group's resources type and resources
 */
export function* updateResourcesOrganizationGroupPageWorker(action) {
    try {
        yield call(updateResourcesSaga, action)
    } catch (error) {
        yield put(updateResourcesOrganizationGroupPageFailure(error))
    }
}

/**
 * Remove a member from an organization group
 */
export function* removeMemberOrganizationGroupPageWorker(action) {
    const organizationGroupId = yield select(selectDomainValue, [
        ORGANIZATION_GROUP_PAGE,
        'organizationGroupId',
    ])

    try {
        yield call(removeMemberSaga, action)
        yield call(fetchOrganizationGroupMembersSaga, organizationGroupId)
    } catch (error) {
        yield put(removeMemberOrganizationGroupPageFailure(error))
    }
}

/**
 * Add a member to an organization group
 */
export function* addMemberOrganizationGroupPageWorker(action) {
    const organizationGroupId = yield select(selectDomainValue, [
        ORGANIZATION_GROUP_PAGE,
        'organizationGroupId',
    ])

    try {
        yield call(addMemberSaga, action)
        yield call(fetchOrganizationGroupMembersSaga, organizationGroupId)
    } catch (error) {
        yield put(addMemberOrganizationGroupPageFailure(error))
    }
}

/**
 * Mounts the organization group page and fetches data
 */
export function* mountOrganizationGroupPageWorker() {
    try {
        yield call(fetchOrganizationGroupPageDataSaga)
        yield put(mountOrganizationGroupPageSuccess())
    } catch (error) {
        yield put(mountOrganizationGroupPageFailure(error))
    }
}
