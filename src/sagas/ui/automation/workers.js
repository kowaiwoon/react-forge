import { call, put, all, select } from 'redux-saga/effects'

import { getAutomationDescription, getChangelog } from 'services/cerebroApi'
import {
    // mounting
    mountAutomationPageSuccess,
    mountAutomationPageFailure,
    // description
    fetchAutomationPageDescriptionSuccess,
    // page data
    fetchAutomationPageDataSuccess,
    fetchAutomationPageDataFailure,
    // table data
    fetchAutomationPageTableFailure,
    fetchAutomationPageTableSuccess,
    fetchAutomationPageTableSettingsSuccess,
} from 'actions/ui'
import cerebroApiSaga from 'sagas/common/cerebroApi'
import { AUTOMATION_PAGE } from 'constants/pages'
import {
    selectPageFilters,
    selectDomainValue as selectUiDomainValue,
} from 'selectors/ui'
import { selectDomainValue as selectAuthDomainValue } from 'selectors/auth'
import { formatPagination, formatSorter } from 'helpers/params'
import {
    isCurrentSchema as isCurrentColumnSettingsSchema,
    createColumnSettingsItem,
    createColumnSettingsKey,
} from 'helpers/columnSettings'
import {
    putItemToUserSettingsTable,
    getItemFromUserSettingsTable,
} from 'services/dynamoApi'
import { formatFilters } from 'helpers/ui/automationPage'
import { hasPermissions } from 'helpers/featurePermissions'
import { AUTOMATION } from 'constants/featurePermissions'
import { fetchPageFilterSettingsSaga } from '../shared/workers'

function* updateAutomationTableColumnSettingsSaga() {
    const userId = yield select(selectAuthDomainValue, ['username'])
    const { actionColumns, order, displayState } = yield select(
        selectUiDomainValue,
        [AUTOMATION_PAGE, 'table', 'columnSettings']
    )

    const item = createColumnSettingsItem({
        userId,
        page: AUTOMATION_PAGE,
        tableName: 'table',
        actionColumns,
        order,
        displayState,
    })

    yield call(putItemToUserSettingsTable, item)
}

function* fetchAutomationPageTableSettingsSaga() {
    const userId = yield select(selectAuthDomainValue, ['username'])
    const columnSettings = yield select(selectUiDomainValue, [
        AUTOMATION_PAGE,
        'table',
        'columnSettings',
    ])

    const key = createColumnSettingsKey({
        userId,
        page: AUTOMATION_PAGE,
        tableName: 'table',
    })

    const { Item } = yield call(getItemFromUserSettingsTable, key)

    if (Item) {
        if (isCurrentColumnSettingsSchema(Item, columnSettings)) {
            yield put(fetchAutomationPageTableSettingsSuccess(Item))
        } else {
            yield call(updateAutomationTableColumnSettingsSaga)
        }
    }
}

function* fetchAutomationPageTableSaga() {
    const filters = yield select(selectPageFilters, AUTOMATION_PAGE)
    const { pagination, sorter } = yield select(selectUiDomainValue, [
        AUTOMATION_PAGE,
        'table',
    ])
    const params = {
        ...formatPagination(pagination),
        ...formatSorter(sorter),
        ...formatFilters(filters),
    }

    yield call(
        cerebroApiSaga,
        fetchAutomationPageTableSuccess,
        getChangelog,
        params
    )
}

function* fetchAutomationDescriptionSaga() {
    yield call(
        cerebroApiSaga,
        fetchAutomationPageDescriptionSuccess,
        getAutomationDescription
    )
}

function* fetchAutomationPageDataSaga() {
    const featurePermissions = yield select(
        selectAuthDomainValue,
        'featurePermissions'
    )

    if (hasPermissions(featurePermissions, AUTOMATION)) {
        yield all([
            call(fetchAutomationDescriptionSaga),
            call(fetchAutomationPageTableSaga),
        ])
    }
}

/**
 * Puts new table settings to DynamoDB
 */
export function* updateAutomationPageTableSettingsWorker() {
    yield call(updateAutomationTableColumnSettingsSaga)
}

/**
 * Fetches data for Automation Page Table
 */
export function* fetchAutomationPageTableWorker() {
    try {
        yield call(fetchAutomationPageTableSaga)
    } catch (error) {
        yield put(fetchAutomationPageTableFailure(error))
    }
}

/**
 * Fetches all data required for the Automation Page
 */
export function* fetchAutomationPageDataWorker() {
    try {
        yield call(fetchAutomationPageDataSaga)
        yield put(fetchAutomationPageDataSuccess())
    } catch (error) {
        yield put(fetchAutomationPageDataFailure(error))
    }
}

/**
 * Mounts the Automation Page and fetches data
 */
export function* mountAutomationPageWorker() {
    try {
        yield call(fetchPageFilterSettingsSaga, AUTOMATION_PAGE)
        yield call(fetchAutomationPageTableSettingsSaga)
        yield call(fetchAutomationPageDataSaga)
        yield put(mountAutomationPageSuccess())
    } catch (error) {
        yield put(mountAutomationPageFailure(error))
    }
}
