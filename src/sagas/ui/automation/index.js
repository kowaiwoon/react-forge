import { all, takeLatest } from 'redux-saga/effects'

import {
    mountAutomationPageRequest,
    fetchAutomationPageDataRequest,
    fetchAutomationPageTableRequest,
    updateAutomationPageTableSettings,
} from 'actions/ui'

import {
    mountAutomationPageWorker,
    fetchAutomationPageDataWorker,
    fetchAutomationPageTableWorker,
    updateAutomationPageTableSettingsWorker,
} from './workers'

export default function* automationPageWorker() {
    yield all([
        takeLatest(
            mountAutomationPageRequest.toString(),
            mountAutomationPageWorker
        ),
        takeLatest(
            fetchAutomationPageDataRequest.toString(),
            fetchAutomationPageDataWorker
        ),
        takeLatest(
            fetchAutomationPageTableRequest.toString(),
            fetchAutomationPageTableWorker
        ),
        takeLatest(
            updateAutomationPageTableSettings.toString(),
            updateAutomationPageTableSettingsWorker
        ),
    ])
}
