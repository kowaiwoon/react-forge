import { all, takeLatest } from 'redux-saga/effects'

import {
    attachCampaignPageKeywordsTableKeywordsRequest,
    attachCampaignPageProductsTableProductsRequest,
    deleteCampaignPageKeywordsTableKeywordRequest,
    deleteCampaignPageProductsTableProductRequest,
    downloadCampaignPageKeywordsTableRequest,
    downloadCampaignPageProductsTableRequest,
    downloadCampaignPageTimeseriesRequest,
    fetchCampaignPageDataRequest,
    fetchCampaignPageHourlyMultipliersRequest,
    fetchCampaignPageKeywordsTableRequest,
    fetchCampaignPageProductsTableRequest,
    mountCampaignPageRequest,
    syncCampaignRequest,
    updateCampaignPageCampaignDetailsRequest,
    updateCampaignPageHourlyMultipliersRequest,
    updateCampaignPageKeywordsTableKeywordRequest,
    updateCampaignPageKeywordsTableSettings,
    updateCampaignPageProductsTableProductRequest,
    updateCampaignPageProductsTableSettings,
} from 'actions/ui'

import {
    attachCampaignPageKeywordsTableKeywordsWorker,
    attachCampaignPageProductsTableProductsWorker,
    deleteCampaignPageKeywordsTableKeywordWorker,
    deleteCampaignPageProductsTableProductWorker,
    downloadCampaignPageKeywordsTableWorker,
    downloadCampaignPageProductsTableWorker,
    downloadCampaignPageTimeseriesWorker,
    fetchCampaignPageDataWorker,
    fetchCampaignPageHourlyMultipliersWorker,
    fetchCampaignPageKeywordsTableWorker,
    fetchCampaignPageProductsTableWorker,
    mountCampaignPageWorker,
    syncCampaignWorker,
    updateCampaignPageCampaignDetailsWorker,
    updateCampaignPageHourlyMultipliersWorker,
    updateCampaignPageKeywordsTableKeywordWorker,
    updateCampaignPageKeywordsTableSettingsWorker,
    updateCampaignPageProductsTableProductWorker,
    updateCampaignPageProductsTableSettingsWorker,
} from './workers'

export default function* campaignPageWorker() {
    yield all([
        takeLatest(
            mountCampaignPageRequest.toString(),
            mountCampaignPageWorker
        ),

        takeLatest(
            fetchCampaignPageDataRequest.toString(),
            fetchCampaignPageDataWorker
        ),

        takeLatest(
            updateCampaignPageCampaignDetailsRequest.toString(),
            updateCampaignPageCampaignDetailsWorker
        ),

        takeLatest(
            fetchCampaignPageHourlyMultipliersRequest.toString(),
            fetchCampaignPageHourlyMultipliersWorker
        ),

        takeLatest(
            updateCampaignPageHourlyMultipliersRequest.toString(),
            updateCampaignPageHourlyMultipliersWorker
        ),

        takeLatest(
            fetchCampaignPageKeywordsTableRequest.toString(),
            fetchCampaignPageKeywordsTableWorker
        ),

        takeLatest(
            fetchCampaignPageProductsTableRequest.toString(),
            fetchCampaignPageProductsTableWorker
        ),

        takeLatest(
            downloadCampaignPageTimeseriesRequest.toString(),
            downloadCampaignPageTimeseriesWorker
        ),

        takeLatest(
            downloadCampaignPageKeywordsTableRequest.toString(),
            downloadCampaignPageKeywordsTableWorker
        ),

        takeLatest(
            attachCampaignPageKeywordsTableKeywordsRequest.toString(),
            attachCampaignPageKeywordsTableKeywordsWorker
        ),

        takeLatest(
            updateCampaignPageKeywordsTableKeywordRequest.toString(),
            updateCampaignPageKeywordsTableKeywordWorker
        ),

        takeLatest(
            deleteCampaignPageKeywordsTableKeywordRequest.toString(),
            deleteCampaignPageKeywordsTableKeywordWorker
        ),

        takeLatest(
            downloadCampaignPageProductsTableRequest.toString(),
            downloadCampaignPageProductsTableWorker
        ),

        takeLatest(
            updateCampaignPageKeywordsTableSettings.toString(),
            updateCampaignPageKeywordsTableSettingsWorker
        ),

        takeLatest(
            updateCampaignPageProductsTableSettings.toString(),
            updateCampaignPageProductsTableSettingsWorker
        ),

        takeLatest(
            attachCampaignPageProductsTableProductsRequest.toString(),
            attachCampaignPageProductsTableProductsWorker
        ),

        takeLatest(
            updateCampaignPageProductsTableProductRequest.toString(),
            updateCampaignPageProductsTableProductWorker
        ),

        takeLatest(
            deleteCampaignPageProductsTableProductRequest.toString(),
            deleteCampaignPageProductsTableProductWorker
        ),

        takeLatest(syncCampaignRequest.toString(), syncCampaignWorker),
    ])
}
