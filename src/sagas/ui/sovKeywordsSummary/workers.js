import { all, call, select, put } from 'redux-saga/effects'
import { delay } from 'redux-saga'
import isEmpty from 'lodash/isEmpty'

import { SOV_CHART } from 'constants/reducerKeys'
import { SOV_AGGREGATION } from 'constants/filters'
import { SOV_KEYWORDS_SUMMARY_PAGE } from 'constants/pages'
import { SEARCH_RESULTS_PER_QUERY } from 'configuration/typeahead'
import {
    formatAllParams,
    formatFilters,
    formatSovFactAggregateParams,
} from 'helpers/params'
import { downloadCsv } from 'helpers/downloads'
import {
    getSovBrands,
    getSovKeywords,
    getSovKeywordCategories,
    getSovKeywordsExport,
    getSovFactAggregate,
} from 'services/cerebroApi'
import {
    putItemToUserSettingsTable,
    getItemFromUserSettingsTable,
} from 'services/dynamoApi'
import {
    isCurrentSchema as isCurrentColumnSettingsSchema,
    createColumnSettingsItem,
    createColumnSettingsKey,
} from 'helpers/columnSettings'
import {
    // mounting
    mountSovKeywordsSummaryPageSuccess,
    mountSovKeywordsSummaryPageFailure,

    // page data
    fetchSovKeywordsSummaryPageDataSuccess,
    fetchSovKeywordsSummaryPageDataFailure,

    // table data
    fetchSovKeywordsSummaryPageTableSuccess,
    fetchSovKeywordsSummaryPageTableFailure,

    // table settings
    fetchSovKeywordsSummaryPageTableSettingsSuccess,

    // table download
    downloadSovKeywordsSummaryPageTableSuccess,
    downloadSovKeywordsSummaryPageTableFailure,

    // table keyword attach
    attachSovKeywordsSummaryPageTableKeywordsSuccess,
    attachSovKeywordsSummaryPageTableKeywordsFailure,

    // table keyword update
    updateSovKeywordsSummaryPageTableKeywordSuccess,
    updateSovKeywordsSummaryPageTableKeywordFailure,

    // table keyword delete
    deleteSovKeywordsSummaryPageTableKeywordSuccess,
    deleteSovKeywordsSummaryPageTableKeywordFailure,

    // search brands
    searchSovBrandsRequest,
    searchSovBrandsSuccess,
    searchSovBrandsFailure,

    // search keywords
    searchSovKeywordsRequest,
    searchSovKeywordsSuccess,
    searchSovKeywordsFailure,

    // search categories
    searchSovKeywordCategoriesRequest,
    searchSovKeywordCategoriesSuccess,
    searchSovKeywordCategoriesFailure,

    // sov chart
    fetchSovKeywordsSummaryPageSovChartSuccess,
} from 'actions/ui'
import { selectDomainValue as selectAuthDomainValue } from 'selectors/auth'
import {
    selectDomainValue as selectUiDomainValue,
    selectPageFilters,
} from 'selectors/ui'
import { selectSovKeywordCategoriesForFilter } from 'selectors/entities'
import cerebroApiSaga from 'sagas/common/cerebroApi'
import {
    attachSovKeywordSaga as attachEntitiesSovKeywordSaga,
    updateSovKeywordSaga as updateEntitiesSovKeywordSaga,
    deleteSovKeywordSaga as deleteEntitiesSovKeywordSaga,
} from 'sagas/entities'
import { fetchPageFilterSettingsSaga } from '../shared/workers'

function* fetchSovKeywordsSummaryPageTableSaga() {
    const filters = yield select(selectPageFilters, SOV_KEYWORDS_SUMMARY_PAGE)
    const { pagination, sorter } = yield select(selectUiDomainValue, [
        SOV_KEYWORDS_SUMMARY_PAGE,
        'table',
    ])
    const params = formatAllParams(filters, pagination, sorter)

    yield call(
        cerebroApiSaga,
        fetchSovKeywordsSummaryPageTableSuccess,
        getSovKeywords,
        params
    )
}

function* downloadSovKeywordsSummaryPageTableSaga() {
    const filters = yield select(selectPageFilters, SOV_KEYWORDS_SUMMARY_PAGE)
    const params = {
        ...formatFilters(filters),
    }

    const response = yield call(
        cerebroApiSaga,
        downloadSovKeywordsSummaryPageTableSuccess,
        getSovKeywordsExport,
        params
    )

    downloadCsv(response.data, 'sov')
}

function* updateSovKeywordsSummaryPageTableSettingsSaga() {
    const userId = yield select(selectAuthDomainValue, ['username'])
    const { actionColumns, order, displayState } = yield select(
        selectUiDomainValue,
        [SOV_KEYWORDS_SUMMARY_PAGE, 'table', 'columnSettings']
    )

    const item = createColumnSettingsItem({
        userId,
        page: SOV_KEYWORDS_SUMMARY_PAGE,
        tableName: 'table',
        actionColumns,
        order,
        displayState,
    })

    yield call(putItemToUserSettingsTable, item)
}

function* fetchSovKeywordsSummaryTableSettingsSaga() {
    const userId = yield select(selectAuthDomainValue, ['username'])
    const columnSettings = yield select(selectUiDomainValue, [
        SOV_KEYWORDS_SUMMARY_PAGE,
        'table',
        'columnSettings',
    ])

    const key = createColumnSettingsKey({
        userId,
        page: SOV_KEYWORDS_SUMMARY_PAGE,
        tableName: 'table',
    })

    const { Item } = yield call(getItemFromUserSettingsTable, key)

    if (Item) {
        if (isCurrentColumnSettingsSchema(Item, columnSettings)) {
            yield put(fetchSovKeywordsSummaryPageTableSettingsSuccess(Item))
        } else {
            yield call(updateSovKeywordsSummaryPageTableSettingsSaga)
        }
    }
}

function* fetchSovKeywordsSummaryPageSovChartSaga() {
    const filters = yield select(selectPageFilters, SOV_KEYWORDS_SUMMARY_PAGE)
    const { pagination, sorter, groups } = yield select(selectUiDomainValue, [
        SOV_KEYWORDS_SUMMARY_PAGE,
        SOV_CHART,
    ])
    const params = formatSovFactAggregateParams({
        filters,
        pagination,
        sorter,
        aggregation: filters[SOV_AGGREGATION],
        groups,
    })

    yield call(
        cerebroApiSaga,
        fetchSovKeywordsSummaryPageSovChartSuccess,
        getSovFactAggregate,
        params
    )
}

function* fetchSovKeywordsSummaryPageDataSaga() {
    try {
        yield all([
            call(fetchSovKeywordsSummaryPageTableSaga),
            call(fetchSovKeywordsSummaryPageSovChartSaga),
        ])
    } catch (error) {
        yield put(fetchSovKeywordsSummaryPageTableFailure(error))
    }
}

function* attachSovKeywordsSaga(action) {
    const { keywords } = action.payload

    yield all(
        keywords.map(keyword => call(attachEntitiesSovKeywordSaga, keyword))
    )
}

function* updateSovKeywordSaga(action) {
    const { keywordId, data } = action.payload

    yield call(updateEntitiesSovKeywordSaga, keywordId, data)
}

function* deleteSovKeywordSaga(action) {
    const { keywordId } = action.payload

    yield call(deleteEntitiesSovKeywordSaga, keywordId)
}

function* searchSovBrandsSaga(query) {
    if (query.length > 1) {
        yield put(searchSovBrandsRequest())
        yield call(cerebroApiSaga, searchSovBrandsSuccess, getSovBrands, {
            limit: SEARCH_RESULTS_PER_QUERY,
            brand__icontains: query,
        })
    }
}

function* searchSovKeywordsSaga(query) {
    if (query.length > 1) {
        yield put(searchSovKeywordsRequest())
        yield call(cerebroApiSaga, searchSovKeywordsSuccess, getSovKeywords, {
            limit: SEARCH_RESULTS_PER_QUERY,
            text__icontains: query,
        })
    }
}

function* searchSovKeywordCategoriesSaga() {
    const categories = yield select(selectSovKeywordCategoriesForFilter)
    if (isEmpty(categories)) {
        yield put(searchSovKeywordCategoriesRequest())
        yield call(
            cerebroApiSaga,
            searchSovKeywordCategoriesSuccess,
            getSovKeywordCategories
        )
    }
}

/**
 * Puts new table settings to DynamoDB
 */
export function* updateSovKeywordsSummaryPageTableSettingsWorker() {
    yield call(updateSovKeywordsSummaryPageTableSettingsSaga)
}

/**
 * Fetches data for SOV Keywords Summary Page Table
 */
export function* fetchSovKeywordsSummaryPageTableWorker() {
    try {
        yield call(fetchSovKeywordsSummaryPageTableSaga)
    } catch (error) {
        yield put(fetchSovKeywordsSummaryPageTableFailure(error))
    }
}

/**
 * Downloads data for the SOV Keywords Summary Page
 */
export function* downloadSovKeywordsSummaryPageTableWorker() {
    try {
        yield call(downloadSovKeywordsSummaryPageTableSaga)
    } catch (error) {
        yield put(downloadSovKeywordsSummaryPageTableFailure(error))
    }
}

/**
 * Attaches Keyword in table
 */
export function* attachSovKeywordsSummaryPageTableKeywordsWorker(action) {
    try {
        yield call(attachSovKeywordsSaga, action)
        yield call(fetchSovKeywordsSummaryPageTableSaga)
        yield put(attachSovKeywordsSummaryPageTableKeywordsSuccess())
    } catch (error) {
        yield put(attachSovKeywordsSummaryPageTableKeywordsFailure(error))
    }
}

/**
 * Updates Keyword in table
 */
export function* updateSovKeywordsSummaryPageTableKeywordWorker(action) {
    try {
        yield call(updateSovKeywordSaga, action)
        yield call(fetchSovKeywordsSummaryPageTableSaga)
        yield put(updateSovKeywordsSummaryPageTableKeywordSuccess())
    } catch (error) {
        yield put(updateSovKeywordsSummaryPageTableKeywordFailure(error))
    }
}

/**
 * Deletes Keyword in table
 */
export function* deleteSovKeywordsSummaryPageTableKeywordWorker(action) {
    try {
        yield call(deleteSovKeywordSaga, action)
        yield call(fetchSovKeywordsSummaryPageTableSaga)
        yield put(deleteSovKeywordsSummaryPageTableKeywordSuccess())
    } catch (error) {
        yield put(deleteSovKeywordsSummaryPageTableKeywordFailure(error))
    }
}

/**
 * Fetches all data required for the SOV Keywords Summary Page
 */
export function* fetchSovKeywordsSummaryPageDataWorker() {
    try {
        yield call(fetchSovKeywordsSummaryPageDataSaga)
        yield put(fetchSovKeywordsSummaryPageDataSuccess())
    } catch (error) {
        yield put(fetchSovKeywordsSummaryPageDataFailure(error))
    }
}

/**
 * Mounts the SOV Keywords Summary Page and fetches data
 */
export function* mountSovKeywordsSummaryPageWorker() {
    try {
        yield call(fetchPageFilterSettingsSaga, SOV_KEYWORDS_SUMMARY_PAGE)
        yield call(fetchSovKeywordsSummaryTableSettingsSaga)
        yield call(fetchSovKeywordsSummaryPageDataSaga)
        yield put(mountSovKeywordsSummaryPageSuccess())
    } catch (error) {
        yield put(mountSovKeywordsSummaryPageFailure(error))
    }
}

/**
 * Searches for SOV brands by prefix for filter component
 */
export function* changeSovBrandsFilterInputWorker(action) {
    // debounce by 500ms
    yield delay(500)
    try {
        yield call(searchSovBrandsSaga, action.payload)
    } catch (error) {
        yield put(searchSovBrandsFailure(error))
    }
}

/**
 * Searches for SOV keywords by prefix for filter component
 */
export function* changeSovKeywordsFilterInputWorker(action) {
    // debounce by 500ms
    yield delay(500)
    try {
        yield call(searchSovKeywordsSaga, action.payload)
    } catch (error) {
        yield put(searchSovKeywordsFailure(error))
    }
}

/**
 * Searches for SOV keyword categories by prefix for filter component
 *
 * This is a temporary hack until Labels are available
 */
export function* changeSovKeywordCategoriesFilterInputWorker() {
    try {
        yield call(searchSovKeywordCategoriesSaga)
    } catch (error) {
        yield put(searchSovKeywordCategoriesFailure(error))
    }
}
