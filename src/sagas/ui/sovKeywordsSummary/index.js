import { all, takeLatest } from 'redux-saga/effects'

import {
    mountSovKeywordsSummaryPageRequest,
    fetchSovKeywordsSummaryPageDataRequest,
    updateSovKeywordsSummaryPageTableSettings,
    fetchSovKeywordsSummaryPageTableRequest,
    downloadSovKeywordsSummaryPageTableRequest,
    attachSovKeywordsSummaryPageTableKeywordsRequest,
    updateSovKeywordsSummaryPageTableKeywordRequest,
    deleteSovKeywordsSummaryPageTableKeywordRequest,
    changeSovBrandsFilterInput,
    changeSovKeywordsFilterInput,
    changeSovKeywordCategoriesFilterInput,
} from 'actions/ui'
import {
    mountSovKeywordsSummaryPageWorker,
    fetchSovKeywordsSummaryPageDataWorker,
    fetchSovKeywordsSummaryPageTableWorker,
    downloadSovKeywordsSummaryPageTableWorker,
    attachSovKeywordsSummaryPageTableKeywordsWorker,
    updateSovKeywordsSummaryPageTableKeywordWorker,
    deleteSovKeywordsSummaryPageTableKeywordWorker,
    updateSovKeywordsSummaryPageTableSettingsWorker,
    changeSovBrandsFilterInputWorker,
    changeSovKeywordsFilterInputWorker,
    changeSovKeywordCategoriesFilterInputWorker,
} from './workers'

export default function* sovKeywordsSummaryPageWorker() {
    yield all([
        takeLatest(
            mountSovKeywordsSummaryPageRequest.toString(),
            mountSovKeywordsSummaryPageWorker
        ),

        takeLatest(
            fetchSovKeywordsSummaryPageDataRequest.toString(),
            fetchSovKeywordsSummaryPageDataWorker
        ),

        takeLatest(
            fetchSovKeywordsSummaryPageTableRequest.toString(),
            fetchSovKeywordsSummaryPageTableWorker
        ),

        takeLatest(
            downloadSovKeywordsSummaryPageTableRequest.toString(),
            downloadSovKeywordsSummaryPageTableWorker
        ),

        takeLatest(
            attachSovKeywordsSummaryPageTableKeywordsRequest.toString(),
            attachSovKeywordsSummaryPageTableKeywordsWorker
        ),

        takeLatest(
            updateSovKeywordsSummaryPageTableKeywordRequest.toString(),
            updateSovKeywordsSummaryPageTableKeywordWorker
        ),

        takeLatest(
            deleteSovKeywordsSummaryPageTableKeywordRequest.toString(),
            deleteSovKeywordsSummaryPageTableKeywordWorker
        ),

        takeLatest(
            updateSovKeywordsSummaryPageTableSettings.toString(),
            updateSovKeywordsSummaryPageTableSettingsWorker
        ),

        takeLatest(
            changeSovBrandsFilterInput.toString(),
            changeSovBrandsFilterInputWorker
        ),

        takeLatest(
            changeSovKeywordsFilterInput.toString(),
            changeSovKeywordsFilterInputWorker
        ),

        takeLatest(
            changeSovKeywordCategoriesFilterInput.toString(),
            changeSovKeywordCategoriesFilterInputWorker
        ),
    ])
}
