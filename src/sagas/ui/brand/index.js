import { all, takeLatest } from 'redux-saga/effects'

import {
    mountBrandPageRequest,
    fetchBrandPageDataRequest,
    updateBrandPageTableSettings,
    fetchBrandPageTreemapRequest,
    fetchBrandPageTableRequest,
    downloadBrandPageTimeseriesRequest,
    downloadBrandPageTableRequest,
    updateBrandPageTableRequest,
    deleteBrandPageTableRequest,
} from 'actions/ui'

import {
    mountBrandPageWorker,
    fetchBrandPageDataWorker,
    fetchBrandPageTreemapWorker,
    fetchBrandPageTableWorker,
    downloadBrandPageTimeseriesWorker,
    downloadBrandPageTableWorker,
    updateBrandPageTableWorker,
    deleteBrandPageTableWorker,
    updateBrandPageTableSettingsWorker,
} from './workers'

export default function* brandPageWorker() {
    yield all([
        takeLatest(mountBrandPageRequest.toString(), mountBrandPageWorker),

        takeLatest(
            fetchBrandPageDataRequest.toString(),
            fetchBrandPageDataWorker
        ),

        takeLatest(
            fetchBrandPageTreemapRequest.toString(),
            fetchBrandPageTreemapWorker
        ),

        takeLatest(
            fetchBrandPageTableRequest.toString(),
            fetchBrandPageTableWorker
        ),

        takeLatest(
            downloadBrandPageTimeseriesRequest.toString(),
            downloadBrandPageTimeseriesWorker
        ),

        takeLatest(
            downloadBrandPageTableRequest.toString(),
            downloadBrandPageTableWorker
        ),

        takeLatest(
            updateBrandPageTableSettings.toString(),
            updateBrandPageTableSettingsWorker
        ),

        takeLatest(
            updateBrandPageTableRequest.toString(),
            updateBrandPageTableWorker
        ),

        takeLatest(
            deleteBrandPageTableRequest.toString(),
            deleteBrandPageTableWorker
        ),
    ])
}
