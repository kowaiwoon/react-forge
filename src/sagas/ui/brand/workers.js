import { all, call, select, put } from 'redux-saga/effects'

import { formatAllParams, formatFilters } from 'helpers/params'
import { downloadCsv } from 'helpers/downloads'
import {
    // aggregates
    getCampaignsForBrandFactAggregates,
    getCampaignsForBrandSponsoredProductFactAggregates,
    getCampaignsForBrandHeadlineSearchFactAggregates,

    // aggregate
    getBrandFactAggregate,
    getBrandSponsoredProductFactAggregate,
    getBrandHeadlineSearchFactAggregate,

    // timeseries
    getBrandHeadlineSearchFactTimeseries,
    getBrandSponsoredProductFactTimeseries,

    // export
    getBrandFactTimeseriesExport,
    getCampaignsForBrandFactAggregatesExport,
} from 'services/cerebroApi'
import {
    putItemToUserSettingsTable,
    getItemFromUserSettingsTable,
} from 'services/dynamoApi'
import {
    isCurrentSchema as isCurrentColumnSettingsSchema,
    createColumnSettingsItem,
    createColumnSettingsKey,
} from 'helpers/columnSettings'
import { BRAND_PAGE } from 'constants/pages'
import { SPONSORED_PRODUCT, HEADLINE_SEARCH } from 'constants/factTypes'
import {
    // mounting
    mountBrandPageSuccess,
    mountBrandPageFailure,

    // page data
    fetchBrandPageDataSuccess,
    fetchBrandPageDataFailure,

    // aggregate
    fetchBrandPageAggregateSuccess,

    // timeseries
    fetchBrandPageSponsoredProductTimeseriesSuccess,
    fetchBrandPageHeadlineSearchTimeseriesSuccess,

    // timeseries download
    downloadBrandPageTimeseriesSuccess,
    downloadBrandPageTimeseriesFailure,

    // treemap data
    fetchBrandPageTreemapSuccess,
    fetchBrandPageTreemapFailure,

    // table data
    fetchBrandPageTableSuccess,
    fetchBrandPageTableFailure,

    // table settings
    fetchBrandPageTableSettingsSuccess,

    // table download
    downloadBrandPageTableSuccess,
    downloadBrandPageTableFailure,

    // update table
    updateBrandPageTableSuccess,
    updateBrandPageTableFailure,

    // delete table
    deleteBrandPageTableSuccess,
    deleteBrandPageTableFailure,
} from 'actions/ui'
import {
    selectDomainValue as selectUiDomainValue,
    selectPageFilters,
    selectPageFactTypes,
    selectTreemapSelectedMetric,
    selectTableSelectedMetrics,
    selectCurrencyCode,
} from 'selectors/ui'
import { selectDomainValue as selectAuthDomainValue } from 'selectors/auth'
import cerebroApiSaga from 'sagas/common/cerebroApi'
import {
    fetchBrandSaga as fetchEntitiesBrandSaga,
    updateCampaignSaga as updateEntitiesCampaignSaga,
    deleteCampaignSaga as deleteEntitiesCampaignSaga,
} from 'sagas/entities'

import { fetchPageFilterSettingsSaga } from '../shared/workers'

const getCampaignAggregatesApi = factTypes => {
    if (factTypes.length === 2) {
        return getCampaignsForBrandFactAggregates
    }

    if (factTypes[0].value === SPONSORED_PRODUCT) {
        return getCampaignsForBrandSponsoredProductFactAggregates
    }

    if (factTypes[0].value === HEADLINE_SEARCH) {
        return getCampaignsForBrandHeadlineSearchFactAggregates
    }

    return null
}

const getBrandAggregateApi = factTypes => {
    if (factTypes.length === 2) {
        return getBrandFactAggregate
    }

    if (factTypes[0].value === SPONSORED_PRODUCT) {
        return getBrandSponsoredProductFactAggregate
    }

    if (factTypes[0].value === HEADLINE_SEARCH) {
        return getBrandHeadlineSearchFactAggregate
    }

    return null
}

function* fetchBrandSaga() {
    const brandId = yield select(selectUiDomainValue, [BRAND_PAGE, 'brandId'])
    yield call(fetchEntitiesBrandSaga, brandId)
}

function* fetchBrandPageSponsoredProductTimeseriesSaga() {
    const brandId = yield select(selectUiDomainValue, [BRAND_PAGE, 'brandId'])
    const factTypes = yield select(selectPageFactTypes, BRAND_PAGE)
    if (factTypes.length === 2 || factTypes[0].value === SPONSORED_PRODUCT) {
        const filters = yield select(selectPageFilters, BRAND_PAGE)
        const currency = yield select(selectCurrencyCode)
        const params = {
            ...formatFilters(filters),
            currency,
        }
        params.currency = currency
        yield call(
            cerebroApiSaga,
            fetchBrandPageSponsoredProductTimeseriesSuccess,
            getBrandSponsoredProductFactTimeseries,
            brandId,
            params
        )
    } else {
        yield put(fetchBrandPageSponsoredProductTimeseriesSuccess([]))
    }
}

function* fetchBrandPageHeadlineSearchTimeseriesSaga() {
    const brandId = yield select(selectUiDomainValue, [BRAND_PAGE, 'brandId'])
    const factTypes = yield select(selectPageFactTypes, BRAND_PAGE)
    if (factTypes.length === 2 || factTypes[0].value === HEADLINE_SEARCH) {
        const filters = yield select(selectPageFilters, BRAND_PAGE)
        const currency = yield select(selectCurrencyCode)
        const params = {
            ...formatFilters(filters),
            currency,
        }
        params.currency = currency
        yield call(
            cerebroApiSaga,
            fetchBrandPageHeadlineSearchTimeseriesSuccess,
            getBrandHeadlineSearchFactTimeseries,
            brandId,
            params
        )
    } else {
        yield put(fetchBrandPageHeadlineSearchTimeseriesSuccess([]))
    }
}

function* fetchBrandPageAggregateSaga() {
    const brandId = yield select(selectUiDomainValue, [BRAND_PAGE, 'brandId'])
    const filters = yield select(selectPageFilters, BRAND_PAGE)
    const factTypes = yield select(selectPageFactTypes, BRAND_PAGE)
    const currency = yield select(selectCurrencyCode)
    const params = {
        ...formatFilters(filters),
        currency,
    }
    const aggregateApi = getBrandAggregateApi(factTypes)

    yield call(
        cerebroApiSaga,
        fetchBrandPageAggregateSuccess,
        aggregateApi,
        brandId,
        params
    )
}

function* fetchBrandPageTreemapSaga() {
    const brandId = yield select(selectUiDomainValue, [BRAND_PAGE, 'brandId'])
    const filters = yield select(selectPageFilters, BRAND_PAGE)
    const factTypes = yield select(selectPageFactTypes, BRAND_PAGE)
    const { pagination, sorter } = yield select(selectUiDomainValue, [
        BRAND_PAGE,
        'treemap',
    ])
    const metric = yield select(
        selectTreemapSelectedMetric,
        BRAND_PAGE,
        'treemap'
    )
    const currency = yield select(selectCurrencyCode)
    const params = formatAllParams(
        filters,
        pagination,
        sorter,
        [metric],
        currency
    )
    const aggregatesApi = getCampaignAggregatesApi(factTypes)

    yield call(
        cerebroApiSaga,
        fetchBrandPageTreemapSuccess,
        aggregatesApi,
        brandId,
        params
    )
}

function* fetchBrandPageTableSaga() {
    const brandId = yield select(selectUiDomainValue, [BRAND_PAGE, 'brandId'])
    const filters = yield select(selectPageFilters, BRAND_PAGE)
    const factTypes = yield select(selectPageFactTypes, BRAND_PAGE)
    const { pagination, sorter } = yield select(selectUiDomainValue, [
        BRAND_PAGE,
        'table',
    ])
    const metrics = yield select(
        selectTableSelectedMetrics,
        BRAND_PAGE,
        'table'
    )
    const currency = yield select(selectCurrencyCode)
    const params = formatAllParams(
        filters,
        pagination,
        sorter,
        metrics,
        currency
    )
    const aggregatesApi = getCampaignAggregatesApi(factTypes)

    yield call(
        cerebroApiSaga,
        fetchBrandPageTableSuccess,
        aggregatesApi,
        brandId,
        params
    )
}

function* downloadBrandPageTimeseriesSaga() {
    const brandId = yield select(selectUiDomainValue, [BRAND_PAGE, 'brandId'])
    const filters = yield select(selectPageFilters, BRAND_PAGE)
    const currency = yield select(selectCurrencyCode)
    const params = {
        ...formatFilters(filters),
        currency,
    }

    const response = yield call(
        cerebroApiSaga,
        downloadBrandPageTimeseriesSuccess,
        getBrandFactTimeseriesExport,
        brandId,
        params
    )

    yield call(downloadCsv, response.data, `brand-${brandId}-timeseries`)
}

function* downloadBrandPageTableSaga() {
    const brandId = yield select(selectUiDomainValue, [BRAND_PAGE, 'brandId'])
    const filters = yield select(selectPageFilters, BRAND_PAGE)
    const { pagination, sorter } = yield select(selectUiDomainValue, [
        BRAND_PAGE,
        'table',
    ])
    const currency = yield select(selectCurrencyCode)
    const params = formatAllParams(filters, pagination, sorter, null, currency)

    const response = yield call(
        cerebroApiSaga,
        downloadBrandPageTableSuccess,
        getCampaignsForBrandFactAggregatesExport,
        brandId,
        params
    )

    yield call(downloadCsv, response.data, `brand-${brandId}-campaigns`)
}

export function* updateBrandPageTableSettings() {
    const userId = yield select(selectAuthDomainValue, ['username'])
    const { actionColumns, order, displayState } = yield select(
        selectUiDomainValue,
        [BRAND_PAGE, 'table', 'columnSettings']
    )

    const item = createColumnSettingsItem({
        userId,
        page: BRAND_PAGE,
        tableName: 'table',
        actionColumns,
        order,
        displayState,
    })

    yield call(putItemToUserSettingsTable, item)
}

function* fetchBrandPageTableSettingsSaga() {
    const userId = yield select(selectAuthDomainValue, ['username'])
    const columnSettings = yield select(selectUiDomainValue, [
        BRAND_PAGE,
        'table',
        'columnSettings',
    ])

    const key = createColumnSettingsKey({
        userId,
        page: BRAND_PAGE,
        tableName: 'table',
    })

    const { Item } = yield call(getItemFromUserSettingsTable, key)

    if (Item) {
        if (isCurrentColumnSettingsSchema(Item, columnSettings)) {
            yield put(fetchBrandPageTableSettingsSuccess(Item))
        } else {
            yield call(updateBrandPageTableSettings)
        }
    }
}

function* updateCampaignSaga(action) {
    const { campaignId, data } = action.payload

    yield call(updateEntitiesCampaignSaga, campaignId, data)
}

function* deleteCampaignSaga(action) {
    const { campaignId } = action.payload

    yield call(deleteEntitiesCampaignSaga, campaignId)
}

function* fetchBrandPageDataSaga() {
    yield all([
        call(fetchBrandPageSponsoredProductTimeseriesSaga),
        call(fetchBrandPageHeadlineSearchTimeseriesSaga),
        call(fetchBrandPageAggregateSaga),
        call(fetchBrandPageTreemapSaga),
        call(fetchBrandPageTableSaga),
    ])
}

/**
 * Puts new table settings to DynamoDB
 */
export function* updateBrandPageTableSettingsWorker() {
    yield call(updateBrandPageTableSettings)
}

/**
 * Fetches data for Brand Page treemap
 */
export function* fetchBrandPageTreemapWorker() {
    try {
        yield call(fetchBrandPageTreemapSaga)
    } catch (error) {
        yield put(fetchBrandPageTreemapFailure(error))
    }
}

/**
 * Downloads timeseries data for Brand Page
 */
export function* downloadBrandPageTimeseriesWorker() {
    try {
        yield call(downloadBrandPageTimeseriesSaga)
    } catch (error) {
        yield put(downloadBrandPageTimeseriesFailure(error))
    }
}

/**
 * Fetches data for Brand Page table
 */
export function* fetchBrandPageTableWorker() {
    try {
        yield call(fetchBrandPageTableSaga)
    } catch (error) {
        yield put(fetchBrandPageTableFailure(error))
    }
}

/**
 * Downloads table data for Brand Page
 */
export function* downloadBrandPageTableWorker() {
    try {
        yield call(downloadBrandPageTableSaga)
    } catch (error) {
        yield put(downloadBrandPageTableFailure(error))
    }
}

/**
 * Updates Campaign in Brands Page Table
 */
export function* updateBrandPageTableWorker(action) {
    try {
        yield call(updateCampaignSaga, action)
        yield call(fetchBrandPageTableSaga)
        yield put(updateBrandPageTableSuccess())
    } catch (error) {
        yield put(updateBrandPageTableFailure(error))
    }
}

/**
 * Deletes Campaign in Brands Page Table
 */
export function* deleteBrandPageTableWorker(action) {
    try {
        yield call(deleteCampaignSaga, action)
        yield call(fetchBrandPageTableSaga)
        yield put(deleteBrandPageTableSuccess())
    } catch (error) {
        yield put(deleteBrandPageTableFailure(error))
    }
}

/**
 * Fetches all data required for the Brand Page
 */
export function* fetchBrandPageDataWorker() {
    try {
        yield call(fetchBrandPageDataSaga)
        yield put(fetchBrandPageDataSuccess())
    } catch (error) {
        yield put(fetchBrandPageDataFailure(error))
    }
}

/**
 * Mounts the Brand Page and fetches data
 */
export function* mountBrandPageWorker() {
    try {
        yield all([
            call(fetchPageFilterSettingsSaga, BRAND_PAGE),
            call(fetchBrandSaga),
            call(fetchBrandPageTableSettingsSaga),
        ])
        yield call(fetchBrandPageDataSaga)
        yield put(mountBrandPageSuccess())
    } catch (error) {
        yield put(mountBrandPageFailure(error))
    }
}
