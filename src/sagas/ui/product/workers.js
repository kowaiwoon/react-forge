import { all, call, select, put } from 'redux-saga/effects'

import { PRODUCT_PAGE } from 'constants/pages'
import { SPONSORED_PRODUCT, HEADLINE_SEARCH } from 'constants/factTypes'
import { formatFilters } from 'helpers/params'
import { downloadCsv } from 'helpers/downloads'
import {
    // aggregate
    getProductSponsoredProductFactAggregate,
    getProductHeadlineSearchFactAggregate,

    // timeseries
    getProductSponsoredProductFactTimeseries,
    getProductHeadlineSearchFactTimeseries,

    // export
    getProductFactTimeseriesExport,
} from 'services/cerebroApi'
import {
    // mounting
    mountProductPageSuccess,
    mountProductPageFailure,

    // page data
    fetchProductPageDataSuccess,
    fetchProductPageDataFailure,

    // update product
    updateProductPageProductDetailsSuccess,
    updateProductPageProductDetailsFailure,

    // aggregate
    fetchProductPageAggregateSuccess,

    // timeseries data
    fetchProductPageSponsoredProductTimeseriesSuccess,
    fetchProductPageHeadlineSearchTimeseriesSuccess,

    // timeseries download
    downloadProductPageTimeseriesSuccess,
    downloadProductPageTimeseriesFailure,
} from 'actions/ui'
import { selectProduct, selectCampaign } from 'selectors/entities'
import {
    selectDomainValue as selectUiDomainValue,
    selectPageFilters,
    selectCurrencyCode,
} from 'selectors/ui'
import cerebroApiSaga from 'sagas/common/cerebroApi'
import {
    fetchProductSaga as fetchEntitiesProductSaga,
    updateProductSaga as updateEntitiesProductSaga,
} from 'sagas/entities'

import { fetchPageFilterSettingsSaga } from '../shared/workers'

const getProductAggregateApi = factType => {
    if (factType === SPONSORED_PRODUCT) {
        return getProductSponsoredProductFactAggregate
    }

    if (factType === HEADLINE_SEARCH) {
        return getProductHeadlineSearchFactAggregate
    }

    return null
}

function* fetchProductSaga() {
    const productId = yield select(selectUiDomainValue, [
        PRODUCT_PAGE,
        'productId',
    ])

    yield call(fetchEntitiesProductSaga, productId)
}

function* updateProductSaga(action) {
    const data = action.payload
    const productId = yield select(selectUiDomainValue, [
        PRODUCT_PAGE,
        'productId',
    ])

    yield call(updateEntitiesProductSaga, productId, data)
}

function* selectProductFactTypeSaga() {
    const productId = yield select(selectUiDomainValue, [
        PRODUCT_PAGE,
        'productId',
    ])
    const product = yield select(selectProduct, productId)

    const { campaign: campaignId } = product
    const campaign = yield select(selectCampaign, campaignId)
    return campaign.campaign_type
}

function* fetchProductPageSponsoredProductTimeseriesSaga() {
    const productId = yield select(selectUiDomainValue, [
        PRODUCT_PAGE,
        'productId',
    ])
    const factType = yield call(selectProductFactTypeSaga)
    if (factType === SPONSORED_PRODUCT) {
        const filters = yield select(selectPageFilters, PRODUCT_PAGE)
        const currency = yield select(selectCurrencyCode)
        const params = {
            ...formatFilters(filters),
            currency,
        }
        params.currency = currency
        yield call(
            cerebroApiSaga,
            fetchProductPageSponsoredProductTimeseriesSuccess,
            getProductSponsoredProductFactTimeseries,
            productId,
            params
        )
    } else {
        yield put(fetchProductPageSponsoredProductTimeseriesSuccess([]))
    }
}

export function* fetchProductPageHeadlineSearchTimeseriesSaga() {
    const productId = yield select(selectUiDomainValue, [
        PRODUCT_PAGE,
        'productId',
    ])
    const factType = yield call(selectProductFactTypeSaga)
    if (factType === HEADLINE_SEARCH) {
        const filters = yield select(selectPageFilters, PRODUCT_PAGE)
        const currency = yield select(selectCurrencyCode)
        const params = {
            ...formatFilters(filters),
            currency,
        }
        params.currency = currency
        yield call(
            cerebroApiSaga,
            fetchProductPageHeadlineSearchTimeseriesSuccess,
            getProductHeadlineSearchFactTimeseries,
            productId,
            params
        )
    } else {
        yield put(fetchProductPageHeadlineSearchTimeseriesSuccess([]))
    }
}

function* fetchProductPageAggregateSaga() {
    const productId = yield select(selectUiDomainValue, [
        PRODUCT_PAGE,
        'productId',
    ])
    const filters = yield select(selectPageFilters, PRODUCT_PAGE)
    const currency = yield select(selectCurrencyCode)
    const params = {
        ...formatFilters(filters),
        currency,
    }
    const factType = yield call(selectProductFactTypeSaga)
    const aggregateApi = getProductAggregateApi(factType)

    yield call(
        cerebroApiSaga,
        fetchProductPageAggregateSuccess,
        aggregateApi,
        productId,
        params
    )
}

function* downloadProductPageTimeseriesSaga() {
    const productId = yield select(selectUiDomainValue, [
        PRODUCT_PAGE,
        'productId',
    ])
    const filters = yield select(selectPageFilters, PRODUCT_PAGE)

    const params = {
        productId,
        ...formatFilters(filters),
    }

    const response = yield call(
        cerebroApiSaga,
        downloadProductPageTimeseriesSuccess,
        getProductFactTimeseriesExport,
        productId,
        params
    )

    yield call(downloadCsv, response.data, `product-${productId}-timeseries`)
}

function* fetchProductPageDataSaga() {
    yield all([
        call(fetchProductPageSponsoredProductTimeseriesSaga),
        call(fetchProductPageHeadlineSearchTimeseriesSaga),
        call(fetchProductPageAggregateSaga),
    ])
}

/**
 * Downloads timeseries data for Product page
 */
export function* downloadProductPageTimeseriesWorker() {
    try {
        yield call(downloadProductPageTimeseriesSaga)
    } catch (error) {
        yield put(downloadProductPageTimeseriesFailure(error))
    }
}

/**
 * Fetches all data required for the Product page
 */
export function* fetchProductPageDataWorker() {
    try {
        yield call(fetchProductPageDataSaga)
        yield put(fetchProductPageDataSuccess())
    } catch (error) {
        yield put(fetchProductPageDataFailure(error))
    }
}

/**
 * Updates Keyword Details
 */
export function* updateProductPageProductDetailsWorker(action) {
    try {
        yield call(updateProductSaga, action)
        yield put(updateProductPageProductDetailsSuccess())
    } catch (error) {
        yield put(updateProductPageProductDetailsFailure(error))
    }
}

/**
 * Mounts the Products Page and fetches data
 */
export function* mountProductPageWorker() {
    try {
        yield all([
            call(fetchProductSaga),
            call(fetchPageFilterSettingsSaga, PRODUCT_PAGE),
        ])
        yield call(fetchProductPageDataSaga)
        yield put(mountProductPageSuccess())
    } catch (error) {
        yield put(mountProductPageFailure(error))
    }
}
