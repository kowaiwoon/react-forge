import { all, takeLatest } from 'redux-saga/effects'

import {
    mountProductPageRequest,
    fetchProductPageDataRequest,
    downloadProductPageTimeseriesRequest,
    updateProductPageProductDetailsRequest,
} from 'actions/ui'

import {
    mountProductPageWorker,
    fetchProductPageDataWorker,
    downloadProductPageTimeseriesWorker,
    updateProductPageProductDetailsWorker,
} from './workers'

export default function* productPageWorker() {
    yield all([
        takeLatest(mountProductPageRequest.toString(), mountProductPageWorker),

        takeLatest(
            fetchProductPageDataRequest.toString(),
            fetchProductPageDataWorker
        ),

        takeLatest(
            downloadProductPageTimeseriesRequest.toString(),
            downloadProductPageTimeseriesWorker
        ),

        takeLatest(
            updateProductPageProductDetailsRequest.toString(),
            updateProductPageProductDetailsWorker
        ),
    ])
}
