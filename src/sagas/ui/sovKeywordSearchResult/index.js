import { all, takeLatest } from 'redux-saga/effects'

import {
    mountSovKeywordSearchResultPageRequest,
    fetchSovKeywordSearchResultPageDataRequest,
    fetchSovKeywordSearchResultPageTableRequest,
    updateSovKeywordSearchResultPageTableSettings,
    downloadSovKeywordSearchResultPageTableRequest,
} from 'actions/ui'

import {
    mountSovKeywordSearchResultPageWorker,
    fetchSovKeywordSearchResultPageDataWorker,
    fetchSovKeywordSearchResultPageTableWorker,
    downloadSovKeywordSearchResultPageTableWorker,
    updateSovKeywordSearchResultPageTableSettingsWorker,
} from './workers'

export default function* sovKeywordSearchResultPageWorker() {
    yield all([
        takeLatest(
            mountSovKeywordSearchResultPageRequest.toString(),
            mountSovKeywordSearchResultPageWorker
        ),

        takeLatest(
            fetchSovKeywordSearchResultPageDataRequest.toString(),
            fetchSovKeywordSearchResultPageDataWorker
        ),

        takeLatest(
            fetchSovKeywordSearchResultPageTableRequest.toString(),
            fetchSovKeywordSearchResultPageTableWorker
        ),

        takeLatest(
            downloadSovKeywordSearchResultPageTableRequest.toString(),
            downloadSovKeywordSearchResultPageTableWorker
        ),

        takeLatest(
            updateSovKeywordSearchResultPageTableSettings.toString(),
            updateSovKeywordSearchResultPageTableSettingsWorker
        ),
    ])
}
