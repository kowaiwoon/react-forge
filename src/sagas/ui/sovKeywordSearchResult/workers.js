import { all, call, select, put } from 'redux-saga/effects'

import { formatAllParams } from 'helpers/params'
import { downloadCsv } from 'helpers/downloads'
import {
    getSovKeywordSearchResult,
    getSovKeywordSearchResultExport,
    getSovKeywordSearchResultScreenshot,
} from 'services/cerebroApi'
import { SOV_KEYWORD_SEARCH_RESULT_PAGE } from 'constants/pages'
import {
    putItemToUserSettingsTable,
    getItemFromUserSettingsTable,
} from 'services/dynamoApi'
import {
    isCurrentSchema,
    createColumnSettingsItem,
    createColumnSettingsKey,
} from 'helpers/columnSettings'
import { fetchSovKeywordSaga as fetchEntitiesSovKeywordSaga } from 'sagas/entities'
import {
    // mounting
    mountSovKeywordSearchResultPageSuccess,
    mountSovKeywordSearchResultPageFailure,

    // page data
    fetchSovKeywordSearchResultPageDataSuccess,
    fetchSovKeywordSearchResultPageDataFailure,

    // table data
    fetchSovKeywordSearchResultPageTableSuccess,
    fetchSovKeywordSearchResultPageTableFailure,

    // table settings
    fetchSovKeywordSearchResultPageTableSettingsSuccess,

    // table download
    downloadSovKeywordSearchResultPageTableSuccess,
    downloadSovKeywordSearchResultPageTableFailure,

    // screenshot
    fetchSovKeywordSearchResultPageScreenshotSuccess,
} from 'actions/ui'
import {
    selectDomainValue as selectUiDomainValue,
    selectPageFilters,
} from 'selectors/ui'
import { selectDomainValue as selectAuthDomainValue } from 'selectors/auth'
import cerebroApiSaga from 'sagas/common/cerebroApi'

function* fetchSovKeywordSaga() {
    const sovKeywordId = yield select(selectUiDomainValue, [
        SOV_KEYWORD_SEARCH_RESULT_PAGE,
        'sovKeywordId',
    ])

    yield call(fetchEntitiesSovKeywordSaga, sovKeywordId)
}

function* fetchSovKeywordSearchResultPageTableSaga() {
    const sovKeywordId = yield select(selectUiDomainValue, [
        SOV_KEYWORD_SEARCH_RESULT_PAGE,
        'sovKeywordId',
    ])
    const scheduledDate = yield select(selectUiDomainValue, [
        SOV_KEYWORD_SEARCH_RESULT_PAGE,
        'scheduledDate',
    ])
    const filters = yield select(
        selectPageFilters,
        SOV_KEYWORD_SEARCH_RESULT_PAGE
    )
    const { pagination, sorter } = yield select(selectUiDomainValue, [
        SOV_KEYWORD_SEARCH_RESULT_PAGE,
        'table',
    ])
    const params = formatAllParams(filters, pagination, sorter)

    yield call(
        cerebroApiSaga,
        fetchSovKeywordSearchResultPageTableSuccess,
        getSovKeywordSearchResult,
        sovKeywordId,
        scheduledDate,
        params
    )
}

function* downloadSovKeywordSearchResultPageTableSaga() {
    const sovKeywordId = yield select(selectUiDomainValue, [
        SOV_KEYWORD_SEARCH_RESULT_PAGE,
        'sovKeywordId',
    ])
    const scheduledDate = yield select(selectUiDomainValue, [
        SOV_KEYWORD_SEARCH_RESULT_PAGE,
        'scheduledDate',
    ])
    const filters = yield select(
        selectPageFilters,
        SOV_KEYWORD_SEARCH_RESULT_PAGE
    )
    const { pagination, sorter } = yield select(selectUiDomainValue, [
        SOV_KEYWORD_SEARCH_RESULT_PAGE,
        'table',
    ])
    const params = formatAllParams(filters, pagination, sorter)

    const response = yield call(
        cerebroApiSaga,
        downloadSovKeywordSearchResultPageTableSuccess,
        getSovKeywordSearchResultExport,
        sovKeywordId,
        scheduledDate,
        params
    )

    yield call(
        downloadCsv,
        response.data,
        `sov-${sovKeywordId}-searches-${scheduledDate}`
    )
}

function* updateSovKeywordSearchResultPageTableSettingsSaga() {
    const userId = yield select(selectAuthDomainValue, ['username'])
    const { actionColumns, order, displayState } = yield select(
        selectUiDomainValue,
        [SOV_KEYWORD_SEARCH_RESULT_PAGE, 'table', 'columnSettings']
    )

    const item = createColumnSettingsItem({
        userId,
        page: SOV_KEYWORD_SEARCH_RESULT_PAGE,
        tableName: 'table',
        actionColumns,
        order,
        displayState,
    })

    yield call(putItemToUserSettingsTable, item)
}

function* fetchSovKeywordSearchResultTableSettingsSaga() {
    const userId = yield select(selectAuthDomainValue, ['username'])
    const columnSettings = yield select(selectUiDomainValue, [
        SOV_KEYWORD_SEARCH_RESULT_PAGE,
        'table',
        'columnSettings',
    ])

    const key = createColumnSettingsKey({
        userId,
        page: SOV_KEYWORD_SEARCH_RESULT_PAGE,
        tableName: 'table',
    })

    const { Item } = yield call(getItemFromUserSettingsTable, key)

    if (Item) {
        if (isCurrentSchema(Item, columnSettings)) {
            yield put(fetchSovKeywordSearchResultPageTableSettingsSuccess(Item))
        } else {
            yield call(updateSovKeywordSearchResultPageTableSettingsSaga)
        }
    }
}

function* fetchSovKeywordSearchResultPageScreenshotSaga() {
    const sovKeywordId = yield select(selectUiDomainValue, [
        SOV_KEYWORD_SEARCH_RESULT_PAGE,
        'sovKeywordId',
    ])
    const scheduledDate = yield select(selectUiDomainValue, [
        SOV_KEYWORD_SEARCH_RESULT_PAGE,
        'scheduledDate',
    ])

    yield call(
        cerebroApiSaga,
        fetchSovKeywordSearchResultPageScreenshotSuccess,
        getSovKeywordSearchResultScreenshot,
        sovKeywordId,
        scheduledDate
    )
}

function* fetchSovKeywordSearchResultPageDataSaga() {
    yield all([
        call(fetchSovKeywordSearchResultPageTableSaga),
        call(fetchSovKeywordSearchResultPageScreenshotSaga),
    ])
}

/**
 * Puts new table settings to DynamoDB
 */
export function* updateSovKeywordSearchResultPageTableSettingsWorker() {
    yield call(updateSovKeywordSearchResultPageTableSettingsSaga)
}

/**
 * Fetches data for SOV Keyword Search Result Page table
 */
export function* fetchSovKeywordSearchResultPageTableWorker() {
    try {
        yield call(fetchSovKeywordSearchResultPageTableSaga)
    } catch (error) {
        yield put(fetchSovKeywordSearchResultPageTableFailure(error))
    }
}

/**
 * Downloads table data for SOV Keyword Search Result Page
 */
export function* downloadSovKeywordSearchResultPageTableWorker() {
    try {
        yield call(downloadSovKeywordSearchResultPageTableSaga)
    } catch (error) {
        yield put(downloadSovKeywordSearchResultPageTableFailure(error))
    }
}

/**
 * Fetches all data required for the SOV Keyword Search Result Page
 */
export function* fetchSovKeywordSearchResultPageDataWorker() {
    try {
        yield call(fetchSovKeywordSearchResultPageDataSaga)
        yield put(fetchSovKeywordSearchResultPageDataSuccess())
    } catch (error) {
        yield put(fetchSovKeywordSearchResultPageDataFailure(error))
    }
}

/**
 * Mounts the SOV Keyword Search Result Page and fetches data
 */
export function* mountSovKeywordSearchResultPageWorker() {
    try {
        yield all([
            call(fetchSovKeywordSaga),
            call(fetchSovKeywordSearchResultTableSettingsSaga),
            call(fetchSovKeywordSearchResultPageDataSaga),
        ])
        yield put(mountSovKeywordSearchResultPageSuccess())
    } catch (error) {
        yield put(mountSovKeywordSearchResultPageFailure(error))
    }
}
