import { all, call, select, put } from 'redux-saga/effects'

import { CAMPAIGNS_SUMMARY_PAGE } from 'constants/pages'
import { SPONSORED_PRODUCT, HEADLINE_SEARCH } from 'constants/factTypes'
import { formatAllParams, formatFilters } from 'helpers/params'
import { downloadCsv } from 'helpers/downloads'
import {
    getCampaignsFactAggregates,
    getCampaignsSponsoredProductFactAggregates,
    getCampaignsHeadlineSearchFactAggregates,
    getCampaignsFactAggregatesExport,
} from 'services/cerebroApi'
import {
    putItemToUserSettingsTable,
    getItemFromUserSettingsTable,
} from 'services/dynamoApi'
import {
    isCurrentSchema as isCurrentColumnSettingsSchema,
    createColumnSettingsItem,
    createColumnSettingsKey,
} from 'helpers/columnSettings'

import {
    // mounting
    mountCampaignsSummaryPageSuccess,
    mountCampaignsSummaryPageFailure,

    // page data
    fetchCampaignsSummaryPageDataSuccess,
    fetchCampaignsSummaryPageDataFailure,

    // treemap data
    fetchCampaignsSummaryPageTreemapSuccess,
    fetchCampaignsSummaryPageTreemapFailure,

    // table data
    fetchCampaignsSummaryPageTableSuccess,
    fetchCampaignsSummaryPageTableFailure,

    // table settings
    fetchCampaignsSummaryPageTableSettingsSuccess,

    // table download
    downloadCampaignsSummaryPageTableSuccess,
    downloadCampaignsSummaryPageTableFailure,

    // update table
    updateCampaignsSummaryPageTableSuccess,
    updateCampaignsSummaryPageTableFailure,

    // delete table
    deleteCampaignsSummaryPageTableSuccess,
    deleteCampaignsSummaryPageTableFailure,
} from 'actions/ui'
import {
    selectDomainValue as selectUiDomainValue,
    selectPageFilters,
    selectPageFactTypes,
    selectTreemapSelectedMetric,
    selectTableSelectedMetrics,
    selectCurrencyCode,
} from 'selectors/ui'
import { selectDomainValue as selectAuthDomainValue } from 'selectors/auth'

import cerebroApiSaga from 'sagas/common/cerebroApi'
import {
    updateCampaignSaga as updateEntitiesCampaignSaga,
    deleteCampaignSaga as deleteEntitiesCampaignSaga,
} from 'sagas/entities'
import { fetchPageFilterSettingsSaga } from '../shared/workers'

const getAggregatesApi = factTypes => {
    if (factTypes.length === 2) {
        return getCampaignsFactAggregates
    }

    if (factTypes[0].value === SPONSORED_PRODUCT) {
        return getCampaignsSponsoredProductFactAggregates
    }

    if (factTypes[0].value === HEADLINE_SEARCH) {
        return getCampaignsHeadlineSearchFactAggregates
    }

    return null
}

function* fetchCampaignsSummaryPageTreemapSaga() {
    const filters = yield select(selectPageFilters, CAMPAIGNS_SUMMARY_PAGE)
    const factTypes = yield select(selectPageFactTypes, CAMPAIGNS_SUMMARY_PAGE)
    const { pagination, sorter } = yield select(selectUiDomainValue, [
        CAMPAIGNS_SUMMARY_PAGE,
        'treemap',
    ])
    const metric = yield select(
        selectTreemapSelectedMetric,
        CAMPAIGNS_SUMMARY_PAGE,
        'treemap'
    )
    const currency = yield select(selectCurrencyCode)
    const params = formatAllParams(
        filters,
        pagination,
        sorter,
        [metric],
        currency
    )
    const aggregatesApi = getAggregatesApi(factTypes)

    yield call(
        cerebroApiSaga,
        fetchCampaignsSummaryPageTreemapSuccess,
        aggregatesApi,
        params
    )
}

function* fetchCampaignsSummaryPageTableSaga() {
    const filters = yield select(selectPageFilters, CAMPAIGNS_SUMMARY_PAGE)
    const factTypes = yield select(selectPageFactTypes, CAMPAIGNS_SUMMARY_PAGE)
    const { pagination, sorter } = yield select(selectUiDomainValue, [
        CAMPAIGNS_SUMMARY_PAGE,
        'table',
    ])
    const metrics = yield select(
        selectTableSelectedMetrics,
        CAMPAIGNS_SUMMARY_PAGE,
        'table'
    )
    const currency = yield select(selectCurrencyCode)
    const params = formatAllParams(
        filters,
        pagination,
        sorter,
        metrics,
        currency
    )
    const aggregatesApi = getAggregatesApi(factTypes)

    yield call(
        cerebroApiSaga,
        fetchCampaignsSummaryPageTableSuccess,
        aggregatesApi,
        params
    )
}

function* downloadCampaignsSummaryPageTableSaga() {
    const filters = yield select(selectPageFilters, CAMPAIGNS_SUMMARY_PAGE)
    const currency = yield select(selectCurrencyCode)
    const params = {
        ...formatFilters(filters),
        currency,
    }

    const response = yield call(
        cerebroApiSaga,
        downloadCampaignsSummaryPageTableSuccess,
        getCampaignsFactAggregatesExport,
        params
    )

    yield call(downloadCsv, response.data, 'products')
}

function* updateCampaignsSummaryPageTableSettingsSaga() {
    const userId = yield select(selectAuthDomainValue, ['username'])
    const { actionColumns, order, displayState } = yield select(
        selectUiDomainValue,
        [CAMPAIGNS_SUMMARY_PAGE, 'table', 'columnSettings']
    )

    const item = createColumnSettingsItem({
        userId,
        page: CAMPAIGNS_SUMMARY_PAGE,
        tableName: 'table',
        actionColumns,
        order,
        displayState,
    })

    yield call(putItemToUserSettingsTable, item)
}

function* fetchCampaignsTableColumnSettingsSaga() {
    const userId = yield select(selectAuthDomainValue, ['username'])
    const columnSettings = yield select(selectUiDomainValue, [
        CAMPAIGNS_SUMMARY_PAGE,
        'table',
        'columnSettings',
    ])

    const key = createColumnSettingsKey({
        userId,
        page: CAMPAIGNS_SUMMARY_PAGE,
        tableName: 'table',
    })

    const { Item } = yield call(getItemFromUserSettingsTable, key)

    if (Item) {
        if (isCurrentColumnSettingsSchema(Item, columnSettings)) {
            yield put(fetchCampaignsSummaryPageTableSettingsSuccess(Item))
        } else {
            yield call(updateCampaignsSummaryPageTableSettingsSaga)
        }
    }
}

function* fetchCampaignsSummaryPageDataSaga() {
    yield all([
        call(fetchCampaignsSummaryPageTreemapSaga),
        call(fetchCampaignsSummaryPageTableSaga),
    ])
}

function* updateCampaignSaga(action) {
    const { campaignId, data } = action.payload

    yield call(updateEntitiesCampaignSaga, campaignId, data)
}

function* deleteCampaignSaga(action) {
    const { campaignId } = action.payload

    yield call(deleteEntitiesCampaignSaga, campaignId)
}

/**
 * Puts new table settings to DynamoDB
 */
export function* updateCampaignsSummaryPageTableSettingsWorker() {
    yield call(updateCampaignsSummaryPageTableSettingsSaga)
}

/**
 * Downloads data for the Campaigns Summary Page table
 */
export function* downloadCampaignsSummaryPageTableWorker() {
    try {
        yield call(downloadCampaignsSummaryPageTableSaga)
    } catch (error) {
        yield put(downloadCampaignsSummaryPageTableFailure(error))
    }
}

/**
 * Updates Campaign in Campaigns Summary Page Table
 */
export function* updateCampaignsSummaryPageTableWorker(action) {
    try {
        yield call(updateCampaignSaga, action)
        yield call(fetchCampaignsSummaryPageTableSaga)
        yield put(updateCampaignsSummaryPageTableSuccess())
    } catch (error) {
        yield put(updateCampaignsSummaryPageTableFailure(error))
    }
}

/**
 * Deletes Campaign in Campaigns Summary Page Table
 */
export function* deleteCampaignsSummaryPageTableWorker(action) {
    try {
        yield call(deleteCampaignSaga, action)
        yield call(fetchCampaignsSummaryPageTableSaga)
        yield put(deleteCampaignsSummaryPageTableSuccess())
    } catch (error) {
        yield put(deleteCampaignsSummaryPageTableFailure(error))
    }
}

/**
 * Fetches data for Campaigns Summary Page table
 */
export function* fetchCampaignsSummaryPageTableWorker() {
    try {
        yield call(fetchCampaignsSummaryPageTableSaga)
    } catch (error) {
        yield put(fetchCampaignsSummaryPageTableFailure(error))
    }
}

/**
 * Fetches data for Campaigns Summary Page Treemap
 */
export function* fetchCampaignsSummaryPageTreemapWorker() {
    try {
        yield call(fetchCampaignsSummaryPageTreemapSaga)
    } catch (error) {
        yield put(fetchCampaignsSummaryPageTreemapFailure(error))
    }
}

/**
 * Fetch data for Campaigns Summary Page
 */
export function* fetchCampaignsSummaryPageDataWorker() {
    try {
        yield call(fetchCampaignsSummaryPageDataSaga)
        yield put(fetchCampaignsSummaryPageDataSuccess())
    } catch (error) {
        yield put(fetchCampaignsSummaryPageDataFailure(error))
    }
}

/**
 * Mounts the Campaigns Summary Page and fetches data
 */
export function* mountCampaignsSummaryPageWorker() {
    try {
        yield call(fetchPageFilterSettingsSaga, CAMPAIGNS_SUMMARY_PAGE)
        yield call(fetchCampaignsTableColumnSettingsSaga)
        yield call(fetchCampaignsSummaryPageDataSaga)
        yield put(mountCampaignsSummaryPageSuccess())
    } catch (error) {
        yield put(mountCampaignsSummaryPageFailure(error))
    }
}
