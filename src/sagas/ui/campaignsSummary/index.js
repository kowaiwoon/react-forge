import { all, takeLatest } from 'redux-saga/effects'

import {
    mountCampaignsSummaryPageRequest,
    fetchCampaignsSummaryPageDataRequest,
    updateCampaignsSummaryPageTableSettingsRequest,
    fetchCampaignsSummaryPageTreemapRequest,
    fetchCampaignsSummaryPageTableRequest,
    downloadCampaignsSummaryPageTableRequest,
    updateCampaignsSummaryPageTableRequest,
    deleteCampaignsSummaryPageTableRequest,
} from 'actions/ui'

import {
    mountCampaignsSummaryPageWorker,
    fetchCampaignsSummaryPageDataWorker,
    fetchCampaignsSummaryPageTreemapWorker,
    fetchCampaignsSummaryPageTableWorker,
    downloadCampaignsSummaryPageTableWorker,
    updateCampaignsSummaryPageTableSettingsWorker,
    deleteCampaignsSummaryPageTableWorker,
    updateCampaignsSummaryPageTableWorker,
} from './workers'

export default function* productsSummaryPageWorker() {
    yield all([
        takeLatest(
            mountCampaignsSummaryPageRequest.toString(),
            mountCampaignsSummaryPageWorker
        ),

        takeLatest(
            fetchCampaignsSummaryPageDataRequest.toString(),
            fetchCampaignsSummaryPageDataWorker
        ),

        takeLatest(
            fetchCampaignsSummaryPageTreemapRequest.toString(),
            fetchCampaignsSummaryPageTreemapWorker
        ),

        takeLatest(
            fetchCampaignsSummaryPageTableRequest.toString(),
            fetchCampaignsSummaryPageTableWorker
        ),

        takeLatest(
            downloadCampaignsSummaryPageTableRequest.toString(),
            downloadCampaignsSummaryPageTableWorker
        ),

        takeLatest(
            updateCampaignsSummaryPageTableSettingsRequest.toString(),
            updateCampaignsSummaryPageTableSettingsWorker
        ),

        takeLatest(
            updateCampaignsSummaryPageTableRequest.toString(),
            updateCampaignsSummaryPageTableWorker
        ),

        takeLatest(
            deleteCampaignsSummaryPageTableRequest.toString(),
            deleteCampaignsSummaryPageTableWorker
        ),
    ])
}
