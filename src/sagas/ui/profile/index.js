import { all, takeLatest } from 'redux-saga/effects'

import {
    createProfilePageOrganizationRequest,
    mountProfilePageRequest,
    acceptInvitationProfilePageRequest,
} from 'actions/ui'
import {
    createProfilePageOrganizationWorker,
    mountProfilePageWorker,
    acceptInvitationProfilePageWorker,
} from './workers'

export default function* profilePageWorker() {
    yield all([
        takeLatest(
            createProfilePageOrganizationRequest.toString(),
            createProfilePageOrganizationWorker
        ),
        takeLatest(mountProfilePageRequest.toString(), mountProfilePageWorker),
        takeLatest(
            acceptInvitationProfilePageRequest.toString(),
            acceptInvitationProfilePageWorker
        ),
    ])
}
