import { call, all, put, select } from 'redux-saga/effects'
import { generatePath } from 'react-router-dom'

import {
    createProfilePageOrganizationFailure,
    createProfilePageOrganizationSuccess,
    mountProfilePageSuccess,
    mountProfilePageFailure,
    fetchInvitationsProfilePageSuccess,
    acceptInvitationProfilePageSuccess,
    acceptInvitationProfilePageFailure,
} from 'actions/ui'
import { changeOrganizationRequest } from 'actions/auth'
import cerebroApiSaga from 'sagas/common/cerebroApi'
import {
    createOrganization,
    getInvitations,
    patchInvitation,
} from 'services/cerebroApi'
import { selectDomainValue as selectAuthDomainValue } from 'selectors/auth'
import { selectOrganizations } from 'selectors/orgs'
import { fetchOrganizationsSaga } from 'sagas/orgs/organizations'
import { getPath } from 'helpers/pages'
import { ORGANIZATION_PAGE, PROFILE_PAGE } from 'constants/pages'

function* createProfilePageOrganizationSaga(data) {
    return yield call(
        cerebroApiSaga,
        createProfilePageOrganizationSuccess,
        createOrganization,
        data
    )
}

function* fetchPendingInvitationsSaga() {
    const username = yield select(selectAuthDomainValue, ['username'])

    yield call(
        cerebroApiSaga,
        fetchInvitationsProfilePageSuccess,
        getInvitations,
        { state: 'pending', user__username: username }
    )
}

function* fetchProfilePageDataSaga() {
    yield all([call(fetchPendingInvitationsSaga), call(fetchOrganizationsSaga)])
}

function* acceptInvitationSaga(invitationId) {
    yield call(
        cerebroApiSaga,
        acceptInvitationProfilePageSuccess,
        patchInvitation,
        invitationId,
        { state: 'accepted' }
    )
}

/**
 * Create an organization
 */
export function* createProfilePageOrganizationWorker(action) {
    try {
        const response = yield call(
            createProfilePageOrganizationSaga,
            action.payload
        )

        // if the user created their first organization, change to it
        const organizations = yield select(selectOrganizations)
        if (organizations.length === 1) {
            const { id: organizationId } = response.data
            yield put(
                changeOrganizationRequest({
                    organizationId,
                    pathname: getPath(PROFILE_PAGE),
                })
            )
        }
    } catch (error) {
        yield put(createProfilePageOrganizationFailure(error))
    }
}

/**
 * Accept an invitation
 */
export function* acceptInvitationProfilePageWorker(action) {
    try {
        yield call(acceptInvitationSaga, action.payload.invitationId)
        yield action.payload.history.push(
            generatePath(getPath(ORGANIZATION_PAGE), {
                organizationId: action.payload.organizationId,
            })
        )
    } catch (error) {
        yield put(acceptInvitationProfilePageFailure(error))
    }
}

/**
 * Mounts the Profile Page and fetches data
 */
export function* mountProfilePageWorker() {
    try {
        yield call(fetchProfilePageDataSaga)
        yield put(mountProfilePageSuccess())
    } catch (error) {
        yield put(mountProfilePageFailure(error))
    }
}
