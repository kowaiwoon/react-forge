import { all, call, select, put } from 'redux-saga/effects'
import { delay } from 'redux-saga'

import { KEYWORDS_SUMMARY_PAGE } from 'constants/pages'
import { SPONSORED_PRODUCT, HEADLINE_SEARCH } from 'constants/factTypes'
import { SEARCH_RESULTS_PER_QUERY } from 'configuration/typeahead'
import { formatAllParams, formatFilters } from 'helpers/params'
import { downloadCsv } from 'helpers/downloads'
import {
    getKeywordsFactAggregates,
    getKeywordsSponsoredProductFactAggregates,
    getKeywordsHeadlineSearchFactAggregates,
    getKeywordsFactAggregatesExport,
    getKeywords,
} from 'services/cerebroApi'
import {
    putItemToUserSettingsTable,
    getItemFromUserSettingsTable,
} from 'services/dynamoApi'
import {
    isCurrentSchema as isCurrentColumnSettingsSchema,
    createColumnSettingsItem,
    createColumnSettingsKey,
} from 'helpers/columnSettings'
import {
    // mounting
    mountKeywordsSummaryPageSuccess,
    mountKeywordsSummaryPageFailure,

    // page data
    fetchKeywordsSummaryPageDataSuccess,
    fetchKeywordsSummaryPageDataFailure,

    // treemap data
    fetchKeywordsSummaryPageTreemapSuccess,
    fetchKeywordsSummaryPageTreemapFailure,

    // table data
    fetchKeywordsSummaryPageTableSuccess,
    fetchKeywordsSummaryPageTableFailure,

    // table settings
    fetchKeywordsSummaryPageTableSettingsSuccess,

    // table download
    downloadKeywordsSummaryPageTableSuccess,
    downloadKeywordsSummaryPageTableFailure,

    // search
    searchKeywordsRequest,
    searchKeywordsSuccess,
    searchKeywordsFailure,
} from 'actions/ui'
import { selectDomainValue as selectAuthDomainValue } from 'selectors/auth'
import {
    selectDomainValue as selectUiDomainValue,
    selectPageFilters,
    selectPageFactTypes,
    selectTreemapSelectedMetric,
    selectTableSelectedMetrics,
    selectCurrencyCode,
} from 'selectors/ui'
import cerebroApiSaga from 'sagas/common/cerebroApi'

import { fetchPageFilterSettingsSaga } from '../shared/workers'

const getAggregatesApi = factTypes => {
    if (factTypes.length === 2) {
        return getKeywordsFactAggregates
    }

    if (factTypes[0].value === SPONSORED_PRODUCT) {
        return getKeywordsSponsoredProductFactAggregates
    }

    if (factTypes[0].value === HEADLINE_SEARCH) {
        return getKeywordsHeadlineSearchFactAggregates
    }

    return null
}

function* fetchKeywordsSummaryPageTreemapSaga() {
    const filters = yield select(selectPageFilters, KEYWORDS_SUMMARY_PAGE)
    const factTypes = yield select(selectPageFactTypes, KEYWORDS_SUMMARY_PAGE)
    const { pagination, sorter } = yield select(selectUiDomainValue, [
        KEYWORDS_SUMMARY_PAGE,
        'treemap',
    ])
    const metric = yield select(
        selectTreemapSelectedMetric,
        KEYWORDS_SUMMARY_PAGE,
        'treemap'
    )
    const currency = yield select(selectCurrencyCode)
    const params = formatAllParams(
        filters,
        pagination,
        sorter,
        [metric],
        currency
    )
    const aggregatesApi = getAggregatesApi(factTypes)

    yield call(
        cerebroApiSaga,
        fetchKeywordsSummaryPageTreemapSuccess,
        aggregatesApi,
        params
    )
}

function* fetchKeywordsSummaryPageTableSaga() {
    const filters = yield select(selectPageFilters, KEYWORDS_SUMMARY_PAGE)
    const factTypes = yield select(selectPageFactTypes, KEYWORDS_SUMMARY_PAGE)
    const { pagination, sorter } = yield select(selectUiDomainValue, [
        KEYWORDS_SUMMARY_PAGE,
        'table',
    ])
    const metrics = yield select(
        selectTableSelectedMetrics,
        KEYWORDS_SUMMARY_PAGE,
        'table'
    )
    const currency = yield select(selectCurrencyCode)
    const params = formatAllParams(
        filters,
        pagination,
        sorter,
        metrics,
        currency
    )
    const aggregatesApi = getAggregatesApi(factTypes)

    yield call(
        cerebroApiSaga,
        fetchKeywordsSummaryPageTableSuccess,
        aggregatesApi,
        params
    )
}

function* downloadKeywordsPageSummaryTableSaga() {
    const filters = yield select(selectPageFilters, KEYWORDS_SUMMARY_PAGE)
    const currency = yield select(selectCurrencyCode)
    const params = {
        ...formatFilters(filters),
        currency,
    }

    const response = yield call(
        cerebroApiSaga,
        downloadKeywordsSummaryPageTableSuccess,
        getKeywordsFactAggregatesExport,
        params
    )

    yield call(downloadCsv, response.data, 'keywords')
}

function* updateKeywordsSummaryPageTableSettingsSaga() {
    const userId = yield select(selectAuthDomainValue, ['username'])
    const { actionColumns, order, displayState } = yield select(
        selectUiDomainValue,
        [KEYWORDS_SUMMARY_PAGE, 'table', 'columnSettings']
    )

    const item = createColumnSettingsItem({
        userId,
        page: KEYWORDS_SUMMARY_PAGE,
        tableName: 'table',
        actionColumns,
        order,
        displayState,
    })

    yield call(putItemToUserSettingsTable, item)
}

function* fetchKeywordsSummaryTableSettingsSaga() {
    const userId = yield select(selectAuthDomainValue, ['username'])
    const columnSettings = yield select(selectUiDomainValue, [
        KEYWORDS_SUMMARY_PAGE,
        'table',
        'columnSettings',
    ])

    const key = createColumnSettingsKey({
        userId,
        page: KEYWORDS_SUMMARY_PAGE,
        tableName: 'table',
    })

    const { Item } = yield call(getItemFromUserSettingsTable, key)

    if (Item) {
        if (isCurrentColumnSettingsSchema(Item, columnSettings)) {
            yield put(fetchKeywordsSummaryPageTableSettingsSuccess(Item))
        } else {
            yield call(updateKeywordsSummaryPageTableSettingsSaga)
        }
    }
}

function* fetchKeywordsSummaryPageDataSaga() {
    yield all([
        call(fetchKeywordsSummaryPageTreemapSaga),
        call(fetchKeywordsSummaryPageTableSaga),
    ])
}

function* searchKeywordsSaga(query) {
    if (query.length > 1) {
        yield put(searchKeywordsRequest())
        yield call(cerebroApiSaga, searchKeywordsSuccess, getKeywords, {
            limit: SEARCH_RESULTS_PER_QUERY,
            text__icontains: query,
        })
    }
}

/**
 * Puts new table settings to DynamoDB
 */
export function* updateKeywordsSummaryPageTableSettingsWorker() {
    yield call(updateKeywordsSummaryPageTableSettingsSaga)
}

/**
 * Downloads data for the Keywords Summary Page Table
 */
export function* downloadKeywordsSummaryPageTableWorker() {
    try {
        yield call(downloadKeywordsPageSummaryTableSaga)
    } catch (error) {
        yield put(downloadKeywordsSummaryPageTableFailure(error))
    }
}

/**
 * Fetches data for Keywords Summary Treemap
 */
export function* fetchKeywordsSummaryPageTreemapWorker() {
    try {
        yield call(fetchKeywordsSummaryPageTreemapSaga)
    } catch (error) {
        yield put(fetchKeywordsSummaryPageTreemapFailure(error))
    }
}

/**
 * Fetches data for Keywords Summary Page Table
 */
export function* fetchKeywordsSummaryPageTableWorker() {
    try {
        yield call(fetchKeywordsSummaryPageTableSaga)
    } catch (error) {
        yield put(fetchKeywordsSummaryPageTableFailure(error))
    }
}

/**
 * Fetch data for Keywords Summary page
 */
export function* fetchKeywordsSummaryPageDataWorker() {
    try {
        yield call(fetchKeywordsSummaryPageDataSaga)
        yield put(fetchKeywordsSummaryPageDataSuccess())
    } catch (error) {
        yield put(fetchKeywordsSummaryPageDataFailure(error))
    }
}

/**
 * Mounts the Keywords Summary page and fetches data
 */
export function* mountKeywordsSummaryPageWorker() {
    try {
        yield call(fetchPageFilterSettingsSaga, KEYWORDS_SUMMARY_PAGE)
        yield call(fetchKeywordsSummaryTableSettingsSaga)
        yield call(fetchKeywordsSummaryPageDataSaga)
        yield put(mountKeywordsSummaryPageSuccess())
    } catch (error) {
        yield put(mountKeywordsSummaryPageFailure(error))
    }
}

export function* changeKeywordsFilterInputWorker(action) {
    // debounce by 500ms
    yield delay(500)
    try {
        yield call(searchKeywordsSaga, action.payload)
    } catch (error) {
        yield put(searchKeywordsFailure(error))
    }
}
