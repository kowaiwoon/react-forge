import { all, takeLatest } from 'redux-saga/effects'

import {
    mountKeywordsSummaryPageRequest,
    fetchKeywordsSummaryPageDataRequest,
    updateKeywordsSummaryPageTableSettings,
    fetchKeywordsSummaryPageTreemapRequest,
    fetchKeywordsSummaryPageTableRequest,
    downloadKeywordsSummaryPageTableRequest,
    changeKeywordsFilterInput,
} from 'actions/ui'

import {
    mountKeywordsSummaryPageWorker,
    fetchKeywordsSummaryPageDataWorker,
    fetchKeywordsSummaryPageTreemapWorker,
    fetchKeywordsSummaryPageTableWorker,
    downloadKeywordsSummaryPageTableWorker,
    updateKeywordsSummaryPageTableSettingsWorker,
    changeKeywordsFilterInputWorker,
} from './workers'

export default function* keywordSummaryPageWorker() {
    yield all([
        takeLatest(
            mountKeywordsSummaryPageRequest.toString(),
            mountKeywordsSummaryPageWorker
        ),

        takeLatest(
            fetchKeywordsSummaryPageDataRequest.toString(),
            fetchKeywordsSummaryPageDataWorker
        ),

        takeLatest(
            fetchKeywordsSummaryPageTreemapRequest.toString(),
            fetchKeywordsSummaryPageTreemapWorker
        ),

        takeLatest(
            fetchKeywordsSummaryPageTableRequest.toString(),
            fetchKeywordsSummaryPageTableWorker
        ),

        takeLatest(
            updateKeywordsSummaryPageTableSettings.toString(),
            updateKeywordsSummaryPageTableSettingsWorker
        ),

        takeLatest(
            downloadKeywordsSummaryPageTableRequest.toString(),
            downloadKeywordsSummaryPageTableWorker
        ),
        takeLatest(
            changeKeywordsFilterInput.toString(),
            changeKeywordsFilterInputWorker
        ),
    ])
}
