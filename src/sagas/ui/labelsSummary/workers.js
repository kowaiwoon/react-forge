import { call, select, put } from 'redux-saga/effects'

import { LABELS_SUMMARY_PAGE } from 'constants/pages'
import { formatPagination, formatSorter } from 'helpers/params'
import {
    putItemToUserSettingsTable,
    getItemFromUserSettingsTable,
} from 'services/dynamoApi'
import {
    isCurrentSchema as isCurrentColumnSettingsSchema,
    createColumnSettingsItem,
    createColumnSettingsKey,
} from 'helpers/columnSettings'
import {
    // mounting
    mountLabelsSummaryPageSuccess,
    mountLabelsSummaryPageFailure,

    // page data
    fetchLabelsSummaryPageDataSuccess,
    fetchLabelsSummaryPageDataFailure,

    // table data
    fetchLabelsSummaryPageTableSuccess,
    fetchLabelsSummaryPageTableFailure,

    // table settings
    fetchLabelsSummaryPageTableSettingsSuccess,

    // create label
    createLabelsSummaryPageLabelSuccess,
    createLabelsSummaryPageLabelFailure,
} from 'actions/ui'
import { selectDomainValue as selectUiDomainValue } from 'selectors/ui'
import { selectDomainValue as selectAuthDomainValue } from 'selectors/auth'
import cerebroApiSaga from 'sagas/common/cerebroApi'
import { getLabels, createLabel } from 'services/cerebroApi'

function* fetchLabelsSummaryPageTableSaga() {
    const { pagination, sorter } = yield select(selectUiDomainValue, [
        LABELS_SUMMARY_PAGE,
        'table',
    ])

    yield call(cerebroApiSaga, fetchLabelsSummaryPageTableSuccess, getLabels, {
        ...formatPagination(pagination),
        ...formatSorter(sorter),
    })
}

function* updateLabelsTableColumnSettingsSaga() {
    const userId = yield select(selectAuthDomainValue, ['username'])
    const { actionColumns, order, displayState } = yield select(
        selectUiDomainValue,
        [LABELS_SUMMARY_PAGE, 'table', 'columnSettings']
    )

    const item = createColumnSettingsItem({
        userId,
        page: LABELS_SUMMARY_PAGE,
        tableName: 'table',
        actionColumns,
        order,
        displayState,
    })

    yield call(putItemToUserSettingsTable, item)
}

function* fetchLabelsSummaryPageTableSettingsSaga() {
    const userId = yield select(selectAuthDomainValue, ['username'])
    const columnSettings = yield select(selectUiDomainValue, [
        LABELS_SUMMARY_PAGE,
        'table',
        'columnSettings',
    ])

    const key = createColumnSettingsKey({
        userId,
        page: LABELS_SUMMARY_PAGE,
        tableName: 'table',
    })

    const { Item } = yield call(getItemFromUserSettingsTable, key)

    if (Item) {
        if (isCurrentColumnSettingsSchema(Item, columnSettings)) {
            yield put(fetchLabelsSummaryPageTableSettingsSuccess(Item))
        } else {
            yield call(updateLabelsTableColumnSettingsSaga)
        }
    }
}

function* fetchLabelsSummaryPageDataSaga() {
    yield call(fetchLabelsSummaryPageTableSaga)
}

function* createLabelSaga(action) {
    yield call(cerebroApiSaga, null, createLabel, action.payload)
}

/**
 * Create a new label
 */
export function* createLabelsSummaryPageLabelWorker(action) {
    try {
        yield call(createLabelSaga, action)
        yield call(fetchLabelsSummaryPageTableSaga)
        yield put(createLabelsSummaryPageLabelSuccess())
    } catch (error) {
        yield put(createLabelsSummaryPageLabelFailure(error))
    }
}

/**
 * Puts new table settings to DynamoDB
 */
export function* updateLabelsSummaryPageTableSettingsWorker() {
    yield call(updateLabelsTableColumnSettingsSaga)
}

/**
 * Fetches data for Labels Summary Page Table
 */
export function* fetchLabelsSummaryPageTableWorker() {
    try {
        yield call(fetchLabelsSummaryPageTableSaga)
    } catch (error) {
        yield put(fetchLabelsSummaryPageTableFailure(error))
    }
}

/**
 * Fetches all data required for the Labels Summary Page
 */
export function* fetchLabelsSummaryPageDataWorker() {
    try {
        yield call(fetchLabelsSummaryPageDataSaga)
        yield put(fetchLabelsSummaryPageDataSuccess())
    } catch (error) {
        yield put(fetchLabelsSummaryPageDataFailure(error))
    }
}

/**
 * Mounts the Labels Summary Page and fetches data
 */
export function* mountLabelsSummaryPageWorker() {
    try {
        yield call(fetchLabelsSummaryPageTableSettingsSaga)
        yield call(fetchLabelsSummaryPageDataSaga)
        yield put(mountLabelsSummaryPageSuccess())
    } catch (error) {
        yield put(mountLabelsSummaryPageFailure(error))
    }
}
