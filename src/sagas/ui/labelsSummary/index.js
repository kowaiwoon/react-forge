import { all, takeLatest } from 'redux-saga/effects'

import {
    mountLabelsSummaryPageRequest,
    fetchLabelsSummaryPageDataRequest,
    updateLabelsSummaryPageTableSettings,
    fetchLabelsSummaryPageTableRequest,
    createLabelsSummaryPageLabelRequest,
} from 'actions/ui'

import {
    mountLabelsSummaryPageWorker,
    fetchLabelsSummaryPageDataWorker,
    fetchLabelsSummaryPageTableWorker,
    updateLabelsSummaryPageTableSettingsWorker,
    createLabelsSummaryPageLabelWorker,
} from './workers'

export default function* brandsSummaryPageWorker() {
    yield all([
        takeLatest(
            mountLabelsSummaryPageRequest.toString(),
            mountLabelsSummaryPageWorker
        ),

        takeLatest(
            fetchLabelsSummaryPageDataRequest.toString(),
            fetchLabelsSummaryPageDataWorker
        ),

        takeLatest(
            fetchLabelsSummaryPageTableRequest.toString(),
            fetchLabelsSummaryPageTableWorker
        ),

        takeLatest(
            updateLabelsSummaryPageTableSettings.toString(),
            updateLabelsSummaryPageTableSettingsWorker
        ),

        takeLatest(
            createLabelsSummaryPageLabelRequest.toString(),
            createLabelsSummaryPageLabelWorker
        ),
    ])
}
