import { all, takeLatest } from 'redux-saga/effects'

import {
    mountSovKeywordPageRequest,
    fetchSovKeywordPageDataRequest,
    updateSovKeywordPageTableSettings,
    fetchSovKeywordPageTableRequest,
    downloadSovKeywordPageTableRequest,
    updateSovKeywordPageKeywordDetailsRequest,
} from 'actions/ui'

import {
    mountSovKeywordPageWorker,
    fetchSovKeywordPageDataWorker,
    fetchSovKeywordPageTableWorker,
    downloadSovKeywordPageTableWorker,
    updateSovKeywordPageKeywordDetailsWorker,
    updateSovKeywordPageTableSettingsWorker,
} from './workers'

export default function* sovKeywordPageWorker() {
    yield all([
        takeLatest(
            mountSovKeywordPageRequest.toString(),
            mountSovKeywordPageWorker
        ),

        takeLatest(
            fetchSovKeywordPageDataRequest.toString(),
            fetchSovKeywordPageDataWorker
        ),

        takeLatest(
            fetchSovKeywordPageTableRequest.toString(),
            fetchSovKeywordPageTableWorker
        ),

        takeLatest(
            downloadSovKeywordPageTableRequest.toString(),
            downloadSovKeywordPageTableWorker
        ),

        takeLatest(
            updateSovKeywordPageKeywordDetailsRequest.toString(),
            updateSovKeywordPageKeywordDetailsWorker
        ),

        takeLatest(
            updateSovKeywordPageTableSettings.toString(),
            updateSovKeywordPageTableSettingsWorker
        ),
    ])
}
