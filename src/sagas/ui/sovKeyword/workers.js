import { all, call, select, put } from 'redux-saga/effects'

import { SOV_CHART } from 'constants/reducerKeys'
import { SOV_AGGREGATION } from 'constants/filters'
import { formatAllParams, formatSovFactAggregateParams } from 'helpers/params'
import { downloadCsv } from 'helpers/downloads'
import {
    getSovKeywordSerpDataPoints,
    getSovKeywordSerpDataPointsExport,
    getSovFactAggregate,
} from 'services/cerebroApi'
import { SOV_KEYWORD_PAGE } from 'constants/pages'
import {
    putItemToUserSettingsTable,
    getItemFromUserSettingsTable,
} from 'services/dynamoApi'
import {
    isCurrentSchema as isCurrentColumnSettingsSchema,
    createColumnSettingsItem,
    createColumnSettingsKey,
} from 'helpers/columnSettings'
import {
    fetchSovKeywordSaga as fetchEntitiesSovKeywordSaga,
    updateSovKeywordSaga as updateEntitiesSovKeywordSaga,
} from 'sagas/entities'
import {
    // mounting
    mountSovKeywordPageSuccess,
    mountSovKeywordPageFailure,

    // page data
    fetchSovKeywordPageDataSuccess,
    fetchSovKeywordPageDataFailure,

    // keyword details
    updateSovKeywordPageKeywordDetailsSuccess,
    updateSovKeywordPageKeywordDetailsFailure,

    // table data
    fetchSovKeywordPageTableSuccess,
    fetchSovKeywordPageTableFailure,

    // table settings
    fetchSovKeywordPageTableSettingsSuccess,

    // table download
    downloadSovKeywordPageTableSuccess,
    downloadSovKeywordPageTableFailure,

    // sov chart
    fetchSovKeywordPageSovChartSuccess,
} from 'actions/ui'
import { selectDomainValue as selectAuthDomainValue } from 'selectors/auth'
import {
    selectDomainValue as selectUiDomainValue,
    selectPageFilters,
} from 'selectors/ui'
import cerebroApiSaga from 'sagas/common/cerebroApi'

import { fetchPageFilterSettingsSaga } from '../shared/workers'

function* fetchSovKeywordSaga() {
    const sovKeywordId = yield select(selectUiDomainValue, [
        SOV_KEYWORD_PAGE,
        'sovKeywordId',
    ])

    yield call(fetchEntitiesSovKeywordSaga, sovKeywordId)
}

function* updateSovKeywordSaga(action) {
    const data = action.payload
    const sovKeywordId = yield select(selectUiDomainValue, [
        SOV_KEYWORD_PAGE,
        'sovKeywordId',
    ])

    yield call(updateEntitiesSovKeywordSaga, sovKeywordId, data)
}

function* fetchSovKeywordPageTableSaga() {
    const sovKeywordId = yield select(selectUiDomainValue, [
        SOV_KEYWORD_PAGE,
        'sovKeywordId',
    ])
    const filters = yield select(selectPageFilters, SOV_KEYWORD_PAGE)
    const { pagination, sorter } = yield select(selectUiDomainValue, [
        SOV_KEYWORD_PAGE,
        'table',
    ])
    const params = formatAllParams(filters, pagination, sorter)

    yield call(
        cerebroApiSaga,
        fetchSovKeywordPageTableSuccess,
        getSovKeywordSerpDataPoints,
        sovKeywordId,
        params
    )
}

function* downloadSovKeywordPageTableSaga() {
    const sovKeywordId = yield select(selectUiDomainValue, [
        SOV_KEYWORD_PAGE,
        'sovKeywordId',
    ])
    const filters = yield select(selectPageFilters, SOV_KEYWORD_PAGE)
    const { pagination, sorter } = yield select(selectUiDomainValue, [
        SOV_KEYWORD_PAGE,
        'table',
    ])
    const params = formatAllParams(filters, pagination, sorter)

    const response = yield call(
        cerebroApiSaga,
        downloadSovKeywordPageTableSuccess,
        getSovKeywordSerpDataPointsExport,
        sovKeywordId,
        params
    )

    yield call(downloadCsv, response.data, `sov-${sovKeywordId}-searches`)
}

function* updateSovKeywordPageTableSettingsSaga() {
    const userId = yield select(selectAuthDomainValue, ['username'])
    const { actionColumns, order, displayState } = yield select(
        selectUiDomainValue,
        [SOV_KEYWORD_PAGE, 'table', 'columnSettings']
    )

    const item = createColumnSettingsItem({
        userId,
        page: SOV_KEYWORD_PAGE,
        tableName: 'table',
        actionColumns,
        order,
        displayState,
    })

    yield call(putItemToUserSettingsTable, item)
}

function* fetchSovKeywordTableSettingsSaga() {
    const userId = yield select(selectAuthDomainValue, ['username'])
    const columnSettings = yield select(selectUiDomainValue, [
        SOV_KEYWORD_PAGE,
        'table',
        'columnSettings',
    ])

    const key = createColumnSettingsKey({
        userId,
        page: SOV_KEYWORD_PAGE,
        tableName: 'table',
    })

    const { Item } = yield call(getItemFromUserSettingsTable, key)

    if (Item) {
        if (isCurrentColumnSettingsSchema(Item, columnSettings)) {
            yield put(fetchSovKeywordPageTableSettingsSuccess(Item))
        } else {
            yield call(updateSovKeywordPageTableSettingsSaga)
        }
    }
}

function* fetchSovKeywordPageSovChartSaga() {
    const sovKeywordId = yield select(selectUiDomainValue, [
        SOV_KEYWORD_PAGE,
        'sovKeywordId',
    ])
    const filters = yield select(selectPageFilters, SOV_KEYWORD_PAGE)
    const { pagination, sorter, groups } = yield select(selectUiDomainValue, [
        SOV_KEYWORD_PAGE,
        SOV_CHART,
    ])
    const params = formatSovFactAggregateParams({
        sovKeywordId,
        filters,
        pagination,
        sorter,
        aggregation: filters[SOV_AGGREGATION],
        groups,
    })

    yield call(
        cerebroApiSaga,
        fetchSovKeywordPageSovChartSuccess,
        getSovFactAggregate,
        params
    )
}

function* fetchSovKeywordPageDataSaga() {
    yield all([
        call(fetchSovKeywordPageTableSaga),
        call(fetchSovKeywordPageSovChartSaga),
    ])
}

/**
 * Puts new table settings to DynamoDB
 */
export function* updateSovKeywordPageTableSettingsWorker() {
    yield call(updateSovKeywordPageTableSettingsSaga)
}

/**
 * Fetches data for SOV Keyword Page table
 */
export function* fetchSovKeywordPageTableWorker() {
    try {
        yield call(fetchSovKeywordPageTableSaga)
    } catch (error) {
        yield put(fetchSovKeywordPageTableFailure(error))
    }
}

/**
 * Downloads table data for SOV Keyword Page
 */
export function* downloadSovKeywordPageTableWorker() {
    try {
        yield call(downloadSovKeywordPageTableSaga)
    } catch (error) {
        yield put(downloadSovKeywordPageTableFailure(error))
    }
}

/**
 * Fetches all data required for the SOV Keyword Page
 */
export function* fetchSovKeywordPageDataWorker() {
    try {
        yield call(fetchSovKeywordPageDataSaga)
        yield put(fetchSovKeywordPageDataSuccess())
    } catch (error) {
        yield put(fetchSovKeywordPageDataFailure(error))
    }
}

/**
 * Updates Keyword Details
 */
export function* updateSovKeywordPageKeywordDetailsWorker(action) {
    try {
        yield call(updateSovKeywordSaga, action)
        yield put(updateSovKeywordPageKeywordDetailsSuccess())
    } catch (error) {
        yield put(updateSovKeywordPageKeywordDetailsFailure(error))
    }
}

/**
 * Mounts the Brand Page and fetches data
 */
export function* mountSovKeywordPageWorker() {
    try {
        yield all([
            call(fetchPageFilterSettingsSaga, SOV_KEYWORD_PAGE),
            call(fetchSovKeywordSaga),
            call(fetchSovKeywordTableSettingsSaga),
            call(fetchSovKeywordPageDataSaga),
        ])
        yield put(mountSovKeywordPageSuccess())
    } catch (error) {
        yield put(mountSovKeywordPageFailure(error))
    }
}
