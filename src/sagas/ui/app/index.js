import { all, takeLatest } from 'redux-saga/effects'

import {
    mountApp,
    changeCurrencyCode,
    changeCampaignsFilterInput,
    changeBrandsFilterInput,
} from 'actions/ui'

import {
    mountAppWorker,
    changeCurrencyCodeWorker,
    changeCampaignsFilterInputWorker,
    changeBrandsFilterInputWorker,
} from './workers'

export default function* appWorker() {
    yield all([
        takeLatest(mountApp.toString(), mountAppWorker),

        takeLatest(changeCurrencyCode.toString(), changeCurrencyCodeWorker),

        takeLatest(
            changeCampaignsFilterInput.toString(),
            changeCampaignsFilterInputWorker
        ),

        takeLatest(
            changeBrandsFilterInput.toString(),
            changeBrandsFilterInputWorker
        ),
    ])
}
