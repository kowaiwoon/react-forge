import { put, call, select, all } from 'redux-saga/effects'

import { KEYWORD_PAGE } from 'constants/pages'
import { SPONSORED_PRODUCT, HEADLINE_SEARCH } from 'constants/factTypes'
import { formatFilters } from 'helpers/params'
import { downloadCsv } from 'helpers/downloads'
import {
    // aggregate
    getKeywordSponsoredProductFactAggregate,
    getKeywordHeadlineSearchFactAggregate,

    // timeseries
    getKeywordSponsoredProductFactTimeseries,
    getKeywordHeadlineSearchFactTimeseries,
    getKeywordFactTimeseriesExport,
} from 'services/cerebroApi'
import {
    // mounting
    mountKeywordPageSuccess,
    mountKeywordPageFailure,

    // page data
    fetchKeywordPageDataSuccess,
    fetchKeywordPageDataFailure,

    // update keyword
    updateKeywordPageKeywordDetailsSuccess,
    updateKeywordPageKeywordDetailsFailure,

    // aggregate
    fetchKeywordPageAggregateSuccess,

    // timeseries
    fetchKeywordPageSponsoredProductTimeseriesSuccess,
    fetchKeywordPageHeadlineSearchTimeseriesSuccess,

    // timeseries download
    downloadKeywordPageTimeseriesSuccess,
    downloadKeywordPageTimeseriesFailure,
} from 'actions/ui'
import { selectKeyword, selectCampaign } from 'selectors/entities'
import {
    selectDomainValue as selectUiDomainValue,
    selectPageFilters,
    selectCurrencyCode,
} from 'selectors/ui'
import cerebroApiSaga from 'sagas/common/cerebroApi'
import {
    fetchKeywordSaga as fetchEntitiesKeywordSaga,
    updateKeywordSaga as updateEntitiesKeywordSaga,
} from 'sagas/entities'

import { fetchPageFilterSettingsSaga } from '../shared/workers'

const getKeywordAggregateApi = factType => {
    if (factType === SPONSORED_PRODUCT) {
        return getKeywordSponsoredProductFactAggregate
    }

    if (factType === HEADLINE_SEARCH) {
        return getKeywordHeadlineSearchFactAggregate
    }

    return null
}

function* fetchKeywordSaga() {
    const keywordId = yield select(selectUiDomainValue, [
        KEYWORD_PAGE,
        'keywordId',
    ])

    yield call(fetchEntitiesKeywordSaga, keywordId)
}

function* updateKeywordSaga(action) {
    const data = action.payload
    const keywordId = yield select(selectUiDomainValue, [
        KEYWORD_PAGE,
        'keywordId',
    ])

    yield call(updateEntitiesKeywordSaga, keywordId, data)
}

function* selectKeywordFactTypeSaga() {
    const keywordId = yield select(selectUiDomainValue, [
        KEYWORD_PAGE,
        'keywordId',
    ])
    const keyword = yield select(selectKeyword, keywordId)

    const { campaign: campaignId } = keyword
    const campaign = yield select(selectCampaign, campaignId)
    return campaign.campaign_type
}

function* fetchKeywordPageSponsoredProductTimeseriesSaga() {
    const keywordId = yield select(selectUiDomainValue, [
        KEYWORD_PAGE,
        'keywordId',
    ])
    const factType = yield call(selectKeywordFactTypeSaga)
    if (factType === SPONSORED_PRODUCT) {
        const filters = yield select(selectPageFilters, KEYWORD_PAGE)
        const currency = yield select(selectCurrencyCode)
        const params = {
            ...formatFilters(filters),
            currency,
        }
        params.currency = currency
        yield call(
            cerebroApiSaga,
            fetchKeywordPageSponsoredProductTimeseriesSuccess,
            getKeywordSponsoredProductFactTimeseries,
            keywordId,
            params
        )
    } else {
        yield put(fetchKeywordPageSponsoredProductTimeseriesSuccess([]))
    }
}

function* fetchKeywordPageHeadlineSearchTimeseriesSaga() {
    const keywordId = yield select(selectUiDomainValue, [
        KEYWORD_PAGE,
        'keywordId',
    ])
    const factType = yield call(selectKeywordFactTypeSaga)
    if (factType === HEADLINE_SEARCH) {
        const filters = yield select(selectPageFilters, KEYWORD_PAGE)
        const currency = yield select(selectCurrencyCode)
        const params = {
            ...formatFilters(filters),
            currency,
        }
        params.currency = currency
        yield call(
            cerebroApiSaga,
            fetchKeywordPageHeadlineSearchTimeseriesSuccess,
            getKeywordHeadlineSearchFactTimeseries,
            keywordId,
            params
        )
    } else {
        yield put(fetchKeywordPageHeadlineSearchTimeseriesSuccess([]))
    }
}

function* fetchKeywordPageAggregateSaga() {
    const keywordId = yield select(selectUiDomainValue, [
        KEYWORD_PAGE,
        'keywordId',
    ])
    const filters = yield select(selectPageFilters, KEYWORD_PAGE)
    const currency = yield select(selectCurrencyCode)
    const params = {
        ...formatFilters(filters),
        currency,
    }
    const factType = yield call(selectKeywordFactTypeSaga)
    const aggregateApi = getKeywordAggregateApi(factType)

    yield call(
        cerebroApiSaga,
        fetchKeywordPageAggregateSuccess,
        aggregateApi,
        keywordId,
        params
    )
}

function* downloadKeywordPageTimeseriesSaga() {
    const keywordId = yield select(selectUiDomainValue, [
        KEYWORD_PAGE,
        'keywordId',
    ])
    const filters = yield select(selectPageFilters, KEYWORD_PAGE)
    const currency = yield select(selectCurrencyCode)
    const params = {
        ...formatFilters(filters),
        keywordId,
        currency,
    }

    const response = yield call(
        cerebroApiSaga,
        downloadKeywordPageTimeseriesSuccess,
        getKeywordFactTimeseriesExport,
        keywordId,
        params
    )
    yield call(downloadCsv, response.data, `keyword-${keywordId}-timeseries`)
}

function* fetchKeywordPageDataSaga() {
    yield all([
        call(fetchKeywordPageSponsoredProductTimeseriesSaga),
        call(fetchKeywordPageHeadlineSearchTimeseriesSaga),
        call(fetchKeywordPageAggregateSaga),
    ])
}

/**
 * Downloads timeseries data for Keyword Page
 */
export function* downloadKeywordPageTimeseriesWorker() {
    try {
        yield call(downloadKeywordPageTimeseriesSaga)
    } catch (error) {
        yield put(downloadKeywordPageTimeseriesFailure(error))
    }
}

/**
 * Fetches all data required for the Keyword Page
 */
export function* fetchKeywordPageDataWorker() {
    try {
        yield call(fetchKeywordPageDataSaga)
        yield put(fetchKeywordPageDataSuccess())
    } catch (error) {
        yield put(fetchKeywordPageDataFailure(error))
    }
}

/**
 * Updates Keyword Details
 */
export function* updateKeywordPageKeywordDetailsWorker(action) {
    try {
        yield call(updateKeywordSaga, action)
        yield put(updateKeywordPageKeywordDetailsSuccess())
    } catch (error) {
        yield put(updateKeywordPageKeywordDetailsFailure(error))
    }
}

/**
 * Mounts the Keyword Page and fetches data
 */
export function* mountKeywordPageWorker() {
    try {
        yield all([
            call(fetchKeywordSaga),
            call(fetchPageFilterSettingsSaga, KEYWORD_PAGE),
        ])
        yield call(fetchKeywordPageDataSaga)
        yield put(mountKeywordPageSuccess())
    } catch (error) {
        yield put(mountKeywordPageFailure(error))
    }
}
