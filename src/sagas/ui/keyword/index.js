import { all, takeLatest } from 'redux-saga/effects'

import {
    mountKeywordPageRequest,
    fetchKeywordPageDataRequest,
    downloadKeywordPageTimeseriesRequest,
    updateKeywordPageKeywordDetailsRequest,
} from 'actions/ui'

import {
    mountKeywordPageWorker,
    fetchKeywordPageDataWorker,
    downloadKeywordPageTimeseriesWorker,
    updateKeywordPageKeywordDetailsWorker,
} from './workers'

export default function* keywordPageWorker() {
    yield all([
        takeLatest(mountKeywordPageRequest.toString(), mountKeywordPageWorker),

        takeLatest(
            fetchKeywordPageDataRequest.toString(),
            fetchKeywordPageDataWorker
        ),

        takeLatest(
            downloadKeywordPageTimeseriesRequest.toString(),
            downloadKeywordPageTimeseriesWorker
        ),

        takeLatest(
            updateKeywordPageKeywordDetailsRequest.toString(),
            updateKeywordPageKeywordDetailsWorker
        ),
    ])
}
