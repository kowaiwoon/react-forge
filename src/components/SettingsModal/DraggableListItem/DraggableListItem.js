/* eslint-disable react/no-find-dom-node */
import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import { Draggable } from 'react-beautiful-dnd'
import { List } from 'antd'

import styles from './styles.scss'

const propTypes = {
    id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
    index: PropTypes.number.isRequired,
    children: PropTypes.node,
}

const defaultProps = {
    children: null,
}

const draggingStyles = {
    WebkitBoxShadow: '2px 2px 1px rgba(0,0,0,0.2)',
    MozBoxShadow: '2px 2px 1px rgba(0,0,0,0.2)',
    boxShadow: '2px 2px 1px rgba(0,0,0,0.2)',
    borderRadius: '3px',
}

const getItemStyle = (isDragging, draggableStyle) => ({
    paddingLeft: '6px',
    paddingRight: '6px',
    backgroundColor: 'white',

    ...(isDragging ? draggingStyles : {}),

    ...draggableStyle,
})

const dragHandle = (
    <div className={styles['drag-handle']}>
        <span />
        <span />
        <span />
    </div>
)

const DraggableListItem = ({ id, index, children, ...props }) => (
    <Draggable draggableId={id} index={index}>
        {(provided, snapshot) => (
            <List.Item
                ref={node => provided.innerRef(ReactDOM.findDOMNode(node))}
                {...provided.draggableProps}
                {...provided.dragHandleProps}
                style={getItemStyle(
                    snapshot.isDragging,
                    provided.draggableProps.style
                )}
                {...props}
            >
                {children}
                {dragHandle}
            </List.Item>
        )}
    </Draggable>
)

DraggableListItem.propTypes = propTypes
DraggableListItem.defaultProps = defaultProps

export default DraggableListItem
