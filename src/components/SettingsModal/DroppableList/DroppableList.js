/* eslint-disable react/no-find-dom-node */
import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import { Droppable } from 'react-beautiful-dnd'
import { List } from 'antd'

const propTypes = {
    droppableId: PropTypes.string.isRequired,
    children: PropTypes.node,
}

const defaultProps = {
    children: null,
}

const DroppableList = ({ children, droppableId, ...props }) => {
    const containerStyles = {
        backgroundColor: 'rgb(240, 240, 240)',
    }

    return (
        <Droppable droppableId={droppableId}>
            {provided => (
                <List
                    ref={node => provided.innerRef(ReactDOM.findDOMNode(node))}
                    style={containerStyles}
                    {...props}
                >
                    {children}
                    {provided.placeholder}
                </List>
            )}
        </Droppable>
    )
}

DroppableList.propTypes = propTypes
DroppableList.defaultProps = defaultProps

export default DroppableList
