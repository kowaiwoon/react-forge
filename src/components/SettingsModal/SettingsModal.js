import React, { Component } from 'react'
import { Modal, Button, Checkbox } from 'antd'
import PropTypes from 'prop-types'
import autobind from 'autobind-decorator'
import set from 'lodash/fp/set'
import { DragDropContext } from 'react-beautiful-dnd'

import DroppableList from './DroppableList/DroppableList'
import DraggableListItem from './DraggableListItem/DraggableListItem'
import styles from './styles.scss'

const reorder = (list, startIndex, endIndex) => {
    const result = [...list]
    const [removed] = result.splice(startIndex, 1)
    result.splice(endIndex, 0, removed)

    return result
}

class SettingsModal extends Component {
    static propTypes = {
        droppableId: PropTypes.string.isRequired,
        modalTitle: PropTypes.string.isRequired,
        visible: PropTypes.bool.isRequired,
        handleCancel: PropTypes.func.isRequired,
        handleOk: PropTypes.func.isRequired,
        settings: PropTypes.shape({
            order: PropTypes.arrayOf(PropTypes.string).isRequired,
            displayState: PropTypes.objectOf(PropTypes.bool).isRequired,
        }).isRequired,
        settingTitles: PropTypes.objectOf(
            PropTypes.shape({
                title: PropTypes.string.isRequired,
            })
        ).isRequired,
        disabledSettings: PropTypes.arrayOf(PropTypes.string),
    }

    static defaultProps = {
        disabledSettings: [],
    }

    state = {
        dirtySettings: this.props.settings,
    }

    @autobind
    toggleItemState(setting) {
        const current = this.state.dirtySettings.displayState[setting]

        this.setState(
            set(
                ['dirtySettings', 'displayState', setting],
                !current,
                this.state
            )
        )
    }

    @autobind
    handleDragEnd(result) {
        // dropped outside the list
        if (!result.destination) {
            return
        }

        const currentOrder = this.state.dirtySettings.order

        const nextOrder = reorder(
            currentOrder,
            result.source.index,
            result.destination.index
        )

        this.setState(set(['dirtySettings', 'order'], nextOrder, this.state))
    }

    @autobind
    handleOk() {
        const { dirtySettings } = this.state

        this.props.handleOk(dirtySettings)
    }

    @autobind
    resetState() {
        const { settings } = this.props

        this.setState({ dirtySettings: settings })
    }

    render() {
        const {
            droppableId,
            visible,
            handleCancel,
            settingTitles,
            modalTitle,
            disabledSettings,
        } = this.props
        const { order, displayState } = this.state.dirtySettings

        const saveButtonProps = {
            key: 'save',
            type: 'primary',
            onClick: this.handleOk,
        }
        const saveButton = <Button {...saveButtonProps}>Save</Button>

        const cancelButtonProps = {
            key: 'cancel',
            onClick: handleCancel,
        }
        const cancelButton = <Button {...cancelButtonProps}>Cancel</Button>

        const listItems = order.map((setting, index) => {
            const isDisabled = disabledSettings.includes(setting)
            const isSelected = displayState[setting]
            const { title } = settingTitles[setting]

            return (
                <DraggableListItem key={setting} id={setting} index={index}>
                    <Checkbox
                        disabled={isDisabled}
                        checked={isSelected}
                        onChange={() => this.toggleItemState(setting)}
                    >
                        {title}
                    </Checkbox>
                </DraggableListItem>
            )
        })

        return (
            <Modal
                bodyStyle={{
                    maxHeight: 'calc(100vh - 300px)',
                    overflowY: 'scroll',
                }}
                wrapClassName={styles['modal-wrap']}
                title={modalTitle}
                visible={visible}
                onCancel={handleCancel}
                footer={[cancelButton, saveButton]}
                afterClose={this.resetState}
            >
                <DragDropContext onDragEnd={this.handleDragEnd}>
                    <DroppableList droppableId={droppableId}>
                        {listItems}
                    </DroppableList>
                </DragDropContext>
            </Modal>
        )
    }
}

export default SettingsModal
