import React, { Component } from 'react'
import PropTypes from 'prop-types'
import identity from 'lodash/identity'
import noop from 'lodash/noop'

import {
    ALL_TYPES,
    NUMBER_INPUT,
    SELECT_INPUT,
    CASCADER_INPUT,
    DATE_INPUT,
} from 'constants/inputTypes'
import { INPUT_TYPES_COMPONENTS } from 'configuration/inputTypes'

// ant requires a stateful component because this is used in forms
// eslint-disable-next-line react/prefer-stateless-function
class InputType extends Component {
    static propTypes = {
        inputType: PropTypes.oneOf(ALL_TYPES).isRequired,
        updating: PropTypes.bool,

        // select options for selectInput
        options: PropTypes.arrayOf(
            PropTypes.shape({
                value: PropTypes.string,
                label: PropTypes.string,
            })
        ),

        // options for numberInput
        formatter: PropTypes.func,
        parser: PropTypes.func,
        min: PropTypes.number,
        max: PropTypes.number,
        step: PropTypes.number,
        precision: PropTypes.number,

        // options for dateInput
        format: PropTypes.string,

        // shared options
        onChange: PropTypes.func,
    }

    static defaultProps = {
        updating: false,
        options: [],
        formatter: identity,
        parser: identity,
        min: -Infinity,
        max: Infinity,
        step: 1,
        precision: 2,
        format: null,
        onChange: noop,
    }

    render() {
        const {
            inputType,
            updating,
            options,
            formatter,
            parser,
            min,
            max,
            step,
            format,
            onChange,
            precision,
            ...rest
        } = this.props
        const InputComponent = INPUT_TYPES_COMPONENTS[inputType]

        if (inputType === SELECT_INPUT) {
            const { Option } = InputComponent
            return (
                <InputComponent
                    disabled={updating}
                    onChange={onChange}
                    {...rest}
                >
                    {options.map(({ value, label }) => (
                        <Option key={value} value={value}>
                            {label}
                        </Option>
                    ))}
                </InputComponent>
            )
        }

        if (inputType === CASCADER_INPUT) {
            return (
                <InputComponent
                    options={options}
                    disabled={updating}
                    onChange={onChange}
                    {...rest}
                />
            )
        }

        if (inputType === DATE_INPUT) {
            return (
                <InputComponent
                    format={format}
                    disabled={updating}
                    onChange={onChange}
                    {...rest}
                />
            )
        }

        if (inputType === NUMBER_INPUT) {
            return (
                <InputComponent
                    formatter={formatter}
                    parser={parser}
                    min={min}
                    max={max}
                    step={step}
                    disabled={updating}
                    onChange={onChange}
                    precision={precision}
                    {...rest}
                />
            )
        }

        return <InputComponent disabled={updating} {...rest} />
    }
}

export default InputType
