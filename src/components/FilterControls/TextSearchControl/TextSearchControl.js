import React from 'react'
import PropTypes from 'prop-types'
import { Input } from 'antd'

const propTypes = {
    onApply: PropTypes.func.isRequired,
    placeholder: PropTypes.string.isRequired,
}

const TextSearchControl = ({ onApply, placeholder }) => (
    <Input.Search
        style={{ width: '200px', marginRight: '8px', height: '32px' }}
        onSearch={onApply}
        placeholder={placeholder}
    />
)

TextSearchControl.propTypes = propTypes

export default TextSearchControl
