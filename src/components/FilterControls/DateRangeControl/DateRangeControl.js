import React, { Component } from 'react'
import PropTypes from 'prop-types'
import isEmpty from 'lodash/isEmpty'
import noop from 'lodash/noop'
import autobind from 'autobind-decorator'

import moment from 'utilities/moment'
import { Popover } from 'components/Popover'

import Footer from '../Footer/Footer'
import Tag from '../Tag/Tag'
import DateRange from './DateRange'

class DateRangeFilter extends Component {
    static propTypes = {
        // filter control interface props
        values: PropTypes.arrayOf(PropTypes.instanceOf(moment)).isRequired,
        onApply: PropTypes.func.isRequired,
        onCloseFilter: PropTypes.func,
        closable: PropTypes.bool.isRequired,
    }

    static defaultProps = {
        onCloseFilter: noop,
    }

    constructor(props) {
        super(props)

        this.state = {
            popupVisible: false,
            closed: false,
            selectedValues: props.values,
        }
    }

    componentWillUnmount() {
        // ensures event listener is removed
        document.removeEventListener('keydown', this.handleKeyDown)
    }

    static getButtonTitle(dateRange) {
        if (isEmpty(dateRange)) return 'Dates'

        const startDate = dateRange[0].format('MMM D')
        const endDate = dateRange[1].format('MMM D')

        return `${startDate} - ${endDate}`
    }

    @autobind
    handleKeyDown(event) {
        // close popover when esc key is pressed
        if (event.keyCode === 27) {
            this.setState({ popupVisible: false })
        }
    }

    @autobind
    handleChange(selectedValues) {
        this.setState({ selectedValues })
    }

    @autobind
    handleClose() {
        this.setState({ closed: true }, this.props.onCloseFilter)
    }

    @autobind
    handlePopupVisibleChange(visible) {
        if (visible) {
            document.addEventListener('keydown', this.handleKeyDown)
        } else {
            document.removeEventListener('keydown', this.handleKeyDown)
        }

        this.setState({
            popupVisible: visible,
            selectedValues: this.props.values,
        })
    }

    @autobind
    handleApply() {
        this.props.onApply(this.state.selectedValues)
        this.setState({
            popupVisible: false,
        })
    }

    renderContent() {
        return (
            <React.Fragment>
                <DateRange
                    onChange={this.handleChange}
                    values={this.state.selectedValues}
                />
                <Footer
                    rightOnClick={this.handleApply}
                    dividerStyle={{
                        marginTop: '0',
                        marginBottom: '12px',
                        marginLeft: '0',
                        marginRight: '0',
                    }}
                />
            </React.Fragment>
        )
    }

    render() {
        const { values, closable } = this.props
        const { popupVisible, closed } = this.state

        return (
            <Popover
                content={this.renderContent()}
                trigger="click"
                placement="bottomLeft"
                visible={popupVisible && !closed}
                onVisibleChange={this.handlePopupVisibleChange}
                mask
            >
                <Tag
                    active={!isEmpty(values)}
                    closable={closable}
                    onClose={this.handleClose}
                >
                    {DateRangeFilter.getButtonTitle(values)}
                </Tag>
            </Popover>
        )
    }
}

export default DateRangeFilter
