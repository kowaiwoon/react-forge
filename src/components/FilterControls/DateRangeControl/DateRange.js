import React from 'react'
import PropTypes from 'prop-types'
import { DateRangePicker } from 'react-date-range'
import 'react-date-range/dist/styles.css'
import 'react-date-range/dist/theme/default.css'

import moment from 'utilities/moment'

import './themeOverrides.scss'
import { LOCAL_TIMEZONE, staticRanges, inputRanges } from './ranges'

const COLOR = '#01C7D1'
const RANGE_KEY = 'selection'
const ISO_FORMAT = 'YYYY-MM-DDTHH:mm:ss'

const propTypes = {
    onChange: PropTypes.func.isRequired,
    values: PropTypes.arrayOf(PropTypes.instanceOf(moment)).isRequired,
}

const defaultProps = {}

const DateRange = ({ onChange, values }) => {
    const convertLocalDateToUTCMoment = date => {
        const localString = moment.tz(date, LOCAL_TIMEZONE).format(ISO_FORMAT)
        return moment(localString)
    }

    const convertUTCMomentToLocalDate = _moment => {
        const utcString = _moment.format(ISO_FORMAT)
        return moment.tz(utcString, LOCAL_TIMEZONE).toDate()
    }

    const getRanges = () => [
        {
            startDate: convertUTCMomentToLocalDate(values[0]),
            endDate: convertUTCMomentToLocalDate(values[1]),
            key: RANGE_KEY,
            color: COLOR,
        },
    ]

    const handleChange = payload => {
        const { selection } = payload

        onChange([
            convertLocalDateToUTCMoment(selection.startDate),
            convertLocalDateToUTCMoment(selection.endDate),
        ])
    }

    return (
        <DateRangePicker
            ranges={getRanges()}
            onChange={handleChange}
            direction="horizontal"
            months={2}
            moveRangeOnFirstSelection={false}
            showSelectionPreview
            staticRanges={staticRanges}
            inputRanges={inputRanges}
        />
    )
}

DateRange.propTypes = propTypes
DateRange.defaultProps = defaultProps

export default DateRange
