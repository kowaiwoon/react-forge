import moment from 'utilities/moment'

export const LOCAL_TIMEZONE = moment.tz.guess()
const localNow = () => moment.tz(LOCAL_TIMEZONE)

const dates = {
    endOfYesterday: localNow()
        .subtract(1, 'days')
        .endOf('day')
        .toDate(),
    startOfYesterday: localNow()
        .subtract(1, 'days')
        .startOf('day')
        .toDate(),

    endOfToday: localNow()
        .endOf('day')
        .toDate(),
    startOfToday: localNow()
        .startOf('day')
        .toDate(),

    endOfWeek: localNow()
        .endOf('week')
        .toDate(),
    startOfWeek: localNow()
        .startOf('week')
        .toDate(),

    endOfLastWeek: localNow()
        .subtract(1, 'week')
        .endOf('week')
        .toDate(),
    startOfLastWeek: localNow()
        .subtract(1, 'week')
        .startOf('week')
        .toDate(),

    startOfLastWeekMinus2: localNow()
        .subtract(2, 'week')
        .startOf('week')
        .toDate(),
    startOfLastWeekMinus3: localNow()
        .subtract(3, 'week')
        .startOf('week')
        .toDate(),
    startOfLastWeekMinus4: localNow()
        .subtract(4, 'week')
        .startOf('week')
        .toDate(),

    endOfMonth: localNow()
        .endOf('month')
        .toDate(),
    startOfMonth: localNow()
        .startOf('month')
        .toDate(),

    endOfLastMonth: localNow()
        .subtract(1, 'month')
        .endOf('month')
        .toDate(),
    startOfLastMonth: localNow()
        .subtract(1, 'month')
        .startOf('month')
        .toDate(),

    startOfLastMonthMinus2: localNow()
        .subtract(2, 'month')
        .startOf('month')
        .toDate(),
    startOfLastMonthMinus3: localNow()
        .subtract(3, 'month')
        .startOf('month')
        .toDate(),
}

const staticRangeHandler = {
    range: {},
    isSelected(range) {
        const definedRange = this.range()
        return (
            moment(range.startDate).isSame(definedRange.startDate, 'day') &&
            moment(range.endDate).isSame(definedRange.endDate, 'day')
        )
    },
}

const createStaticRanges = ranges =>
    ranges.map(range => ({ ...staticRangeHandler, ...range }))

export const staticRanges = createStaticRanges([
    {
        label: 'This Week',
        range: () => ({
            endDate: dates.endOfWeek,
            startDate: dates.startOfWeek,
        }),
    },
    {
        label: 'Last Week',
        range: () => ({
            endDate: dates.endOfLastWeek,
            startDate: dates.startOfLastWeek,
        }),
    },
    {
        label: 'Trailing Two Weeks',
        range: () => ({
            endDate: dates.endOfLastWeek,
            startDate: dates.startOfLastWeekMinus2,
        }),
    },
    {
        label: 'Trailing Three Weeks',
        range: () => ({
            endDate: dates.endOfLastWeek,
            startDate: dates.startOfLastWeekMinus3,
        }),
    },
    {
        label: 'Trailing Four Weeks',
        range: () => ({
            endDate: dates.endOfLastWeek,
            startDate: dates.startOfLastWeekMinus4,
        }),
    },
    {
        label: 'This Month',
        range: () => ({
            endDate: dates.endOfMonth,
            startDate: dates.startOfMonth,
        }),
    },
    {
        label: 'Last Month',
        range: () => ({
            endDate: dates.endOfLastMonth,
            startDate: dates.startOfLastMonth,
        }),
    },
    {
        label: 'Trailing Two Months',
        range: () => ({
            endDate: dates.endOfLastMonth,
            startDate: dates.startOfLastMonthMinus2,
        }),
    },
    {
        label: 'Trailing Three Months',
        range: () => ({
            endDate: dates.endOfLastMonth,
            startDate: dates.startOfLastMonthMinus3,
        }),
    },
])

export const inputRanges = [
    {
        label: 'Trailing days',
        range(value) {
            return {
                endDate: dates.endOfYesterday,
                startDate: moment(
                    value ? dates.startOfYesterday : dates.endOfYesterday
                ).add((Math.max(Number(value), 1) - 1) * -1, 'days'),
            }
        },
        getCurrentValue(range) {
            if (!moment(range.endDate).isSame(dates.endOfYesterday, 'day')) {
                return '-'
            }
            if (!range.startDate) {
                return '∞'
            }
            if (moment(range.startDate).isSame(dates.endOfYesterday, 'day')) {
                return ''
            }
            return (
                moment(dates.endOfYesterday).diff(range.startDate, 'days') + 1
            )
        },
    },
]
