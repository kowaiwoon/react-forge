import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Select, InputNumber } from 'antd'
import noop from 'lodash/noop'
import identity from 'lodash/identity'
import isNull from 'lodash/isNull'
import isEmpty from 'lodash/isEmpty'
import autobind from 'autobind-decorator'

import { Popover } from 'components/Popover'
import {
    NUMBER_RANGE_OPERATOR_OPTIONS,
    NUMBER_RANGE_OPERATOR_DISPLAY,
} from 'constants/filters'

import Footer from '../Footer/Footer'
import Tag from '../Tag/Tag'
import styles from './styles.scss'

class NumberRangeControl extends Component {
    static propTypes = {
        // filter control interface props
        value: PropTypes.number,
        onApply: PropTypes.func,
        onCloseFilter: PropTypes.func,
        closable: PropTypes.bool.isRequired,

        // other props
        operator: PropTypes.string,
        operatorOptions: PropTypes.arrayOf(PropTypes.string),
        valueFormatter: PropTypes.func,
        min: PropTypes.number,
        max: PropTypes.number,
        step: PropTypes.number,
        precision: PropTypes.number,
        resourceName: PropTypes.string.isRequired,
    }

    static defaultProps = {
        onApply: noop,
        onCloseFilter: noop,
        operator: null,
        operatorOptions: [],
        value: null,
        valueFormatter: identity,
        min: -Infinity,
        max: Infinity,
        precision: 0,
        step: 1,
    }

    constructor(props) {
        super(props)

        this.state = {
            popupVisible: false,
            closed: false,
            operator: props.operator,
            value: props.value,
            error: null,
        }
    }

    componentWillUnmount() {
        // ensures event listener is removed
        document.removeEventListener('keydown', this.handleKeyDown)
    }

    @autobind
    handleReset() {
        this.setState({
            operator: null,
            value: null,
            error: null,
        })
    }

    @autobind
    handleKeyDown(event) {
        // close popover when esc key is pressed
        if (event.keyCode === 27) {
            this.setState({ popupVisible: false })
        }
    }

    @autobind
    handleClose() {
        this.setState({ closed: true }, this.props.onCloseFilter)
    }

    @autobind
    handlePopupVisibleChange(visible) {
        if (visible) {
            document.addEventListener('keydown', this.handleKeyDown)
        } else {
            document.removeEventListener('keydown', this.handleKeyDown)
        }

        this.setState({
            popupVisible: visible,
            error: null,
            operator: this.props.operator,
            value: this.props.value,
        })
    }

    @autobind
    handleOperatorChange(operator) {
        this.setState({ operator })
    }

    @autobind
    handleValueChange(value) {
        this.setState({ value })
    }

    @autobind
    handleApply() {
        const { operator, value } = this.state
        const { min, max } = this.props

        if (isNull(operator) && !isNull(value)) {
            this.setState({ error: 'Please select a valid operator.' })
        } else if (!isNull(operator) && isNull(value)) {
            this.setState({
                error: `Please enter a value between ${min} and ${max}`,
            })
        } else {
            this.props.onApply(operator, value)
            this.setState({
                popupVisible: false,
                error: null,
            })
        }
    }

    @autobind
    renderContent() {
        const {
            operatorOptions,
            resourceName,
            min,
            max,
            step,
            precision,
        } = this.props
        return (
            <div className={styles.content}>
                <div className={styles['select-container']}>
                    <Select
                        onChange={this.handleOperatorChange}
                        value={this.state.operator}
                        style={{ width: 200 }}
                    >
                        {NUMBER_RANGE_OPERATOR_OPTIONS.reduce(
                            (accumulator, option) => {
                                if (
                                    isEmpty(operatorOptions) ||
                                    operatorOptions.includes(option.value)
                                ) {
                                    accumulator.push(
                                        <Select.Option
                                            key={option.value}
                                            value={option.value}
                                        >
                                            {option.label}
                                        </Select.Option>
                                    )
                                }
                                return accumulator
                            },
                            []
                        )}
                    </Select>
                    <InputNumber
                        min={min}
                        max={max}
                        step={step}
                        precision={precision}
                        onChange={this.handleValueChange}
                        value={this.state.value}
                        formatter={this.props.valueFormatter}
                    />
                </div>
                {this.state.error && (
                    <div className={styles.error}>{this.state.error}</div>
                )}
                <Footer
                    rightOnClick={this.handleApply}
                    leftElement={`All ${resourceName}s`}
                    leftOnClick={this.handleReset}
                />
            </div>
        )
    }

    render() {
        const {
            operator,
            value,
            resourceName,
            valueFormatter,
            closable,
        } = this.props
        const { popupVisible, closed } = this.state

        return (
            <Popover
                content={this.renderContent()}
                trigger="click"
                placement="bottomLeft"
                visible={popupVisible && !closed}
                onVisibleChange={this.handlePopupVisibleChange}
                mask
            >
                <Tag
                    active={!isNull(operator) && !isNull(value)}
                    closable={closable}
                    onClose={this.handleClose}
                >
                    {!isNull(operator)
                        ? `${resourceName} ${
                              NUMBER_RANGE_OPERATOR_DISPLAY[operator]
                          } ${valueFormatter(value)}`
                        : `All ${resourceName}s`}
                </Tag>
            </Popover>
        )
    }
}

export default NumberRangeControl
