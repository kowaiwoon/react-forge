import React from 'react'
import PropTypes from 'prop-types'
import { Tag as AntTag } from 'antd'

import styles from './styles.scss'

const propTypes = {
    active: PropTypes.bool.isRequired,
    children: PropTypes.node.isRequired,
}
const defaultProps = {}

const Tag = ({ active, children, ...rest }) => (
    <div className={styles.content}>
        <AntTag color={active ? '#01c7d1' : ''} {...rest}>
            {children}
        </AntTag>
    </div>
)

Tag.propTypes = propTypes
Tag.defaultProps = defaultProps

export default Tag
