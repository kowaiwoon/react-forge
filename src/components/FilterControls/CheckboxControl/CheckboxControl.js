import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Checkbox } from 'antd'
import autobind from 'autobind-decorator'
import union from 'lodash/fp/union'
import remove from 'lodash/fp/remove'
import some from 'lodash/some'
import isEmpty from 'lodash/isEmpty'
import noop from 'lodash/noop'

import { hasAllOptionsSelected } from 'helpers/filters'
import { Popover } from 'components/Popover'

import Tag from '../Tag/Tag'
import Footer from '../Footer/Footer'
import styles from './styles.scss'

class CheckboxControl extends Component {
    static propTypes = {
        // filter control interface props
        values: PropTypes.arrayOf(
            PropTypes.shape({
                value: PropTypes.string,
                label: PropTypes.string,
            })
        ).isRequired,
        options: PropTypes.arrayOf(
            PropTypes.shape({
                value: PropTypes.string,
                label: PropTypes.string,
            })
        ).isRequired,
        onApply: PropTypes.func.isRequired,
        onCloseFilter: PropTypes.func,
        closable: PropTypes.bool.isRequired,

        // other props
        resourceName: PropTypes.string.isRequired,
        errorMsg: PropTypes.string.isRequired,
        helpTextFunc: PropTypes.func,
        disabled: PropTypes.bool,
        showSelectedFilters: PropTypes.bool,
        allowNoSelection: PropTypes.bool,
    }

    static defaultProps = {
        helpTextFunc: noop,
        onCloseFilter: noop,
        disabled: false,
        showSelectedFilters: true,
        allowNoSelection: false,
    }

    constructor(props) {
        super(props)
        this.state = {
            popupVisible: false,
            closed: false,
            selectedValues:
                !props.allowNoSelection && isEmpty(props.values)
                    ? props.options
                    : props.values,
            error: null,
        }
    }

    componentWillUnmount() {
        // ensures event listener is removed
        document.removeEventListener('keydown', this.handleKeyDown)
    }

    getButtonTitle() {
        const {
            resourceName,
            values,
            options,
            showSelectedFilters,
        } = this.props

        if (!showSelectedFilters) {
            return resourceName
        } else if (values.length === 0 || values.length === options.length) {
            return `All ${resourceName}`
        } else if (values.length === 1) {
            return values[0].label
        }
        return `${values.length} ${resourceName}`
    }

    @autobind
    handleKeyDown(event) {
        // close popover when esc key is pressed
        if (event.keyCode === 27) {
            this.setState({ popupVisible: false })
        }
    }

    @autobind
    handleApply() {
        const { allowNoSelection, errorMsg, onApply } = this.props
        if (!allowNoSelection && this.state.selectedValues.length === 0) {
            this.setState({
                error: errorMsg,
            })
        } else {
            this.setState(
                {
                    popupVisible: false,
                    error: null,
                },
                () => onApply(this.state.selectedValues)
            )
        }
    }

    @autobind
    handleCheckboxChange(option, checked) {
        if (checked) {
            this.setState({
                selectedValues: union([option], this.state.selectedValues),
                error: null,
            })
        } else {
            const removeOption = remove(i => i.value === option.value)
            this.setState({
                selectedValues: removeOption(this.state.selectedValues),
                error: null,
            })
        }
    }

    @autobind
    handleSelectAllCheckboxChange(checked) {
        const { options } = this.props
        this.setState({
            selectedValues: checked ? options : [],
            error: null,
        })
    }

    @autobind
    handleClose() {
        this.setState({ closed: true }, this.props.onCloseFilter)
    }

    @autobind
    handlePopupVisibleChange(visible) {
        if (visible) {
            document.addEventListener('keydown', this.handleKeyDown)
        } else {
            document.removeEventListener('keydown', this.handleKeyDown)
        }

        this.setState({
            popupVisible: visible,
            selectedValues:
                !this.props.allowNoSelection && isEmpty(this.props.values)
                    ? this.props.options
                    : this.props.values, // reset selectedValues on popover change
        })
    }

    renderCheckbox(option, extraProps) {
        const { selectedValues } = this.state
        return (
            <Checkbox
                key={option.value}
                className={styles.checkbox}
                onChange={event => {
                    const {
                        target: { checked },
                    } = event
                    this.handleCheckboxChange(option, checked)
                }}
                checked={some(selectedValues, option)}
                {...extraProps}
            >
                {option.label}
            </Checkbox>
        )
    }

    renderSelectAllCheckbox() {
        const { options } = this.props
        const { selectedValues } = this.state

        return (
            <Checkbox
                className={styles['all-checkbox']}
                indeterminate={
                    !isEmpty(selectedValues) &&
                    selectedValues.length < options.length
                }
                onChange={event => {
                    const {
                        target: { checked },
                    } = event
                    this.handleSelectAllCheckboxChange(checked)
                }}
                checked={hasAllOptionsSelected(selectedValues, options)}
            >
                Select All
            </Checkbox>
        )
    }

    renderHelpText(option) {
        const helpText = this.props.helpTextFunc(option)
        return helpText && <div className={styles['help-text']}>{helpText}</div>
    }

    renderError() {
        return (
            this.state.error && (
                <div className={styles.error}>{this.state.error}</div>
            )
        )
    }

    renderPopoverContent() {
        const { options, values, disabled } = this.props

        if (disabled) {
            return (
                values[0] && (
                    <div className={styles.content}>
                        {options.map(value =>
                            this.renderCheckbox(value, { disabled: true })
                        )}
                        {this.renderHelpText(values[0])}
                    </div>
                )
            )
        }

        return (
            <div className={styles.content}>
                {options.map(value => this.renderCheckbox(value))}
                {this.renderError()}
                <Footer
                    leftElement={this.renderSelectAllCheckbox()}
                    rightOnClick={this.handleApply}
                />
            </div>
        )
    }

    render() {
        const { options, values, showSelectedFilters, closable } = this.props
        const { popupVisible, closed } = this.state

        return (
            <Popover
                content={this.renderPopoverContent()}
                trigger="click"
                placement="bottomLeft"
                visible={popupVisible && !closed}
                onVisibleChange={this.handlePopupVisibleChange}
                mask
            >
                <Tag
                    active={
                        !(
                            !showSelectedFilters ||
                            isEmpty(values) ||
                            values.length === options.length
                        )
                    }
                    closable={closable}
                    onClose={this.handleClose}
                >
                    {this.getButtonTitle()}
                </Tag>
            </Popover>
        )
    }
}

export default CheckboxControl
