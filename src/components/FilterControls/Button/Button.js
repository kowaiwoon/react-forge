import React from 'react'
import PropTypes from 'prop-types'
import { Button as AntButton } from 'antd'

const propTypes = {
    active: PropTypes.bool.isRequired,
    children: PropTypes.node.isRequired,
}
const defaultProps = {}

const Button = ({ active, children, ...rest }) => (
    <AntButton
        style={{ marginRight: '10px' }}
        type={active ? 'primary' : ''}
        {...rest}
    >
        {children}
    </AntButton>
)

Button.propTypes = propTypes
Button.defaultProps = defaultProps

export default Button
