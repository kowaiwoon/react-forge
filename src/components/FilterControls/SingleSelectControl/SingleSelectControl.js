import React from 'react'
import PropTypes from 'prop-types'
import { Select } from 'antd'
import map from 'lodash/map'
import noop from 'lodash/noop'

import { metrics } from 'configuration/metrics'

const getSelectOptions = (filterType, children) => {
    if (filterType === 'metrics') {
        return map(metrics, (value, key) => (
            <Select.Option value={key} key={key}>
                {value.name}
            </Select.Option>
        ))
    } else if (filterType === 'order') {
        return [
            <Select.Option key="asc">Ascending</Select.Option>,
            <Select.Option key="desc">Descending</Select.Option>,
        ]
    } else if (filterType === 'custom') {
        return children
    }

    return null
}

const propTypes = {
    selectType: PropTypes.oneOf(['metrics', 'order', 'custom']),
    children: PropTypes.node,
    styles: PropTypes.shape(),
    disabled: PropTypes.bool,
    onChange: PropTypes.func,
}

const defaultProps = {
    children: null,
    styles: { width: '100%' },
    selectType: 'custom',
    disabled: false,
    onChange: noop,
}

const SingleSelectControl = ({
    selectType,
    children,
    styles,
    ...otherProps
}) => (
    <Select style={styles} mode="default" allowClear={false} {...otherProps}>
        {getSelectOptions(selectType, children)}
    </Select>
)

SingleSelectControl.propTypes = propTypes
SingleSelectControl.defaultProps = defaultProps

export default SingleSelectControl
