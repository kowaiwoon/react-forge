import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import noop from 'lodash/noop'
import { Divider } from 'antd'

import { TextButton } from 'components/TextButton'

import styles from './styles.scss'

const propTypes = {
    additionalContent: PropTypes.node,
    leftElement: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    leftOnClick: PropTypes.func,
    leftDisabled: PropTypes.bool,
    rightElement: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    rightOnClick: PropTypes.func,
    rightDisabled: PropTypes.bool,
    divider: PropTypes.bool,
    dividerStyle: PropTypes.shape(),
}

const defaultProps = {
    additionalContent: null,
    leftElement: null,
    leftOnClick: noop,
    leftDisabled: false,
    rightOnClick: noop,
    rightElement: 'Apply',
    rightDisabled: false,
    divider: true,
    dividerStyle: {
        margin: '12px 0',
    },
}

const Footer = ({
    additionalContent,
    leftElement,
    leftOnClick,
    leftDisabled,
    rightElement,
    rightOnClick,
    rightDisabled,
    divider,
    dividerStyle,
}) => (
    <div className={styles.container}>
        {divider && <Divider style={dividerStyle} />}
        {additionalContent}
        <div className={styles.apply}>
            {leftElement && (
                <TextButton
                    onClick={leftOnClick}
                    className={classNames(styles.button, styles.left)}
                    disabled={leftDisabled}
                    link
                >
                    {leftElement}
                </TextButton>
            )}
            <TextButton
                onClick={rightOnClick}
                className={classNames(styles.button, styles.right)}
                disabled={rightDisabled}
                link
            >
                {rightElement}
            </TextButton>
        </div>
    </div>
)

Footer.propTypes = propTypes
Footer.defaultProps = defaultProps

export default Footer
