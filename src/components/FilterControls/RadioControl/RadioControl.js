import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Radio } from 'antd'
import upperFirst from 'lodash/upperFirst'
import noop from 'lodash/noop'
import autobind from 'autobind-decorator'

import { Popover } from 'components/Popover'

import Footer from '../Footer/Footer'
import Tag from '../Tag/Tag'
import styles from './styles.scss'

class RadioControl extends Component {
    static propTypes = {
        // filter control interface props
        value: PropTypes.string.isRequired,
        options: PropTypes.arrayOf(
            PropTypes.shape({
                label: PropTypes.string,
                value: PropTypes.string,
            })
        ).isRequired,
        onApply: PropTypes.func,
        onCloseFilter: PropTypes.func,
        closable: PropTypes.bool,

        // other props
        formatValue: PropTypes.func,
    }

    static defaultProps = {
        onApply: noop,
        formatValue: value => `By ${upperFirst(value)}`,
        onCloseFilter: noop,
        closable: false,
    }

    constructor(props) {
        super(props)

        this.state = {
            popupVisible: false,
            closed: false,
            selectedValue: props.value,
        }
    }

    componentWillUnmount() {
        // ensures event listener is removed
        document.removeEventListener('keydown', this.handleKeyDown)
    }

    @autobind
    handleKeyDown(event) {
        // close popover when esc key is pressed
        if (event.keyCode === 27) {
            this.setState({ popupVisible: false })
        }
    }

    @autobind
    handleChange(event) {
        this.setState({ selectedValue: event.target.value })
    }

    @autobind
    handleClose() {
        this.setState({ closed: true }, this.props.onCloseFilter)
    }

    @autobind
    handlePopupVisibleChange(visible) {
        if (visible) {
            document.addEventListener('keydown', this.handleKeyDown)
        } else {
            document.removeEventListener('keydown', this.handleKeyDown)
        }

        this.setState({
            popupVisible: visible,
            selectedValue: this.props.value,
        })
    }

    @autobind
    handleApply() {
        this.props.onApply(this.state.selectedValue)
        this.setState({
            popupVisible: false,
        })
    }

    renderContent() {
        return (
            <div className={styles.content}>
                <Radio.Group
                    onChange={this.handleChange}
                    value={this.state.selectedValue}
                >
                    {this.props.options.map(option => (
                        <Radio
                            key={option.value}
                            style={{
                                display: 'block',
                                lineHeight: '35px',
                            }}
                            value={option.value}
                        >
                            {option.label}
                        </Radio>
                    ))}
                </Radio.Group>
                <Footer rightOnClick={this.handleApply} />
            </div>
        )
    }

    render() {
        const { formatValue, closable } = this.props
        const { popupVisible, closed } = this.state

        return (
            <Popover
                content={this.renderContent()}
                trigger="click"
                placement="bottomLeft"
                visible={popupVisible && !closed}
                onVisibleChange={this.handlePopupVisibleChange}
                mask
            >
                <Tag
                    active // radio control is away active
                    closable={closable}
                    onClose={this.handleClose}
                >
                    {formatValue(this.props.value)}
                </Tag>
            </Popover>
        )
    }
}

export default RadioControl
