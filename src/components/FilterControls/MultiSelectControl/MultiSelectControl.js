import React, { Component } from 'react'
import PropTypes from 'prop-types'
import autobind from 'autobind-decorator'
import union from 'lodash/fp/union'
import pull from 'lodash/fp/pull'
import noop from 'lodash/noop'
import isEqual from 'lodash/isEqual'

import { Popover } from 'components/Popover'
import { Typeahead } from 'components/Typeahead'

import Tag from '../Tag/Tag'

class MultiSelectControl extends Component {
    static propTypes = {
        // filter control interface props
        values: PropTypes.arrayOf(
            PropTypes.shape({
                value: PropTypes.string,
                label: PropTypes.string,
            })
        ).isRequired,
        options: PropTypes.arrayOf(
            PropTypes.shape({
                label: PropTypes.string,
                value: PropTypes.string,
            })
        ),
        onApply: PropTypes.func.isRequired,
        onCloseFilter: PropTypes.func,
        onChangeInput: PropTypes.func,
        closable: PropTypes.bool.isRequired,

        // filter control interface props
        placeholder: PropTypes.string.isRequired,
        resourceName: PropTypes.string.isRequired,
        loading: PropTypes.bool,
    }

    static defaultProps = {
        options: [],
        onChangeInput: noop,
        onCloseFilter: noop,
        loading: false,
    }

    constructor(props) {
        super(props)
        this.state = {
            popupVisible: false,
            closed: false,
            selectedOptions: props.values,
        }
    }

    @autobind
    handleKeyDown(event) {
        // close popover when esc key is pressed and downshift has initial state
        if (event.keyCode === 27) {
            this.setState({ popupVisible: false })
        }
    }

    @autobind
    addOptionToState(option) {
        const { selectedOptions } = this.state
        this.setState({
            selectedOptions: union([option], selectedOptions),
        })
    }

    @autobind
    removeOptionFromState(option) {
        const { selectedOptions } = this.state
        this.setState({
            selectedOptions: pull(option, selectedOptions),
        })
    }

    @autobind
    handleApply() {
        this.setState(
            {
                popupVisible: false,
            },
            () => this.props.onApply(this.state.selectedOptions)
        )
    }

    @autobind
    handleReset() {
        this.setState({
            selectedOptions: [],
        })
    }

    @autobind
    handleClose() {
        this.setState({ closed: true }, this.props.onCloseFilter)
    }

    @autobind
    handlePopupVisibleChange(visible) {
        this.setState({
            popupVisible: visible,
            selectedOptions: this.props.values,
        })
    }

    @autobind
    deselectOption(option) {
        this.removeOptionFromState(option)
    }

    @autobind
    toggleOptionSelection(option) {
        const { selectedOptions } = this.state
        if (selectedOptions.find(i => i.value === option.value)) {
            this.removeOptionFromState(option)
        } else {
            this.addOptionToState(option)
        }
    }

    renderButtonTitle() {
        const { values, resourceName } = this.props

        if (values.length === 0) {
            return `All ${resourceName}`
        } else if (values.length === 1) {
            return values[0].label
        }
        return `${values.length} ${resourceName}`
    }

    render() {
        const {
            values,
            placeholder,
            onChangeInput,
            loading,
            resourceName,
            options,
            closable,
        } = this.props
        const { popupVisible, selectedOptions, closed } = this.state

        return (
            <Popover
                content={
                    <Typeahead
                        placeholder={placeholder}
                        toggleOptionSelection={this.toggleOptionSelection}
                        onChangeInput={onChangeInput}
                        visible={popupVisible}
                        loading={loading}
                        selectedOptions={selectedOptions}
                        deselectOption={this.deselectOption}
                        resetLinkText={`Clear Selected ${resourceName}`}
                        handleReset={this.handleReset}
                        handleApply={this.handleApply}
                        handleKeyDown={this.handleKeyDown}
                        options={options}
                        dirty={!isEqual(values, selectedOptions)}
                    />
                }
                trigger="click"
                placement="bottomLeft"
                visible={popupVisible && !closed}
                onVisibleChange={this.handlePopupVisibleChange}
                mask
            >
                <Tag
                    active={values.length > 0}
                    closable={closable}
                    onClose={this.handleClose}
                >
                    {this.renderButtonTitle()}
                </Tag>
            </Popover>
        )
    }
}

export default MultiSelectControl
