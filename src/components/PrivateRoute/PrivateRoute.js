import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Redirect, Route, matchPath } from 'react-router-dom'
import { message } from 'antd'
import noop from 'lodash/noop'
import isEmpty from 'lodash/isEmpty'

import { getPath } from 'helpers/pages'
import { AUTH_PAGE, HOME_PAGE, PROFILE_PAGE } from 'constants/pages'

class PrivateRoute extends Component {
    static MESSAGE_DELAY_IN_SECONDS = 2

    static propTypes = {
        hasPermissions: PropTypes.func,

        // React Router
        location: PropTypes.shape({
            pathname: PropTypes.string,
            search: PropTypes.string,
        }).isRequired,
        lastLocation: PropTypes.shape({
            pathname: PropTypes.string,
            search: PropTypes.string,
        }),

        // redux state
        auth: PropTypes.shape({
            signedIn: PropTypes.bool,
            featurePermissions: PropTypes.arrayOf(PropTypes.string),
        }).isRequired,
        organizations: PropTypes.arrayOf(PropTypes.object).isRequired,
        hasIntegration: PropTypes.bool.isRequired,
    }

    static defaultProps = {
        hasPermissions: noop,
        lastLocation: null,
    }

    componentDidUpdate() {
        if (this.shouldCreateOrganization()) {
            message.info(
                'You must first create an organization before accessing that page.',
                PrivateRoute.MESSAGE_DELAY_IN_SECONDS
            )
        } else if (this.shouldCreateIntegration()) {
            message.info(
                'You must first create an integration before accessing that page.',
                PrivateRoute.MESSAGE_DELAY_IN_SECONDS
            )
        }
    }

    isProfilePage(location = this.props.location) {
        const match = matchPath(location.pathname, {
            path: getPath(PROFILE_PAGE),
        })
        return Boolean(match)
    }

    hasNoOrganizations() {
        return isEmpty(this.props.organizations)
    }

    shouldCreateOrganization(location) {
        return (
            this.hasNoOrganizations() &&
            !this.isProfilePage(location) &&
            this.props.auth.signedIn
        )
    }

    shouldCreateIntegration(location) {
        return (
            !this.props.hasIntegration &&
            !this.isProfilePage(location) &&
            this.props.auth.signedIn
        )
    }

    render() {
        const {
            hasPermissions,
            location,
            lastLocation,
            auth: { signedIn, featurePermissions },
            ...rest
        } = this.props

        // redirect to auth page if the user is not signed in
        if (!signedIn) {
            return (
                <Redirect
                    to={{
                        pathname: getPath(AUTH_PAGE),
                        state: { from: location },
                    }}
                />
            )
        }

        // redirect if the user has no organizations or integrations
        if (this.shouldCreateOrganization() || this.shouldCreateIntegration()) {
            if (lastLocation && this.isProfilePage(lastLocation)) {
                return (
                    <Redirect
                        to={{
                            ...lastLocation,
                            ...{ state: { from: location } },
                        }}
                    />
                )
            }
            return (
                <Redirect
                    to={{
                        pathname: getPath(PROFILE_PAGE),
                        state: { from: location },
                    }}
                />
            )
        }

        // redirect to the home page if the user doesn't have page permissions
        if (hasPermissions !== noop && !hasPermissions(featurePermissions)) {
            return <Redirect to={getPath(HOME_PAGE)} />
        }

        return <Route {...rest} />
    }
}

export default PrivateRoute
