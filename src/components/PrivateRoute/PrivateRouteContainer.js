import { connect } from 'react-redux'

import { selectOrganizations } from 'selectors/orgs'
import { selectHasIntegration } from 'selectors/auth'
import { withLastLocation } from 'components/HigherOrderComponents'

import PrivateRoute from './PrivateRoute'

const mapStateToProps = state => ({
    auth: state.auth,
    organizations: selectOrganizations(state),
    hasIntegration: selectHasIntegration(state),
})

const PrivateRouteContainer = connect(mapStateToProps)(PrivateRoute)

export default withLastLocation(PrivateRouteContainer)
