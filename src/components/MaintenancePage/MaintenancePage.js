import React from 'react'

import { TextButton } from 'components/TextButton'
import logoIcon from 'images/logo-icon.svg'

import styles from './styles.scss'

const showDriftSidebar = e => {
    e.preventDefault()
    drift.on('ready', api => {
        api.sidebar.open()
    })
}

const propTypes = {}
const defaultProps = {}

const MaintenancePage = () => (
    <div className={styles.background}>
        <div className={styles.container}>
            <div className={styles.logo}>
                <img alt="logo" src={logoIcon} />
                <span>Downstream</span>
            </div>
            <p>
                We are currently offline for planned maintenance. If you have
                questions or concerns, please let us know{' '}
                <TextButton link onClick={showDriftSidebar}>
                    over chat
                </TextButton>
                .
            </p>
            <p>Thanks for your patience, Downstream Engineering Team.</p>
        </div>
    </div>
)

MaintenancePage.propTypes = propTypes
MaintenancePage.defaultProps = defaultProps

export default MaintenancePage
