import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ReactGA from 'react-ga'

/**
 * A Higher-Order Component that wraps React components
 * with a Google Analytics tracker
 *
 * @see https://github.com/react-ga/react-ga/wiki/React-Router-v4-withTracker
 *
 * @param WrappedComponent the component to wrap
 * @param options
 *
 * @returns wrapped component that will track page changes
 */
const withGoogleTracker = (WrappedComponent, options = {}) => {
    const trackPage = page => {
        ReactGA.set({
            page,
            ...options,
        })
        ReactGA.pageview(page)
    }

    return class extends Component {
        static propTypes = {
            location: PropTypes.shape({
                pathname: PropTypes.string,
            }).isRequired,
        }

        componentDidMount() {
            const page = this.props.location.pathname
            trackPage(page)
        }

        componentWillReceiveProps(nextProps) {
            const currentPage = this.props.location.pathname
            const nextPage = nextProps.location.pathname

            if (currentPage !== nextPage) {
                trackPage(nextPage)
            }
        }

        render() {
            return <WrappedComponent {...this.props} />
        }
    }
}

export default withGoogleTracker
