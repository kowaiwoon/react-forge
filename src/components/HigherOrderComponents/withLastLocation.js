import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom'

const withLastLocation = WrappedComponent => {
    class LastLocationProvider extends Component {
        static propTypes = {
            location: PropTypes.shape({
                pathname: PropTypes.string,
                search: PropTypes.string,
                hash: PropTypes.string,
                key: PropTypes.string,
                state: PropTypes.object,
            }).isRequired,
        }

        state = {
            lastLocation: null,
        }

        UNSAFE_componentWillReceiveProps(nextProps) {
            if (this.props.location === nextProps.location) {
                return
            }

            this.setState({
                lastLocation: this.props.location,
            })
        }

        render() {
            return (
                <WrappedComponent
                    lastLocation={this.state.lastLocation}
                    {...this.props}
                />
            )
        }
    }

    return withRouter(LastLocationProvider)
}

export default withLastLocation
