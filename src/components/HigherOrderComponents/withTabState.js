import React, { Component } from 'react'
import PropTypes from 'prop-types'
import queryString from 'query-string'
import has from 'lodash/has'
import autobind from 'autobind-decorator'

const withTabState = (WrappedComponent, defaultTab) =>
    class extends Component {
        static propTypes = {
            location: PropTypes.shape({
                search: PropTypes.string,
            }).isRequired,
            history: PropTypes.shape({
                push: PropTypes.func,
            }).isRequired,
        }

        @autobind
        handleTabChange(tab) {
            const { location, history } = this.props

            const params = queryString.parse(location.search)
            params.tab = tab
            history.push({
                search: `?${queryString.stringify(params)}`,
            })
        }

        render() {
            const { location } = this.props
            const params = queryString.parse(location.search)
            const tab = has(params, 'tab') ? params.tab : defaultTab

            return (
                <WrappedComponent
                    tab={tab}
                    handleTabChange={this.handleTabChange}
                    {...this.props}
                />
            )
        }
    }

export default withTabState
