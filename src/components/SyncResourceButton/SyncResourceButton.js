import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Button, Tooltip } from 'antd'

import moment from 'utilities/moment'
import message from 'utilities/message'

class SyncResourceButton extends Component {
    static propTypes = {
        onClick: PropTypes.func.isRequired,
        syncing: PropTypes.bool.isRequired,
        syncedDate: PropTypes.string.isRequired,
    }

    state = {
        justSynced: false,
    }

    componentDidUpdate(prevProps) {
        // set justSynced when syncing is complete
        if (prevProps.syncing && !this.props.syncing) {
            this.setJustSynced(true)
        } else if (this.state.justSynced) {
            // reset justSynced with timeout
            setTimeout(() => this.setJustSynced(false), 1000)
        }
    }

    setJustSynced(justSynced) {
        if (justSynced) {
            this.setState({ justSynced })
        } else {
            this.setState({ justSynced }, () =>
                message.success(
                    'We are syncing your Amazon Advertising data. Please refresh the page to see the latest.'
                )
            )
        }
    }

    render() {
        const { onClick, syncing, syncedDate } = this.props
        const { justSynced } = this.state
        return (
            <Tooltip
                title={`Sync Amazon Advertising data. Last synced on ${moment(
                    syncedDate
                )
                    .local()
                    .format('L @ LT')}`}
            >
                <Button
                    shape="circle"
                    size="small"
                    onClick={onClick}
                    icon={justSynced ? 'check' : 'sync'}
                    loading={syncing}
                />
            </Tooltip>
        )
    }
}

export default SyncResourceButton
