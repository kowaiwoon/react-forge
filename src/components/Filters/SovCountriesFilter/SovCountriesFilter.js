import React from 'react'
import PropTypes from 'prop-types'
import noop from 'lodash/noop'

import { CheckboxControl } from 'components/FilterControls'
import { SOV_COUNTRIES } from 'constants/filters'
import { hasAllOptionsSelected } from 'helpers/filters'
import { getSovCountryOptions } from 'helpers/codes'

const type = SOV_COUNTRIES

const title = 'Countries'

const allOptions = getSovCountryOptions()

const propTypes = {
    values: PropTypes.arrayOf(
        PropTypes.shape({
            label: PropTypes.string,
            value: PropTypes.string,
        })
    ).isRequired,
    options: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.string,
            label: PropTypes.string,
        })
    ),
    onApply: PropTypes.func.isRequired,
    onCloseFilter: PropTypes.func,
    closable: PropTypes.bool.isRequired,
}

const defaultProps = {
    onCloseFilter: noop,
    options: allOptions,
}

const SovCountriesFilter = ({
    values,
    onApply,
    onCloseFilter,
    options,
    closable,
}) => (
    <CheckboxControl
        values={values}
        options={options}
        onApply={selectedValues =>
            onApply(
                SOV_COUNTRIES,
                hasAllOptionsSelected(selectedValues, allOptions)
                    ? []
                    : selectedValues
            )
        }
        onCloseFilter={() => onCloseFilter(SOV_COUNTRIES)}
        closable={closable}
        resourceName="Countries"
        errorMsg="Must select a Country"
    />
)

SovCountriesFilter.propTypes = propTypes
SovCountriesFilter.defaultProps = defaultProps
SovCountriesFilter.type = type
SovCountriesFilter.title = title

export default SovCountriesFilter
