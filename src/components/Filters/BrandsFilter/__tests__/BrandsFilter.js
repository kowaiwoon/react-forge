import React from 'react'
import { render, mount } from 'enzyme'
import noop from 'lodash/noop'

import BrandsFilter from '../BrandsFilter'

describe('[Filters] BrandsFilterContainer', () => {
    it('should mount with no options', () => {
        expect(
            mount(
                <BrandsFilter
                    values={[]}
                    onApply={noop}
                    closable
                    onChangeFilter={noop}
                    onCloseFilter={noop}
                    options={[]}
                    loading={false}
                    onChangeInput={noop}
                />
            ).props().onCloseFilter
        ).toEqual(noop)
    })

    it('should render with no options', () => {
        expect(
            render(
                <BrandsFilter
                    values={[]}
                    onApply={noop}
                    closable
                    onChangeFilter={noop}
                    onCloseFilter={noop}
                    options={[]}
                    loading={false}
                    onChangeInput={noop}
                />
            ).text()
        ).toEqual('All Brands')
    })
})
