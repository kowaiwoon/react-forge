import React from 'react'
import PropTypes from 'prop-types'
import noop from 'lodash/noop'

import { MultiSelectControl } from 'components/FilterControls'
import { BRANDS } from 'constants/filters'

const type = BRANDS

const title = 'Brands'

const propTypes = {
    values: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.string,
            label: PropTypes.string,
        })
    ).isRequired,
    options: PropTypes.arrayOf(PropTypes.shape()),
    onApply: PropTypes.func.isRequired,
    onCloseFilter: PropTypes.func,
    closable: PropTypes.bool.isRequired,

    onChangeInput: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired,
}

const defaultProps = {
    onCloseFilter: noop,
    options: [],
}

const BrandsFilter = ({
    onApply,
    onCloseFilter,
    values,
    options,
    onChangeInput,
    loading,
    closable,
}) => (
    <MultiSelectControl
        values={values}
        options={options}
        onApply={selectedValues => onApply(BRANDS, selectedValues)}
        onCloseFilter={() => onCloseFilter(BRANDS)}
        closable={closable}
        onChangeInput={onChangeInput}
        placeholder="Search Brands..."
        resourceName="Brands"
        loading={loading}
    />
)

BrandsFilter.propTypes = propTypes
BrandsFilter.defaultProps = defaultProps
BrandsFilter.type = type
BrandsFilter.title = title

export default BrandsFilter
