import React from 'react'
import PropTypes from 'prop-types'
import noop from 'lodash/noop'

import moment from 'utilities/moment'
import { DATES } from 'constants/filters'
import { DateRangeControl } from 'components/FilterControls'

const type = DATES

const title = 'Date Range'

const propTypes = {
    values: PropTypes.arrayOf(PropTypes.instanceOf(moment)).isRequired,
    onApply: PropTypes.func.isRequired,
    onCloseFilter: PropTypes.func,
    closable: PropTypes.bool.isRequired,
}

const defaultProps = {
    onCloseFilter: noop,
}

const DateRangeFilter = ({ onApply, values, onCloseFilter, closable }) => (
    <DateRangeControl
        values={values}
        onApply={dates => onApply(DATES, dates)}
        onCloseFilter={() => onCloseFilter(DATES)}
        closable={closable}
    />
)

DateRangeFilter.propTypes = propTypes
DateRangeFilter.defaultProps = defaultProps
DateRangeFilter.type = type
DateRangeFilter.title = title

export default DateRangeFilter
