import React from 'react'
import PropTypes from 'prop-types'
import noop from 'lodash/noop'

import { MultiSelectControl } from 'components/FilterControls'
import { PRODUCT_ASINS } from 'constants/filters'

const type = PRODUCT_ASINS

const title = 'Product ASINs'

const propTypes = {
    values: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.string,
            label: PropTypes.string,
        })
    ).isRequired,
    options: PropTypes.arrayOf(PropTypes.shape()),
    onApply: PropTypes.func.isRequired,
    onCloseFilter: PropTypes.func,
    closable: PropTypes.bool.isRequired,
    onChangeInput: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired,
}

const defaultProps = {
    onCloseFilter: noop,
    options: [],
}

const ProductAsinsFilter = ({
    onApply,
    onCloseFilter,
    values,
    options,
    closable,
    onChangeInput,
    loading,
}) => (
    <MultiSelectControl
        values={values}
        options={options}
        onApply={selectedValues => onApply(PRODUCT_ASINS, selectedValues)}
        onCloseFilter={() => onCloseFilter(PRODUCT_ASINS)}
        closable={closable}
        onChangeInput={onChangeInput}
        placeholder="Search Products by ASIN..."
        resourceName="Product ASINs"
        loading={loading}
    />
)

ProductAsinsFilter.propTypes = propTypes
ProductAsinsFilter.defaultProps = defaultProps
ProductAsinsFilter.type = type
ProductAsinsFilter.title = title

export default ProductAsinsFilter
