import React from 'react'
import PropTypes from 'prop-types'
import noop from 'lodash/noop'

import { CheckboxControl } from 'components/FilterControls'
import { REGIONS } from 'constants/filters'
import { hasAllOptionsSelected } from 'helpers/filters'
import { getAmsRegionOptions } from 'helpers/codes'

const type = REGIONS

const title = 'Regions'

const allOptions = getAmsRegionOptions()

const propTypes = {
    values: PropTypes.arrayOf(
        PropTypes.shape({
            label: PropTypes.string,
            value: PropTypes.string,
        })
    ).isRequired,
    options: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.string,
            label: PropTypes.string,
        })
    ),
    onApply: PropTypes.func.isRequired,
    onCloseFilter: PropTypes.func,
    closable: PropTypes.bool.isRequired,
}

const defaultProps = {
    onCloseFilter: noop,
    options: allOptions,
}

const RegionsFilter = ({
    values,
    onApply,
    onCloseFilter,
    options,
    closable,
}) => (
    <CheckboxControl
        values={values}
        options={options}
        onApply={selectedValues =>
            onApply(
                REGIONS,
                hasAllOptionsSelected(selectedValues, allOptions)
                    ? []
                    : selectedValues
            )
        }
        onCloseFilter={() => onCloseFilter(REGIONS)}
        closable={closable}
        resourceName="Regions"
        errorMsg="Must select a Region"
    />
)

RegionsFilter.propTypes = propTypes
RegionsFilter.defaultProps = defaultProps
RegionsFilter.type = type
RegionsFilter.title = title

export default RegionsFilter
