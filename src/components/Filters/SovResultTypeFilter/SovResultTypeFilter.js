import React from 'react'
import PropTypes from 'prop-types'
import noop from 'lodash/noop'

import { getResultTypeObject } from 'helpers/resultTypes'
import { HEADLINE_SECTION, SPONSORED, ORGANIC } from 'constants/resultTypes'
import { SOV_RESULT_TYPES } from 'constants/filters'
import { CheckboxControl } from 'components/FilterControls'

const type = SOV_RESULT_TYPES

const title = 'Result Type'

const allOptions = [
    getResultTypeObject(HEADLINE_SECTION),
    getResultTypeObject(SPONSORED),
    getResultTypeObject(ORGANIC),
]

const propTypes = {
    values: PropTypes.arrayOf(
        PropTypes.shape({
            label: PropTypes.string,
            value: PropTypes.string,
        })
    ).isRequired,
    options: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.string,
            label: PropTypes.string,
        })
    ),
    onApply: PropTypes.func.isRequired,
    onCloseFilter: PropTypes.func,
    closable: PropTypes.bool.isRequired,
}

const defaultProps = {
    onCloseFilter: noop,
    options: allOptions,
}

const SovResultTypeFilter = ({
    values,
    onApply,
    onCloseFilter,
    options,
    closable,
}) => (
    <CheckboxControl
        values={values}
        options={options}
        onApply={selectedValues => onApply(SOV_RESULT_TYPES, selectedValues)}
        onCloseFilter={() => onCloseFilter(SOV_RESULT_TYPES)}
        closable={closable}
        resourceName="Result Types"
        errorMsg="Must select a Result Type"
    />
)

SovResultTypeFilter.propTypes = propTypes
SovResultTypeFilter.defaultProps = defaultProps
SovResultTypeFilter.type = type
SovResultTypeFilter.title = title

export default SovResultTypeFilter
