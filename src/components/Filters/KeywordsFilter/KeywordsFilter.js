import React from 'react'
import PropTypes from 'prop-types'
import noop from 'lodash/noop'

import { MultiSelectControl } from 'components/FilterControls'
import { KEYWORDS } from 'constants/filters'

const type = KEYWORDS

const title = 'Keywords'

const propTypes = {
    values: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.string,
            label: PropTypes.string,
        })
    ).isRequired,
    options: PropTypes.arrayOf(PropTypes.shape()),
    onApply: PropTypes.func.isRequired,
    onCloseFilter: PropTypes.func,
    closable: PropTypes.bool.isRequired,

    onChangeInput: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired,
}

const defaultProps = {
    onCloseFilter: noop,
    options: [],
}

const KeywordsFilter = ({
    onApply,
    onCloseFilter,
    values,
    options,
    closable,
    onChangeInput,
    loading,
}) => (
    <MultiSelectControl
        values={values}
        options={options}
        onApply={selectedValues => onApply(KEYWORDS, selectedValues)}
        onCloseFilter={() => onCloseFilter(KEYWORDS)}
        closable={closable}
        onChangeInput={onChangeInput}
        placeholder="Search Keywords..."
        resourceName="Keywords"
        loading={loading}
    />
)

KeywordsFilter.propTypes = propTypes
KeywordsFilter.defaultProps = defaultProps
KeywordsFilter.type = type
KeywordsFilter.title = title

export default KeywordsFilter
