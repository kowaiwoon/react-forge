import React from 'react'
import PropTypes from 'prop-types'
import noop from 'lodash/noop'

import { getFactTypeObject } from 'helpers/factTypes'
import { SPONSORED_PRODUCT, HEADLINE_SEARCH } from 'constants/factTypes'
import { FACT_TYPES } from 'constants/filters'
import { CheckboxControl } from 'components/FilterControls'

const type = FACT_TYPES

const title = 'Ad Types'

const propTypes = {
    values: PropTypes.arrayOf(
        PropTypes.shape({
            label: PropTypes.string,
            value: PropTypes.string,
        })
    ).isRequired,
    options: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.string,
            label: PropTypes.string,
        })
    ),
    onApply: PropTypes.func.isRequired,
    onCloseFilter: PropTypes.func,
    closable: PropTypes.bool.isRequired,

    helpTextFunc: PropTypes.func,
    disabled: PropTypes.bool,
}

const defaultProps = {
    onCloseFilter: noop,
    helpTextFunc: noop,
    disabled: false,
    options: [
        getFactTypeObject(SPONSORED_PRODUCT),
        getFactTypeObject(HEADLINE_SEARCH),
    ],
}

const AdTypeFilter = ({
    values,
    onApply,
    helpTextFunc,
    disabled,
    options,
    onCloseFilter,
    closable,
}) => (
    <CheckboxControl
        values={values}
        options={options}
        onApply={selectedValue => onApply(FACT_TYPES, selectedValue)}
        onCloseFilter={() => onCloseFilter(FACT_TYPES)}
        closable={closable}
        resourceName="Ad Types"
        errorMsg="Must select an Ad Type"
        helpTextFunc={helpTextFunc}
        disabled={disabled}
    />
)

AdTypeFilter.propTypes = propTypes
AdTypeFilter.defaultProps = defaultProps
AdTypeFilter.type = type
AdTypeFilter.title = title

export default AdTypeFilter
