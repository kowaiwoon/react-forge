import React from 'react'
import PropTypes from 'prop-types'
import noop from 'lodash/noop'

import { CheckboxControl } from 'components/FilterControls'
import { SOV_STATES } from 'constants/filters'
import { hasAllOptionsSelected } from 'helpers/filters'
import { ARCHIVED, ENABLED, PAUSED } from 'constants/resourceStates'

const type = SOV_STATES

const title = 'Keyword State'

const allOptions = [
    { value: ENABLED, label: 'Enabled' },
    { value: PAUSED, label: 'Paused' },
    { value: ARCHIVED, label: 'Archived' },
]

const propTypes = {
    values: PropTypes.arrayOf(
        PropTypes.shape({
            label: PropTypes.string,
            value: PropTypes.string,
        })
    ).isRequired,
    options: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.string,
            label: PropTypes.string,
        })
    ),
    onApply: PropTypes.func.isRequired,
    onCloseFilter: PropTypes.func,
    closable: PropTypes.bool.isRequired,
}

const defaultProps = {
    onCloseFilter: noop,
    options: allOptions,
}

const SovStatesFilter = ({
    values,
    onApply,
    onCloseFilter,
    options,
    closable,
}) => (
    <CheckboxControl
        values={values}
        options={options}
        onApply={selectedValues =>
            onApply(
                SOV_STATES,
                hasAllOptionsSelected(selectedValues, options)
                    ? []
                    : selectedValues
            )
        }
        onCloseFilter={() => onCloseFilter(SOV_STATES)}
        closable={closable}
        resourceName="States"
        errorMsg="Must select a State"
    />
)

SovStatesFilter.propTypes = propTypes
SovStatesFilter.defaultProps = defaultProps
SovStatesFilter.type = type
SovStatesFilter.title = title

export default SovStatesFilter
