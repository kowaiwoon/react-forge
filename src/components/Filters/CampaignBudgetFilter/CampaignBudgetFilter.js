import React from 'react'
import PropTypes from 'prop-types'
import noop from 'lodash/noop'

import { NumberRangeControl } from 'components/FilterControls'
import { CAMPAIGN_BUDGET } from 'constants/filters'
import {
    CAMPAIGN_BUDGET_MIN,
    CAMPAIGN_BUDGET_MAX,
    CAMPAIGN_BUDGET_STEP,
    CAMPAIGN_BUDGET_PRECISION,
} from 'constants/campaigns'

const type = CAMPAIGN_BUDGET

const title = 'Campaign Budget'

const propTypes = {
    value: PropTypes.number,
    onApply: PropTypes.func.isRequired,
    onCloseFilter: PropTypes.func,
    closable: PropTypes.bool.isRequired,

    operator: PropTypes.string,
    valueFormatter: PropTypes.func.isRequired,
}

const defaultProps = {
    operator: null,
    value: null,
    onCloseFilter: noop,
}

const CampaignBudgetFilter = ({
    valueFormatter,
    onApply,
    onCloseFilter,
    operator,
    value,
    closable,
}) => (
    <NumberRangeControl
        operator={operator}
        value={value}
        resourceName="Campaign Budget"
        onApply={(op, val) =>
            onApply(CAMPAIGN_BUDGET, { operator: op, value: val })
        }
        onCloseFilter={() => onCloseFilter(CAMPAIGN_BUDGET)}
        closable={closable}
        valueFormatter={valueFormatter}
        min={CAMPAIGN_BUDGET_MIN}
        max={CAMPAIGN_BUDGET_MAX}
        step={CAMPAIGN_BUDGET_STEP}
        precision={CAMPAIGN_BUDGET_PRECISION}
    />
)

CampaignBudgetFilter.propTypes = propTypes
CampaignBudgetFilter.defaultProps = defaultProps
CampaignBudgetFilter.type = type
CampaignBudgetFilter.title = title

export default CampaignBudgetFilter
