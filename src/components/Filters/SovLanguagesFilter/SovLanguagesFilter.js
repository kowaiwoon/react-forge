import React from 'react'
import PropTypes from 'prop-types'
import noop from 'lodash/noop'

import { CheckboxControl } from 'components/FilterControls'
import { SOV_LANGUAGES } from 'constants/filters'
import { hasAllOptionsSelected } from 'helpers/filters'
import { LANGUAGE_CODES } from 'constants/codes'

const type = SOV_LANGUAGES

const title = 'Language'

const allOptions = Object.keys(LANGUAGE_CODES).map(code => ({
    value: code,
    label: LANGUAGE_CODES[code],
}))

const propTypes = {
    values: PropTypes.arrayOf(
        PropTypes.shape({
            label: PropTypes.string,
            value: PropTypes.string,
        })
    ).isRequired,
    options: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.string,
            label: PropTypes.string,
        })
    ),
    onApply: PropTypes.func.isRequired,
    onCloseFilter: PropTypes.func,
    closable: PropTypes.bool.isRequired,
}

const defaultProps = {
    onCloseFilter: noop,
    options: allOptions,
}

const SovLanguagesFilter = ({
    values,
    onApply,
    onCloseFilter,
    options,
    closable,
}) => (
    <CheckboxControl
        values={values}
        options={options}
        onApply={selectedValues =>
            onApply(
                SOV_LANGUAGES,
                hasAllOptionsSelected(selectedValues, options)
                    ? []
                    : selectedValues
            )
        }
        onCloseFilter={() => onCloseFilter(SOV_LANGUAGES)}
        closable={closable}
        resourceName="Languages"
        errorMsg="Must select a Language"
    />
)

SovLanguagesFilter.propTypes = propTypes
SovLanguagesFilter.defaultProps = defaultProps
SovLanguagesFilter.type = type
SovLanguagesFilter.title = title

export default SovLanguagesFilter
