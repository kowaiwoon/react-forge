import React from 'react'
import PropTypes from 'prop-types'
import noop from 'lodash/noop'

import { AGGREGATION } from 'constants/filters'
import { RadioControl } from 'components/FilterControls'

const type = AGGREGATION

const title = 'Date Aggregation'

const propTypes = {
    value: PropTypes.string.isRequired,
    options: PropTypes.array,
    onApply: PropTypes.func.isRequired,
    onCloseFilter: PropTypes.func,
    closable: PropTypes.bool.isRequired,
}

const defaultProps = {
    onCloseFilter: noop,
    options: [
        { value: 'day', label: 'Aggregate by Day' },
        { value: 'week', label: 'Aggregate by Week' },
        { value: 'month', label: 'Aggregate by Month' },
    ],
}

const AggregationFilter = ({
    value,
    onApply,
    options,
    onCloseFilter,
    closable,
}) => (
    <RadioControl
        value={value}
        options={options}
        onApply={selectedValue => onApply(AGGREGATION, selectedValue)}
        onCloseFilter={() => onCloseFilter(AGGREGATION)}
        closable={closable}
    />
)

AggregationFilter.propTypes = propTypes
AggregationFilter.defaultProps = defaultProps
AggregationFilter.type = type
AggregationFilter.title = title

export default AggregationFilter
