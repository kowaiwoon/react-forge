import React from 'react'
import PropTypes from 'prop-types'
import noop from 'lodash/noop'

import moment from 'utilities/moment'
import { DateRangeControl } from 'components/FilterControls'
import { SOV_SEARCH_TIMES } from 'constants/filters'

const type = SOV_SEARCH_TIMES

const title = 'Search Date Range'

const propTypes = {
    values: PropTypes.arrayOf(PropTypes.instanceOf(moment)).isRequired,
    onApply: PropTypes.func.isRequired,
    onCloseFilter: PropTypes.func,
    closable: PropTypes.bool.isRequired,
}

const defaultProps = {
    onCloseFilter: noop,
}

const SearchTimeFilter = ({ onApply, values, onCloseFilter, closable }) => (
    <DateRangeControl
        values={values}
        onApply={dates => onApply(SOV_SEARCH_TIMES, dates)}
        onCloseFilter={() => onCloseFilter(SOV_SEARCH_TIMES)}
        closable={closable}
    />
)

SearchTimeFilter.propTypes = propTypes
SearchTimeFilter.defaultProps = defaultProps
SearchTimeFilter.type = type
SearchTimeFilter.title = title

export default SearchTimeFilter
