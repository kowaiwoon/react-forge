import React from 'react'
import PropTypes from 'prop-types'
import noop from 'lodash/noop'

import { MultiSelectControl } from 'components/FilterControls'
import { SOV_KEYWORD_CATEGORIES } from 'constants/filters'

const type = SOV_KEYWORD_CATEGORIES

const title = 'Keyword Categories'

const propTypes = {
    values: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.string,
            label: PropTypes.string,
        })
    ).isRequired,
    options: PropTypes.arrayOf(PropTypes.shape()),
    onApply: PropTypes.func.isRequired,
    onCloseFilter: PropTypes.func,
    closable: PropTypes.bool.isRequired,
    onChangeInput: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired,
}

const defaultProps = {
    options: [],
    onCloseFilter: noop,
}

const SovKeywordCategoriesFilter = ({
    onApply,
    values,
    options,
    onChangeInput,
    onCloseFilter,
    closable,
    loading,
}) => (
    <MultiSelectControl
        values={values}
        options={options}
        onApply={selectedValues =>
            onApply(SOV_KEYWORD_CATEGORIES, selectedValues)
        }
        onChangeInput={onChangeInput}
        onCloseFilter={() => onCloseFilter(SOV_KEYWORD_CATEGORIES)}
        closable={closable}
        placeholder="Search Keyword Categories..."
        resourceName="Categories"
        loading={loading}
    />
)

SovKeywordCategoriesFilter.propTypes = propTypes
SovKeywordCategoriesFilter.defaultProps = defaultProps
SovKeywordCategoriesFilter.type = type
SovKeywordCategoriesFilter.title = title

export default SovKeywordCategoriesFilter
