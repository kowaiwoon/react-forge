import React from 'react'
import PropTypes from 'prop-types'
import noop from 'lodash/noop'

import { CheckboxControl } from 'components/FilterControls'
import { CAMPAIGN_STATES } from 'constants/filters'
import { hasAllOptionsSelected } from 'helpers/filters'
import { ARCHIVED, ENABLED, PAUSED } from 'constants/resourceStates'

const type = CAMPAIGN_STATES

const title = 'Campaign State'

const propTypes = {
    values: PropTypes.arrayOf(
        PropTypes.shape({
            label: PropTypes.string,
            value: PropTypes.string,
        })
    ).isRequired,
    options: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.string,
            label: PropTypes.string,
        })
    ),
    onApply: PropTypes.func.isRequired,
    onCloseFilter: PropTypes.func,
    closable: PropTypes.bool.isRequired,
}

const defaultProps = {
    onCloseFilter: noop,
    options: [
        { value: ENABLED, label: 'Enabled' },
        { value: PAUSED, label: 'Paused' },
        { value: ARCHIVED, label: 'Archived' },
    ],
}

const CampaignStatesFilter = ({
    values,
    onApply,
    onCloseFilter,
    options,
    closable,
}) => (
    <CheckboxControl
        values={values}
        options={options}
        onApply={selectedValues =>
            onApply(
                CAMPAIGN_STATES,
                hasAllOptionsSelected(selectedValues, options)
                    ? []
                    : selectedValues
            )
        }
        onCloseFilter={() => onCloseFilter(CAMPAIGN_STATES)}
        closable={closable}
        resourceName="Campaign States"
        errorMsg="Must select a Campaign State"
    />
)

CampaignStatesFilter.propTypes = propTypes
CampaignStatesFilter.defaultProps = defaultProps
CampaignStatesFilter.type = type
CampaignStatesFilter.title = title

export default CampaignStatesFilter
