import React from 'react'
import PropTypes from 'prop-types'
import noop from 'lodash/noop'

import { CheckboxControl } from 'components/FilterControls'
import { KEYWORD_MATCH_TYPES } from 'constants/filters'
import { hasAllOptionsSelected } from 'helpers/filters'
import { EXACT, BROAD, PHRASE } from 'constants/matchTypes'

const type = KEYWORD_MATCH_TYPES

const title = 'Keyword Match Type'

const propTypes = {
    values: PropTypes.arrayOf(
        PropTypes.shape({
            label: PropTypes.string,
            value: PropTypes.string,
        })
    ).isRequired,
    options: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.string,
            label: PropTypes.string,
        })
    ),
    onApply: PropTypes.func.isRequired,
    onCloseFilter: PropTypes.func,
    closable: PropTypes.bool.isRequired,
}

const defaultProps = {
    onCloseFilter: noop,
    options: [
        { value: EXACT, label: 'Exact' },
        { value: BROAD, label: 'Broad' },
        { value: PHRASE, label: 'Phrase' },
    ],
}

const KeywordMatchTypesFilter = ({
    values,
    onApply,
    onCloseFilter,
    options,
    closable,
}) => (
    <CheckboxControl
        values={values}
        options={options}
        onApply={selectedValues =>
            onApply(
                KEYWORD_MATCH_TYPES,
                hasAllOptionsSelected(selectedValues, options)
                    ? []
                    : selectedValues
            )
        }
        onCloseFilter={() => onCloseFilter(KEYWORD_MATCH_TYPES)}
        closable={closable}
        resourceName="Keyword Match Types"
        errorMsg="Must select a Keyword Match Type"
    />
)

KeywordMatchTypesFilter.propTypes = propTypes
KeywordMatchTypesFilter.defaultProps = defaultProps
KeywordMatchTypesFilter.type = type
KeywordMatchTypesFilter.title = title

export default KeywordMatchTypesFilter
