import React from 'react'
import PropTypes from 'prop-types'
import noop from 'lodash/noop'

import { CheckboxControl } from 'components/FilterControls'
import { CAMPAIGN_TARGETING_TYPES } from 'constants/filters'
import { hasAllOptionsSelected } from 'helpers/filters'
import { MANUAL, AUTO } from 'constants/targetingTypes'

const type = CAMPAIGN_TARGETING_TYPES

const title = 'Campaign Targeting Type'

const propTypes = {
    values: PropTypes.arrayOf(
        PropTypes.shape({
            label: PropTypes.string,
            value: PropTypes.string,
        })
    ).isRequired,
    options: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.string,
            label: PropTypes.string,
        })
    ),
    onApply: PropTypes.func.isRequired,
    onCloseFilter: PropTypes.func,
    closable: PropTypes.bool.isRequired,
}

const defaultProps = {
    onCloseFilter: noop,
    options: [
        { value: MANUAL, label: 'Manual' },
        { value: AUTO, label: 'Auto' },
    ],
}

const CampaignTargetingTypesFilter = ({
    values,
    onApply,
    onCloseFilter,
    options,
    closable,
}) => (
    <CheckboxControl
        values={values}
        options={options}
        onApply={selectedValues =>
            onApply(
                CAMPAIGN_TARGETING_TYPES,
                hasAllOptionsSelected(selectedValues, options)
                    ? []
                    : selectedValues
            )
        }
        onCloseFilter={() => onCloseFilter(CAMPAIGN_TARGETING_TYPES)}
        closable={closable}
        resourceName="Campaign Targeting Types"
        errorMsg="Must select a Campaign Targeting Type"
    />
)

CampaignTargetingTypesFilter.propTypes = propTypes
CampaignTargetingTypesFilter.defaultProps = defaultProps
CampaignTargetingTypesFilter.type = type
CampaignTargetingTypesFilter.title = title

export default CampaignTargetingTypesFilter
