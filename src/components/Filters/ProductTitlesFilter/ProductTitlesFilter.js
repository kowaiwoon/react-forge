import React from 'react'
import PropTypes from 'prop-types'
import noop from 'lodash/noop'

import { MultiSelectControl } from 'components/FilterControls'
import { PRODUCT_TITLES } from 'constants/filters'

const type = PRODUCT_TITLES

const title = 'Product Title'

const propTypes = {
    values: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.string,
            label: PropTypes.string,
        })
    ).isRequired,
    options: PropTypes.arrayOf(PropTypes.shape()),
    onApply: PropTypes.func.isRequired,
    onCloseFilter: PropTypes.func,
    closable: PropTypes.bool.isRequired,
    onChangeInput: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired,
}

const defaultProps = {
    onCloseFilter: noop,
    options: [],
}

const ProductTitlesFilter = ({
    onApply,
    onCloseFilter,
    values,
    options,
    closable,
    onChangeInput,
    loading,
}) => (
    <MultiSelectControl
        values={values}
        options={options}
        onApply={selectedValues => onApply(PRODUCT_TITLES, selectedValues)}
        onCloseFilter={() => onCloseFilter(PRODUCT_TITLES)}
        closable={closable}
        onChangeInput={onChangeInput}
        placeholder="Search Products by Title..."
        resourceName="Product Titles"
        loading={loading}
    />
)

ProductTitlesFilter.propTypes = propTypes
ProductTitlesFilter.defaultProps = defaultProps
ProductTitlesFilter.type = type
ProductTitlesFilter.title = title

export default ProductTitlesFilter
