import React from 'react'
import PropTypes from 'prop-types'
import noop from 'lodash/noop'

import { RadioControl } from 'components/FilterControls'
import { SOV_AGGREGATION_UNIT } from 'constants/reducerKeys'
import { SOV_AGGREGATION } from 'constants/filters'

const type = SOV_AGGREGATION

const title = 'Aggregation'

const propTypes = {
    value: PropTypes.string.isRequired,
    options: PropTypes.array,
    onApply: PropTypes.func.isRequired,
    onCloseFilter: PropTypes.func,
    closable: PropTypes.bool.isRequired,
}

const defaultProps = {
    onCloseFilter: noop,
    options: [
        { value: SOV_AGGREGATION_UNIT.DAY, label: 'Aggregate by Day' },
        { value: SOV_AGGREGATION_UNIT.WEEK, label: 'Aggregate by Week' },
        { value: SOV_AGGREGATION_UNIT.MONTH, label: 'Aggregate by Month' },
    ],
}

const buttonLabels = {
    [SOV_AGGREGATION_UNIT.DAY]: 'By Day',
    [SOV_AGGREGATION_UNIT.WEEK]: 'By Week',
    [SOV_AGGREGATION_UNIT.MONTH]: 'By Month',
}

const formatValue = value => buttonLabels[value]

const SovAggregationFilter = ({
    value,
    onApply,
    options,
    onCloseFilter,
    closable,
}) => (
    <RadioControl
        value={value}
        options={options}
        onApply={selectedValue => onApply(SOV_AGGREGATION, selectedValue)}
        onCloseFilter={() => onCloseFilter(SOV_AGGREGATION)}
        closable={closable}
        formatValue={formatValue}
    />
)

SovAggregationFilter.propTypes = propTypes
SovAggregationFilter.defaultProps = defaultProps
SovAggregationFilter.type = type
SovAggregationFilter.title = title

export default SovAggregationFilter
