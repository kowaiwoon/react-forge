import React from 'react'
import PropTypes from 'prop-types'
import noop from 'lodash/noop'

import { MultiSelectControl } from 'components/FilterControls'
import { SOV_KEYWORDS } from 'constants/filters'

const type = SOV_KEYWORDS

const title = 'Keywords'

const propTypes = {
    values: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.string,
            label: PropTypes.string,
        })
    ).isRequired,
    options: PropTypes.arrayOf(PropTypes.shape()),
    onApply: PropTypes.func.isRequired,
    onCloseFilter: PropTypes.func,
    closable: PropTypes.bool.isRequired,

    onChangeInput: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired,
}

const defaultProps = {
    onCloseFilter: noop,
    options: [],
}

const SovKeywordsFilter = ({
    onApply,
    onCloseFilter,
    values,
    options,
    closable,
    onChangeInput,
    loading,
}) => (
    <MultiSelectControl
        values={values}
        options={options}
        onApply={selectedValues => onApply(SOV_KEYWORDS, selectedValues)}
        onCloseFilter={() => onCloseFilter(SOV_KEYWORDS)}
        closable={closable}
        onChangeInput={onChangeInput}
        placeholder="Search Keywords..."
        resourceName="Keywords"
        loading={loading}
    />
)

SovKeywordsFilter.propTypes = propTypes
SovKeywordsFilter.defaultProps = defaultProps
SovKeywordsFilter.type = type
SovKeywordsFilter.title = title

export default SovKeywordsFilter
