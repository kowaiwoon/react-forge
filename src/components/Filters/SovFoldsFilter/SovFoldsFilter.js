import React from 'react'
import PropTypes from 'prop-types'
import noop from 'lodash/noop'

import { NumberRangeControl } from 'components/FilterControls'
import {
    SOV_FOLDS,
    GREATER_THAN_OR_EQUAL,
    LESS_THAN_OR_EQUAL,
} from 'constants/filters'
import { FOLD_MIN, FOLD_MAX, FOLD_STEP, FOLD_PRECISION } from 'constants/folds'

const type = SOV_FOLDS

const title = 'Folds'

const propTypes = {
    operator: PropTypes.string,
    value: PropTypes.number,
    onApply: PropTypes.func.isRequired,
    onCloseFilter: PropTypes.func,
    closable: PropTypes.bool.isRequired,
}

const defaultProps = {
    operator: null,
    value: null,
    onCloseFilter: noop,
}

const SovFoldsFilter = ({
    onApply,
    onCloseFilter,
    operator,
    value,
    closable,
}) => (
    <NumberRangeControl
        operator={operator}
        operatorOptions={[GREATER_THAN_OR_EQUAL, LESS_THAN_OR_EQUAL]}
        value={value}
        resourceName="Fold"
        onApply={(op, val) => onApply(SOV_FOLDS, { operator: op, value: val })}
        onCloseFilter={() => onCloseFilter(SOV_FOLDS)}
        closable={closable}
        min={FOLD_MIN}
        max={FOLD_MAX}
        step={FOLD_STEP}
        precision={FOLD_PRECISION}
    />
)

SovFoldsFilter.propTypes = propTypes
SovFoldsFilter.defaultProps = defaultProps
SovFoldsFilter.type = type
SovFoldsFilter.title = title

export default SovFoldsFilter
