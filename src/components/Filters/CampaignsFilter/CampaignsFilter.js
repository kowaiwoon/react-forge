import React from 'react'
import PropTypes from 'prop-types'
import noop from 'lodash/noop'

import { MultiSelectControl } from 'components/FilterControls'
import { CAMPAIGNS } from 'constants/filters'

const type = CAMPAIGNS

const title = 'Campaigns'

const propTypes = {
    values: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.string,
            label: PropTypes.string,
        })
    ).isRequired,
    options: PropTypes.arrayOf(PropTypes.shape()),
    onApply: PropTypes.func.isRequired,
    onCloseFilter: PropTypes.func,
    closable: PropTypes.bool.isRequired,

    onChangeInput: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired,
}

const defaultProps = {
    onCloseFilter: noop,
    options: [],
}

const CampaignsFilter = ({
    onApply,
    onCloseFilter,
    values,
    options,
    onChangeInput,
    loading,
    closable,
}) => (
    <MultiSelectControl
        values={values}
        options={options}
        onApply={selectedValues => onApply(CAMPAIGNS, selectedValues)}
        onCloseFilter={() => onCloseFilter(CAMPAIGNS)}
        closable={closable}
        onChangeInput={onChangeInput}
        placeholder="Search Campaigns..."
        resourceName="Campaigns"
        loading={loading}
    />
)

CampaignsFilter.propTypes = propTypes
CampaignsFilter.defaultProps = defaultProps
CampaignsFilter.type = type
CampaignsFilter.title = title

export default CampaignsFilter
