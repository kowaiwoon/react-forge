import React from 'react'
import PropTypes from 'prop-types'
import noop from 'lodash/noop'

import { CheckboxControl } from 'components/FilterControls'
import { COUNTRIES } from 'constants/filters'
import { hasAllOptionsSelected } from 'helpers/filters'
import { getAmsCountryOptions } from 'helpers/codes'

const type = COUNTRIES

const title = 'Countries'

const allOptions = getAmsCountryOptions()

const propTypes = {
    values: PropTypes.arrayOf(
        PropTypes.shape({
            label: PropTypes.string,
            value: PropTypes.string,
        })
    ).isRequired,
    options: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.string,
            label: PropTypes.string,
        })
    ),
    onApply: PropTypes.func.isRequired,
    onCloseFilter: PropTypes.func,
    closable: PropTypes.bool.isRequired,
}

const defaultProps = {
    onCloseFilter: noop,
    options: allOptions,
}

const CountriesFilter = ({
    values,
    onApply,
    onCloseFilter,
    options,
    closable,
}) => (
    <CheckboxControl
        values={values}
        options={options}
        onApply={selectedValues =>
            onApply(
                COUNTRIES,
                hasAllOptionsSelected(selectedValues, allOptions)
                    ? []
                    : selectedValues
            )
        }
        onCloseFilter={() => onCloseFilter(COUNTRIES)}
        closable={closable}
        resourceName="Countries"
        errorMsg="Must select a Country"
    />
)

CountriesFilter.propTypes = propTypes
CountriesFilter.defaultProps = defaultProps
CountriesFilter.type = type
CountriesFilter.title = title

export default CountriesFilter
