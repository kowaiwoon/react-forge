import React from 'react'
import PropTypes from 'prop-types'

import { CAMPAIGN_NAME } from 'constants/filters'
import { TextSearchControl } from 'components/FilterControls'

const type = CAMPAIGN_NAME

const title = 'Search Campaigns'

const propTypes = {
    onApply: PropTypes.func.isRequired,
}

const CampaignNameFilter = ({ onApply }) => (
    <TextSearchControl
        onApply={value => onApply(CAMPAIGN_NAME, value)}
        placeholder="Search Campaigns"
    />
)

CampaignNameFilter.propTypes = propTypes
CampaignNameFilter.type = type
CampaignNameFilter.title = title

export default CampaignNameFilter
