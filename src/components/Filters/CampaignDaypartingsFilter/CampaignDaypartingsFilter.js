import React from 'react'
import PropTypes from 'prop-types'
import noop from 'lodash/noop'

import { CheckboxControl } from 'components/FilterControls'
import { CAMPAIGN_DAYPARTINGS } from 'constants/filters'
import { hasAllOptionsSelected } from 'helpers/filters'
import { ENABLED, DISABLED } from 'constants/dayparting'

const type = CAMPAIGN_DAYPARTINGS

const title = 'Campaign Dayparting Status'

const propTypes = {
    values: PropTypes.arrayOf(
        PropTypes.shape({
            label: PropTypes.string,
            value: PropTypes.string,
        })
    ).isRequired,
    options: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.string,
            label: PropTypes.string,
        })
    ),
    onApply: PropTypes.func.isRequired,
    onCloseFilter: PropTypes.func,
    closable: PropTypes.bool.isRequired,
}

const defaultProps = {
    onCloseFilter: noop,
    options: [
        { value: ENABLED, label: 'Enabled' },
        { value: DISABLED, label: 'Disabled' },
    ],
}

const CampaignDaypartingsFilter = ({
    values,
    onApply,
    onCloseFilter,
    options,
    closable,
}) => (
    <CheckboxControl
        values={values}
        options={options}
        onApply={selectedValues =>
            onApply(
                CAMPAIGN_DAYPARTINGS,
                hasAllOptionsSelected(selectedValues, options)
                    ? []
                    : selectedValues
            )
        }
        onCloseFilter={() => onCloseFilter(CAMPAIGN_DAYPARTINGS)}
        closable={closable}
        resourceName="Campaign Daypartings"
        errorMsg="Must select a Campaign Dayparting"
    />
)

CampaignDaypartingsFilter.propTypes = propTypes
CampaignDaypartingsFilter.defaultProps = defaultProps
CampaignDaypartingsFilter.type = type
CampaignDaypartingsFilter.title = title

export default CampaignDaypartingsFilter
