import React from 'react'
import { render, mount } from 'enzyme'
import noop from 'lodash/noop'

import SovBrandsFilter from '../SovBrandsFilter'

describe('[Filters] SovBrandsFilterContainer', () => {
    it('should mount with no options', () => {
        expect(
            mount(
                <SovBrandsFilter
                    values={[]}
                    onApply={noop}
                    closable
                    onChangeFilter={noop}
                    onCloseFilter={noop}
                    options={[]}
                    loading={false}
                    onChangeInput={noop}
                />
            ).props().onCloseFilter
        ).toEqual(noop)
    })

    it('should render with no options', () => {
        expect(
            render(
                <SovBrandsFilter
                    values={[]}
                    onApply={noop}
                    closable
                    onChangeFilter={noop}
                    onCloseFilter={noop}
                    options={[]}
                    loading={false}
                    onChangeInput={noop}
                />
            ).text()
        ).toEqual('All Brands')
    })
})
