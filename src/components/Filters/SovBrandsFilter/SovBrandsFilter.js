import React from 'react'
import PropTypes from 'prop-types'
import noop from 'lodash/noop'

import { MultiSelectControl } from 'components/FilterControls'
import { SOV_BRANDS } from 'constants/filters'

const type = SOV_BRANDS

const title = 'Brands'

const propTypes = {
    values: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.string,
            label: PropTypes.string,
        })
    ).isRequired,
    options: PropTypes.arrayOf(PropTypes.shape()),
    onApply: PropTypes.func.isRequired,
    onCloseFilter: PropTypes.func,
    closable: PropTypes.bool.isRequired,

    onChangeInput: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired,
}

const defaultProps = {
    onCloseFilter: noop,
    options: [],
}

const SovBrandsFilter = ({
    onApply,
    onCloseFilter,
    values,
    options,
    onChangeInput,
    loading,
    closable,
}) => (
    <MultiSelectControl
        values={values}
        options={options}
        onApply={selectedValues => onApply(SOV_BRANDS, selectedValues)}
        onCloseFilter={() => onCloseFilter(SOV_BRANDS)}
        closable={closable}
        onChangeInput={onChangeInput}
        placeholder="Search Brands..."
        resourceName="Brands"
        loading={loading}
    />
)

SovBrandsFilter.propTypes = propTypes
SovBrandsFilter.defaultProps = defaultProps
SovBrandsFilter.type = type
SovBrandsFilter.title = title

export default SovBrandsFilter
