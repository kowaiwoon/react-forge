import React from 'react'
import PropTypes from 'prop-types'
import { Popover } from 'antd'

import styles from './styles.scss'

const CustomPopover = ({ children, ...rest }) => (
    <div className={styles['popover-wrapper']}>
        <Popover {...rest}>{children}</Popover>
    </div>
)

CustomPopover.propTypes = {
    children: PropTypes.node,
}
CustomPopover.defaultProps = {
    children: null,
}

export default CustomPopover
