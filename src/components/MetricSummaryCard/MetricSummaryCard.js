import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Card } from 'antd'
import { withSize } from 'react-sizeme'
import toNumber from 'lodash/toNumber'
import isNaN from 'lodash/isNaN'

import { metrics } from 'configuration/metrics'
import { formatNumber, formatAbbreviatedMetric } from 'helpers/formatting'
import { UNDEFINED_VALUE } from 'constants/formatting'
import { NoData } from 'components/NoData'
import { ToolTip } from 'components/ToolTip'

import styles from './styles.scss'

class MetricSummaryCard extends Component {
    static propTypes = {
        loading: PropTypes.bool.isRequired,
        data: PropTypes.object.isRequired,
        metric: PropTypes.string.isRequired,
        mouseEnterDelay: PropTypes.number,
        mouseExitDelay: PropTypes.number,
    }

    static defaultProps = {
        mouseEnterDelay: 0.3,
        mouseExitDelay: 0,
    }

    getMetricName(format) {
        return metrics[this.props.metric][format]
    }

    formatValue(value, abbreviate = false) {
        if (isNaN(value)) {
            return UNDEFINED_VALUE
        }

        const { format, short_format } = metrics[this.props.metric]
        return abbreviate
            ? formatAbbreviatedMetric(value, format, short_format)
            : formatNumber(value, format)
    }

    renderTooltipWithHelptext() {
        const { metric } = this.props
        const { name, helptext } = metrics[metric]

        return (
            <div className={styles.tooltip}>
                <span className={styles.longname}>{name}</span>
                {helptext !== undefined && (
                    <span className={styles.helptext}>{helptext}</span>
                )}
            </div>
        )
    }

    render() {
        const {
            loading,
            data,
            metric,
            mouseEnterDelay,
            mouseExitDelay,
        } = this.props
        const summaryMetric = toNumber(data[metric])
        const [shortName, metricValue, metricAbbreviatedValue] = [
            this.getMetricName('short_name'),
            this.formatValue(summaryMetric),
            this.formatValue(summaryMetric, true),
        ]
        const cardHeadStyle = {
            marginBottom: '-8px',
            padding: '0 15px',
            border: 'none',
        }
        const cardBodyStyle = {
            padding: '0 15px 15px',
        }

        return (
            <Card
                className={styles.card}
                headStyle={cardHeadStyle}
                bodyStyle={cardBodyStyle}
                title={
                    <div className={styles['title-container']}>
                        <ToolTip
                            title={this.renderTooltipWithHelptext()}
                            mouseEnterDelay={mouseEnterDelay}
                            mouseExitDelay={mouseExitDelay}
                            placement="topLeft"
                        >
                            <p>{shortName}</p>
                        </ToolTip>
                    </div>
                }
                loading={loading}
            >
                {isNaN(summaryMetric) ? (
                    <NoData />
                ) : (
                    <div>
                        <ToolTip
                            title={metricValue}
                            mouseEnterDelay={mouseEnterDelay}
                            mouseExitDelay={mouseExitDelay}
                            placement="topLeft"
                        >
                            <p className={styles['summary-text']}>
                                {metricAbbreviatedValue}
                            </p>
                        </ToolTip>
                    </div>
                )}
            </Card>
        )
    }
}

export default withSize()(MetricSummaryCard)
