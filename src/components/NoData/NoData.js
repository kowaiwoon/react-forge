import React from 'react'

import styles from './styles.scss'

const NoData = () => (
    <div className={styles['no-data-placeholder']}>No data</div>
)

export default NoData
