import React from 'react'
import PropTypes from 'prop-types'
import { Col, Row, Select, Divider } from 'antd'
import autobind from 'autobind-decorator'

import { getMetricName } from 'helpers/metrics'
import { CHART_METRIC_KEYS } from 'constants/charts'
import { CHART_AXIS_TITLES } from 'configuration/charts'
import { FilterTitle } from 'components/FilterTitle'
import { Collapse } from 'components/Collapse'
import { DownloadButton, SettingsButton } from 'components/Buttons'

import styles from './styles.scss'

class CardTitle extends React.Component {
    static propTypes = {
        downloading: PropTypes.bool.isRequired,
        chartConfig: PropTypes.shape({
            title: PropTypes.string,
            titleHelpText: PropTypes.string,
            metricOptions: PropTypes.objectOf(
                PropTypes.arrayOf(PropTypes.string)
            ),
            defaultMetrics: PropTypes.objectOf(PropTypes.string),
        }).isRequired,
        chartMetrics: PropTypes.objectOf(PropTypes.string).isRequired,
        isMetricAvailable: PropTypes.func.isRequired,
        updateChartMetrics: PropTypes.func.isRequired,
        downloadData: PropTypes.func.isRequired,
    }

    state = {
        isOpened: false,
    }

    @autobind
    handleCollapseToggle(event) {
        event.preventDefault()

        this.setState(state => ({
            isOpened: !state.isOpened,
        }))
    }

    @autobind
    handleDownload() {
        const { downloadData } = this.props

        downloadData()
    }

    @autobind
    handleMetricChange(metricKey) {
        const { updateChartMetrics } = this.props

        return value => updateChartMetrics(metricKey, value)
    }

    renderMetricOptions(metrics) {
        const { isMetricAvailable } = this.props

        return metrics.map(metric => (
            <Select.Option
                key={metric}
                value={metric}
                disabled={!isMetricAvailable(metric)}
            >
                {getMetricName(metric)}
            </Select.Option>
        ))
    }

    render() {
        const {
            downloading,
            chartMetrics,
            chartConfig: { title, titleHelpText, metricOptions },
        } = this.props
        const { isOpened } = this.state

        return (
            <React.Fragment>
                <Row className={styles['card-header']}>
                    <div className={styles['card-title']}>
                        <p>{title}</p>
                        <p>{titleHelpText}</p>
                    </div>

                    <SettingsButton
                        onClick={this.handleCollapseToggle}
                        tooltipTitle="Customize chart settings"
                    />

                    <DownloadButton
                        loading={downloading}
                        onClick={this.handleDownload}
                    />
                </Row>
                <Collapse isOpened={isOpened}>
                    <Divider />

                    <Row gutter={16}>
                        {CHART_METRIC_KEYS.map(chartMetricKey => {
                            const chartMetric =
                                CHART_AXIS_TITLES[chartMetricKey]
                            const value = chartMetrics[chartMetricKey]
                            const options = metricOptions[chartMetricKey]

                            return (
                                <Col span={8} key={chartMetricKey}>
                                    <FilterTitle {...chartMetric} />

                                    <Select
                                        value={value}
                                        style={{ width: '100%' }}
                                        onChange={this.handleMetricChange(
                                            chartMetricKey
                                        )}
                                        disabled={options.length === 1}
                                    >
                                        {this.renderMetricOptions(options)}
                                    </Select>
                                </Col>
                            )
                        })}
                    </Row>
                </Collapse>
            </React.Fragment>
        )
    }
}

export default CardTitle
