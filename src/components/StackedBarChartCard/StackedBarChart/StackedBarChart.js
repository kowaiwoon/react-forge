import React from 'react'
import PropTypes from 'prop-types'
import zipObject from 'lodash/zipObject'
import isEmpty from 'lodash/isEmpty'
import flatten from 'lodash/flatten'

import { getMetricName } from 'helpers/metrics'
import {
    LEFT_AXIS_FIRST,
    LEFT_AXIS_SECOND,
    RIGHT_AXIS,
    CHART_METRIC_KEYS,
} from 'constants/charts'
import { FACT_TYPE_LABELS } from 'configuration/factTypes'
import {
    SPONSORED_PRODUCT,
    HEADLINE_SEARCH,
    FACT_TYPE_KEYS,
} from 'constants/factTypes'
import { HighCharts } from 'components/HighCharts'

import styles from './styles.scss'

const DEFAULT_CHART_SETTINGS = {
    chartMargins: undefined, // the actual space is dynamically calculated from the options
    colors: {
        [SPONSORED_PRODUCT]: {
            [LEFT_AXIS_FIRST]: 'rgb(0, 147, 153)',
            [LEFT_AXIS_SECOND]: 'rgb(108, 201, 226)',
            [RIGHT_AXIS]: 'rgb(73, 39, 92)',
        },
        [HEADLINE_SEARCH]: {
            [LEFT_AXIS_FIRST]: 'rgb(0, 60, 64)',
            [LEFT_AXIS_SECOND]: 'rgb(193, 237, 249)',
            [RIGHT_AXIS]: 'rgb(55, 164, 201)',
        },
    },
}

class StackedBarChart extends React.Component {
    static propTypes = {
        chartMetrics: PropTypes.objectOf(PropTypes.string).isRequired,
        axes: PropTypes.objectOf(
            PropTypes.shape({
                tickInterval: PropTypes.number,
                tickFormat: PropTypes.func,
            })
        ).isRequired,
        series: PropTypes.objectOf(
            PropTypes.shape({
                [LEFT_AXIS_FIRST]: PropTypes.arrayOf(PropTypes.array),
                [LEFT_AXIS_SECOND]: PropTypes.arrayOf(PropTypes.array),
                [RIGHT_AXIS]: PropTypes.arrayOf(PropTypes.array),
            })
        ).isRequired,
    }

    getChartMetricNames() {
        const { chartMetrics } = this.props

        return zipObject(
            CHART_METRIC_KEYS,
            CHART_METRIC_KEYS.map(metricKey => {
                const metric = chartMetrics[metricKey]

                return getMetricName(metric)
            })
        )
    }

    makeChartConfig() {
        const { axes, series } = this.props
        const { colors, chartMargins } = DEFAULT_CHART_SETTINGS
        const chartMetricNames = this.getChartMetricNames()
        const chartConfig = {
            chart: {
                zoomType: 'xy',
                margin: chartMargins,
            },
            title: {
                text: null,
            },
            credits: {
                enabled: false,
            },
            legend: {
                itemMarginBottom: 10,
            },
            xAxis: [
                {
                    type: 'datetime',
                    labels: {
                        useHTML: true,
                        formatter() {
                            return axes.date.tickFormat(this.value)
                        },
                    },
                    startOfWeek: -1,
                    tickInterval: axes.date.tickInterval,
                },
            ],
            yAxis: [
                {
                    // Primary yAxis
                    labels: {
                        formatter() {
                            return axes.left.tickFormat(this.value)
                        },
                    },
                    title: {
                        text: `${chartMetricNames[LEFT_AXIS_FIRST]} / ${
                            chartMetricNames[LEFT_AXIS_SECOND]
                        }`,
                    },
                    min: 0,
                },
                {
                    // Secondary yAxis
                    title: {
                        text: chartMetricNames[RIGHT_AXIS],
                    },
                    labels: {
                        formatter() {
                            return axes.right.tickFormat(this.value)
                        },
                    },
                    opposite: true,
                    min: 0,
                },
            ],
            plotOptions: {
                column: {
                    borderWidth: 0,
                },
                line: {
                    lineWidth: 3,
                    marker: {
                        radius: 0,
                        states: {
                            hover: {
                                radius: 5,
                            },
                        },
                    },
                },
                series: {
                    pointPadding: 0,
                    groupPadding: 0.05,
                    stacking: 'normal',
                    tooltip: {
                        headerFormat: '',
                    },
                },
            },
        }

        const chartSeries = flatten(
            FACT_TYPE_KEYS.map(factTypeKey =>
                CHART_METRIC_KEYS.filter(
                    metricKey => !isEmpty(series[factTypeKey][metricKey])
                ).map(metricKey => ({
                    type: metricKey === RIGHT_AXIS ? 'line' : 'column',
                    name: `${chartMetricNames[metricKey]} [${
                        FACT_TYPE_LABELS[factTypeKey]
                    }]`,
                    yAxis: metricKey === RIGHT_AXIS ? 1 : 0,
                    data: series[factTypeKey][metricKey],
                    color: colors[factTypeKey][metricKey],
                    stack: metricKey,
                    tooltip: {
                        pointFormatter() {
                            return metricKey === RIGHT_AXIS
                                ? axes.right.tickFormat(this.y)
                                : axes.left.tickFormat(this.y)
                        },
                    },
                }))
            )
        )

        return {
            ...chartConfig,
            series: chartSeries,
        }
    }

    render() {
        return (
            <div className={styles['chart-wrap']}>
                <HighCharts config={this.makeChartConfig()} />
            </div>
        )
    }
}

export default StackedBarChart
