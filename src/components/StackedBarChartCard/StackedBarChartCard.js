import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Card } from 'antd'
import autobind from 'autobind-decorator'
import isEmpty from 'lodash/isEmpty'
import noop from 'lodash/noop'
import some from 'lodash/some'

import { headlineSearchMetrics } from 'configuration/metrics'
import { HEADLINE_SEARCH } from 'constants/factTypes'
import { NoData } from 'components/NoData'

import CardTitle from './CardTitle/CardTitle'
import StackedBarChart from './StackedBarChart/StackedBarChart'
import styles from './styles.scss'

class StackedBarChartCard extends Component {
    static propTypes = {
        pageName: PropTypes.string.isRequired,
        chartName: PropTypes.string.isRequired,
        height: PropTypes.number.isRequired,

        chartConfig: PropTypes.shape({
            title: PropTypes.string,
            titleHelpText: PropTypes.string,
            metricOptions: PropTypes.objectOf(
                PropTypes.arrayOf(PropTypes.string)
            ),
            defaultMetrics: PropTypes.objectOf(PropTypes.string),
        }).isRequired,
        chartMetrics: PropTypes.objectOf(PropTypes.string).isRequired,
        factTypes: PropTypes.arrayOf(
            PropTypes.shape({
                value: PropTypes.string,
                label: PropTypes.string,
            })
        ).isRequired,

        // Chart data
        loading: PropTypes.bool.isRequired,
        axes: PropTypes.objectOf(
            PropTypes.shape({
                tickInterval: PropTypes.number,
                tickFormat: PropTypes.func,
            })
        ).isRequired,
        series: PropTypes.objectOf(
            PropTypes.shape({
                normalizedData: PropTypes.arrayOf(PropTypes.object),
                labelData: PropTypes.arrayOf(PropTypes.object),
            })
        ).isRequired,
        downloading: PropTypes.bool.isRequired,

        // Actions
        updateChartMetrics: PropTypes.func.isRequired,
        downloadData: PropTypes.func,
    }

    static defaultProps = {
        downloadData: noop,
    }

    @autobind
    handleUpdateChartMetrics(key, value) {
        const { pageName, chartName, updateChartMetrics } = this.props

        updateChartMetrics({
            pageName,
            chartName,
            key,
            value,
        })
    }

    isSeriesEmpty() {
        const { series } = this.props

        return Object.values(series).some(data => isEmpty(data))
    }

    @autobind
    isMetricAvailableForChart(metric) {
        const { factTypes } = this.props

        if (some(factTypes, ['value', HEADLINE_SEARCH])) {
            return headlineSearchMetrics.includes(metric)
        }

        return true
    }

    render() {
        const {
            chartMetrics,

            // Card Title
            chartConfig,
            downloadData,
            downloading,

            // Chart
            height,

            loading,
            axes,
            series,
        } = this.props

        const cardStyle = {
            textAlign: 'center',
            minHeight: `calc(${height}px + 48px)`, // timeseries height + card padding
        }

        const cardTitle = (
            <CardTitle
                downloading={downloading}
                chartConfig={chartConfig}
                chartMetrics={chartMetrics}
                isMetricAvailable={this.isMetricAvailableForChart}
                updateChartMetrics={this.handleUpdateChartMetrics}
                downloadData={downloadData}
            />
        )

        return (
            <Card
                className={styles.card}
                bodyStyle={cardStyle}
                title={cardTitle}
                loading={loading}
            >
                {this.isSeriesEmpty() ? (
                    <NoData />
                ) : (
                    <StackedBarChart
                        height={height}
                        chartMetrics={chartMetrics}
                        axes={axes}
                        series={series}
                    />
                )}
            </Card>
        )
    }
}

export default StackedBarChartCard
