import React from 'react'
import PropTypes from 'prop-types'
import { Breadcrumb, Icon } from 'antd'

import { AppLink } from 'components/AppLink'

const propTypes = {
    items: PropTypes.arrayOf(
        PropTypes.shape({
            name: PropTypes.string,
            url: PropTypes.string,
            icon: PropTypes.string,
        })
    ).isRequired,
}

const AppBreadcrumb = ({ items }) => {
    const breadcrumbs = items
        .filter(({ name }) => name)
        .map(({ name, url, icon }) => (
            <Breadcrumb.Item key={name}>
                {icon && <Icon type={icon} />}

                <span>{url ? <AppLink to={url}>{name}</AppLink> : name}</span>
            </Breadcrumb.Item>
        ))

    return <Breadcrumb>{breadcrumbs}</Breadcrumb>
}

AppBreadcrumb.propTypes = propTypes

export default AppBreadcrumb
