import React from 'react'
import PropTypes from 'prop-types'
import { Form, Select, Spin } from 'antd'
import values from 'lodash/values'

import { REGION_CODES, COUNTRY_CODES } from 'constants/codes'
import { resourcesRequired } from 'helpers/organizations'
import {
    RESOURCE_TYPE_BRANDS,
    RESOURCE_TYPE_COUNTRIES,
    RESOURCE_TYPE_REGIONS,
} from 'constants/organizations'

function getResourceOptions(resourceType, brands) {
    if (resourceType === RESOURCE_TYPE_REGIONS) {
        return values(REGION_CODES).map(region => (
            <Select.Option key={region} value={region}>
                {region}
            </Select.Option>
        ))
    } else if (resourceType === RESOURCE_TYPE_COUNTRIES) {
        return values(COUNTRY_CODES).map(country => (
            <Select.Option key={country} value={country}>
                {country}
            </Select.Option>
        ))
    } else if (resourceType === RESOURCE_TYPE_BRANDS) {
        return brands.map(brand => (
            <Select.Option value={brand.value} key={brand.value}>
                {brand.label}
            </Select.Option>
        ))
    }
    return null
}

const propTypes = {
    getFieldDecorator: PropTypes.func.isRequired,
    selectedResourceType: PropTypes.string,
    brands: PropTypes.array.isRequired,
    brandSearchLoading: PropTypes.bool.isRequired,
    changeBrandSearchInput: PropTypes.func.isRequired,
    initialValue: PropTypes.array,
    labelInValue: PropTypes.bool,
    disabled: PropTypes.bool,
}

const defaultProps = {
    selectedResourceType: null,
    initialValue: null,
    labelInValue: false,
    disabled: false,
}

const ResourceDetailsField = ({
    getFieldDecorator,
    selectedResourceType,
    brands,
    brandSearchLoading,
    changeBrandSearchInput,
    initialValue,
    labelInValue,
    disabled,
}) => {
    const selectProps = {
        mode: 'multiple',
        optionFilterProp: 'children',
        disabled: !resourcesRequired(selectedResourceType) || disabled,
        labelInValue,
        ...(selectedResourceType === RESOURCE_TYPE_BRANDS
            ? {
                  placeholder: 'Search brands...',
                  notFoundContent: brandSearchLoading ? (
                      <Spin size="small" />
                  ) : null,
                  onSearch: changeBrandSearchInput,
              }
            : {}),
    }
    return (
        <Form.Item label="Resources">
            {getFieldDecorator('resources', {
                rules: [
                    {
                        required: resourcesRequired(selectedResourceType),
                        message: 'Resources are required',
                    },
                ],
                validateTrigger: 'onSubmit',
                ...(initialValue ? { initialValue } : {}),
            })(
                <Select {...selectProps}>
                    {getResourceOptions(selectedResourceType, brands)}
                </Select>
            )}
        </Form.Item>
    )
}

ResourceDetailsField.propTypes = propTypes
ResourceDetailsField.defaultProps = defaultProps

export default ResourceDetailsField
