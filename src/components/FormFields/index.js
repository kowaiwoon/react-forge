export {
    default as ResourceTypeField,
} from './ResourceTypeField/ResourceTypeField'
export {
    default as ResourceDetailsField,
} from './ResourceDetailsField/ResourceDetailsField'
