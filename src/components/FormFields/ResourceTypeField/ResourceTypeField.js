import React from 'react'
import PropTypes from 'prop-types'
import { Form, Select } from 'antd'

import {
    RESOURCE_TYPE_ALL,
    RESOURCE_TYPE_REGIONS,
    RESOURCE_TYPE_BRANDS,
    RESOURCE_TYPE_COUNTRIES,
    RESOURCE_TYPE_LABELS,
} from 'constants/organizations'

const propTypes = {
    getFieldDecorator: PropTypes.func.isRequired,
    setFieldsValue: PropTypes.func.isRequired,
    initialValue: PropTypes.string,
    disabled: PropTypes.bool,
}

const defaultProps = {
    initialValue: null,
    disabled: false,
}

const ResourceTypeField = ({
    getFieldDecorator,
    setFieldsValue,
    initialValue,
    disabled,
}) => (
    <Form.Item label="Resource Type">
        {getFieldDecorator('resourceType', {
            rules: [
                {
                    required: true,
                    message: 'Resource Type is required',
                },
            ],
            validateTrigger: 'onSubmit',
            ...(initialValue ? { initialValue } : {}),
        })(
            <Select
                onChange={() => setFieldsValue({ resources: [] })}
                disabled={disabled}
            >
                <Select.Option value={RESOURCE_TYPE_ALL}>
                    {RESOURCE_TYPE_LABELS[RESOURCE_TYPE_ALL]}
                </Select.Option>
                <Select.Option value={RESOURCE_TYPE_REGIONS}>
                    {RESOURCE_TYPE_LABELS[RESOURCE_TYPE_REGIONS]}
                </Select.Option>
                <Select.Option value={RESOURCE_TYPE_COUNTRIES}>
                    {RESOURCE_TYPE_LABELS[RESOURCE_TYPE_COUNTRIES]}
                </Select.Option>
                <Select.Option value={RESOURCE_TYPE_BRANDS}>
                    {RESOURCE_TYPE_LABELS[RESOURCE_TYPE_BRANDS]}
                </Select.Option>
            </Select>
        )}
    </Form.Item>
)

ResourceTypeField.propTypes = propTypes
ResourceTypeField.defaultProps = defaultProps

export default ResourceTypeField
