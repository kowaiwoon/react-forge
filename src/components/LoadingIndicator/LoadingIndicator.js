import React from 'react'
import PropTypes from 'prop-types'
import { Icon, Spin } from 'antd'

import styles from './styles.scss'

const propTypes = {
    size: PropTypes.oneOf(['small', 'medium', 'large']),
}

const defaultProps = {
    size: 'large',
}

const SIZE_MAP = {
    small: 50,
    medium: 80,
    large: 100,
}

const LoadingIndicator = ({ size }) => (
    <div className={styles['spin-wrapper']}>
        <Spin
            indicator={
                <Icon
                    spin
                    type="loading"
                    style={{ fontSize: SIZE_MAP[size] }}
                />
            }
        />
    </div>
)

LoadingIndicator.propTypes = propTypes
LoadingIndicator.defaultProps = defaultProps

export default LoadingIndicator
