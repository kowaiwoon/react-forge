import React from 'react'
import PropTypes from 'prop-types'
import { Icon } from 'antd'

import { ToolTip } from 'components/ToolTip'

import styles from './styles.scss'

const propTypes = {
    title: PropTypes.string.isRequired,
    helpText: PropTypes.string.isRequired,
    style: PropTypes.shape(),
}

const defaultProps = {
    style: {},
}

const FilterTitle = ({ title, helpText, style }) => (
    <p className={styles['select-title']} style={style}>
        <span>{title}</span>
        <ToolTip title={helpText}>
            <Icon type="question-circle-o" className={styles.icon} />
        </ToolTip>
    </p>
)

FilterTitle.propTypes = propTypes
FilterTitle.defaultProps = defaultProps

export default FilterTitle
