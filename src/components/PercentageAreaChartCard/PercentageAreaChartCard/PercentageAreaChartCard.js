import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Card } from 'antd'
import isEmpty from 'lodash/isEmpty'
import noop from 'lodash/noop'

import { ALL_SOV_WEIGHTS } from 'configuration/sovWeights'
import { NoData } from 'components/NoData'
import { SOV_AGGREGATION_UNIT } from 'constants/reducerKeys'

import CardTitle from '../CardTitle/CardTitle'
import PercentageAreaChart from '../PercentageAreaChart/PercentageAreaChart'
import styles from './styles.scss'

class PercentageAreaChartCard extends Component {
    static propTypes = {
        title: PropTypes.string.isRequired,
        excerpt: PropTypes.string.isRequired,
        loading: PropTypes.bool.isRequired,
        series: PropTypes.array.isRequired,
        showDownload: PropTypes.bool,
        showSettings: PropTypes.bool,
        downloading: PropTypes.bool,
        aggregate: PropTypes.oneOf([
            SOV_AGGREGATION_UNIT.DAY,
            SOV_AGGREGATION_UNIT.WEEK,
            SOV_AGGREGATION_UNIT.MONTH,
        ]).isRequired,
        maxDataPoints: PropTypes.number.isRequired,
        weight: PropTypes.oneOf(ALL_SOV_WEIGHTS).isRequired,

        // actions
        downloadData: PropTypes.func,
        updateChartWeight: PropTypes.func,
    }

    static defaultProps = {
        downloadData: noop,
        updateChartWeight: noop,
        downloading: false,
        showDownload: true,
        showSettings: true,
    }

    isSeriesEmpty() {
        const { series } = this.props
        return isEmpty(series)
    }

    render() {
        const {
            title,
            excerpt,
            downloadData,
            downloading,
            showDownload,
            showSettings,
            aggregate,
            weight,
            maxDataPoints,
            loading,
            series,
            updateChartWeight,
        } = this.props

        return (
            <Card
                className={styles.card}
                bodyStyle={{ textAlign: 'center' }}
                title={
                    <CardTitle
                        title={title}
                        excerpt={excerpt}
                        showDownload={showDownload}
                        downloading={downloading}
                        downloadData={downloadData}
                        showSettings={showSettings}
                        weight={weight}
                        updateChartWeight={updateChartWeight}
                    />
                }
                loading={loading}
            >
                {this.isSeriesEmpty() ? (
                    <NoData />
                ) : (
                    <PercentageAreaChart
                        series={series}
                        aggregate={aggregate}
                        maxDataPoints={maxDataPoints}
                    />
                )}
            </Card>
        )
    }
}

export default PercentageAreaChartCard
