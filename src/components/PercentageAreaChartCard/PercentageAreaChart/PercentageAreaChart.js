import React from 'react'
import PropTypes from 'prop-types'

import { SOV_AGGREGATION_UNIT } from 'constants/reducerKeys'
import moment from 'utilities/moment'
import { HighCharts } from 'components/HighCharts'

import styles from './styles.scss'

const propTypes = {
    series: PropTypes.array.isRequired,
    aggregate: PropTypes.oneOf([
        SOV_AGGREGATION_UNIT.DAY,
        SOV_AGGREGATION_UNIT.WEEK,
        SOV_AGGREGATION_UNIT.MONTH,
    ]).isRequired,
    maxDataPoints: PropTypes.number.isRequired,
}
const defaultProps = {}

const PercentageAreaChart = ({ series, aggregate, maxDataPoints }) => {
    const type = maxDataPoints > 1 ? 'area' : 'column'
    const plotOptions =
        type === 'area'
            ? {
                  area: {
                      stacking: 'percent',
                      lineColor: '#ffffff',
                      lineWidth: 1,
                      marker: {
                          lineWidth: 1,
                          lineColor: '#ffffff',
                      },
                  },
              }
            : {
                  column: {
                      stacking: 'percent',
                  },
              }

    let tickInterval
    switch (aggregate) {
        case SOV_AGGREGATION_UNIT.DAY:
            tickInterval = moment.duration(1, 'day').asMilliseconds()
            break
        case SOV_AGGREGATION_UNIT.WEEK:
            tickInterval = moment.duration(1, 'week').asMilliseconds()
            break
        case SOV_AGGREGATION_UNIT.MONTH:
            tickInterval = moment.duration(1, 'month').asMilliseconds()
            break
        default:
            tickInterval = moment.duration(1, 'day').asMilliseconds()
    }

    return (
        <div className={styles['chart-wrap']}>
            <HighCharts
                config={{
                    chart: {
                        type,
                    },
                    title: {
                        text: null,
                    },
                    credits: {
                        enabled: false,
                    },
                    legend: {
                        itemMarginBottom: 10,
                    },
                    xAxis: {
                        type: 'datetime',
                        tickmarkPlacement: 'on',
                        title: {
                            enabled: false,
                        },
                        tickInterval,
                        ...(aggregate === SOV_AGGREGATION_UNIT.WEEK
                            ? {
                                  labels: {
                                      useHTML: true,
                                      formatter() {
                                          const formattedValue = this.axis.defaultLabelFormatter.call(
                                              this
                                          )
                                          return `${formattedValue}<br /> Wk. End`
                                      },
                                  },
                              }
                            : {}),
                    },
                    yAxis: {
                        title: {
                            text: 'Share of Voice',
                        },
                    },
                    tooltip: {
                        pointFormat:
                            '<b>{series.name}</b>: <b>{point.percentage:.0f}%</b>',
                        split: false,
                    },
                    plotOptions,
                    series,
                }}
            />
        </div>
    )
}

PercentageAreaChart.propTypes = propTypes
PercentageAreaChart.defaultProps = defaultProps

export default PercentageAreaChart
