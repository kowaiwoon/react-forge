import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Row, Divider, Col, Select } from 'antd'
import autobind from 'autobind-decorator'

import { ALL_SOV_WEIGHTS, SOV_WEIGHT_LABELS } from 'configuration/sovWeights'
import { Collapse } from 'components/Collapse'
import { FilterTitle } from 'components/FilterTitle'
import { SettingsButton, DownloadButton } from 'components/Buttons'

import styles from './styles.scss'

class CardTitle extends Component {
    static propTypes = {
        title: PropTypes.string.isRequired,
        excerpt: PropTypes.string.isRequired,
        showDownload: PropTypes.bool.isRequired,
        showSettings: PropTypes.bool.isRequired,
        downloading: PropTypes.bool.isRequired,
        downloadData: PropTypes.func.isRequired,
        weight: PropTypes.oneOf(ALL_SOV_WEIGHTS),

        // actions
        updateChartWeight: PropTypes.func.isRequired,
    }

    static defaultProps = {
        weight: null,
    }

    state = {
        isOpened: false,
    }

    @autobind
    handleCollapseToggle(event) {
        event.preventDefault()

        this.setState(state => ({
            isOpened: !state.isOpened,
        }))
    }

    render() {
        const {
            title,
            excerpt,
            showSettings,
            showDownload,
            downloadData,
            downloading,
            weight,
            updateChartWeight,
        } = this.props
        const { isOpened } = this.state

        return (
            <React.Fragment>
                <Row className={styles['card-header']}>
                    <div className={styles['card-title']}>
                        <p>{title}</p>
                        <p>{excerpt}</p>
                    </div>

                    {showSettings && (
                        <SettingsButton
                            onClick={this.handleCollapseToggle}
                            tooltipTitle="Customize chart"
                        />
                    )}

                    {showDownload && (
                        <DownloadButton
                            onClick={downloadData}
                            loading={downloading}
                        />
                    )}
                </Row>
                {showSettings && (
                    <Collapse isOpened={isOpened}>
                        <Divider
                            style={{
                                marginTop: '15px',
                                marginBottom: '15px',
                            }}
                        />

                        <Row gutter={16}>
                            <Col span={8}>
                                <FilterTitle
                                    title="Share of Voice Weighting Methodology"
                                    helpText="Select how search results should be weighted to calculate overall Share of Voice."
                                />
                                <Select
                                    value={weight}
                                    style={{ width: '100%' }}
                                    onChange={updateChartWeight}
                                >
                                    {ALL_SOV_WEIGHTS.map(weightKey => (
                                        <Select.Option key={weightKey}>
                                            {SOV_WEIGHT_LABELS[weightKey]}
                                        </Select.Option>
                                    ))}
                                </Select>
                            </Col>
                        </Row>
                    </Collapse>
                )}
            </React.Fragment>
        )
    }
}

export default CardTitle
