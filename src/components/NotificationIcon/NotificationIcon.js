import React from 'react'
import { Icon } from 'antd'

import styles from './styles.scss'

const NotificationIcon = () => (
    <div className={styles.notification}>
        <Icon type="exclamation" />
    </div>
)

export default NotificationIcon
