import React from 'react'
import PropTypes from 'prop-types'
import sizeMe from 'react-sizeme'
import { Card } from 'antd'
import classNames from 'classnames'
import noop from 'lodash/noop'

import { TextButton } from 'components/TextButton'
import { NotificationIcon } from 'components/NotificationIcon'
import styles from './styles.scss'

const propTypes = {
    organization: PropTypes.shape({
        name: PropTypes.string,
        group_count: PropTypes.number,
        member_count: PropTypes.number,
    }).isRequired,
    pendingInvite: PropTypes.bool,
    acceptingInvite: PropTypes.bool,
    handleAcceptInvite: PropTypes.func,
    className: PropTypes.string,
}

const defaultProps = {
    pendingInvite: false,
    acceptingInvite: false,
    handleAcceptInvite: noop,
    className: '',
}

const OrganizationCard = ({
    organization,
    pendingInvite,
    handleAcceptInvite,
    acceptingInvite,
    className,
}) => (
    <Card
        className={classNames(styles.card, className)}
        bordered={false}
        cover={<div className={styles.cover}>{organization.name}</div>}
    >
        <div className={styles.container}>
            <div>
                <div className={styles.label}>Groups</div>
                <span>{organization.group_count}</span>
            </div>
            <div>
                <div className={styles.label}>Members</div>
                <span>{organization.member_count}</span>
            </div>
        </div>
        {pendingInvite && (
            <div className={styles['accept-invite']}>
                <NotificationIcon />
                <TextButton
                    link
                    onClick={handleAcceptInvite}
                    disabled={acceptingInvite}
                >
                    Accept Invite
                </TextButton>
            </div>
        )}
    </Card>
)

OrganizationCard.propTypes = propTypes
OrganizationCard.defaultProps = defaultProps

export default sizeMe({ monitorHeight: true })(OrganizationCard)
