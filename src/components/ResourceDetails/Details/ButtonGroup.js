import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Button } from 'antd'

import { ToolTip } from 'components/ToolTip'

import styles from './styles.scss'

class ActionsIconGroup extends Component {
    static propTypes = {
        // antd form
        form: PropTypes.shape().isRequired,

        allowEditing: PropTypes.bool.isRequired,
        editToolTip: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
        updating: PropTypes.bool.isRequired,
        editing: PropTypes.bool.isRequired,
        onEdit: PropTypes.func.isRequired,
        onCancel: PropTypes.func.isRequired,
        onSave: PropTypes.func.isRequired,
        justUpdated: PropTypes.bool.isRequired,
        setJustUpdated: PropTypes.func.isRequired,
    }

    static defaultProps = {
        editToolTip: null,
    }

    componentDidUpdate(prevProps) {
        // set justUpdated when updating is complete
        if (prevProps.updating && !this.props.updating) {
            this.props.setJustUpdated(true)
        } else if (this.props.justUpdated) {
            // reset justUpdated with timeout
            setTimeout(() => this.props.setJustUpdated(false), 1000)
        }
    }

    render() {
        const {
            form,
            allowEditing,
            editToolTip,
            updating,
            editing,
            onEdit,
            onCancel,
            onSave,
            justUpdated,
        } = this.props
        const saveBtnProps = {
            type: 'primary',
            disabled: !form.isFieldsTouched(),
            onClick: onSave,
            loading: updating,
            ...(justUpdated ? { icon: 'check' } : { icon: 'save' }),
        }
        const cancelBtnProps = {
            onClick: onCancel,
            disabled: updating || justUpdated,
        }
        if (!allowEditing) {
            return (
                <div className={styles.buttons}>
                    {editToolTip ? (
                        <ToolTip
                            mouseEnterDelay={0.3}
                            placement="topLeft"
                            title={editToolTip}
                        >
                            <Button icon="form" onClick={onEdit} disabled>
                                Edit
                            </Button>
                        </ToolTip>
                    ) : (
                        <Button icon="form" onClick={onEdit} disabled>
                            Edit
                        </Button>
                    )}
                </div>
            )
        }
        if (!editing) {
            return (
                <div className={styles.buttons}>
                    <Button icon="form" onClick={onEdit}>
                        Edit
                    </Button>
                </div>
            )
        }
        return (
            <div className={styles.buttons}>
                <Button {...saveBtnProps}>Save</Button>
                <Button {...cancelBtnProps}>Cancel</Button>
            </div>
        )
    }
}

export default ActionsIconGroup
