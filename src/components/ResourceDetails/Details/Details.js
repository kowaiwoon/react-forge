import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Icon, Form } from 'antd'
import has from 'lodash/has'
import isUndefined from 'lodash/isUndefined'
import identity from 'lodash/identity'
import autobind from 'autobind-decorator'
import classNames from 'classnames'

import { TextButton } from 'components/TextButton'
import { InputType } from 'components/InputType'
import { ToolTip } from 'components/ToolTip'
import moment from 'utilities/moment'
import { ALL_TYPES, DATE_INPUT, SWITCH_INPUT } from 'constants/inputTypes'
import { isEditable } from 'helpers/inputTypes'

import ButtonGroup from './ButtonGroup'
import styles from './styles.scss'

class Details extends Component {
    static propTypes = {
        // antd form
        form: PropTypes.shape({
            getFieldDecorator: PropTypes.func,
            validateFields: PropTypes.func,
            isFieldsTouched: PropTypes.func,
            resetFields: PropTypes.func,
        }).isRequired,

        // other props
        allowEditing: PropTypes.bool.isRequired,
        editToolTip: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
        details: PropTypes.arrayOf(
            PropTypes.shape({
                label: PropTypes.string,
                value: PropTypes.oneOfType([
                    PropTypes.number,
                    PropTypes.string,
                    PropTypes.bool,
                ]),
                toolTip: PropTypes.oneOfType([
                    PropTypes.string,
                    PropTypes.node,
                ]),
                formatValue: PropTypes.func,
                format: PropTypes.string,
                show: PropTypes.bool,
                type: PropTypes.oneOf(ALL_TYPES),
                fieldId: PropTypes.string,
            })
        ).isRequired,
        onSave: PropTypes.func.isRequired,
        updating: PropTypes.bool.isRequired,
    }

    static defaultProps = {
        editToolTip: null,
    }

    state = {
        editing: false,
        justUpdated: false,
    }

    @autobind
    setJustUpdated(justUpdated) {
        if (justUpdated) {
            this.setState({ justUpdated })
        } else {
            // only reset form when justUpdated has been set to false
            // by child component
            this.setState({ justUpdated }, this.resetForm)
        }
    }

    @autobind
    resetForm() {
        this.setState({ editing: false }, this.props.form.resetFields)
    }

    @autobind
    save() {
        const { form, onSave } = this.props
        form.validateFields((errors, values) => {
            if (errors) {
                return
            }
            onSave(values)
        })
    }

    @autobind
    turnOffEditing() {
        this.setState({
            editing: false,
        })
    }

    @autobind
    turnOnEditing() {
        this.setState({
            editing: true,
        })
    }

    @autobind
    renderValue(detail) {
        const formatValue = has(detail, 'formatValue')
            ? detail.formatValue
            : identity

        if (isEditable(detail.type) && this.props.allowEditing) {
            return (
                <TextButton onClick={this.turnOnEditing}>
                    {formatValue(detail.value)}
                </TextButton>
            )
        }
        return formatValue(detail.value)
    }

    renderFormItem(detail) {
        const { form, updating } = this.props
        return (
            <Form.Item style={{ margin: 0 }}>
                {form.getFieldDecorator(detail.fieldId, {
                    initialValue: (() => {
                        const { type, value } = detail
                        if (type === DATE_INPUT && value) {
                            return moment(value)
                        }
                        return value
                    })(),
                    getValueProps: value => {
                        const { type } = detail
                        if (type === SWITCH_INPUT) {
                            return { checked: value }
                        }
                        if (type === DATE_INPUT && value) {
                            return { value: moment(value) }
                        }
                        return { value }
                    },
                })(
                    <InputType
                        inputType={detail.type}
                        updating={updating}
                        options={detail.options}
                        formatter={detail.formatter}
                        parser={detail.parser}
                        min={detail.min}
                        max={detail.max}
                        step={detail.step}
                        precision={detail.precision}
                        format={detail.format}
                    />
                )}
            </Form.Item>
        )
    }

    render() {
        const {
            details,
            form,
            updating,
            allowEditing,
            editToolTip,
        } = this.props
        const { editing, justUpdated } = this.state
        return (
            <div className={styles.container}>
                {details.map((detail, idx) => {
                    if (!isUndefined(detail.show) && !detail.show) {
                        return null
                    }
                    return (
                        <div key={idx} className={styles.setting}>
                            {detail.label && (
                                <div className={styles.label}>
                                    {detail.label}
                                    {detail.toolTip && (
                                        <ToolTip
                                            mouseEnterDelay={0.3}
                                            placement="topLeft"
                                            title={detail.toolTip}
                                        >
                                            <span>
                                                <Icon type="question-circle-o" />
                                            </span>
                                        </ToolTip>
                                    )}
                                </div>
                            )}
                            {allowEditing ? (
                                <div
                                    className={classNames({
                                        [styles.editable]:
                                            !editing && isEditable(detail.type),
                                        [styles.value]:
                                            !editing ||
                                            !isEditable(detail.type),
                                        [styles.editing]:
                                            editing && isEditable(detail.type),
                                    })}
                                    style={
                                        detail.type === DATE_INPUT
                                            ? { width: 150 }
                                            : {}
                                    }
                                >
                                    {editing && isEditable(detail.type)
                                        ? this.renderFormItem(detail)
                                        : this.renderValue(detail)}
                                </div>
                            ) : (
                                <div className={styles.value}>
                                    {this.renderValue(detail)}
                                </div>
                            )}
                        </div>
                    )
                })}
                <ButtonGroup
                    form={form}
                    allowEditing={allowEditing}
                    editToolTip={editToolTip}
                    updating={updating}
                    editing={editing}
                    onCancel={this.turnOffEditing}
                    onSave={this.save}
                    onEdit={this.turnOnEditing}
                    justUpdated={justUpdated}
                    setJustUpdated={this.setJustUpdated}
                />
            </div>
        )
    }
}

export default Form.create()(Details)
