import React from 'react'
import PropTypes from 'prop-types'
import { Divider } from 'antd'
import noop from 'lodash/noop'

import { ALL_TYPES } from 'constants/inputTypes'

import Title from './Title/Title'
import Details from './Details/Details'
import styles from './styles.scss'

const propTypes = {
    name: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    showDetails: PropTypes.bool.isRequired,
    allowEditing: PropTypes.bool,
    editToolTip: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    onShowDetailsClick: PropTypes.func.isRequired,
    onSave: PropTypes.func,
    updating: PropTypes.bool,
    details: PropTypes.arrayOf(
        PropTypes.shape({
            label: PropTypes.string,
            value: PropTypes.oneOfType([
                PropTypes.number,
                PropTypes.string,
                PropTypes.bool,
            ]),
            toolTip: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
            formatValue: PropTypes.func,
            show: PropTypes.bool,
            type: PropTypes.oneOf(ALL_TYPES),
        })
    ).isRequired,
    allowSync: PropTypes.bool,
    onSyncResourceClick: PropTypes.func,
    syncing: PropTypes.bool,
    syncedDate: PropTypes.string,
}
const defaultProps = {
    allowEditing: true,
    editToolTip: null,
    updating: false,
    name: null,
    onSave: noop,
    allowSync: false,
    onSyncResourceClick: noop,
    syncing: false,
    syncedDate: null,
}

const ResourceDetails = ({
    name,
    allowEditing,
    editToolTip,
    updating,
    details,
    showDetails,
    onShowDetailsClick,
    onSave,
    allowSync,
    onSyncResourceClick,
    syncing,
    syncedDate,
}) =>
    name ? (
        <div className={styles.container}>
            <Title
                name={name}
                onShowDetailsClick={onShowDetailsClick}
                showDetails={showDetails}
                allowSync={allowSync}
                onSyncResourceClick={onSyncResourceClick}
                syncing={syncing}
                syncedDate={syncedDate}
            />
            {showDetails && (
                <React.Fragment>
                    <Details
                        allowEditing={allowEditing}
                        editToolTip={editToolTip}
                        details={details}
                        onSave={onSave}
                        updating={updating}
                    />
                    <Divider className={styles.divider} />
                </React.Fragment>
            )}
        </div>
    ) : null

ResourceDetails.propTypes = propTypes
ResourceDetails.defaultProps = defaultProps

export default ResourceDetails
