import React from 'react'
import PropTypes from 'prop-types'
import noop from 'lodash/noop'

import { TextButton } from 'components/TextButton'
import { SyncResourceButton } from 'components/SyncResourceButton'

import styles from './styles.scss'

const propTypes = {
    name: PropTypes.oneOfType([PropTypes.string, PropTypes.object]).isRequired,
    onShowDetailsClick: PropTypes.func.isRequired,
    showDetails: PropTypes.bool.isRequired,
    allowSync: PropTypes.bool,
    onSyncResourceClick: PropTypes.func,
    syncing: PropTypes.bool,
    syncedDate: PropTypes.string,
}
const defaultProps = {
    allowSync: false,
    onSyncResourceClick: noop,
    syncing: false,
    syncedDate: null,
}

const Title = ({
    name,
    onShowDetailsClick,
    showDetails,
    allowSync,
    onSyncResourceClick,
    syncing,
    syncedDate,
}) => (
    <div className={styles.container}>
        <div>{name}</div>
        {allowSync && (
            <SyncResourceButton
                onClick={onSyncResourceClick}
                syncing={syncing}
                syncedDate={syncedDate}
            />
        )}
        <TextButton onClick={onShowDetailsClick} link>
            {showDetails ? 'Hide Details' : 'Show Details'}
        </TextButton>
    </div>
)

Title.propTypes = propTypes
Title.defaultProps = defaultProps

export default Title
