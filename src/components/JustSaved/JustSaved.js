import { Component } from 'react'
import PropTypes from 'prop-types'
import noop from 'lodash/noop'

class JustSaved extends Component {
    static propTypes = {
        saving: PropTypes.bool.isRequired,
        children: PropTypes.func.isRequired,
        justSavedCallback: PropTypes.func,
    }

    static defaultProps = {
        justSavedCallback: noop,
    }

    state = {
        justSaved: false,
    }

    componentDidUpdate(prevProps) {
        // set justSaved when creating is complete
        if (prevProps.saving && !this.props.saving) {
            this.setJustSaved(true)
        } else if (this.state.justSaved) {
            // reset justSaved with timeout
            setTimeout(() => this.setJustSaved(false), 1000)
        }
    }

    setJustSaved(justSaved) {
        if (justSaved) {
            this.setState({ justSaved }, this.props.justSavedCallback)
        } else {
            this.setState({ justSaved })
        }
    }

    render() {
        return this.props.children(this.state)
    }
}

export default JustSaved
