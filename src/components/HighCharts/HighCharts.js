/**
 * Highcharts component used throughout application
 *
 * @see https://www.highcharts.com/blog/frameworks/react/192-use-highcharts-to-create-charts-in-react/
 */
import HighCharts from 'react-highcharts'
import HeatMap from 'highcharts/modules/heatmap'
import TreeMap from 'highcharts/modules/treemap'

HeatMap(HighCharts.Highcharts)
TreeMap(HighCharts.Highcharts)

export default HighCharts
