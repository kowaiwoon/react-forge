import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col } from 'antd'

import { AppBreadcrumb } from 'components/AppBreadcrumb'
import { ProfileMenuContainer } from 'components/ProfileMenu'

import styles from './styles.scss'

const propTypes = {
    breadcrumbs: PropTypes.arrayOf(
        PropTypes.shape({
            name: PropTypes.string,
            url: PropTypes.string,
            icon: PropTypes.string,
        })
    ),
    titleComponent: PropTypes.node,
    filterGroupComponent: PropTypes.node,
}

const defaultProps = {
    breadcrumbs: [],
    titleComponent: null,
    filterGroupComponent: null,
}

const PageHeader = ({ filterGroupComponent, breadcrumbs, titleComponent }) => (
    <div className={styles.wrapper}>
        <Row type="flex" justify="space-between" className={styles.breadcrumb}>
            <Col>
                <AppBreadcrumb items={breadcrumbs} />
            </Col>

            <Col>
                <ProfileMenuContainer />
            </Col>
        </Row>

        {titleComponent && <Row type="flex">{titleComponent}</Row>}

        {filterGroupComponent && (
            <Row
                className={styles.filters}
                type="flex"
                justify="start"
                align="bottom"
            >
                <Col>{filterGroupComponent}</Col>
            </Row>
        )}
    </div>
)

PageHeader.propTypes = propTypes
PageHeader.defaultProps = defaultProps

export default PageHeader
