import React from 'react'
import PropTypes from 'prop-types'
import autobind from 'autobind-decorator'
import sortBy from 'lodash/sortBy'
import get from 'lodash/get'
import noop from 'lodash/noop'

import { getComponentProperties } from 'helpers/react'
import { ResetButton, FiltersButton } from 'components/Buttons'
import { SettingsModal } from 'components/SettingsModal'

import styles from './styles.scss'

class FilterGroup extends React.Component {
    static propTypes = {
        filterSettings: PropTypes.shape({
            order: PropTypes.array,
            displayState: PropTypes.object,
        }).isRequired,
        children: PropTypes.array.isRequired,
        updateFilterSettings: PropTypes.func.isRequired,
        resetFilters: PropTypes.func,
    }

    static defaultProps = {
        resetFilters: noop,
    }

    state = {
        settingsModalVisible: false,
    }

    getFilterTitles() {
        return this.props.children.reduce((accumulator, child) => {
            const { type, title } = getComponentProperties(child)

            return {
                ...accumulator,
                [type]: { title },
            }
        }, {})
    }

    getNonClosableFilters() {
        return this.props.children.reduce((accumulator, child) => {
            if (!get(child, ['props', 'closable'], true)) {
                const { type } = getComponentProperties(child)
                accumulator.push(type)
            }
            return accumulator
        }, [])
    }

    @autobind
    handleToggleSettingsModal() {
        const { settingsModalVisible } = this.state

        this.setState({
            settingsModalVisible: !settingsModalVisible,
        })
    }

    @autobind
    handleUpdateSettings(settings) {
        this.props.updateFilterSettings(settings)

        // Toggle settings modal
        this.handleToggleSettingsModal()
    }

    render() {
        const { filterSettings, children, resetFilters } = this.props
        const { settingsModalVisible } = this.state

        return (
            <div className={styles.filters}>
                {sortBy(children, child => {
                    const { type } = getComponentProperties(child)
                    return filterSettings.order.indexOf(type)
                }).filter(child => {
                    const { type } = getComponentProperties(child)
                    return type && filterSettings.displayState[type]
                })}

                <FiltersButton
                    onClick={this.handleToggleSettingsModal}
                    tooltipTitle="Customize filters to display on page"
                />

                {resetFilters !== noop && (
                    <ResetButton
                        onClick={resetFilters}
                        tooltipTitle="Reset filters"
                    />
                )}

                {settingsModalVisible && (
                    <SettingsModal
                        droppableId="filterGroupModal"
                        modalTitle="Customize Page Filters"
                        visible={settingsModalVisible}
                        handleCancel={this.handleToggleSettingsModal}
                        handleOk={this.handleUpdateSettings}
                        settings={filterSettings}
                        settingTitles={this.getFilterTitles()}
                        disabledSettings={this.getNonClosableFilters()}
                    />
                )}
            </div>
        )
    }
}

export default FilterGroup
