import React from 'react'
import PropTypes from 'prop-types'
import { Dropdown, Button, Icon, Menu } from 'antd'

import { AppLink } from 'components/AppLink'
import { getPath } from 'helpers/pages'
import { PROFILE_PAGE, AUTH_PAGE } from 'constants/pages'
import { TextButton } from 'components/TextButton'

import styles from './styles.scss'

const propTypes = { signOutRequest: PropTypes.func.isRequired }

const ProfileMenu = ({ signOutRequest }) => (
    <Dropdown
        overlay={
            <Menu>
                <Menu.Item key="0">
                    <AppLink
                        to={{
                            pathname: getPath(PROFILE_PAGE),
                            search: '?tab=organizations',
                        }}
                    >
                        <TextButton
                            icon="solution"
                            className={styles['menu-dropdown-text']}
                        >
                            Organizations
                        </TextButton>
                    </AppLink>
                </Menu.Item>

                <Menu.Item key="1">
                    <AppLink
                        to={{
                            pathname: getPath(PROFILE_PAGE),
                            search: '?tab=permissions',
                        }}
                    >
                        <TextButton
                            icon="check-circle-o"
                            className={styles['menu-dropdown-text']}
                        >
                            Permissions
                        </TextButton>
                    </AppLink>
                </Menu.Item>

                <Menu.Divider />

                <Menu.Item key="3">
                    <AppLink
                        to={{
                            pathname: getPath(AUTH_PAGE),
                        }}
                        onClick={
                            // wrap function so click event is not included in payload
                            () => signOutRequest()
                        }
                    >
                        <TextButton
                            icon="logout"
                            className={styles['menu-dropdown-text']}
                        >
                            Sign Out
                        </TextButton>
                    </AppLink>
                </Menu.Item>
            </Menu>
        }
        trigger={['click']}
        placement="bottomRight"
    >
        <Button className={styles['profile-btn']}>
            <Icon type="user" /> Profile
        </Button>
    </Dropdown>
)

ProfileMenu.propTypes = propTypes

export default ProfileMenu
