import { connect } from 'react-redux'

import { signOutRequest } from 'actions/auth'

import ProfileMenu from './ProfileMenu'

const mapDispatchToProps = { signOutRequest }

export default connect(
    null,
    mapDispatchToProps
)(ProfileMenu)
