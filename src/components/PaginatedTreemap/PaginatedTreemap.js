import React, { Component } from 'react'
import { renderToString } from 'react-dom/server'
import PropTypes from 'prop-types'
import { Col, Row, Card, Pagination, Select } from 'antd'
import { withSize } from 'react-sizeme'
import autobind from 'autobind-decorator'
import pluralize from 'pluralize'
import isEmpty from 'lodash/isEmpty'
import capitalize from 'lodash/capitalize'
import noop from 'lodash/noop'
import some from 'lodash/some'
import isUndefined from 'lodash/isUndefined'
import get from 'lodash/get'

import { metrics, headlineSearchMetrics } from 'configuration/metrics'
import { HEADLINE_SEARCH } from 'constants/factTypes'
import { NoData } from 'components/NoData'
import { FilterTitle } from 'components/FilterTitle'
import { SingleSelectControl } from 'components/FilterControls'
import { HighCharts } from 'components/HighCharts'

import styles from './styles.scss'

class PaginatedTreemap extends Component {
    static propTypes = {
        size: PropTypes.shape({
            width: PropTypes.number,
        }).isRequired,
        treemapOptions: PropTypes.shape({
            name: PropTypes.string,
            idAttribute: PropTypes.string,
            titleAttribute: PropTypes.string,
            height: PropTypes.number,
            colors: PropTypes.shape({
                minColor: PropTypes.string,
                maxColor: PropTypes.string,
            }),
        }).isRequired,
        defaultPagination: PropTypes.shape({
            pageSizeOptions: PropTypes.array,
            showSizeChanger: PropTypes.bool,
            showTotal: PropTypes.func,
            style: PropTypes.object,
        }).isRequired,
        factTypes: PropTypes.arrayOf(
            PropTypes.shape({
                value: PropTypes.string,
                label: PropTypes.string,
            })
        ).isRequired,
        data: PropTypes.arrayOf(PropTypes.object).isRequired,
        loading: PropTypes.bool.isRequired,
        pagination: PropTypes.object.isRequired,
        sorter: PropTypes.object.isRequired,
        getTitle: PropTypes.func,
        getTooltipContent: PropTypes.func,

        updatePagination: PropTypes.func.isRequired,
        updateSorter: PropTypes.func.isRequired,
        reloadData: PropTypes.func.isRequired,
        onLeafClick: PropTypes.func,
    }

    static defaultProps = {
        onLeafClick: noop,
        getTitle: noop,
        getTooltipContent: noop,
    }

    getCardTitle() {
        const {
            sorter,
            treemapOptions: { name },
        } = this.props
        const pluralizedName = capitalize(pluralize(name))
        const treemapTitle = `${pluralizedName} Tree Chart`
        const treemapHelpText = `Visualize how ${pluralizedName} compare to each other for a given metric.`

        return (
            <React.Fragment>
                <Row className={styles['card-header']}>
                    <div className={styles['card-title']}>
                        <p>{treemapTitle}</p>
                        <p>{treemapHelpText}</p>
                    </div>
                    <Col span={8}>
                        <FilterTitle
                            title="Metric"
                            helpText="Select a metric to display in the tree chart."
                        />
                        <SingleSelectControl
                            value={sorter.field}
                            style={{ width: '100%' }}
                            onChange={this.handleMetricChange}
                        >
                            {this.getSelectOptions()}
                        </SingleSelectControl>
                    </Col>
                </Row>
            </React.Fragment>
        )
    }

    getSelectOptions() {
        return Object.keys(metrics).map((key, i) => {
            const { name } = metrics[key]

            return (
                <Select.Option
                    value={key}
                    key={i}
                    disabled={!this.isMetricAvailableForChart(key)}
                >
                    {name}
                </Select.Option>
            )
        })
    }

    getDataForChart() {
        const {
            data,
            getTitle,
            getTooltipContent,
            treemapOptions: {
                name,
                idAttribute,
                titleAttribute,
                sellerStringIdAttribute,
                countryCodeAttribute,
            },
        } = this.props

        return data.map((row, index) => {
            const {
                metric: { formattedValue, nativeValue },
                metricMetadata,
                resourceData,
            } = row

            const id = get(resourceData, idAttribute)
            const countryCode = get(resourceData, countryCodeAttribute)
            const sellerStringId = get(resourceData, sellerStringIdAttribute)
            const title = get(resourceData, titleAttribute)

            let formattedTitle
            if (isUndefined(getTitle(row))) {
                if (sellerStringId) {
                    formattedTitle = `Seller Account ${sellerStringId}`
                } else {
                    formattedTitle = title
                }
            } else {
                formattedTitle = getTitle(row)
            }

            return {
                id,
                value: nativeValue,
                colorValue: index,
                titleFormat: renderToString(
                    <div className={styles['title-label']}>
                        <p>
                            {formattedTitle}
                            {countryCode && ` (${countryCode})`}
                        </p>
                        <p>{formattedValue}</p>
                    </div>
                ),
                tooltipFormat: renderToString(
                    <div className={styles['tooltip-title']}>
                        {isUndefined(getTooltipContent(row)) ? (
                            <React.Fragment>
                                <p>
                                    <strong>{capitalize(name)}</strong>:{' '}
                                    {formattedTitle}
                                    {countryCode && ` (${countryCode})`}
                                </p>
                                <p>
                                    <strong>{metricMetadata.name}</strong>:{' '}
                                    {formattedValue}
                                </p>
                            </React.Fragment>
                        ) : (
                            getTooltipContent(row)
                        )}
                    </div>
                ),
            }
        })
    }

    @autobind
    handleMetricChange(value) {
        const { updateSorter, reloadData } = this.props
        const sorter = {
            field: value,
            order: 'descend', // Always order in descending order
            filterPositiveValues: true, // Filter only positive values
        }

        // Update sorter
        updateSorter(sorter)

        // Reload data
        reloadData()
    }

    @autobind
    handlePaginationChange(current, pageSize) {
        const { updatePagination, reloadData } = this.props

        // Update pagination
        updatePagination({
            current,
            pageSize,
        })

        // Reload data
        reloadData()
    }

    isMetricAvailableForChart(metric) {
        const { factTypes } = this.props

        if (some(factTypes, ['value', HEADLINE_SEARCH])) {
            return headlineSearchMetrics.includes(metric)
        }

        return true
    }

    render() {
        const {
            size,
            loading,
            pagination,
            defaultPagination,
            treemapOptions: {
                height,
                colors: { minColor, maxColor },
            },
            onLeafClick,
        } = this.props

        const paginationOptions = {
            ...defaultPagination,
            ...pagination,
        }

        const cardTitle = this.getCardTitle()
        const chartData = this.getDataForChart()

        const chartConfig = {
            chart: {
                margin: 0,
                width: Math.max(size.width - 32 * 2, 0),
                height,
            },
            title: {
                text: null,
            },
            credits: {
                enabled: false,
            },
            legend: {
                enabled: false,
            },
            colorAxis: {
                minColor,
                maxColor,
            },
            tooltip: {
                pointFormatter() {
                    return this.tooltipFormat
                },
                backgroundColor: 'rgba(0, 0, 0, 0.75)',
                useHTML: true,
            },
            plotOptions: {
                treemap: {
                    animation: false,
                    cursor: 'pointer',
                    layoutAlgorithm: 'squarified',
                    alternateStartingDirection: true,
                    dataLabels: {
                        defer: true,
                        allowOverlap: true,
                        formatter() {
                            return this.point.titleFormat
                        },
                        useHTML: true,
                    },
                    point: {
                        events: {
                            click() {
                                if (onLeafClick) {
                                    onLeafClick(this.id)
                                }
                            },
                        },
                    },
                },
            },
            series: [
                {
                    type: 'treemap',
                    data: chartData,
                },
            ],
        }

        return (
            <Card
                className={styles.card}
                bodyStyle={{
                    textAlign: 'center',
                    minHeight: `calc(${height}px + 48px + 48px)`, // treemap height + pagination height + card padding
                }}
                title={cardTitle}
                loading={loading}
            >
                <div>
                    {isEmpty(chartData) ? (
                        <NoData />
                    ) : (
                        <HighCharts config={chartConfig} isPureConfig />
                    )}
                </div>
                <Row>
                    <Col span={24}>
                        <Pagination
                            {...paginationOptions}
                            onChange={this.handlePaginationChange}
                            onShowSizeChange={this.handlePaginationChange}
                        />
                    </Col>
                </Row>
            </Card>
        )
    }
}

export default withSize()(PaginatedTreemap)
