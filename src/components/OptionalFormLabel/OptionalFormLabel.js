import React from 'react'
import PropTypes from 'prop-types'

import styles from './styles.scss'

const propTypes = {
    fieldName: PropTypes.string.isRequired,
}

const OptionalFormLabel = ({ fieldName }) => (
    <span>
        {fieldName} <span className={styles['optional-label']}>(Optional)</span>
    </span>
)

OptionalFormLabel.propTypes = propTypes

export default OptionalFormLabel
