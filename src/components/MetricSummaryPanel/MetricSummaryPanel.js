import React from 'react'
import PropTypes from 'prop-types'
import { Row } from 'antd'

import { MetricSummaryCard } from 'components/MetricSummaryCard'
import { categories } from 'configuration/metrics'

import styles from './styles.scss'

const propTypes = {
    loading: PropTypes.bool.isRequired,
    data: PropTypes.object.isRequired,
    category: PropTypes.string.isRequired,
}

const defaultProps = {}

const MetricSummaryPanel = ({ loading, data, category }) => (
    <React.Fragment>
        <Row gutter={0} className={styles.panel}>
            {categories[category].map(metric => (
                <MetricSummaryCard
                    loading={loading}
                    data={data}
                    metric={metric}
                    key={metric}
                />
            ))}
        </Row>
    </React.Fragment>
)

MetricSummaryPanel.propTypes = propTypes
MetricSummaryPanel.defaultProps = defaultProps

export default MetricSummaryPanel
