import React from 'react'
import PropTypes from 'prop-types'
import { Table } from 'antd'
import MediaQuery from 'react-responsive'
import { withSize } from 'react-sizeme'
import without from 'lodash/without'
import get from 'lodash/get'
import isEmpty from 'lodash/isEmpty'
import isNull from 'lodash/isNull'

const propTypes = {
    components: PropTypes.shape({
        table: PropTypes.any,
        header: PropTypes.shape({
            wrapper: PropTypes.any,
            row: PropTypes.any,
            cell: PropTypes.any,
        }),
        body: PropTypes.shape({
            wrapper: PropTypes.any,
            row: PropTypes.any,
            cell: PropTypes.any,
        }),
    }),
    columns: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    rowKey: PropTypes.oneOfType([PropTypes.string, PropTypes.func]).isRequired,
    dataSource: PropTypes.arrayOf(PropTypes.object).isRequired,
    pagination: PropTypes.oneOfType([PropTypes.bool, PropTypes.object])
        .isRequired,
    loading: PropTypes.bool.isRequired,
    onChange: PropTypes.func.isRequired,
    onRow: PropTypes.func.isRequired,
    tableSize: PropTypes.oneOf(['default', 'middle', 'small']),
    maxHeight: PropTypes.number,
    maxScrollY: PropTypes.func,
    locale: PropTypes.shape({
        emptyText: PropTypes.text,
    }),

    // size me props
    size: PropTypes.shape({
        width: PropTypes.number.isRequired,
    }).isRequired,
}

const defaultProps = {
    components: {},
    tableSize: 'default',
    maxHeight: 830,
    maxScrollY: () => 500,
    locale: {},
}

const ScrollableTable = ({
    components,
    columns: originalColumns,
    rowKey,
    dataSource,
    pagination,
    loading,
    onChange,
    onRow,
    tableSize,
    maxHeight,
    maxScrollY,
    size,
    locale,
}) => {
    const tableWidth = size.width
    const colWidthSum = originalColumns.reduce(
        (acc, curr) => acc + get(curr, 'width', 0),
        0
    )

    // if the sum of the calculated widths of all columns is greater than the
    // table component width, set explicit scrollX width = to width of all cols
    //
    // if less, disable scrolling and remove the 'fixed' properties from columns
    let scrollX
    let columns

    if (colWidthSum > tableWidth) {
        scrollX = colWidthSum
        // adjust column order if there are fixed columns and scrollX
        const leftColumns = originalColumns.filter(
            column => get(column, 'fixed') === 'left'
        )
        const rightColumns = originalColumns.filter(
            column => get(column, 'fixed') === 'right'
        )
        const middleColumns = without(
            originalColumns,
            ...leftColumns,
            ...rightColumns
        )
        columns = [...leftColumns, ...middleColumns, ...rightColumns]
    } else {
        scrollX = null
        columns = originalColumns.map(column => {
            const { fixed, ...rest } = column
            return rest
        })
    }

    const props = {
        columns,
        rowKey,
        dataSource,
        pagination,
        loading,
        onChange,
        onRow,
        locale,
        size: tableSize,
        ...(isEmpty(components) ? {} : { components }),
    }

    const scroll = { x: scrollX }
    const scrollY = maxScrollY(size)

    if (isNull(maxHeight)) {
        if (scrollY > 0) {
            scroll.y = scrollY
        }
        return <Table {...props} scroll={scroll} />
    }

    return (
        <MediaQuery maxHeight={maxHeight}>
            {matches => {
                if (matches && scrollY > 0) {
                    scroll.y = scrollY
                }
                return <Table {...props} scroll={scroll} />
            }}
        </MediaQuery>
    )
}

ScrollableTable.propTypes = propTypes
ScrollableTable.defaultProps = defaultProps

export default withSize({ monitorHeight: true })(ScrollableTable)
