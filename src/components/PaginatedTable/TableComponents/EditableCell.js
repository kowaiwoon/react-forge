import React from 'react'
import PropTypes from 'prop-types'
import { Form } from 'antd'
import noop from 'lodash/noop'
import identity from 'lodash/identity'
import get from 'lodash/get'
import classNames from 'classnames'

import moment from 'utilities/moment'
import { isActions, isEditable } from 'helpers/inputTypes'
import { ALL_TYPES, DATE_INPUT, SWITCH_INPUT } from 'constants/inputTypes'
import { TextButton } from 'components/TextButton'
import { InputType } from 'components/InputType'

import { FormContext } from '../PaginatedTable'
import ActionsCell from '../ActionsCell/ActionsCell'
import styles from './styles.scss'

class EditableCell extends React.Component {
    static propTypes = {
        record: PropTypes.shape(),
        recordName: PropTypes.string,
        rowKey: PropTypes.string,
        dataIndex: PropTypes.string,
        fieldId: PropTypes.string,
        cellType: PropTypes.oneOf(ALL_TYPES),
        actionColumnWidth: PropTypes.number,
        disableRowActions: PropTypes.bool,
        actionsToolTip: PropTypes.oneOfType([PropTypes.node, PropTypes.string]),

        // select options for selectInput
        options: PropTypes.arrayOf(
            PropTypes.shape({
                value: PropTypes.string,
                label: PropTypes.string,
            })
        ),

        // options for numberInput
        formatter: PropTypes.func,
        parser: PropTypes.func,
        min: PropTypes.number,
        max: PropTypes.number,
        step: PropTypes.number,
        precision: PropTypes.number,

        // options for dateInput
        format: PropTypes.string,

        editing: PropTypes.bool,
        updating: PropTypes.bool,
        deleting: PropTypes.bool,
        onEdit: PropTypes.func,
        onCancel: PropTypes.func,
        onSave: PropTypes.func,
        onDelete: PropTypes.func,
    }

    static defaultProps = {
        record: null,
        recordName: '',
        rowKey: null,
        dataIndex: null,
        fieldId: null,
        cellType: null,
        actionColumnWidth: null,
        disableRowActions: false,
        actionsToolTip: null,
        options: [],
        formatter: identity,
        parser: identity,
        min: -Infinity,
        max: Infinity,
        step: 1,
        precision: 0,
        format: null,
        updating: false,
        editing: false,
        deleting: false,
        onEdit: noop,
        onCancel: noop,
        onSave: noop,
        onDelete: noop,
    }

    render() {
        const {
            record,
            recordName,
            rowKey,
            dataIndex,
            fieldId,
            cellType,
            actionColumnWidth,
            disableRowActions,
            actionsToolTip,
            options,
            formatter,
            parser,
            min,
            max,
            step,
            precision,
            format,
            deleting,
            updating,
            editing,
            onEdit,
            onCancel,
            onSave,
            onDelete,
            ...restProps
        } = this.props

        return (
            <FormContext.Consumer>
                {form => {
                    if (isEditable(cellType)) {
                        const allowEdits = !disableRowActions && !deleting
                        const uniqueFieldId = `${fieldId}-${get(
                            record,
                            rowKey
                        )}`
                        return (
                            <td {...restProps} className={styles}>
                                {editing ? (
                                    <Form.Item style={{ margin: 0 }}>
                                        {form.getFieldDecorator(uniqueFieldId, {
                                            initialValue: (() => {
                                                const value = get(
                                                    record,
                                                    dataIndex
                                                )
                                                if (
                                                    cellType === DATE_INPUT &&
                                                    value
                                                ) {
                                                    return moment(value)
                                                }
                                                return value
                                            })(),
                                            getValueProps: value => {
                                                if (cellType === SWITCH_INPUT) {
                                                    return { checked: value }
                                                }
                                                if (
                                                    cellType === DATE_INPUT &&
                                                    value
                                                ) {
                                                    return {
                                                        value: moment(value),
                                                    }
                                                }
                                                return { value }
                                            },
                                        })(
                                            <InputType
                                                inputType={cellType}
                                                updating={updating}
                                                options={options}
                                                formatter={formatter}
                                                parser={parser}
                                                min={min}
                                                max={max}
                                                step={step}
                                                format={format}
                                                precision={precision}
                                            />
                                        )}
                                    </Form.Item>
                                ) : (
                                    <TextButton
                                        className={classNames(
                                            styles['cell-value-wrap'],
                                            {
                                                [styles.editable]: allowEdits,
                                            }
                                        )}
                                        onClick={
                                            allowEdits
                                                ? () => onEdit(record)
                                                : noop
                                        }
                                    >
                                        {restProps.children}
                                    </TextButton>
                                )}
                            </td>
                        )
                    }

                    if (isActions(cellType)) {
                        return (
                            <td
                                {...restProps}
                                style={{ width: actionColumnWidth }}
                            >
                                <ActionsCell
                                    recordName={recordName}
                                    onEdit={() => onEdit(record)}
                                    onCancel={onCancel}
                                    onSave={() => onSave(record)}
                                    onDelete={() => onDelete(record)}
                                    updating={updating}
                                    editing={editing}
                                    deleting={deleting}
                                    disabled={disableRowActions}
                                    actionsToolTip={actionsToolTip}
                                />
                            </td>
                        )
                    }

                    return (
                        <td {...restProps}>
                            <div className={styles['normal-cell']}>
                                {restProps.children}
                            </div>
                        </td>
                    )
                }}
            </FormContext.Consumer>
        )
    }
}

export default EditableCell
