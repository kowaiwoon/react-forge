import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'

import styles from './styles.scss'

const propTypes = {
    deleting: PropTypes.bool.isRequired,
    className: PropTypes.string.isRequired,
    disableRowActions: PropTypes.bool.isRequired,
}
const defaultProps = {}

const EditableRow = ({
    deleting,
    className: antClassNames,
    disableRowActions,
    ...props
}) => (
    <tr
        {...props}
        className={classNames(antClassNames, {
            [styles['editable-row']]: !disableRowActions && !deleting,
        })}
    />
)

EditableRow.propTypes = propTypes
EditableRow.defaultProps = defaultProps

export default EditableRow
