import React from 'react'
import PropTypes from 'prop-types'
import { Icon } from 'antd'
import noop from 'lodash/noop'
import classNames from 'classnames'

import styles from './styles.scss'

const propTypes = {
    type: PropTypes.oneOf([
        'save',
        'form',
        'close-circle-o',
        'check',
        'delete',
        'loading',
        'picture',
    ]).isRequired,
    onClick: PropTypes.func,
    disabled: PropTypes.bool,
}

const defaultProps = {
    onClick: noop,
    disabled: false,
}

const getStyle = type => {
    if (type === 'check') {
        return {
            color: '#52c41a',
            fontWeight: 'bold',
        }
    }
    if (type === 'delete') {
        return {
            color: '#f41e28',
        }
    }
    return undefined
}

const ActionIcon = ({ type, onClick, disabled }) => (
    <div
        className={classNames(styles.icon, { [styles.disabled]: disabled })}
        onClick={disabled ? noop : onClick}
        role="button"
        tabIndex="-1"
        onKeyPress={noop}
    >
        <Icon type={type} style={getStyle(type)} />
    </div>
)

ActionIcon.propTypes = propTypes
ActionIcon.defaultProps = defaultProps

export default ActionIcon
