import React, { Component } from 'react'
import PropTypes from 'prop-types'
import autobind from 'autobind-decorator'

import ActionIconGroup from './ActionIconGroup'

class ActionsCell extends Component {
    static propTypes = {
        recordName: PropTypes.string.isRequired,
        updating: PropTypes.bool.isRequired,
        editing: PropTypes.bool.isRequired,
        deleting: PropTypes.bool.isRequired,
        onEdit: PropTypes.func.isRequired,
        onCancel: PropTypes.func.isRequired,
        onSave: PropTypes.func.isRequired,
        onDelete: PropTypes.func.isRequired,
        disabled: PropTypes.bool.isRequired,
        actionsToolTip: PropTypes.oneOfType([PropTypes.node, PropTypes.string]),
    }

    static defaultProps = {
        actionsToolTip: null,
    }

    state = {
        justUpdated: false,
    }

    @autobind
    setJustUpdated(justUpdated) {
        this.setState({ justUpdated })
    }

    render() {
        const {
            recordName,
            updating,
            editing,
            deleting,
            onEdit,
            onCancel,
            onSave,
            onDelete,
            disabled,
            actionsToolTip,
        } = this.props
        const { justUpdated } = this.state
        return (
            <ActionIconGroup
                recordName={recordName}
                updating={updating}
                editing={editing}
                deleting={deleting}
                onEdit={onEdit}
                onCancel={onCancel}
                onSave={onSave}
                onDelete={onDelete}
                disabled={disabled}
                actionsToolTip={actionsToolTip}
                justUpdated={justUpdated}
                setJustUpdated={this.setJustUpdated}
            />
        )
    }
}

export default ActionsCell
