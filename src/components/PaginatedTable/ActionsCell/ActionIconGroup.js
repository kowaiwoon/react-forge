import React, { Component } from 'react'
import PropTypes from 'prop-types'
import upperFirst from 'lodash/upperFirst'
import isNull from 'lodash/isNull'

import ToolTip from 'components/ToolTip/ToolTip'
import { ConfirmDeleteButton } from 'components/Buttons'

import ActionIcon from './ActionIcon'
import styles from './styles.scss'

class ActionsIconGroup extends Component {
    static propTypes = {
        recordName: PropTypes.string.isRequired,
        updating: PropTypes.bool.isRequired,
        editing: PropTypes.bool.isRequired,
        deleting: PropTypes.bool.isRequired,
        onEdit: PropTypes.func.isRequired,
        onCancel: PropTypes.func.isRequired,
        onSave: PropTypes.func.isRequired,
        onDelete: PropTypes.func.isRequired,
        disabled: PropTypes.bool.isRequired,
        actionsToolTip: PropTypes.oneOfType([PropTypes.node, PropTypes.string]),
        justUpdated: PropTypes.bool.isRequired,
        setJustUpdated: PropTypes.func.isRequired,
    }

    static defaultProps = {
        actionsToolTip: null,
    }

    componentDidUpdate(prevProps) {
        // set justUpdated when updating/deleting is complete
        if (
            (prevProps.updating && !this.props.updating) ||
            (prevProps.deleting && !this.props.deleting)
        ) {
            this.props.setJustUpdated(true)
        } else if (this.props.justUpdated) {
            // reset justUpdated with timeout
            setTimeout(() => this.props.setJustUpdated(false), 1000)
        }
    }

    static getTitle = name =>
        `Are you sure you want to archive this ${upperFirst(
            name
        )}? This action cannot be undone.`

    renderActionButtons() {
        const {
            recordName,
            updating,
            deleting,
            editing,
            onEdit,
            onCancel,
            onSave,
            onDelete,
            disabled,
            justUpdated,
        } = this.props

        if (justUpdated) {
            return (
                <div className={styles.container}>
                    <ActionIcon type="check" />
                </div>
            )
        }
        if (updating || deleting) {
            return (
                <div className={styles.container}>
                    <ActionIcon type="loading" />
                </div>
            )
        }
        if (disabled) {
            return (
                <div className={styles.container}>
                    <ActionIcon type="form" disabled={disabled} />
                    <ActionIcon type="delete" disabled={disabled} />
                </div>
            )
        }
        if (editing) {
            return (
                <div className={styles.container}>
                    <ActionIcon type="save" onClick={onSave} />
                    <ActionIcon type="close-circle-o" onClick={onCancel} />
                </div>
            )
        }
        return (
            <div className={styles.container}>
                <ActionIcon type="form" onClick={onEdit} />
                <ConfirmDeleteButton
                    title={ActionsIconGroup.getTitle(recordName)}
                    onConfirm={onDelete}
                />
            </div>
        )
    }

    render() {
        const { actionsToolTip } = this.props
        if (isNull(actionsToolTip)) {
            return this.renderActionButtons()
        }
        return (
            <ToolTip
                mouseEnterDelay={0.3}
                placement="topLeft"
                title={actionsToolTip}
            >
                {this.renderActionButtons()}
            </ToolTip>
        )
    }
}

export default ActionsIconGroup
