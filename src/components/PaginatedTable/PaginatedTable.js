import React from 'react'
import PropTypes from 'prop-types'
import { Form } from 'antd'
import autobind from 'autobind-decorator'
import has from 'lodash/has'
import get from 'lodash/get'
import isUndefined from 'lodash/isUndefined'
import mapKeys from 'lodash/mapKeys'
import createGetter from 'lodash/fp/get'
import noop from 'lodash/noop'

import ScrollableTable from './ScrollableTable/ScrollableTable'
import { EditableCell, EditableRow } from './TableComponents'

// Give 6px width for each character in title + 100px padding
const getColumnWidth = column => column.title.length * 6 + 110

const ACTION_COLUMN_WIDTH = 85

export const FormContext = React.createContext()

class PaginatedTable extends React.Component {
    static propTypes = {
        tableOptions: PropTypes.shape({
            name: PropTypes.string, // @TODO this is only used to display delete confirmation. refactor to pass in the message or components
            rowKey: PropTypes.string,
            columns: PropTypes.object,
        }).isRequired,
        data: PropTypes.arrayOf(PropTypes.object).isRequired,
        loading: PropTypes.bool.isRequired,
        updating: PropTypes.bool.isRequired,
        deleting: PropTypes.bool.isRequired,
        pagination: PropTypes.object.isRequired,
        sorter: PropTypes.object.isRequired,
        columnSettings: PropTypes.shape({
            actionColumns: PropTypes.array,
            order: PropTypes.array,
            displayState: PropTypes.shape(),
        }).isRequired,
        disableRowActions: PropTypes.func,
        actionsToolTip: PropTypes.func,

        // actions
        updatePagination: PropTypes.func.isRequired,
        updateSorter: PropTypes.func.isRequired,
        reloadData: PropTypes.func.isRequired,
        saveRecord: PropTypes.func,
        deleteRecord: PropTypes.func,

        // antd form
        form: PropTypes.shape().isRequired,
    }

    static defaultProps = {
        saveRecord: noop,
        deleteRecord: noop,
        disableRowActions: () => false,
        actionsToolTip: () => null,
    }

    constructor(props) {
        super(props)
        this.state = {
            editingKey: null,
            deletingKey: null,
            updating: props.updating,
        }
    }

    static getDerivedStateFromProps(props, state) {
        // turn off editing mode when table finishes updating
        if (!props.updating && state.updating) {
            return {
                editingKey: null,
                updating: props.updating,
            }
        }
        return {
            updating: props.updating,
        }
    }

    getColumns() {
        const {
            tableOptions: {
                columns: columnsOriginal,
                rowKey,
                name: recordName,
            },
            disableRowActions,
            actionsToolTip,
            sorter: { field, order },
            columnSettings: {
                actionColumns: actionColumnsOriginal,
                order: columnOrder,
                displayState,
            },
        } = this.props
        const columns = columnOrder
            .filter(column => displayState[column])
            .map(column => columnsOriginal[column])
            .map(column => ({
                width: getColumnWidth(column),
                sortOrder: column.dataIndex === field ? order : false,
                ...column,
            }))
            // add extra props for editable cells
            .map(column => {
                if (has(column, 'type')) {
                    return {
                        ...column,
                        onCell: record => ({
                            record,
                            rowKey,
                            cellType: column.type,
                            fieldId: column.fieldId,
                            dataIndex: column.dataIndex,
                            deleting: this.isDeleting(record),
                            updating: this.isUpdating(record),
                            editing: this.isEditing(record),
                            onEdit: this.edit,
                            disableRowActions: disableRowActions(record),
                            options: column.options,
                            formatter: value => {
                                if (has(column, 'formatter')) {
                                    return column.formatter(value, record)
                                }
                                return value
                            },
                            parser: column.parser,
                            min: column.min,
                            max: column.max,
                            step: column.step,
                            format: column.format,
                            precision: column.precision,
                        }),
                    }
                }
                return column
            })

        let actionColumns = []
        if (!isUndefined(actionColumnsOriginal)) {
            actionColumns = actionColumnsOriginal
                .map(column => columnsOriginal[column])
                .map(column => ({
                    width: ACTION_COLUMN_WIDTH,
                    ...column,
                }))
                // add extra props for action cells
                .map(column => {
                    if (has(column, 'type')) {
                        return {
                            ...column,
                            onCell: record => ({
                                record,
                                rowKey,
                                recordName,
                                cellType: column.type,
                                dataIndex: column.dataIndex,
                                actionColumnWidth: ACTION_COLUMN_WIDTH,
                                editing: this.isEditing(record),
                                updating: this.isUpdating(record),
                                deleting: this.isDeleting(record),
                                disableRowActions: disableRowActions(record),
                                actionsToolTip: actionsToolTip(record),
                                onEdit: this.edit,
                                onCancel: this.cancel,
                                onSave: this.save,
                                onDelete: this.delete,
                            }),
                        }
                    }
                    return column
                })
        }

        return [...columns, ...actionColumns]
    }

    @autobind
    getRowProps(record, index) {
        const { disableRowActions } = this.props
        return {
            disableRowActions: disableRowActions(record),
            deleting: this.isDeleting(record),
            record,
            index,
        }
    }

    @autobind
    handleTableChange(nextPagination, filters, nextSorter) {
        const {
            pagination,
            updatePagination,
            sorter,
            updateSorter,
            reloadData,
        } = this.props

        // Update pagination and sorter
        if (
            sorter.field !== nextSorter.field ||
            sorter.order !== nextSorter.order
        ) {
            updateSorter({
                field: nextSorter.field,
                order: nextSorter.order,
            })
        }

        if (
            pagination.current !== nextPagination.current ||
            pagination.pageSize !== nextPagination.pageSize
        ) {
            updatePagination({
                current: nextPagination.current,
                pageSize: nextPagination.pageSize,
            })
        }

        // Reload data
        reloadData()
    }

    @autobind
    isEditing(record) {
        const {
            tableOptions: { rowKey },
        } = this.props
        return get(record, rowKey) === this.state.editingKey
    }

    @autobind
    isUpdating(record) {
        return this.isEditing(record) && this.state.updating
    }

    @autobind
    isDeleting(record) {
        const {
            tableOptions: { rowKey },
            deleting,
        } = this.props
        const { deletingKey } = this.state
        return get(record, rowKey) === deletingKey && deleting
    }

    @autobind
    edit(record) {
        const {
            tableOptions: { rowKey },
        } = this.props
        this.setState({ editingKey: get(record, rowKey) })
    }

    @autobind
    cancel() {
        this.setState({ editingKey: null })
    }

    @autobind
    save(record) {
        const {
            tableOptions: { rowKey },
            saveRecord,
            form,
        } = this.props

        form.validateFields((error, formValues) => {
            if (error) {
                return
            }
            const values = mapKeys(formValues, (value, key) => {
                const [fieldId] = key.split('-', 1)
                return fieldId
            })
            saveRecord({ id: get(record, rowKey), values })
        })
    }

    @autobind
    delete(record) {
        const {
            tableOptions: { rowKey },
            deleteRecord,
        } = this.props

        this.setState({ deletingKey: get(record, rowKey) })
        deleteRecord({ id: get(record, rowKey) })
    }

    render() {
        const {
            data,
            pagination,
            loading,
            tableOptions: { rowKey },
            form,
        } = this.props

        return (
            <FormContext.Provider value={form}>
                <ScrollableTable
                    components={{
                        body: {
                            cell: EditableCell,
                            row: EditableRow,
                        },
                    }}
                    columns={this.getColumns()}
                    rowKey={createGetter(rowKey)}
                    dataSource={data}
                    pagination={pagination}
                    loading={loading}
                    onChange={this.handleTableChange}
                    onRow={this.getRowProps}
                />
            </FormContext.Provider>
        )
    }
}

export default Form.create()(PaginatedTable)
