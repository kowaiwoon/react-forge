import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form, Button, Row, Col, Select } from 'antd'
import autobind from 'autobind-decorator'

import { JustSaved } from 'components/JustSaved'

class AddMemberForm extends Component {
    static propTypes = {
        addingMember: PropTypes.bool.isRequired,
        members: PropTypes.array.isRequired,
        addMemberRequest: PropTypes.func.isRequired,
        onCancel: PropTypes.func.isRequired,

        // Antd form
        form: PropTypes.object.isRequired,
    }

    @autobind
    handleSubmit(e) {
        e.preventDefault()
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.addMemberRequest({ user_id: values.email })
            }
        })
    }

    @autobind
    handleCancel() {
        this.props.form.resetFields()
        this.props.onCancel()
    }

    render() {
        const {
            addingMember,
            members,
            form: { getFieldDecorator, resetFields },
        } = this.props

        return (
            <Form
                layout="vertical"
                onSubmit={this.handleSubmit}
                hideRequiredMark
            >
                <Row gutter={{ md: 16, xl: 32 }}>
                    <Col md={12} xl={6}>
                        <Form.Item label="Email Address">
                            {getFieldDecorator('email', {
                                rules: [
                                    {
                                        required: true,
                                        message: 'Email Address is required',
                                    },
                                ],
                                validateTrigger: 'onSubmit',
                            })(
                                <Select>
                                    {members.map(member => (
                                        <Select.Option
                                            value={member.id}
                                            key={member.id}
                                        >
                                            {member.email}
                                        </Select.Option>
                                    ))}
                                </Select>
                            )}
                        </Form.Item>
                    </Col>
                    <Col md={12} xl={6}>
                        <JustSaved
                            saving={addingMember}
                            justSavedCallback={resetFields}
                        >
                            {({ justSaved }) => (
                                <div style={{ paddingTop: '29px' }}>
                                    <Button
                                        type="primary"
                                        htmlType="submit"
                                        icon={justSaved ? 'check' : 'plus'}
                                        disabled={addingMember}
                                        loading={addingMember}
                                        style={{ marginRight: '15px' }}
                                    >
                                        Add Member
                                    </Button>
                                    <Button
                                        onClick={this.handleCancel}
                                        disabled={addingMember}
                                    >
                                        Cancel
                                    </Button>
                                </div>
                            )}
                        </JustSaved>
                    </Col>
                </Row>
            </Form>
        )
    }
}

export default Form.create()(AddMemberForm)
