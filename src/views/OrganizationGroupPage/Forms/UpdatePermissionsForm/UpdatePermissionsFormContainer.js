import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import {
    selectOrganization,
    selectOrganizationGroup,
    selectOrganizationUserPermissions,
} from 'selectors/orgs'
import { ORGANIZATION_GROUP_PAGE } from 'constants/pages'
import { selectDomainValue as selectUiDomainValue } from 'selectors/ui'
import { updatePermissionsOrganizationGroupPageRequest } from 'actions/ui'

import UpdatePermissionsFormState from './UpdatePermissionsFormState'

const mapStateToProps = (state, ownProps) => {
    const { organizationGroupId, organizationId } = ownProps.match.params

    return {
        organization: selectOrganization(state, organizationId),
        organizationGroup: selectOrganizationGroup(state, organizationGroupId),
        userPermissions: selectOrganizationUserPermissions(
            state,
            organizationId
        ),
        updatingPermissions: selectUiDomainValue(state, [
            ORGANIZATION_GROUP_PAGE,
            'updatingPermissions',
        ]),
    }
}

const mapDispatchToProps = {
    updatePermissionsRequest: updatePermissionsOrganizationGroupPageRequest,
}

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(UpdatePermissionsFormState)
)
