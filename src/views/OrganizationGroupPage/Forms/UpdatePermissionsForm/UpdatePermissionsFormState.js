import React, { Component } from 'react'
import PropTypes from 'prop-types'
import autobind from 'autobind-decorator'
import sortBy from 'lodash/fp/sortBy'
import identity from 'lodash/identity'

import UpdatePermissionsForm from './UpdatePermissionsForm'

class UpdatePermissionsFormState extends Component {
    static propTypes = {
        organization: PropTypes.object.isRequired,
        organizationGroup: PropTypes.shape({
            permissions: PropTypes.array,
        }).isRequired,
        userPermissions: PropTypes.array.isRequired,
        updatingPermissions: PropTypes.bool.isRequired,

        // actions
        updatePermissionsRequest: PropTypes.func.isRequired,
    }

    constructor(props) {
        super(props)
        this.state = {
            fields: {
                permissions: props.organizationGroup.permissions,
            },
        }
    }

    @autobind
    onValuesChange(allValues) {
        this.setState({
            fields: allValues,
        })
    }

    @autobind
    onCancel() {
        this.setState({
            fields: {
                permissions: this.props.organizationGroup.permissions,
            },
        })
    }

    isDirty() {
        const {
            fields: { permissions: statePermissions },
        } = this.state
        const {
            organizationGroup: { permissions: propPermissions },
        } = this.props

        const immutableSort = sortBy(identity)

        return (
            JSON.stringify(immutableSort(statePermissions)) !==
            JSON.stringify(immutableSort(propPermissions))
        )
    }

    render() {
        return (
            <UpdatePermissionsForm
                onValuesChange={this.onValuesChange}
                onCancel={this.onCancel}
                isDirty={this.isDirty()}
                fields={this.state.fields}
                {...this.props}
            />
        )
    }
}

export default UpdatePermissionsFormState
