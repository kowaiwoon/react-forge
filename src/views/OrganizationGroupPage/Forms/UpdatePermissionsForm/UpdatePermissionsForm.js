import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form, Checkbox, Button } from 'antd'
import autobind from 'autobind-decorator'
import get from 'lodash/get'

import { ORGANIZATION_ADMIN } from 'constants/featurePermissions'
import {
    getPermissionOption,
    hasOrgAdminPermissions,
} from 'helpers/featurePermissions'

import styles from './styles.scss'

class UpdatePermissionsForm extends Component {
    static propTypes = {
        organization: PropTypes.shape({
            permissions: PropTypes.array,
        }).isRequired,
        userPermissions: PropTypes.array.isRequired,
        updatingPermissions: PropTypes.bool.isRequired,
        onCancel: PropTypes.func.isRequired,
        isDirty: PropTypes.bool.isRequired,

        // Antd form
        form: PropTypes.object.isRequired,

        // actions
        updatePermissionsRequest: PropTypes.func.isRequired,
    }

    @autobind
    handleSubmit(e) {
        e.preventDefault()
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.updatePermissionsRequest(values)
            }
        })
    }

    render() {
        const {
            organization,
            userPermissions,
            updatingPermissions,
            onCancel,
            isDirty,
            form: { getFieldDecorator },
        } = this.props

        const availableOrganizationPermissions = get(
            organization,
            'permissions',
            []
        )

        return (
            <Form onSubmit={this.handleSubmit}>
                <Form.Item>
                    {getFieldDecorator('permissions', {})(
                        <Checkbox.Group
                            options={availableOrganizationPermissions
                                .concat(ORGANIZATION_ADMIN) // appending the org admin permission since it's not a feature permission returned from cerebro
                                .map(permission =>
                                    getPermissionOption(permission)
                                )}
                            className={styles['permissions-group']}
                            disabled={!hasOrgAdminPermissions(userPermissions)}
                        />
                    )}
                </Form.Item>
                {hasOrgAdminPermissions(userPermissions) &&
                    isDirty && (
                        <div>
                            <Button
                                type="primary"
                                htmlType="submit"
                                style={{ marginRight: '15px' }}
                                loading={updatingPermissions}
                                disabled={updatingPermissions}
                            >
                                Save
                            </Button>
                            <Button onClick={onCancel}>Cancel</Button>
                        </div>
                    )}
            </Form>
        )
    }
}

export default Form.create({
    onValuesChange: (props, changedValues, allValues) =>
        props.onValuesChange(allValues),
    mapPropsToFields: props => {
        const {
            fields: { permissions },
        } = props
        return {
            permissions: Form.createFormField({ value: permissions }),
        }
    },
})(UpdatePermissionsForm)
