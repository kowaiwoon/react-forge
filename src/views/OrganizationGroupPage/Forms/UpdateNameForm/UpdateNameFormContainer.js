import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import {
    selectOrganizationGroup,
    selectOrganizationUserPermissions,
} from 'selectors/orgs'
import { ORGANIZATION_GROUP_PAGE } from 'constants/pages'
import { selectDomainValue as selectUiDomainValue } from 'selectors/ui'
import { updateNameOrganizationGroupPageRequest } from 'actions/ui'

import UpdateNameFormState from './UpdateNameFormState'

const mapStateToProps = (state, ownProps) => {
    const { organizationGroupId, organizationId } = ownProps.match.params

    return {
        organizationGroup: selectOrganizationGroup(state, organizationGroupId),
        userPermissions: selectOrganizationUserPermissions(
            state,
            organizationId
        ),
        updatingName: selectUiDomainValue(state, [
            ORGANIZATION_GROUP_PAGE,
            'updatingName',
        ]),
    }
}

const mapDispatchToProps = {
    updateNameRequest: updateNameOrganizationGroupPageRequest,
}

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(UpdateNameFormState)
)
