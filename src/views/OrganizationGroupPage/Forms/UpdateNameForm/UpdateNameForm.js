import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form, Button, Input } from 'antd'
import autobind from 'autobind-decorator'

import { hasOrgAdminPermissions } from 'helpers/featurePermissions'

class UpdateNameForm extends Component {
    static propTypes = {
        organizationGroup: PropTypes.shape({
            name: PropTypes.string,
        }).isRequired,
        userPermissions: PropTypes.array.isRequired,
        updatingName: PropTypes.bool.isRequired,
        onCancel: PropTypes.func.isRequired,
        isDirty: PropTypes.bool.isRequired,
        isSaveDisabled: PropTypes.bool.isRequired,

        // antd form
        form: PropTypes.object.isRequired,

        // actions
        updateNameRequest: PropTypes.func.isRequired,
    }

    @autobind
    handleSubmit(e) {
        e.preventDefault()
        this.props.form.validateFields((err, values) => {
            if (!err) {
                const { name } = values
                this.props.updateNameRequest({ name })
            }
        })
    }

    render() {
        const {
            organizationGroup: { name },
            form: { getFieldDecorator },
            userPermissions,
            updatingName,
            isDirty,
            isSaveDisabled,
            onCancel,
        } = this.props

        if (hasOrgAdminPermissions(userPermissions)) {
            return (
                <Form
                    layout="vertical"
                    onSubmit={this.handleSubmit}
                    hideRequiredMark
                >
                    <Form.Item label="Name">
                        {getFieldDecorator('name', {
                            rules: [
                                {
                                    min: 1,
                                    message:
                                        'Name must be at least 1 character',
                                },
                            ],
                            validateTrigger: 'onSubmit',
                        })(<Input />)}
                    </Form.Item>
                    {isDirty && (
                        <Form.Item>
                            <Button
                                type="primary"
                                htmlType="submit"
                                style={{ marginRight: '15px' }}
                                loading={updatingName}
                                disabled={isSaveDisabled || updatingName}
                            >
                                Save
                            </Button>
                            <Button onClick={onCancel}>Cancel</Button>
                        </Form.Item>
                    )}
                </Form>
            )
        }
        return name
    }
}

export default Form.create({
    onValuesChange: (props, changedValues, allValues) =>
        props.onValuesChange(allValues),
    mapPropsToFields: props => {
        const {
            fields: { name },
        } = props
        return {
            name: Form.createFormField({ value: name }),
        }
    },
})(UpdateNameForm)
