import React, { Component } from 'react'
import PropTypes from 'prop-types'
import autobind from 'autobind-decorator'
import isEmpty from 'lodash/isEmpty'

import UpdateNameForm from './UpdateNameForm'

class UpdateNameFormState extends Component {
    static propTypes = {
        organizationGroup: PropTypes.shape({
            name: PropTypes.string,
        }).isRequired,
        userPermissions: PropTypes.array.isRequired,
        updatingName: PropTypes.bool.isRequired,

        // actions
        updateNameRequest: PropTypes.func.isRequired,
    }

    constructor(props) {
        super(props)
        this.state = {
            fields: {
                name: this.props.organizationGroup.name,
            },
        }
    }

    @autobind
    onValuesChange(allValues) {
        this.setState({
            fields: allValues,
        })
    }

    @autobind
    onCancel() {
        this.setState({
            fields: {
                name: this.props.organizationGroup.name,
            },
        })
    }

    isDirty() {
        const {
            fields: { name: stateName },
        } = this.state
        const {
            organizationGroup: { name: propName },
        } = this.props

        return stateName !== propName
    }

    isSaveDisabled() {
        const {
            fields: { name },
        } = this.state
        return isEmpty(name)
    }

    render() {
        return (
            <UpdateNameForm
                onValuesChange={this.onValuesChange}
                onCancel={this.onCancel}
                isDirty={this.isDirty()}
                isSaveDisabled={this.isSaveDisabled()}
                fields={this.state.fields}
                {...this.props}
            />
        )
    }
}

export default UpdateNameFormState
