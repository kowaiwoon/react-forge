import React, { Component } from 'react'
import PropTypes from 'prop-types'
import autobind from 'autobind-decorator'
import sortBy from 'lodash/fp/sortBy'
import isEmpty from 'lodash/isEmpty'

import {
    RESOURCE_TYPE_ALL,
    RESOURCE_TYPE_BRANDS,
    RESOURCE_TYPE_COUNTRIES,
    RESOURCE_TYPE_REGIONS,
} from 'constants/organizations'
import { formatBrandName } from 'helpers/formatting'
import { resourcesRequired } from 'helpers/organizations'

import UpdateResourcesForm from './UpdateResourcesForm'

class UpdateResourcesFormState extends Component {
    static mapPropsToFields(props) {
        const {
            organizationGroup: { brands, regions, countries },
        } = props
        if (regions.length > 0) {
            return {
                resourceType: RESOURCE_TYPE_REGIONS,
                resources: regions.map(region => ({
                    key: region,
                    label: region,
                })),
            }
        } else if (countries.length > 0) {
            return {
                resourceType: RESOURCE_TYPE_COUNTRIES,
                resources: countries.map(country => ({
                    key: country,
                    label: country,
                })),
            }
        } else if (brands.length > 0) {
            return {
                resourceType: RESOURCE_TYPE_BRANDS,
                resources: brands.map(brand => ({
                    key: brand.id,
                    label: formatBrandName(brand),
                })),
            }
        }
        return { resourceType: RESOURCE_TYPE_ALL, resources: [] }
    }

    static propTypes = {
        brands: PropTypes.arrayOf(
            PropTypes.shape({
                value: PropTypes.string,
                label: PropTypes.string,
            })
        ).isRequired,
        organizationGroup: PropTypes.shape({
            brands: PropTypes.array,
            regions: PropTypes.array,
            countries: PropTypes.array,
        }).isRequired,
        brandSearchLoading: PropTypes.bool.isRequired,
        userPermissions: PropTypes.array.isRequired,
        updatingResources: PropTypes.bool.isRequired,

        // actions
        changeBrandSearchInput: PropTypes.func.isRequired,
        updateResourcesRequest: PropTypes.func.isRequired,
    }

    constructor(props) {
        super(props)
        this.state = {
            fields: UpdateResourcesFormState.mapPropsToFields(props),
        }
    }

    @autobind
    onValuesChange(allValues) {
        this.setState({
            fields: allValues,
        })
    }

    @autobind
    onCancel() {
        this.setState({
            fields: UpdateResourcesFormState.mapPropsToFields(this.props),
        })
    }

    isDirty() {
        const {
            fields: {
                resourceType: stateResourceType,
                resources: stateResources,
            },
        } = this.state
        const {
            resourceType: propResourceType,
            resources: propResources,
        } = UpdateResourcesFormState.mapPropsToFields(this.props)

        const sortResources = sortBy(['key'])

        return (
            stateResourceType !== propResourceType ||
            JSON.stringify(sortResources(stateResources)) !==
                JSON.stringify(sortResources(propResources))
        )
    }

    isSaveDisabled() {
        const {
            fields: { resourceType, resources },
        } = this.state
        return resourcesRequired(resourceType) && isEmpty(resources)
    }

    render() {
        return (
            <UpdateResourcesForm
                onValuesChange={this.onValuesChange}
                onCancel={this.onCancel}
                isDirty={this.isDirty()}
                isSaveDisabled={this.isSaveDisabled()}
                fields={this.state.fields}
                {...this.props}
            />
        )
    }
}

export default UpdateResourcesFormState
