import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import {
    selectOrganizationGroup,
    selectOrganizationBrandsForSearch,
    selectOrganizationUserPermissions,
} from 'selectors/orgs'
import { ORGANIZATION_GROUP_PAGE } from 'constants/pages'
import { selectDomainValue as selectUiDomainValue } from 'selectors/ui'
import {
    changeOrganizationGroupPageBrandsSearchInput,
    updateResourcesOrganizationGroupPageRequest,
} from 'actions/ui'

import UpdateResourcesFormState from './UpdateResourcesFormState'

const mapStateToProps = (state, ownProps) => {
    const { organizationGroupId, organizationId } = ownProps.match.params

    return {
        brands: selectOrganizationBrandsForSearch(state, organizationId),
        organizationGroup: selectOrganizationGroup(state, organizationGroupId),
        brandSearchLoading: selectUiDomainValue(state, [
            ORGANIZATION_GROUP_PAGE,
            'brandSearchLoading',
        ]),
        userPermissions: selectOrganizationUserPermissions(
            state,
            organizationId
        ),
        updatingResources: selectUiDomainValue(state, [
            ORGANIZATION_GROUP_PAGE,
            'updatingResources',
        ]),
    }
}

const mapDispatchToProps = {
    changeBrandSearchInput: changeOrganizationGroupPageBrandsSearchInput,
    updateResourcesRequest: updateResourcesOrganizationGroupPageRequest,
}

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(UpdateResourcesFormState)
)
