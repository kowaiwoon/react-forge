import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form, Col, Row, Button } from 'antd'
import autobind from 'autobind-decorator'

import { ResourceTypeField, ResourceDetailsField } from 'components/FormFields'
import { hasOrgAdminPermissions } from 'helpers/featurePermissions'
import {
    RESOURCE_TYPE_ALL,
    RESOURCE_TYPE_BRANDS,
} from 'constants/organizations'

class UpdateResourcesForm extends Component {
    static propTypes = {
        brands: PropTypes.arrayOf(
            PropTypes.shape({
                value: PropTypes.string,
                label: PropTypes.string,
            })
        ).isRequired,
        brandSearchLoading: PropTypes.bool.isRequired,
        userPermissions: PropTypes.array.isRequired,
        updatingResources: PropTypes.bool.isRequired,
        onCancel: PropTypes.func.isRequired,
        isDirty: PropTypes.bool.isRequired,
        isSaveDisabled: PropTypes.bool.isRequired,
        fields: PropTypes.shape({
            resourceType: PropTypes.string,
            resources: PropTypes.array,
        }).isRequired,

        // antd form
        form: PropTypes.object.isRequired,

        // actions
        changeBrandSearchInput: PropTypes.func.isRequired,
        updateResourcesRequest: PropTypes.func.isRequired,
    }

    @autobind
    handleSubmit(e) {
        e.preventDefault()
        this.props.form.validateFields((err, values) => {
            if (!err) {
                const { resourceType } = values

                // to specify "all resources" permissions, any resource
                // type can be sent with an empty array
                const key =
                    resourceType === RESOURCE_TYPE_ALL
                        ? RESOURCE_TYPE_BRANDS
                        : resourceType

                // send an empty array to specify all resources
                const value =
                    resourceType === RESOURCE_TYPE_ALL
                        ? []
                        : values.resources.map(resource => resource.key)

                this.props.updateResourcesRequest({ [key]: value })
            }
        })
    }

    render() {
        const {
            brands,
            form: { getFieldDecorator, setFieldsValue },
            brandSearchLoading,
            changeBrandSearchInput,
            userPermissions,
            updatingResources,
            isDirty,
            isSaveDisabled,
            onCancel,
            fields: { resourceType },
        } = this.props

        return (
            <Form
                layout="vertical"
                onSubmit={this.handleSubmit}
                hideRequiredMark
            >
                <Row gutter={16}>
                    <Col span={12}>
                        <ResourceTypeField
                            getFieldDecorator={getFieldDecorator}
                            setFieldsValue={setFieldsValue}
                            disabled={!hasOrgAdminPermissions(userPermissions)}
                        />
                    </Col>
                    <Col span={12}>
                        <ResourceDetailsField
                            labelInValue
                            getFieldDecorator={getFieldDecorator}
                            selectedResourceType={resourceType}
                            brands={brands}
                            brandSearchLoading={brandSearchLoading}
                            changeBrandSearchInput={changeBrandSearchInput}
                            disabled={!hasOrgAdminPermissions(userPermissions)}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col span={12}>
                        {hasOrgAdminPermissions(userPermissions) &&
                            isDirty && (
                                <div>
                                    <Button
                                        type="primary"
                                        htmlType="submit"
                                        style={{ marginRight: '15px' }}
                                        loading={updatingResources}
                                        disabled={
                                            isSaveDisabled || updatingResources
                                        }
                                    >
                                        Save
                                    </Button>
                                    <Button onClick={onCancel}>Cancel</Button>
                                </div>
                            )}
                    </Col>
                </Row>
            </Form>
        )
    }
}

export default Form.create({
    onValuesChange: (props, changedValues, allValues) =>
        props.onValuesChange(allValues),
    mapPropsToFields: props => {
        const {
            fields: { resourceType, resources },
        } = props
        return {
            resourceType: Form.createFormField({ value: resourceType }),
            resources: Form.createFormField({ value: resources }),
        }
    },
})(UpdateResourcesForm)
