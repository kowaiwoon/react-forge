export {
    default as UpdateResourcesFormContainer,
} from './UpdateResourcesForm/UpdateResourcesFormContainer'
export { default as AddMemberForm } from './AddMemberForm/AddMemberForm'
export {
    default as UpdatePermissionsFormContainer,
} from './UpdatePermissionsForm/UpdatePermissionsFormContainer'
export {
    default as UpdateNameFormContainer,
} from './UpdateNameForm/UpdateNameFormContainer'
