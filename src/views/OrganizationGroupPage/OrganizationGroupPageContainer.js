import { connect } from 'react-redux'

import { selectDomainValue } from 'selectors/ui'
import {
    selectOrganizationGroup,
    selectOrganization,
    selectOrganizationMembers,
    selectOrganizationGroupMembers,
    selectOrganizationUserPermissions,
} from 'selectors/orgs'
import { ORGANIZATION_GROUP_PAGE } from 'constants/pages'
import {
    mountOrganizationGroupPageRequest,
    unmountOrganizationGroupPage,
    addMemberOrganizationGroupPageRequest,
    removeMemberOrganizationGroupPageRequest,
} from 'actions/ui'

import OrganizationGroupPage from './OrganizationGroupPage'

const mapStateToProps = (state, ownProps) => {
    const { organizationGroupId, organizationId } = ownProps.match.params

    return {
        organizationId,
        organizationGroupId,
        organizationGroup: selectOrganizationGroup(state, organizationGroupId),
        organization: selectOrganization(state, organizationId),
        mounting: selectDomainValue(state, [
            ORGANIZATION_GROUP_PAGE,
            'mounting',
        ]),
        organizationMembers: selectOrganizationMembers(state, organizationId),
        groupMembers: selectOrganizationGroupMembers(
            state,
            organizationGroupId
        ),
        addingMember: selectDomainValue(state, [
            ORGANIZATION_GROUP_PAGE,
            'addingMember',
        ]),
        removingMemberId: selectDomainValue(state, [
            ORGANIZATION_GROUP_PAGE,
            'removingMemberId',
        ]),
        userPermissions: selectOrganizationUserPermissions(
            state,
            organizationId
        ),
    }
}

const mapDispatchToProps = {
    mountOrganizationGroupPageRequest,
    unmountOrganizationGroupPage,
    addMemberRequest: addMemberOrganizationGroupPageRequest,
    removeMemberRequest: removeMemberOrganizationGroupPageRequest,
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(OrganizationGroupPage)
