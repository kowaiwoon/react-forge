import React, { Component } from 'react'
import PropTypes from 'prop-types'
import differenceBy from 'lodash/differenceBy'
import { Row, Col, List } from 'antd'
import { generatePath } from 'react-router-dom'
import autobind from 'autobind-decorator'

import { LoadingIndicator } from 'components/LoadingIndicator'
import { ContentCard } from 'components/ContentCard'
import { hasOrgAdminPermissions } from 'helpers/featurePermissions'
import { PageHeader } from 'components/PageHeader'
import { ORGANIZATION_PAGE, PROFILE_PAGE } from 'constants/pages'
import { getPath } from 'helpers/pages'
import { CreateResourceButton, ConfirmDeleteButton } from 'components/Buttons'
import { ActionIcon } from 'components/PaginatedTable'

import {
    AddMemberForm,
    UpdatePermissionsFormContainer,
    UpdateResourcesFormContainer,
    UpdateNameFormContainer,
} from './Forms'
import styles from './styles.scss'

class OrganizationGroupPage extends Component {
    static propTypes = {
        // redux state
        organizationId: PropTypes.string.isRequired,
        organizationGroupId: PropTypes.string.isRequired,
        organizationGroup: PropTypes.object,
        organization: PropTypes.object,
        mounting: PropTypes.bool.isRequired,
        organizationMembers: PropTypes.array,
        addingMember: PropTypes.bool.isRequired,
        groupMembers: PropTypes.array,
        userPermissions: PropTypes.array,
        removingMemberId: PropTypes.string,

        // actions
        mountOrganizationGroupPageRequest: PropTypes.func.isRequired,
        unmountOrganizationGroupPage: PropTypes.func.isRequired,
        addMemberRequest: PropTypes.func.isRequired,
        removeMemberRequest: PropTypes.func.isRequired,
    }

    static defaultProps = {
        organizationGroup: {},
        organization: {},
        organizationMembers: [],
        groupMembers: [],
        userPermissions: [],
        removingMemberId: null,
    }

    state = {
        addMemberFormVisible: false,
    }

    componentDidMount() {
        const {
            organizationId,
            organizationGroupId,
            mountOrganizationGroupPageRequest,
        } = this.props
        mountOrganizationGroupPageRequest({
            organizationId,
            organizationGroupId,
        })
    }

    componentWillUnmount() {
        this.props.unmountOrganizationGroupPage()
    }

    getMemberListItemActions(item) {
        const {
            userPermissions,
            organizationGroup: { name: groupName },
            removingMemberId,
        } = this.props

        if (hasOrgAdminPermissions(userPermissions)) {
            return item.id === removingMemberId
                ? [<ActionIcon type="loading" />]
                : [
                      <ConfirmDeleteButton
                          title={`Are you sure you want to remove ${
                              item.email
                          } from the ${groupName} group?`}
                          onConfirm={() => this.handleRemoveMember(item.id)}
                      />,
                  ]
        }

        return null
    }

    @autobind
    toggleAddMemberFormVisible() {
        this.setState({
            addMemberFormVisible: !this.state.addMemberFormVisible,
        })
    }

    @autobind
    handleRemoveMember(memberId) {
        this.props.removeMemberRequest({ memberId })
    }

    render() {
        const {
            organizationId,
            organization,
            organizationGroup: { name: groupName },
            organizationMembers,
            mounting,
            addingMember,
            addMemberRequest,
            groupMembers,
            userPermissions,
        } = this.props
        const { addMemberFormVisible } = this.state

        if (mounting) {
            return (
                <div className={styles['empty-content']}>
                    <LoadingIndicator size="small" />
                </div>
            )
        }

        const contentCardProps = {
            title: groupName,
            subTitle: "Review and manage your organization's groups",
            ...(hasOrgAdminPermissions(userPermissions)
                ? {
                      actions: [
                          <CreateResourceButton
                              onClick={this.toggleAddMemberFormVisible}
                          >
                              Add New Members
                          </CreateResourceButton>,
                      ],
                      collapseOpen: addMemberFormVisible,
                      collapseContent: (
                          <AddMemberForm
                              members={differenceBy(
                                  organizationMembers,
                                  groupMembers,
                                  'id'
                              )}
                              onCancel={this.toggleAddMemberFormVisible}
                              addingMember={addingMember}
                              addMemberRequest={addMemberRequest}
                          />
                      ),
                  }
                : {}),
        }

        return (
            <React.Fragment>
                <PageHeader
                    breadcrumbs={[
                        { name: 'Profile', url: getPath(PROFILE_PAGE) },
                        {
                            name: organization.name,
                            url: generatePath(getPath(ORGANIZATION_PAGE), {
                                organizationId,
                            }),
                        },
                        { name: groupName },
                    ]}
                    titleComponent={
                        <div className={styles.title}>{groupName}</div>
                    }
                />

                <Row className={styles.content}>
                    <Col span={24}>
                        <ContentCard {...contentCardProps}>
                            <Row gutter={32} className={styles.row}>
                                <Col xs={24} sm={24} md={8} lg={6} xl={6}>
                                    <h3>Group Name</h3>
                                    <UpdateNameFormContainer />
                                </Col>
                                <Col xs={24} sm={24} md={16} lg={16} xl={16}>
                                    <h3>Resources</h3>
                                    <UpdateResourcesFormContainer />
                                </Col>
                            </Row>

                            <Row gutter={32} className={styles.row}>
                                <Col xs={24} sm={24} md={8} lg={6} xl={6}>
                                    <h3>Permissions</h3>
                                    <UpdatePermissionsFormContainer />
                                </Col>
                                <Col xs={24} sm={24} md={8} lg={6} xl={6}>
                                    <h3>
                                        Members {`(${groupMembers.length})`}
                                    </h3>
                                    <List
                                        size="small"
                                        split={false}
                                        dataSource={groupMembers}
                                        renderItem={item => (
                                            <List.Item
                                                actions={this.getMemberListItemActions(
                                                    item
                                                )}
                                            >
                                                {item.email}
                                            </List.Item>
                                        )}
                                    />
                                </Col>
                            </Row>
                        </ContentCard>
                    </Col>
                </Row>
            </React.Fragment>
        )
    }
}

export default OrganizationGroupPage
