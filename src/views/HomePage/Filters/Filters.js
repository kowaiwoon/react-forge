import React from 'react'
import PropTypes from 'prop-types'
import autobind from 'autobind-decorator'
import set from 'lodash/fp/set'

import {
    FACT_TYPES,
    AGGREGATION,
    BRANDS,
    COUNTRIES,
    REGIONS,
    DATES,
} from 'constants/filters'
import {
    AggregationFilter,
    DateRangeFilter,
    AdTypeFilter,
    BrandsFilter,
    CountriesFilter,
    RegionsFilter,
} from 'components/Filters'
import { FilterGroup } from 'components/FilterGroup'

class Filters extends React.Component {
    static propTypes = {
        filterSettings: PropTypes.shape().isRequired,
        selectedFilterValues: PropTypes.shape().isRequired,
        brandsFilterOptions: PropTypes.array.isRequired,
        brandsFilterLoading: PropTypes.bool.isRequired,

        // actions
        updatePageFilter: PropTypes.func.isRequired,
        resetFilters: PropTypes.func.isRequired,
        updateFilterSettings: PropTypes.func.isRequired,
        changeBrandsFilterInput: PropTypes.func.isRequired,
    }

    @autobind
    handleApply(key, value) {
        this.props.updatePageFilter({
            key,
            value,
        })
    }

    @autobind
    handleClose(filter) {
        const { filterSettings } = this.props
        this.props.updateFilterSettings(
            set(['displayState', filter], false, filterSettings)
        )
    }

    render() {
        const {
            filterSettings,
            brandsFilterLoading,
            brandsFilterOptions,
            selectedFilterValues,
            changeBrandsFilterInput,
            updateFilterSettings,
            resetFilters,
        } = this.props
        return (
            <FilterGroup
                filterSettings={filterSettings}
                updateFilterSettings={updateFilterSettings}
                resetFilters={resetFilters}
            >
                <AggregationFilter
                    value={selectedFilterValues[AGGREGATION]}
                    onApply={this.handleApply}
                    closable={false}
                />
                <DateRangeFilter
                    values={selectedFilterValues[DATES]}
                    onApply={this.handleApply}
                    closable={false}
                />
                <AdTypeFilter
                    values={selectedFilterValues[FACT_TYPES]}
                    onApply={this.handleApply}
                    closable={false}
                />
                <BrandsFilter
                    values={selectedFilterValues[BRANDS]}
                    options={brandsFilterOptions}
                    onApply={this.handleApply}
                    onCloseFilter={this.handleClose}
                    closable
                    onChangeInput={changeBrandsFilterInput}
                    loading={brandsFilterLoading}
                />
                <CountriesFilter
                    values={selectedFilterValues[COUNTRIES]}
                    onApply={this.handleApply}
                    onCloseFilter={this.handleClose}
                    closable
                />
                <RegionsFilter
                    values={selectedFilterValues[REGIONS]}
                    onApply={this.handleApply}
                    onCloseFilter={this.handleClose}
                    closable
                />
            </FilterGroup>
        )
    }
}

export default Filters
