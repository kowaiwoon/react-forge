import { compose } from 'redux'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import {
    selectDomainValue as selectUiDomainValue,
    selectPageFilters,
    selectPageFilterSettings,
} from 'selectors/ui'
import {
    selectBrandPageCampaignsForFilters,
    selectBrand,
} from 'selectors/entities'
import {
    updatePageFilterForPage,
    resetPageFiltersForPage,
    updatePageFilterSettingsForPage,
    changeCampaignsFilterInputForPage,
} from 'actions/ui'
import { BRAND_PAGE } from 'constants/pages'

import Filters from './Filters'

const mapStateToProps = (state, ownProps) => {
    const { brandId } = ownProps.match.params

    return {
        brand: selectBrand(state, brandId),
        filterSettings: selectPageFilterSettings(state, BRAND_PAGE),
        selectedFilterValues: selectPageFilters(state, BRAND_PAGE),
        campaignsFilterOptions: selectBrandPageCampaignsForFilters(state),
        campaignsFilterLoading: selectUiDomainValue(state, [
            'app',
            'campaignsFilterLoading',
        ]),
    }
}

const mapDispatchToProps = {
    updatePageFilter: updatePageFilterForPage(BRAND_PAGE),
    resetFilters: resetPageFiltersForPage(BRAND_PAGE),
    updateFilterSettings: updatePageFilterSettingsForPage(BRAND_PAGE),
    changeCampaignsFilterInput: changeCampaignsFilterInputForPage(BRAND_PAGE),
}

const FiltersContainer = compose(
    withRouter,
    connect(
        mapStateToProps,
        mapDispatchToProps
    )
)(Filters)

export default FiltersContainer
