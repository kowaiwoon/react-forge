import React from 'react'
import PropTypes from 'prop-types'
import autobind from 'autobind-decorator'
import set from 'lodash/fp/set'

import {
    FACT_TYPES,
    AGGREGATION,
    DATES,
    CAMPAIGNS,
    CAMPAIGN_BUDGET,
    CAMPAIGN_STATES,
    CAMPAIGN_TARGETING_TYPES,
    CAMPAIGN_DAYPARTINGS,
} from 'constants/filters'
import {
    AggregationFilter,
    DateRangeFilter,
    AdTypeFilter,
    CampaignsFilter,
    CampaignStatesFilter,
    CampaignBudgetFilter,
    CampaignTargetingTypesFilter,
    CampaignDaypartingsFilter,
    CampaignNameFilter,
} from 'components/Filters'
import { FilterGroup } from 'components/FilterGroup'
import { formatCurrency } from 'helpers/formatting'

class Filters extends React.Component {
    static propTypes = {
        brand: PropTypes.shape({
            currency_code: PropTypes.string,
        }),
        filterSettings: PropTypes.shape().isRequired,
        selectedFilterValues: PropTypes.shape().isRequired,
        campaignsFilterOptions: PropTypes.array.isRequired,
        campaignsFilterLoading: PropTypes.bool.isRequired,

        // actions
        updatePageFilter: PropTypes.func.isRequired,
        resetFilters: PropTypes.func.isRequired,
        updateFilterSettings: PropTypes.func.isRequired,
        changeCampaignsFilterInput: PropTypes.func.isRequired,
    }

    static defaultProps = {
        brand: {},
    }

    @autobind
    handleApply(key, value) {
        this.props.updatePageFilter({
            key,
            value,
        })
    }

    @autobind
    handleClose(filter) {
        const { filterSettings } = this.props
        this.props.updateFilterSettings(
            set(['displayState', filter], false, filterSettings)
        )
    }

    render() {
        const {
            brand,
            filterSettings,
            selectedFilterValues,
            campaignsFilterLoading,
            campaignsFilterOptions,
            changeCampaignsFilterInput,
            updateFilterSettings,
            resetFilters,
        } = this.props
        return (
            <FilterGroup
                filterSettings={filterSettings}
                updateFilterSettings={updateFilterSettings}
                resetFilters={resetFilters}
            >
                <CampaignNameFilter
                    onApply={this.handleApply}
                    closable={false}
                />
                <AggregationFilter
                    value={selectedFilterValues[AGGREGATION]}
                    onApply={this.handleApply}
                    closable={false}
                />
                <DateRangeFilter
                    values={selectedFilterValues[DATES]}
                    onApply={this.handleApply}
                    closable={false}
                />
                <AdTypeFilter
                    values={selectedFilterValues[FACT_TYPES]}
                    onApply={this.handleApply}
                    closable={false}
                />
                <CampaignsFilter
                    values={selectedFilterValues[CAMPAIGNS]}
                    options={campaignsFilterOptions}
                    onApply={this.handleApply}
                    onCloseFilter={this.handleClose}
                    closable
                    onChangeInput={changeCampaignsFilterInput}
                    loading={campaignsFilterLoading}
                />
                <CampaignBudgetFilter
                    value={selectedFilterValues[CAMPAIGN_BUDGET].value}
                    onApply={this.handleApply}
                    onCloseFilter={this.handleClose}
                    closable
                    operator={selectedFilterValues[CAMPAIGN_BUDGET].operator}
                    valueFormatter={value =>
                        formatCurrency(value, {
                            decimal: true,
                            currencyCode: brand.currency_code,
                        })
                    }
                />
                <CampaignStatesFilter
                    values={selectedFilterValues[CAMPAIGN_STATES]}
                    onApply={this.handleApply}
                    onCloseFilter={this.handleClose}
                    closable
                />
                <CampaignTargetingTypesFilter
                    values={selectedFilterValues[CAMPAIGN_TARGETING_TYPES]}
                    onApply={this.handleApply}
                    onCloseFilter={this.handleClose}
                    closable
                />
                <CampaignDaypartingsFilter
                    values={selectedFilterValues[CAMPAIGN_DAYPARTINGS]}
                    onApply={this.handleApply}
                    onCloseFilter={this.handleClose}
                    closable
                />
            </FilterGroup>
        )
    }
}

export default Filters
