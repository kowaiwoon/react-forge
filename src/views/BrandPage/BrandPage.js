import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Tabs, Row, Col } from 'antd'

import { PageHeader } from 'components/PageHeader'
import { MetricSummaryPanel } from 'components/MetricSummaryPanel'
import { ResourceDetails } from 'components/ResourceDetails'
import { LoadingIndicator } from 'components/LoadingIndicator'
import { BRANDS_SUMMARY_PAGE } from 'constants/pages'
import { COUNTRY_CODES, REGION_CODES } from 'constants/codes'
import { getPath } from 'helpers/pages'
import { brandAttributeTooltips } from 'configuration/attributes'

import {
    CampaignsChartContainer,
    ReachChartContainer,
    ConversionsChartContainer,
    RoiChartContainer,
} from './Charts'
import { CampaignsTableContainer } from './Tables'
import { FiltersContainer } from './Filters'
import styles from './styles.scss'

const renderTooltipWithHelptext = attribute => {
    const info = brandAttributeTooltips[attribute]

    return info.map(item => {
        const { title, description } = item

        return (
            <div className={styles.tooltip} key={description}>
                {title && <span className={styles.title}>{title}</span>}
                <span className={styles.description}>{description}</span>
            </div>
        )
    })
}

class BrandPage extends Component {
    static propTypes = {
        // Redux state
        brandId: PropTypes.string.isRequired,
        brand: PropTypes.shape({
            brand_name: PropTypes.string,
            region: PropTypes.string,
            country_code: PropTypes.string,
            currency_code: PropTypes.string,
            timezone: PropTypes.string,
        }),
        showDetails: PropTypes.bool.isRequired,
        brandAggregate: PropTypes.shape().isRequired,
        mounting: PropTypes.bool.isRequired,

        // Redux actions
        mountBrandPage: PropTypes.func.isRequired,
        unmountBrandPage: PropTypes.func.isRequired,
        toggleBrandPageDetails: PropTypes.func.isRequired,

        // tab state
        tab: PropTypes.oneOf([
            'roi',
            'reach',
            'conversions',
            'treemap',
            'table',
        ]).isRequired,
        handleTabChange: PropTypes.func.isRequired,
    }

    static defaultProps = {
        brand: {},
    }

    componentDidMount() {
        const { brandId } = this.props

        this.props.mountBrandPage({ brandId })
    }

    componentWillUnmount() {
        this.props.unmountBrandPage()
    }

    getHeaderName() {
        const { brand } = this.props
        const {
            seller_string_id: sellerStringId,
            brand_name: brandName,
        } = brand

        return sellerStringId ? `Seller Account ${sellerStringId}` : brandName
    }

    render() {
        const {
            brand,
            mounting,
            showDetails,
            brandAggregate,
            toggleBrandPageDetails,
            tab,
            handleTabChange,
        } = this.props

        if (mounting) {
            return (
                <div className={styles['empty-content']}>
                    <LoadingIndicator size="small" />
                </div>
            )
        }

        const headerName = this.getHeaderName()

        return (
            <React.Fragment>
                <PageHeader
                    breadcrumbs={[
                        {
                            name: 'All Brands',
                            url: getPath(BRANDS_SUMMARY_PAGE),
                        },
                        { name: headerName, icon: 'shop' },
                    ]}
                    titleComponent={
                        <ResourceDetails
                            name={brand.brand_name}
                            showDetails={showDetails}
                            allowEditing={false}
                            editToolTip="Modifying Brand is not yet supported."
                            onShowDetailsClick={toggleBrandPageDetails}
                            details={[
                                {
                                    label: 'Country Code',
                                    value: brand.country_code,
                                    toolTip: renderTooltipWithHelptext(
                                        'country_code'
                                    ),
                                    formatValue: value => COUNTRY_CODES[value],
                                },
                                {
                                    label: 'Currency Code',
                                    value: brand.currency_code,
                                    toolTip: renderTooltipWithHelptext(
                                        'currency_code'
                                    ),
                                },
                                {
                                    label: 'Region',
                                    value: brand.region,
                                    toolTip: renderTooltipWithHelptext(
                                        'region'
                                    ),
                                    formatValue: value => REGION_CODES[value],
                                },
                                {
                                    label: 'Timezone',
                                    value: brand.timezone,
                                    toolTip: renderTooltipWithHelptext(
                                        'timezone'
                                    ),
                                },
                            ]}
                        />
                    }
                    filterGroupComponent={<FiltersContainer />}
                />

                <Tabs
                    activeKey={tab}
                    onChange={handleTabChange}
                    tabPosition="top"
                    size="default"
                    tabBarStyle={{
                        fontWeight: '500', // same font-weight as selected tab
                    }}
                >
                    <Tabs.TabPane
                        tab="Return on Investment"
                        key="roi"
                        forceRender
                    >
                        <MetricSummaryPanel
                            loading={brandAggregate.loading}
                            data={brandAggregate.data}
                            category="roi"
                        />
                        <Row>
                            <Col span={24} className={styles['row-spacing']}>
                                <RoiChartContainer />
                            </Col>
                        </Row>
                    </Tabs.TabPane>

                    <Tabs.TabPane tab="Reach" key="reach" forceRender>
                        <MetricSummaryPanel
                            loading={brandAggregate.loading}
                            data={brandAggregate.data}
                            category="reach"
                        />
                        <Row className={styles['row-spacing']}>
                            <Col span={24}>
                                <ReachChartContainer />
                            </Col>
                        </Row>
                    </Tabs.TabPane>

                    <Tabs.TabPane
                        tab="Conversions"
                        key="conversions"
                        forceRender
                    >
                        <MetricSummaryPanel
                            loading={brandAggregate.loading}
                            data={brandAggregate.data}
                            category="conversions"
                        />
                        <Row className={styles['row-spacing']}>
                            <Col span={24}>
                                <ConversionsChartContainer />
                            </Col>
                        </Row>
                    </Tabs.TabPane>

                    <Tabs.TabPane
                        tab="Campaigns Tree Chart"
                        key="treemap"
                        forceRender
                    >
                        <CampaignsChartContainer />
                    </Tabs.TabPane>

                    <Tabs.TabPane tab="Campaigns Table" key="table" forceRender>
                        <CampaignsTableContainer />
                    </Tabs.TabPane>
                </Tabs>
            </React.Fragment>
        )
    }
}

export default BrandPage
