import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import { BRAND_PAGE } from 'constants/pages'
import { selectBrand } from 'selectors/entities'
import {
    selectDomainValue as selectUiDomainValue,
    selectPageDownloading,
} from 'selectors/ui'
import {
    updateBrandPageTablePagination,
    updateBrandPageTableSorter,
    updateBrandPageTableSettings,
    fetchBrandPageTableRequest,
    downloadBrandPageTableRequest,
    updateBrandPageTableRequest,
    deleteBrandPageTableRequest,
} from 'actions/ui'

import CampaignsTable from './CampaignsTable'

const mapStateToProps = (state, ownProps) => {
    const { brandId } = ownProps.match.params

    return {
        brand: selectBrand(state, brandId),
        tableData: selectUiDomainValue(state, [BRAND_PAGE, 'table']),
        downloading: selectPageDownloading(state, BRAND_PAGE),
    }
}

const mapDispatchToProps = {
    updatePagination: updateBrandPageTablePagination,
    updateSorter: updateBrandPageTableSorter,
    updateColumnSettings: updateBrandPageTableSettings,
    reloadData: fetchBrandPageTableRequest,
    downloadData: downloadBrandPageTableRequest,
    updateCampaignRequest: updateBrandPageTableRequest,
    deleteCampaignRequest: deleteBrandPageTableRequest,
}

const CampaignsTableContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(CampaignsTable)

export default withRouter(CampaignsTableContainer)
