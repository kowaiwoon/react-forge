import { connect } from 'react-redux'

import { BRAND_PAGE } from 'constants/pages'
import { selectBrand } from 'selectors/entities'
import { selectDomainValue as selectUiDomainValue } from 'selectors/ui'
import {
    mountBrandPageRequest,
    unmountBrandPage,
    toggleBrandPageDetails,
} from 'actions/ui'
import { withTabState } from 'components/HigherOrderComponents'

import BrandPage from './BrandPage'

const mapStateToProps = (state, ownProps) => {
    const { brandId } = ownProps.match.params

    return {
        brandId,
        brand: selectBrand(state, brandId),
        showDetails: selectUiDomainValue(state, [BRAND_PAGE, 'showDetails']),
        brandAggregate: selectUiDomainValue(state, [BRAND_PAGE, 'aggregate']),
        mounting: selectUiDomainValue(state, [BRAND_PAGE, 'mounting']),
    }
}

const mapDispatchToProps = {
    mountBrandPage: mountBrandPageRequest,
    unmountBrandPage,
    toggleBrandPageDetails,
}

const BrandPageContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(BrandPage)

export default withTabState(BrandPageContainer, 'roi')
