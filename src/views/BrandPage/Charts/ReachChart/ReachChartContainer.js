import { connect } from 'react-redux'

import {
    selectChartMetrics,
    selectPageFactTypes,
    selectDataForStackedBarChart,
    selectLoadingForStackedBarChart,
    selectPageDownloading,
} from 'selectors/ui'
import {
    updateChartMetrics,
    downloadBrandPageTimeseriesRequest,
} from 'actions/ui'
import { BRAND_PAGE } from 'constants/pages'
import { REACH } from 'constants/charts'

import ReachChart from './ReachChart'

const mapStateToProps = state => {
    const chartMetrics = selectChartMetrics(state, BRAND_PAGE, REACH)
    const factTypes = selectPageFactTypes(state, BRAND_PAGE)
    const loading = selectLoadingForStackedBarChart(state, BRAND_PAGE)
    const downloading = selectPageDownloading(state, BRAND_PAGE)
    const { axes, series } = selectDataForStackedBarChart(
        state,
        BRAND_PAGE,
        REACH
    )

    return {
        chartMetrics,
        factTypes,
        axes,
        series,
        loading,
        downloading,
    }
}

const mapDispatchToProps = {
    updateChartMetrics,
    downloadData: downloadBrandPageTimeseriesRequest,
}

const ReachChartContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ReachChart)

export default ReachChartContainer
