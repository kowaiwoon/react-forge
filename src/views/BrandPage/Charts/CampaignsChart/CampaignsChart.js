import React from 'react'
import PropTypes from 'prop-types'

import { PaginatedTreemap } from 'components/PaginatedTreemap'
import { getTreemapPaginationOptions } from 'helpers/pagination'

const propTypes = {
    // React router
    history: PropTypes.shape({
        push: PropTypes.func,
    }).isRequired,

    // Redux state
    brandId: PropTypes.string.isRequired,
    factTypes: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.string,
            label: PropTypes.string,
        })
    ).isRequired,
    data: PropTypes.array.isRequired,
    loading: PropTypes.bool.isRequired,
    pagination: PropTypes.object.isRequired,
    sorter: PropTypes.object.isRequired,

    // Redux actions
    updateBrandTreemapPagination: PropTypes.func.isRequired,
    updateBrandTreemapSorter: PropTypes.func.isRequired,
    fetchBrandTreemapRequest: PropTypes.func.isRequired,
}

const defaultProps = {}

const CampaignsChart = ({
    history,
    brandId,
    factTypes,
    data,
    loading,
    pagination,
    sorter,
    updateBrandTreemapPagination,
    updateBrandTreemapSorter,
    fetchBrandTreemapRequest,
}) => (
    <PaginatedTreemap
        treemapOptions={{
            name: 'campaign',
            idAttribute: 'campaign.id',
            titleAttribute: 'campaign.name',
            height: 500,
            colors: {
                minColor: '#c4e1ea',
                maxColor: '#8884d8',
            },
        }}
        defaultPagination={getTreemapPaginationOptions('Campaigns')}
        factTypes={factTypes}
        data={data}
        loading={loading}
        pagination={pagination}
        sorter={sorter}
        updatePagination={updateBrandTreemapPagination}
        updateSorter={updateBrandTreemapSorter}
        reloadData={fetchBrandTreemapRequest}
        onLeafClick={leafNodeId => {
            history.push(`/brands/${brandId}/campaigns/${leafNodeId}`)
        }}
    />
)

CampaignsChart.propTypes = propTypes
CampaignsChart.defaultProps = defaultProps

export default CampaignsChart
