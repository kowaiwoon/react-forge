import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import { BRAND_PAGE } from 'constants/pages'
import {
    selectPageFactTypes,
    selectTreemapChartData,
    selectTreemap,
} from 'selectors/ui'
import {
    updateBrandPageTreemapPagination,
    updateBrandPageTreemapSorter,
    fetchBrandPageTreemapRequest,
} from 'actions/ui'

import CampaignsChart from './CampaignsChart'

const mapStateToProps = (state, ownProps) => {
    const { brandId } = ownProps.match.params
    const { loading, pagination, sorter } = selectTreemap(
        state,
        BRAND_PAGE,
        'treemap'
    )

    return {
        brandId,
        factTypes: selectPageFactTypes(state, BRAND_PAGE),
        data: selectTreemapChartData(state, BRAND_PAGE, 'treemap'),
        loading,
        pagination,
        sorter,
    }
}

const mapDispatchToProps = {
    updateBrandTreemapPagination: updateBrandPageTreemapPagination,
    updateBrandTreemapSorter: updateBrandPageTreemapSorter,
    fetchBrandTreemapRequest: fetchBrandPageTreemapRequest,
}

const CampaignsChartContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(CampaignsChart)

export default withRouter(CampaignsChartContainer)
