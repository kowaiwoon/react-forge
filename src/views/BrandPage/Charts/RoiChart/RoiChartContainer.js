import { connect } from 'react-redux'

import {
    selectChartMetrics,
    selectPageFactTypes,
    selectDataForStackedBarChart,
    selectLoadingForStackedBarChart,
    selectPageDownloading,
} from 'selectors/ui'
import {
    updateChartMetrics,
    downloadHomePageTimeseriesRequest,
} from 'actions/ui'
import { BRAND_PAGE } from 'constants/pages'
import { ROI } from 'constants/charts'

import RoiChart from './RoiChart'

const mapStateToProps = state => {
    const chartMetrics = selectChartMetrics(state, BRAND_PAGE, ROI)
    const factTypes = selectPageFactTypes(state, BRAND_PAGE)
    const loading = selectLoadingForStackedBarChart(state, BRAND_PAGE)
    const downloading = selectPageDownloading(state, BRAND_PAGE)
    const { axes, series } = selectDataForStackedBarChart(
        state,
        BRAND_PAGE,
        ROI
    )

    return {
        chartMetrics,
        factTypes,
        axes,
        series,
        loading,
        downloading,
    }
}

const mapDispatchToProps = {
    updateChartMetrics,
    downloadData: downloadHomePageTimeseriesRequest,
}

const RoiChartContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(RoiChart)

export default RoiChartContainer
