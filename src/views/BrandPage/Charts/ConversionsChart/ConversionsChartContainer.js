import { connect } from 'react-redux'

import {
    selectChartMetrics,
    selectPageFactTypes,
    selectDataForStackedBarChart,
    selectLoadingForStackedBarChart,
    selectPageDownloading,
} from 'selectors/ui'
import {
    updateChartMetrics,
    downloadBrandPageTimeseriesRequest,
} from 'actions/ui'
import { BRAND_PAGE } from 'constants/pages'
import { CONVERSIONS } from 'constants/charts'

import ConversionsChart from './ConversionsChart'

const mapStateToProps = state => {
    const chartMetrics = selectChartMetrics(state, BRAND_PAGE, CONVERSIONS)
    const factTypes = selectPageFactTypes(state, BRAND_PAGE)
    const loading = selectLoadingForStackedBarChart(state, BRAND_PAGE)
    const downloading = selectPageDownloading(state, BRAND_PAGE)
    const { axes, series } = selectDataForStackedBarChart(
        state,
        BRAND_PAGE,
        CONVERSIONS
    )

    return {
        chartMetrics,
        factTypes,
        axes,
        series,
        loading,
        downloading,
    }
}

const mapDispatchToProps = {
    updateChartMetrics,
    downloadData: downloadBrandPageTimeseriesRequest,
}

const ConversionsChartContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ConversionsChart)

export default ConversionsChartContainer
