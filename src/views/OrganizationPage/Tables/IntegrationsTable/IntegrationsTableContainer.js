import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import { ORGANIZATION_PAGE } from 'constants/pages'
import { selectDomainValue } from 'selectors/ui'
import { selectOrganizationUserPermissions } from 'selectors/orgs'
import {
    updateOrganizationPageIntegrationsTablePagination,
    fetchOrganizationPageIntegrationsTableRequest,
} from 'actions/ui'

import IntegrationsTable from './IntegrationsTable'

const mapStateToProps = (state, ownProps) => {
    const { organizationId } = ownProps.match.params
    return {
        tableData: selectDomainValue(state, [
            ORGANIZATION_PAGE,
            'integrationsTable',
        ]),
        userPermissions: selectOrganizationUserPermissions(
            state,
            organizationId
        ),
    }
}

const mapDispatchToProps = {
    updatePagination: updateOrganizationPageIntegrationsTablePagination,
    reloadData: fetchOrganizationPageIntegrationsTableRequest,
}

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(IntegrationsTable)
)
