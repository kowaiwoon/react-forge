import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ReactJoyride, { STATUS } from 'react-joyride'
import noop from 'lodash/noop'
import autobind from 'autobind-decorator'
import isEmpty from 'lodash/isEmpty'
import isNull from 'lodash/isNull'

import moment from 'utilities/moment'
import { PaginatedTable } from 'components/PaginatedTable'
import { getTablePaginationOptions } from 'helpers/pagination'
import { hasOrgAdminPermissions } from 'helpers/featurePermissions'
import { ContentCard } from 'components/ContentCard'
import { CreateResourceButton } from 'components/Buttons'

import { CreateIntegrationFormContainer } from '../../Forms'

class IntegrationsTable extends Component {
    static propTypes = {
        tab: PropTypes.string.isRequired,

        // redux state
        tableData: PropTypes.object.isRequired,
        userPermissions: PropTypes.array.isRequired,
        updatePagination: PropTypes.func.isRequired,
        reloadData: PropTypes.func.isRequired,
    }

    state = {
        collapseOpen: false,
        run: false,
        stepIndex: 0,
        steps: [
            {
                title: 'Create an Integration',
                content: (
                    <p>
                        Click here to create an Amazon Advertising Integration
                        for your Organization.
                    </p>
                ),
                placement: 'left',
                target: '.create-integrations-btn',
                spotlightClicks: true,
            },
        ],
    }

    componentDidMount() {
        this.setInitialWalkThroughState()
    }

    componentDidUpdate(prevProps, prevState) {
        this.updateWalkThroughState(prevProps, prevState)
    }

    componentWillUnmount() {
        if (!isNull(this.timer)) {
            clearTimeout(this.timer)
        }
    }

    setInitialWalkThroughState() {
        const {
            tab,
            tableData: { data: integrations },
        } = this.props

        if (tab === 'integrations' && isEmpty(integrations)) {
            this.setState({ run: true })
        }
    }

    timer = null

    updateWalkThroughState(prevProps, prevState) {
        const {
            tab,
            tableData: { data: integrations },
        } = this.props

        if (isEmpty(integrations)) {
            if (prevState.run && tab !== 'integrations') {
                this.setState({ run: false })
            } else if (!prevState.run && tab === 'integrations') {
                // delay the state change so page can switch to tab
                this.timer = setTimeout(() => {
                    this.setState({ run: true })
                }, 500)
            }
        }
    }

    @autobind
    handleToggleCollapse() {
        this.setState({
            collapseOpen: !this.state.collapseOpen,
            run: this.state.collapseOpen,
        })
    }

    @autobind
    handleJoyrideCallback(data) {
        const { status } = data

        if (status === STATUS.FINISHED) {
            this.setState({ run: false })
        }
    }

    render() {
        const {
            tableData,
            userPermissions,
            updatePagination,
            reloadData,
        } = this.props
        const { collapseOpen, run, stepIndex, steps } = this.state

        return (
            <React.Fragment>
                <ReactJoyride
                    hideBackButton
                    disableScrolling
                    disableScrollParentFix // prevents overflow=initial from being set in parent div
                    callback={this.handleJoyrideCallback}
                    run={run}
                    stepIndex={stepIndex}
                    steps={steps}
                    styles={{
                        options: {
                            zIndex: 10000,
                        },
                    }}
                />
                <ContentCard
                    title="Amazon Advertising Integrations"
                    subTitle="Review and manage your organization's integrations."
                    actions={
                        hasOrgAdminPermissions(userPermissions)
                            ? [
                                  <CreateResourceButton
                                      onClick={this.handleToggleCollapse}
                                      className="create-integrations-btn"
                                  >
                                      Add New Integrations
                                  </CreateResourceButton>,
                              ]
                            : []
                    }
                    collapseOpen={collapseOpen}
                    collapseContent={
                        hasOrgAdminPermissions(userPermissions) && (
                            <CreateIntegrationFormContainer
                                onCancel={this.handleToggleCollapse}
                            />
                        )
                    }
                >
                    <PaginatedTable
                        tableOptions={{
                            name: 'integrations',
                            rowKey: 'id',
                            columns: {
                                description: {
                                    title: 'Integration Description',
                                    dataIndex: 'alias',
                                },
                                state: {
                                    title: 'State',
                                    dataIndex: 'active',
                                    render: active =>
                                        active ? 'Active' : 'Inactive',
                                },
                                created: {
                                    title: 'Created Date',
                                    dataIndex: 'created_at',
                                    render: utcDateString => (
                                        <div>
                                            {moment(utcDateString)
                                                .local()
                                                .format(
                                                    'MM/DD/YYYY, h:mm:ss a'
                                                )}
                                        </div>
                                    ),
                                },
                            },
                        }}
                        data={tableData.data}
                        loading={tableData.loading}
                        updating={tableData.updating}
                        deleting={tableData.deleting}
                        pagination={{
                            ...getTablePaginationOptions('Integrations'),
                            ...tableData.pagination,
                        }}
                        sorter={tableData.sorter}
                        columnSettings={tableData.columnSettings}
                        updatePagination={updatePagination}
                        updateSorter={noop}
                        reloadData={reloadData}
                    />
                </ContentCard>
            </React.Fragment>
        )
    }
}

export default IntegrationsTable
