import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import { ORGANIZATION_PAGE } from 'constants/pages'
import { selectDomainValue } from 'selectors/ui'
import {
    selectOrganizationOwner,
    selectOrganizationUserPermissions,
} from 'selectors/orgs'
import {
    updateOrganizationPageMembersTablePagination,
    fetchOrganizationPageMembersTableRequest,
    removeMemberOrganizationPageRequest,
} from 'actions/ui'

import MembersTable from './MembersTable'

const mapStateToProps = (state, ownProps) => {
    const { organizationId } = ownProps.match.params
    return {
        tableData: selectDomainValue(state, [
            ORGANIZATION_PAGE,
            'membersTable',
        ]),
        owner: selectOrganizationOwner(state, organizationId),
        userPermissions: selectOrganizationUserPermissions(
            state,
            organizationId
        ),
    }
}

const mapDispatchToProps = {
    updatePagination: updateOrganizationPageMembersTablePagination,
    reloadData: fetchOrganizationPageMembersTableRequest,
    removeMember: removeMemberOrganizationPageRequest,
}

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(MembersTable)
)
