import React, { Component } from 'react'
import PropTypes from 'prop-types'
import noop from 'lodash/noop'

import { PaginatedTable, ActionIcon } from 'components/PaginatedTable'
import { ConfirmDeleteButton } from 'components/Buttons'
import { getTablePaginationOptions } from 'helpers/pagination'
import { hasOrgAdminPermissions } from 'helpers/featurePermissions'
import { ContentCard } from 'components/ContentCard'

import styles from './styles.scss'

class MembersTable extends Component {
    static propTypes = {
        tableData: PropTypes.object.isRequired,
        owner: PropTypes.object.isRequired,
        updatePagination: PropTypes.func.isRequired,
        reloadData: PropTypes.func.isRequired,
        removeMember: PropTypes.func.isRequired,
        userPermissions: PropTypes.array.isRequired,
    }

    getTableOptions() {
        const {
            owner,
            removeMember,
            userPermissions,
            tableData: { deletingMemberId },
        } = this.props
        return {
            name: 'members',
            rowKey: 'id',
            columns: {
                actions: {
                    title: 'Actions',
                    dataIndex: 'id',
                    fixed: 'right',
                    align: 'center',
                    render: memberId =>
                        hasOrgAdminPermissions(userPermissions) &&
                        memberId !== owner.id &&
                        (memberId === deletingMemberId ? (
                            <ActionIcon type="loading" />
                        ) : (
                            <ConfirmDeleteButton
                                title="Are you sure you want to remove this member?"
                                onConfirm={() => removeMember({ memberId })}
                            />
                        )),
                },
                email: {
                    title: 'Email Address',
                    dataIndex: 'email',
                    align: 'left',
                    render: (text, record) => (
                        <div>
                            {text}
                            {owner.id === record.id && (
                                <span className={styles.muted}> (Owner)</span>
                            )}
                        </div>
                    ),
                },
            },
        }
    }

    render() {
        const { tableData, updatePagination, reloadData } = this.props
        return (
            <ContentCard
                title="Members"
                subTitle="Review and manage your organization's members."
            >
                <PaginatedTable
                    tableOptions={this.getTableOptions()}
                    data={tableData.data}
                    loading={tableData.loading}
                    updating={tableData.updating}
                    deleting={tableData.deleting}
                    pagination={{
                        ...getTablePaginationOptions('Members'),
                        ...tableData.pagination,
                    }}
                    sorter={tableData.sorter}
                    columnSettings={tableData.columnSettings}
                    updatePagination={updatePagination}
                    updateSorter={noop}
                    reloadData={reloadData}
                />
            </ContentCard>
        )
    }
}

export default MembersTable
