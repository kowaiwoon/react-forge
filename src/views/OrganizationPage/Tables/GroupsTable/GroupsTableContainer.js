import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import { ORGANIZATION_PAGE } from 'constants/pages'
import { selectDomainValue as selectUiDomainValue } from 'selectors/ui'
import { selectOrganizationUserPermissions } from 'selectors/orgs'
import {
    updateOrganizationPageGroupsTablePagination,
    fetchOrganizationPageGroupsTableRequest,
} from 'actions/ui'

import GroupsTable from './GroupsTable'

const mapStateToProps = (state, ownProps) => {
    const { organizationId } = ownProps.match.params
    return {
        tableData: selectUiDomainValue(state, [
            ORGANIZATION_PAGE,
            'groupsTable',
        ]),
        userPermissions: selectOrganizationUserPermissions(
            state,
            organizationId
        ),
    }
}

const mapDispatchToProps = {
    updatePagination: updateOrganizationPageGroupsTablePagination,
    reloadData: fetchOrganizationPageGroupsTableRequest,
}

const GroupsTableContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(GroupsTable)

export default withRouter(GroupsTableContainer)
