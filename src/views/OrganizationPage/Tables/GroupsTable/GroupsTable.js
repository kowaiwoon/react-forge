import React, { Component } from 'react'
import PropTypes from 'prop-types'
import noop from 'lodash/noop'
import { generatePath } from 'react-router-dom'
import autobind from 'autobind-decorator'

import { PaginatedTable } from 'components/PaginatedTable'
import { getTablePaginationOptions } from 'helpers/pagination'
import { formatBrandName } from 'helpers/formatting'
import { AppLink } from 'components/AppLink'
import { getPath } from 'helpers/pages'
import { ORGANIZATION_GROUP_PAGE } from 'constants/pages'
import { hasOrgAdminPermissions } from 'helpers/featurePermissions'
import {
    RESOURCE_TYPE_REGIONS,
    RESOURCE_TYPE_COUNTRIES,
    RESOURCE_TYPE_BRANDS,
    RESOURCE_TYPE_LABELS,
    RESOURCE_TYPE_ALL,
} from 'constants/organizations'
import { ContentCard } from 'components/ContentCard'
import { CreateResourceButton } from 'components/Buttons'

import { CreateGroupFormContainer } from '../../Forms'
import styles from './styles.scss'

const renderResourceCell = (label, content) => (
    <div>
        <span className={styles['resource-label']}>{label}: </span>
        {content}
    </div>
)

const renderResources = (text, group) => {
    if (group.regions.length > 0) {
        return renderResourceCell(
            RESOURCE_TYPE_LABELS[RESOURCE_TYPE_REGIONS],
            group[RESOURCE_TYPE_REGIONS].join(', ')
        )
    } else if (group.countries.length > 0) {
        return renderResourceCell(
            RESOURCE_TYPE_LABELS[RESOURCE_TYPE_COUNTRIES],
            group[RESOURCE_TYPE_COUNTRIES].join(', ')
        )
    } else if (group.brands.length > 0) {
        return renderResourceCell(
            RESOURCE_TYPE_LABELS[RESOURCE_TYPE_BRANDS],
            group[RESOURCE_TYPE_BRANDS].map(brand =>
                formatBrandName(brand)
            ).join(', ')
        )
    }
    return <div>{RESOURCE_TYPE_LABELS[RESOURCE_TYPE_ALL]}</div>
}

class GroupsTable extends Component {
    static propTypes = {
        tableData: PropTypes.object.isRequired,
        userPermissions: PropTypes.array.isRequired,

        // actions
        updatePagination: PropTypes.func.isRequired,
        reloadData: PropTypes.func.isRequired,
    }

    state = {
        collapseOpen: false,
    }

    @autobind
    handleToggleCollapse() {
        this.setState({
            collapseOpen: !this.state.collapseOpen,
        })
    }

    render() {
        const {
            tableData,
            userPermissions,
            updatePagination,
            reloadData,
        } = this.props
        const { collapseOpen } = this.state

        return (
            <ContentCard
                title="Groups"
                subTitle="Review and manage your organization's groups."
                actions={
                    hasOrgAdminPermissions(userPermissions)
                        ? [
                              <CreateResourceButton
                                  onClick={this.handleToggleCollapse}
                              >
                                  Add New Groups
                              </CreateResourceButton>,
                          ]
                        : []
                }
                collapseOpen={collapseOpen}
                collapseContent={
                    hasOrgAdminPermissions(userPermissions) && (
                        <CreateGroupFormContainer
                            onCancel={this.handleToggleCollapse}
                        />
                    )
                }
            >
                <PaginatedTable
                    tableOptions={{
                        name: 'group',
                        rowKey: 'id',
                        columns: {
                            name: {
                                title: 'Group Name',
                                dataIndex: 'name',
                                align: 'left',
                                render: (text, group) => (
                                    <AppLink
                                        to={generatePath(
                                            getPath(ORGANIZATION_GROUP_PAGE),
                                            {
                                                organizationId:
                                                    group.organization_id,
                                                organizationGroupId: group.id,
                                            }
                                        )}
                                    >
                                        {text}
                                    </AppLink>
                                ),
                            },
                            resources: {
                                title: 'Resources',
                                dataIndex: 'resources',
                                align: 'left',
                                render: renderResources,
                            },
                            memberCount: {
                                title: 'Members',
                                dataIndex: 'member_count',
                                align: 'left',
                            },
                        },
                    }}
                    data={tableData.data}
                    loading={tableData.loading}
                    updating={tableData.updating}
                    deleting={tableData.deleting}
                    pagination={{
                        ...getTablePaginationOptions('Groups'),
                        ...tableData.pagination,
                    }}
                    sorter={tableData.sorter}
                    columnSettings={tableData.columnSettings}
                    updatePagination={updatePagination}
                    updateSorter={noop}
                    reloadData={reloadData}
                />
            </ContentCard>
        )
    }
}

export default GroupsTable
