import React, { Component } from 'react'
import PropTypes from 'prop-types'
import get from 'lodash/get'
import upperFirst from 'lodash/upperFirst'
import { Icon } from 'antd'
import classNames from 'classnames'
import autobind from 'autobind-decorator'

import { PaginatedTable } from 'components/PaginatedTable'
import { getTablePaginationOptions } from 'helpers/pagination'
import { hasOrgAdminPermissions } from 'helpers/featurePermissions'
import moment from 'utilities/moment'
import { ContentCard } from 'components/ContentCard'
import { CreateResourceButton } from 'components/Buttons'

import { InviteMemberFormContainer } from '../../Forms'
import styles from './styles.scss'

const getStatusIcon = text => {
    if (text === 'pending') {
        return <Icon type="exclamation-circle" />
    } else if (text === 'accepted') {
        return <Icon type="check-circle" />
    } else if (text === 'revoked') {
        return <Icon type="close-circle" />
    }
    return null
}

class InvitationsTable extends Component {
    static propTypes = {
        tableData: PropTypes.object.isRequired,
        userPermissions: PropTypes.array.isRequired,
        updatePagination: PropTypes.func.isRequired,
        updateSorter: PropTypes.func.isRequired,
        reloadData: PropTypes.func.isRequired,
    }

    state = {
        collapseOpen: false,
    }

    static getTableOptions() {
        return {
            name: 'invitations',
            rowKey: 'id',
            columns: {
                email: {
                    title: 'Email Address',
                    dataIndex: 'email',
                    align: 'left',
                    render: (text, record) => get(record, ['user', 'email']),
                },
                status: {
                    title: 'Status',
                    dataIndex: 'state',
                    sorter: true,
                    render: text => (
                        <span
                            className={classNames({
                                [styles.success]: text === 'accepted',
                                [styles.warning]: text === 'pending',
                                [styles.danger]: text === 'revoked',
                            })}
                        >
                            {getStatusIcon(text)} {upperFirst(text)}
                        </span>
                    ),
                },
                inviteDate: {
                    title: 'Invite Date',
                    dataIndex: 'created_date',
                    sorter: true,
                    render: utcDateString => (
                        <div>
                            {moment(utcDateString)
                                .local()
                                .format('MM/DD/YYYY, h:mm:ss a')}
                        </div>
                    ),
                },
            },
        }
    }

    @autobind
    handleToggleCollapse() {
        this.setState({
            collapseOpen: !this.state.collapseOpen,
        })
    }

    render() {
        const {
            tableData,
            userPermissions,
            updatePagination,
            updateSorter,
            reloadData,
        } = this.props
        const { collapseOpen } = this.state

        return (
            <ContentCard
                title="Invitations"
                subTitle="Review and manage your organization's invitations."
                actions={
                    hasOrgAdminPermissions(userPermissions)
                        ? [
                              <CreateResourceButton
                                  onClick={this.handleToggleCollapse}
                              >
                                  Invite Member
                              </CreateResourceButton>,
                          ]
                        : []
                }
                collapseOpen={collapseOpen}
                collapseContent={
                    hasOrgAdminPermissions(userPermissions) && (
                        <InviteMemberFormContainer
                            onCancel={this.handleToggleCollapse}
                        />
                    )
                }
            >
                <PaginatedTable
                    tableOptions={InvitationsTable.getTableOptions()}
                    data={tableData.data}
                    loading={tableData.loading}
                    updating={tableData.updating}
                    deleting={tableData.deleting}
                    pagination={{
                        ...getTablePaginationOptions('Invitations'),
                        ...tableData.pagination,
                    }}
                    sorter={tableData.sorter}
                    columnSettings={tableData.columnSettings}
                    updatePagination={updatePagination}
                    updateSorter={updateSorter}
                    reloadData={reloadData}
                />
            </ContentCard>
        )
    }
}

export default InvitationsTable
