import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import { ORGANIZATION_PAGE } from 'constants/pages'
import { selectDomainValue } from 'selectors/ui'
import { selectOrganizationUserPermissions } from 'selectors/orgs'
import {
    updateOrganizationPageInvitationsTablePagination,
    fetchOrganizationPageInvitationsTableRequest,
    updateOrganizationPageInvitationsTableSorter,
} from 'actions/ui'

import InvitationsTable from './InvitationsTable'

const mapStateToProps = (state, ownProps) => {
    const { organizationId } = ownProps.match.params
    return {
        tableData: selectDomainValue(state, [
            ORGANIZATION_PAGE,
            'invitationsTable',
        ]),
        userPermissions: selectOrganizationUserPermissions(
            state,
            organizationId
        ),
    }
}

const mapDispatchToProps = {
    updatePagination: updateOrganizationPageInvitationsTablePagination,
    updateSorter: updateOrganizationPageInvitationsTableSorter,
    reloadData: fetchOrganizationPageInvitationsTableRequest,
}

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(InvitationsTable)
)
