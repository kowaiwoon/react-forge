import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form, Input, Button, Select, Row, Col } from 'antd'

import { ORGANIZATION_ADMIN } from 'constants/featurePermissions'
import { getPermissionOption } from 'helpers/featurePermissions'
import { ResourceTypeField, ResourceDetailsField } from 'components/FormFields'
import { resourcesRequired } from 'helpers/organizations'

import styles from './styles.scss'

class CreateGroupForm extends Component {
    static propTypes = {
        brands: PropTypes.array.isRequired,
        creating: PropTypes.bool.isRequired,
        brandSearchLoading: PropTypes.bool.isRequired,
        organization: PropTypes.object.isRequired,

        // Added by PaginatedTable
        onCancel: PropTypes.func.isRequired,

        // Antd form
        form: PropTypes.object.isRequired,

        // actions
        createGroupRequest: PropTypes.func.isRequired,
        changeBrandSearchInput: PropTypes.func.isRequired,
    }

    state = {
        justCreated: false,
    }

    componentDidUpdate(prevProps) {
        // set justCreated when creating is complete
        if (prevProps.creating && !this.props.creating) {
            this.setJustCreated(true)
        } else if (this.state.justCreated) {
            // reset justAttached with timeout
            setTimeout(() => this.setJustCreated(false), 1000)
        }
    }

    setJustCreated(justCreated) {
        if (justCreated) {
            this.setState({ justCreated }, this.props.form.resetFields)
        } else {
            this.setState({ justCreated })
        }
    }

    handleSubmit = e => {
        e.preventDefault()
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.createGroupRequest({
                    name: values.groupName,
                    ...(values.permissions
                        ? { permissions: values.permissions }
                        : {}),
                    ...(resourcesRequired(values.resourceType)
                        ? { [values.resourceType]: values.resources }
                        : {}),
                })
            }
        })
    }

    handleCancel = e => {
        this.props.form.resetFields()
        this.props.onCancel(e)
    }

    render() {
        const {
            creating,
            brands,
            brandSearchLoading,
            changeBrandSearchInput,
            organization: { permissions },
            form: { getFieldDecorator, getFieldValue, setFieldsValue },
        } = this.props
        const { justCreated } = this.state
        const selectedResourceType = getFieldValue('resourceType')

        return (
            <Form
                layout="vertical"
                onSubmit={this.handleSubmit}
                hideRequiredMark
            >
                <Row gutter={{ md: 16, xl: 32 }}>
                    <Col md={12} xl={6}>
                        <Form.Item label="Group Name">
                            {getFieldDecorator('groupName', {
                                rules: [
                                    {
                                        required: true,
                                        message: 'Group Name is required',
                                    },
                                ],
                                validateTrigger: 'onSubmit',
                            })(<Input />)}
                        </Form.Item>
                    </Col>
                    <Col md={12} xl={6}>
                        <ResourceTypeField
                            getFieldDecorator={getFieldDecorator}
                            setFieldsValue={setFieldsValue}
                        />
                    </Col>
                    <Col md={12} xl={6}>
                        <ResourceDetailsField
                            getFieldDecorator={getFieldDecorator}
                            selectedResourceType={selectedResourceType}
                            brands={brands}
                            brandSearchLoading={brandSearchLoading}
                            changeBrandSearchInput={changeBrandSearchInput}
                        />
                    </Col>
                    <Col md={12} xl={6}>
                        <Form.Item label="Permissions">
                            {getFieldDecorator('permissions')(
                                <Select
                                    mode="multiple"
                                    optionFilterProp="children"
                                >
                                    {permissions
                                        .concat(ORGANIZATION_ADMIN) // appending the org admin permission since it's not a feature permission returned from cerebro
                                        .map(option => {
                                            const p = getPermissionOption(
                                                option
                                            )
                                            return (
                                                <Select.Option
                                                    value={p.value}
                                                    key={p.value}
                                                >
                                                    {p.label}
                                                </Select.Option>
                                            )
                                        })}
                                </Select>
                            )}
                        </Form.Item>
                    </Col>
                </Row>
                <Row type="flex" justify="end" className={styles.buttons}>
                    <Button
                        type="primary"
                        htmlType="submit"
                        icon={justCreated ? 'check' : 'plus'}
                        disabled={creating}
                        loading={creating}
                    >
                        Add Group
                    </Button>
                    <Button onClick={this.handleCancel} disabled={creating}>
                        Cancel
                    </Button>
                </Row>
            </Form>
        )
    }
}

export default Form.create()(CreateGroupForm)
