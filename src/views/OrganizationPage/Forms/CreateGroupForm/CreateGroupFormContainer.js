import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import { ORGANIZATION_PAGE } from 'constants/pages'
import { selectDomainValue as selectUiDomainValue } from 'selectors/ui'
import {
    createOrganizationPageGroupRequest,
    changeOrganizationPageBrandsSearchInput,
} from 'actions/ui'
import {
    selectOrganizationBrandsForSearch,
    selectOrganization,
} from 'selectors/orgs'

import CreateGroupForm from './CreateGroupForm'

const mapStateToProps = (state, ownProps) => {
    const { organizationId } = ownProps.match.params
    return {
        brands: selectOrganizationBrandsForSearch(state, organizationId),
        brandSearchLoading: selectUiDomainValue(state, [
            ORGANIZATION_PAGE,
            'brandSearchLoading',
        ]),
        creating: selectUiDomainValue(state, [
            ORGANIZATION_PAGE,
            'groupsTable',
            'attaching',
        ]),
        organization: selectOrganization(state, organizationId),
    }
}

const mapDispatchToProps = {
    createGroupRequest: createOrganizationPageGroupRequest,
    changeBrandSearchInput: changeOrganizationPageBrandsSearchInput,
}

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(CreateGroupForm)
)
