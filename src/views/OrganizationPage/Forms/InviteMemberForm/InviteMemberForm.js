import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form, Input, Button, Row, Col, Select } from 'antd'
import autobind from 'autobind-decorator'

import styles from './styles.scss'

class InviteMemberForm extends Component {
    static propTypes = {
        inviting: PropTypes.bool.isRequired,
        groups: PropTypes.array.isRequired,
        inviteMemberRequest: PropTypes.func.isRequired,

        // Added by PaginatedTable
        onCancel: PropTypes.func.isRequired,

        // Antd form
        form: PropTypes.object.isRequired,
    }

    state = {
        justInvited: false,
    }

    componentDidUpdate(prevProps) {
        // set justInvited when creating is complete
        if (prevProps.inviting && !this.props.inviting) {
            this.setJustInvited(true)
        } else if (this.state.justInvited) {
            // reset justInvited with timeout
            setTimeout(() => this.setJustInvited(false), 1000)
        }
    }

    setJustInvited(justInvited) {
        if (justInvited) {
            this.setState({ justInvited }, this.props.form.resetFields)
        } else {
            this.setState({ justInvited })
        }
    }

    @autobind
    handleSubmit(e) {
        e.preventDefault()
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.inviteMemberRequest({
                    user_email: values.email.trim(),
                    organization_group_ids: values.groups,
                })
            }
        })
    }

    @autobind
    handleCancel(e) {
        this.props.form.resetFields()
        this.props.onCancel(e)
    }

    handleResourceTypeChange = () => {
        this.props.form.setFieldsValue({ resources: [] })
    }

    render() {
        const {
            inviting,
            groups,
            form: { getFieldDecorator },
        } = this.props
        const { justInvited } = this.state

        return (
            <Form
                layout="vertical"
                onSubmit={this.handleSubmit}
                hideRequiredMark
            >
                <Row gutter={{ md: 16, xl: 32 }}>
                    <Col md={12} xl={8}>
                        <Form.Item label="Email Address *">
                            {getFieldDecorator('email', {
                                rules: [
                                    {
                                        required: true,
                                        message: 'Email Address is required',
                                    },
                                ],
                                validateTrigger: 'onSubmit',
                            })(<Input />)}
                        </Form.Item>
                    </Col>
                    <Col md={12} xl={8}>
                        <Form.Item label="Groups">
                            {getFieldDecorator('groups', {
                                rules: [
                                    {
                                        required: true,
                                        message:
                                            'Please choose one or more groups.',
                                    },
                                ],
                                validateTrigger: 'onSubmit',
                            })(
                                <Select mode="multiple">
                                    {groups.map(group => (
                                        <Select.Option
                                            key={group.id}
                                            value={group.id}
                                        >
                                            {group.name}
                                        </Select.Option>
                                    ))}
                                </Select>
                            )}
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={{ md: 16, xl: 32 }}>
                    <Col span={24}>
                        <div className={styles.muted}>
                            * Email address must belong to an existing
                            Downstream user account
                        </div>
                    </Col>
                </Row>
                <Row type="flex" justify="end" className={styles.buttons}>
                    <Button
                        type="primary"
                        htmlType="submit"
                        icon={justInvited ? 'check' : 'plus'}
                        disabled={inviting}
                        loading={inviting}
                    >
                        Invite Member
                    </Button>
                    <Button onClick={this.handleCancel} disabled={inviting}>
                        Cancel
                    </Button>
                </Row>
            </Form>
        )
    }
}

export default Form.create()(InviteMemberForm)
