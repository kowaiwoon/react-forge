import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import { ORGANIZATION_PAGE } from 'constants/pages'
import { selectDomainValue as selectUiDomainValue } from 'selectors/ui'
import { selectOrganizationGroups } from 'selectors/orgs'
import { inviteMemberOrganizationPageRequest } from 'actions/ui'

import InviteMemberForm from './InviteMemberForm'

const mapStateToProps = (state, ownProps) => {
    const { organizationId } = ownProps.match.params
    return {
        inviting: selectUiDomainValue(state, [
            ORGANIZATION_PAGE,
            'membersTable',
            'attaching',
        ]),
        groups: selectOrganizationGroups(state, organizationId),
    }
}

const mapDispatchToProps = {
    inviteMemberRequest: inviteMemberOrganizationPageRequest,
}

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(InviteMemberForm)
)
