import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import get from 'lodash/get'

import { createOrganizationPageIntegrationRequest } from 'actions/ui'
import { ORGANIZATION_PAGE } from 'constants/pages'
import { selectDomainValue as selectUiDomainValue } from 'selectors/ui'

import CreateIntegrationForm from './CreateIntegrationForm'

const mapStateToProps = (state, ownProps) => ({
    organizationId: get(ownProps, ['match', 'params', 'organizationId']),
    creating: selectUiDomainValue(state, [
        ORGANIZATION_PAGE,
        'integrationsTable',
        'attaching',
    ]),
})

const mapDispatchToProps = {
    createIntegrationRequest: createOrganizationPageIntegrationRequest,
}

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(CreateIntegrationForm)
)
