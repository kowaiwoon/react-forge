import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form, Input, Button, Row, Col } from 'antd'
import autobind from 'autobind-decorator'

import styles from './styles.scss'

class CreateIntegrationForm extends Component {
    static propTypes = {
        organizationId: PropTypes.string.isRequired,
        creating: PropTypes.bool.isRequired,

        // actions
        createIntegrationRequest: PropTypes.func.isRequired,

        // Added by PaginatedTable
        onCancel: PropTypes.func.isRequired,

        // Antd form
        form: PropTypes.object.isRequired,
    }

    state = {
        justCreated: false,
    }

    componentDidUpdate(prevProps) {
        // set justCreated when creating is complete
        if (prevProps.creating && !this.props.creating) {
            this.setJustCreated(true)
        } else if (this.state.justCreated) {
            // reset justAttached with timeout
            setTimeout(() => this.setJustCreated(false), 1000)
        }
    }

    setJustCreated(justCreated) {
        if (justCreated) {
            this.setState({ justCreated }, this.props.form.resetFields)
        } else {
            this.setState({ justCreated })
        }
    }

    @autobind
    handleSubmit(e) {
        e.preventDefault()
        const { form, organizationId, createIntegrationRequest } = this.props

        form.validateFields((err, values) => {
            if (!err) {
                createIntegrationRequest({
                    alias: values.alias,
                    organizationId,
                })
            }
        })
    }

    @autobind
    handleCancel(e) {
        this.props.form.resetFields()
        this.props.onCancel(e)
    }

    render() {
        const {
            creating,
            form: { getFieldDecorator },
        } = this.props
        const { justCreated } = this.state

        return (
            <Form
                layout="vertical"
                onSubmit={this.handleSubmit}
                hideRequiredMark
            >
                <Row>
                    <Col span={24} className={styles.info}>
                        Adding an Amazon Advertising integration gives
                        Downstream secure access to your data.{' '}
                        <strong>
                            This step is required before you can use Downstream
                            to analyze your data
                        </strong>
                        . Enter a description for your Amazon Advertising
                        account then click the{' '}
                        <strong>
                            <em>&quot;Create Integration&quot;</em>
                        </strong>{' '}
                        button below. This will redirect you to an Amazon.com
                        web page where you will authenticate using your Amazon
                        credentials.
                    </Col>
                </Row>
                <Row>
                    <Col span={24}>
                        <Form.Item
                            label="Integration Description"
                            extra={
                                <span>
                                    For example,{' '}
                                    <em>Integration for ACME Brand</em> or{' '}
                                    <em>European Data</em>
                                </span>
                            }
                        >
                            {getFieldDecorator('alias', {
                                rules: [
                                    {
                                        required: true,
                                        message:
                                            'Integration Description is required',
                                    },
                                ],
                                validateTrigger: 'onSubmit',
                            })(<Input />)}
                        </Form.Item>
                    </Col>
                </Row>
                <Row type="flex" justify="end" className={styles.buttons}>
                    <Button
                        type="primary"
                        htmlType="submit"
                        icon={justCreated ? 'check' : 'plus'}
                        disabled={creating}
                        loading={creating}
                    >
                        Create Integration
                    </Button>
                    <Button onClick={this.handleCancel} disabled={creating}>
                        Cancel
                    </Button>
                </Row>
            </Form>
        )
    }
}

export default Form.create()(CreateIntegrationForm)
