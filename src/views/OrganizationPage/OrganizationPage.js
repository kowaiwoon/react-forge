/* global REQUEST_A_DEMO */

import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Tabs, Modal, Icon, Button } from 'antd'
import ReactJoyride, { STATUS } from 'react-joyride'
import autobind from 'autobind-decorator'
import queryString from 'query-string'
import get from 'lodash/get'
import isEmpty from 'lodash/isEmpty'

import { LoadingIndicator } from 'components/LoadingIndicator'
import { PageHeader } from 'components/PageHeader'
import { getPath } from 'helpers/pages'
import { PROFILE_PAGE } from 'constants/pages'

import { ResourceDetailsContainer } from './ResourceDetails'
import {
    GroupsTableContainer,
    MembersTableContainer,
    IntegrationsTableContainer,
    InvitationsTableContainer,
} from './Tables'
import styles from './styles.scss'

class OrganizationPage extends Component {
    static propTypes = {
        // tab state
        tab: PropTypes.oneOf([
            'groups',
            'members',
            'invitations',
            'integrations',
        ]).isRequired,
        handleTabChange: PropTypes.func.isRequired,

        // redux state
        organizationId: PropTypes.string.isRequired,
        organization: PropTypes.shape({
            name: PropTypes.string,
            website: PropTypes.string,
            group_count: PropTypes.number,
            member_count: PropTypes.number,
        }).isRequired,
        mounting: PropTypes.bool.isRequired,
        integrationsTableData: PropTypes.object.isRequired,

        // react router
        location: PropTypes.shape({
            search: PropTypes.string,
        }).isRequired,
        history: PropTypes.shape({
            push: PropTypes.func,
        }).isRequired,

        // actions
        mountOrganizationPageRequest: PropTypes.func.isRequired,
        unmountOrganizationPage: PropTypes.func.isRequired,
    }

    constructor(props) {
        super(props)

        const params = queryString.parse(props.location.search)
        this.state = {
            showSuccessModal: get(params, 'success') === 'true',
            run: false,
            stepIndex: 0,
            steps: [
                {
                    title: 'Create an Integration',
                    content: (
                        <p>
                            Go to the <strong>Integrations</strong> tab to
                            create an Amazon Advertising Integration for your
                            Organization.
                        </p>
                    ),
                    placement: 'bottom',
                    target: '.integrations-tab',
                    spotlightClicks: true,
                },
            ],
        }
    }

    componentDidMount() {
        const { organizationId, mountOrganizationPageRequest } = this.props
        mountOrganizationPageRequest({ organizationId })
    }

    componentDidUpdate(prevProps, prevState) {
        this.updateWalkThroughState(prevProps, prevState)
    }

    componentWillUnmount() {
        this.props.unmountOrganizationPage()
    }

    updateWalkThroughState(prevProps, prevState) {
        const {
            tab,
            integrationsTableData: { data: integrations },
        } = this.props

        if (isEmpty(integrations)) {
            if (!prevState.run && tab !== 'integrations') {
                this.setState({ run: true })
            } else if (prevState.run && tab === 'integrations') {
                this.setState({ run: false })
            }
        }
    }

    @autobind
    handleJoyrideCallback(data) {
        const { status } = data

        if (status === STATUS.FINISHED) {
            this.setState({ run: false })
        }
    }

    @autobind
    closeSuccessModal() {
        const { location, history } = this.props

        this.setState({
            showSuccessModal: false,
        })

        // remove success param
        const params = queryString.parse(location.search)
        delete params.success
        history.push({
            search: `?${queryString.stringify(params)}`,
        })
    }

    @autobind
    handleTabChangeWithWalkThrough(tabName) {
        const {
            integrationsTableData: { data: integrations },
        } = this.props

        this.props.handleTabChange(tabName)
        if (tabName !== 'integrations' && isEmpty(integrations)) {
            this.setState({ run: true })
        } else if (this.state.run) {
            this.setState({ run: false })
        }
    }

    render() {
        const { mounting, organization, tab } = this.props
        const { showSuccessModal, run, stepIndex, steps } = this.state

        if (mounting) {
            return (
                <div className={styles['empty-content']}>
                    <LoadingIndicator size="small" />
                </div>
            )
        }

        return (
            <React.Fragment>
                <ReactJoyride
                    hideBackButton
                    disableScrolling
                    disableScrollParentFix // prevents overflow=initial from being set in parent div
                    callback={this.handleJoyrideCallback}
                    run={run}
                    stepIndex={stepIndex}
                    steps={steps}
                    styles={{
                        options: {
                            zIndex: 10000,
                        },
                    }}
                />

                <PageHeader
                    breadcrumbs={[
                        { name: 'Profile', url: getPath(PROFILE_PAGE) },
                        { name: organization.name },
                    ]}
                    titleComponent={<ResourceDetailsContainer />}
                />

                <Tabs
                    activeKey={tab}
                    tabPosition="top"
                    size="default"
                    tabBarStyle={{
                        fontWeight: '500', // same font-weight as selected tab
                    }}
                    onChange={this.handleTabChangeWithWalkThrough}
                >
                    <Tabs.TabPane tab="Groups" key="groups" forceRender>
                        <GroupsTableContainer />
                    </Tabs.TabPane>
                    <Tabs.TabPane tab="Members" key="members" forceRender>
                        <MembersTableContainer />
                    </Tabs.TabPane>
                    <Tabs.TabPane
                        tab="Invitations"
                        key="invitations"
                        forceRender
                    >
                        <InvitationsTableContainer />
                    </Tabs.TabPane>
                    <Tabs.TabPane
                        tab={
                            <span className="integrations-tab">
                                Integrations
                            </span>
                        }
                        key="integrations"
                        forceRender
                    >
                        <IntegrationsTableContainer tab={tab} />
                    </Tabs.TabPane>
                </Tabs>

                <Modal
                    title={
                        <div>
                            <Icon
                                type="check-circle"
                                theme="twoTone"
                                twoToneColor="#52c41a"
                                className={styles['success-icon']}
                            />{' '}
                            New Amazon Advertising Integration Added!
                        </div>
                    }
                    onOk={this.closeSuccessModal}
                    visible={showSuccessModal}
                    closable={false}
                    footer={[
                        <Button
                            key="ok"
                            onClick={this.closeSuccessModal}
                            type="primary"
                        >
                            Ok
                        </Button>,
                    ]}
                >
                    <p>
                        Nice work{' '}
                        <span role="img" aria-label="clap">
                            👏
                        </span>{' '}
                        You have successfully added a new Amazon Advertising
                        integration to the <strong>{organization.name}</strong>{' '}
                        organization! Your Amazon Advertising data will be
                        available to your organization by tomorrow morning.
                    </p>
                    <p>
                        The next step is to{' '}
                        <a
                            href={REQUEST_A_DEMO}
                            rel="noopener noreferrer"
                            target="_blank"
                        >
                            schedule a demo
                        </a>{' '}
                        with our customer service team. We&apos;ll walk you
                        through the Downstream application and your data in
                        detail, to help you understand how we can improve the
                        effectiveness of your Amazon Advertising investments.
                    </p>
                </Modal>
            </React.Fragment>
        )
    }
}

export default OrganizationPage
