import React from 'react'
import PropTypes from 'prop-types'
import get from 'lodash/get'

import { ResourceDetails } from 'components/ResourceDetails'

const propTypes = {
    // redux state
    organization: PropTypes.shape({
        name: PropTypes.string,
        website: PropTypes.string,
        group_count: PropTypes.number,
        member_count: PropTypes.number,
    }).isRequired,
    owner: PropTypes.object.isRequired,
    showDetails: PropTypes.bool.isRequired,
    groupCount: PropTypes.number.isRequired,
    memberCount: PropTypes.number.isRequired,

    // actions
    toggleOrganizationPageDetails: PropTypes.func.isRequired,
}

const OrganizationResourceDetails = ({
    organization: { name, website },
    showDetails,
    toggleOrganizationPageDetails,
    owner,
    groupCount,
    memberCount,
}) => (
    <ResourceDetails
        name={name}
        allowEditing={false}
        editToolTip="Editing organization information is not yet supported."
        showDetails={showDetails}
        onShowDetailsClick={toggleOrganizationPageDetails}
        details={[
            { label: 'Website', value: website },
            { label: 'Owner', value: get(owner, 'email', '') },
            { label: 'Groups', value: groupCount },
            { label: 'Members', value: memberCount },
        ]}
    />
)

OrganizationResourceDetails.propTypes = propTypes

export default OrganizationResourceDetails
