import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import { selectDomainValue } from 'selectors/ui'
import { selectOrganization, selectOrganizationOwner } from 'selectors/orgs'
import { toggleOrganizationPageDetails } from 'actions/ui'
import { ORGANIZATION_PAGE } from 'constants/pages'

import OrganizationResourceDetails from './ResourceDetails'

const mapStateToProps = (state, ownProps) => {
    const { organizationId } = ownProps.match.params
    return {
        showDetails: selectDomainValue(state, [
            ORGANIZATION_PAGE,
            'showDetails',
        ]),
        organization: selectOrganization(state, organizationId),
        owner: selectOrganizationOwner(state, organizationId),
        groupCount: selectDomainValue(state, [
            ORGANIZATION_PAGE,
            'groupsTable',
            'pagination',
            'total',
        ]),
        memberCount: selectDomainValue(state, [
            ORGANIZATION_PAGE,
            'membersTable',
            'pagination',
            'total',
        ]),
    }
}

const mapDispatchToProps = {
    toggleOrganizationPageDetails,
}

const OrganizationResourceDetailsContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(OrganizationResourceDetails)

export default withRouter(OrganizationResourceDetailsContainer)
