import { connect } from 'react-redux'
import get from 'lodash/get'

import { selectDomainValue } from 'selectors/ui'
import {
    mountOrganizationPageRequest,
    unmountOrganizationPage,
} from 'actions/ui'
import { ORGANIZATION_PAGE } from 'constants/pages'
import { selectOrganization } from 'selectors/orgs'
import { withTabState } from 'components/HigherOrderComponents'

import OrganizationPage from './OrganizationPage'

const mapStateToProps = (state, ownProps) => {
    const organizationId = get(ownProps, ['match', 'params', 'organizationId'])

    return {
        organizationId,
        organization: selectOrganization(state, organizationId),
        mounting: selectDomainValue(state, [ORGANIZATION_PAGE, 'mounting']),
        integrationsTableData: selectDomainValue(state, [
            ORGANIZATION_PAGE,
            'integrationsTable',
        ]),
    }
}

const mapDispatchToProps = {
    unmountOrganizationPage,
    mountOrganizationPageRequest,
}

const OrganizationPageContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(OrganizationPage)

export default withTabState(OrganizationPageContainer, 'groups')
