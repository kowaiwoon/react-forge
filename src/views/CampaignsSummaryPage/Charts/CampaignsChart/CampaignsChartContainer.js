import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import { CAMPAIGNS_SUMMARY_PAGE } from 'constants/pages'
import {
    selectPageFactTypes,
    selectTreemap,
    selectTreemapChartData,
} from 'selectors/ui'
import {
    updateCampaignsSummaryPageTreemapPagination,
    updateCampaignsSummaryPageTreemapSorter,
    fetchCampaignsSummaryPageTreemapRequest,
} from 'actions/ui'

import CampaignsChart from './CampaignsChart'

const mapStateToProps = state => {
    const { loading, pagination, sorter } = selectTreemap(
        state,
        CAMPAIGNS_SUMMARY_PAGE,
        'treemap'
    )

    return {
        factTypes: selectPageFactTypes(state, CAMPAIGNS_SUMMARY_PAGE),
        data: selectTreemapChartData(state, CAMPAIGNS_SUMMARY_PAGE, 'treemap'),
        loading,
        pagination,
        sorter,
    }
}

const mapDispatchToProps = {
    updateCampaignsTreemapPagination: updateCampaignsSummaryPageTreemapPagination,
    updateCampaignsTreemapSorter: updateCampaignsSummaryPageTreemapSorter,
    fetchCampaignsTreemapRequest: fetchCampaignsSummaryPageTreemapRequest,
}

const CampaignsChartContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(CampaignsChart)

export default withRouter(CampaignsChartContainer)
