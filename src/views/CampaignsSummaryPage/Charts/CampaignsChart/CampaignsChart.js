import React from 'react'
import PropTypes from 'prop-types'

import { PaginatedTreemap } from 'components/PaginatedTreemap'
import { getTreemapPaginationOptions } from 'helpers/pagination'

const propTypes = {
    // React router
    history: PropTypes.shape({
        push: PropTypes.func,
    }).isRequired,

    // Redux state
    factTypes: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.string,
            label: PropTypes.string,
        })
    ).isRequired,
    data: PropTypes.array.isRequired,
    loading: PropTypes.bool.isRequired,
    pagination: PropTypes.object.isRequired,
    sorter: PropTypes.object.isRequired,

    // Redux actions
    updateCampaignsTreemapPagination: PropTypes.func.isRequired,
    updateCampaignsTreemapSorter: PropTypes.func.isRequired,
    fetchCampaignsTreemapRequest: PropTypes.func.isRequired,
}

const defaultProps = {}

const CampaignsChart = ({
    history,
    factTypes,
    data,
    loading,
    pagination,
    sorter,
    updateCampaignsTreemapPagination,
    updateCampaignsTreemapSorter,
    fetchCampaignsTreemapRequest,
}) => (
    <PaginatedTreemap
        treemapOptions={{
            name: 'campaign',
            idAttribute: 'campaign.id',
            titleAttribute: 'campaign.name',
            height: 500,
            colors: {
                minColor: '#c4e1ea',
                maxColor: '#8884d8',
            },
        }}
        defaultPagination={getTreemapPaginationOptions('Campaigns')}
        factTypes={factTypes}
        data={data}
        loading={loading}
        pagination={pagination}
        sorter={sorter}
        updatePagination={updateCampaignsTreemapPagination}
        updateSorter={updateCampaignsTreemapSorter}
        reloadData={fetchCampaignsTreemapRequest}
        onLeafClick={leafNodeId => {
            history.push(`/campaigns/${leafNodeId}`)
        }}
    />
)

CampaignsChart.propTypes = propTypes
CampaignsChart.defaultProps = defaultProps

export default CampaignsChart
