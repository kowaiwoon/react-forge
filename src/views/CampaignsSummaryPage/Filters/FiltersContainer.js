import { connect } from 'react-redux'

import {
    selectDomainValue as selectUiDomainValue,
    selectPageFilters,
    selectPageFilterSettings,
} from 'selectors/ui'
import {
    selectBrandsForFilters,
    selectCampaignsForFilters,
} from 'selectors/entities'
import {
    updatePageFilterForPage,
    resetPageFiltersForPage,
    updatePageFilterSettingsForPage,
    changeBrandsFilterInput,
    changeCampaignsFilterInputForPage,
} from 'actions/ui'
import { CAMPAIGNS_SUMMARY_PAGE } from 'constants/pages'

import Filters from './Filters'

const mapStateToProps = state => ({
    filterSettings: selectPageFilterSettings(state, CAMPAIGNS_SUMMARY_PAGE),
    selectedFilterValues: selectPageFilters(state, CAMPAIGNS_SUMMARY_PAGE),
    brandsFilterOptions: selectBrandsForFilters(state),
    brandsFilterLoading: selectUiDomainValue(state, [
        'app',
        'brandsFilterLoading',
    ]),
    campaignsFilterOptions: selectCampaignsForFilters(state),
    campaignsFilterLoading: selectUiDomainValue(state, [
        'app',
        'campaignsFilterLoading',
    ]),
})

const mapDispatchToProps = {
    updatePageFilter: updatePageFilterForPage(CAMPAIGNS_SUMMARY_PAGE),
    resetFilters: resetPageFiltersForPage(CAMPAIGNS_SUMMARY_PAGE),
    updateFilterSettings: updatePageFilterSettingsForPage(
        CAMPAIGNS_SUMMARY_PAGE
    ),
    changeBrandsFilterInput,
    changeCampaignsFilterInput: changeCampaignsFilterInputForPage(
        CAMPAIGNS_SUMMARY_PAGE
    ),
}

const FiltersContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Filters)

export default FiltersContainer
