import React, { Component } from 'react'
import PropTypes from 'prop-types'
import isUndefined from 'lodash/isUndefined'
import isNull from 'lodash/isNull'
import { generatePath } from 'react-router-dom'
import autobind from 'autobind-decorator'

import { AppLink } from 'components/AppLink'
import { PaginatedTable } from 'components/PaginatedTable'
import { getTablePaginationOptions } from 'helpers/pagination'
import { METRIC_COLUMNS } from 'configuration/tables'
import {
    SELECT_INPUT,
    NUMBER_INPUT,
    DATE_INPUT,
    ACTIONS,
} from 'constants/inputTypes'
import { PAUSED, ARCHIVED, ENABLED } from 'constants/resourceStates'
import { formatDate, titleCase, formatCurrency } from 'helpers/formatting'
import { UNDEFINED_VALUE, DAY_FORMAT } from 'constants/formatting'
import { getPath } from 'helpers/pages'
import { CAMPAIGN_PAGE } from 'constants/pages'
import { HEADLINE_SEARCH } from 'constants/factTypes'
import {
    CAMPAIGN_BUDGET_MIN,
    CAMPAIGN_BUDGET_MAX,
    CAMPAIGN_BUDGET_STEP,
    CAMPAIGN_BUDGET_PRECISION,
} from 'constants/campaigns'
import { ContentCard } from 'components/ContentCard'
import { SettingsButton, DownloadButton } from 'components/Buttons'
import { SettingsModal } from 'components/SettingsModal'

class CampaignsTable extends Component {
    static disableRowActions(record) {
        return (
            record.campaign.state === ARCHIVED ||
            record.campaign.campaign_type === HEADLINE_SEARCH
        )
    }

    static actionsToolTip(record) {
        if (record.campaign.state === ARCHIVED) {
            return 'Archived campaigns cannot be modified.'
        }
        if (record.campaign.campaign_type === HEADLINE_SEARCH) {
            return 'Modifying Headline Search campaigns is not yet supported.'
        }
        return null
    }

    static propTypes = {
        downloading: PropTypes.bool.isRequired,
        tableData: PropTypes.shape({
            data: PropTypes.arrayOf(PropTypes.shape()),
            updating: PropTypes.bool,
            deleting: PropTypes.bool,
            loading: PropTypes.bool,
            pagination: PropTypes.shape({
                pageSize: PropTypes.number,
                current: PropTypes.number,
                total: PropTypes.number,
            }),
            sorter: PropTypes.shape({
                field: PropTypes.string,
                order: PropTypes.oneOf(['descend', 'ascend']),
            }),
            columnSettings: PropTypes.shape({
                actionColumns: PropTypes.array,
                order: PropTypes.array,
                displayState: PropTypes.shape(),
            }),
        }).isRequired,
        currencyCode: PropTypes.string.isRequired,

        // actions
        updatePagination: PropTypes.func.isRequired,
        updateSorter: PropTypes.func.isRequired,
        updateColumnSettings: PropTypes.func.isRequired,
        reloadData: PropTypes.func.isRequired,
        downloadData: PropTypes.func.isRequired,
        updateCampaignRequest: PropTypes.func.isRequired,
        deleteCampaignRequest: PropTypes.func.isRequired,
    }

    state = {
        settingsModalVisible: false,
    }

    getTableOptions() {
        const { currencyCode } = this.props

        return {
            name: 'campaign',
            rowKey: 'campaign.id',
            columns: {
                actions: {
                    title: 'Actions',
                    dataIndex: 'keyword.id',
                    fixed: 'right',
                    align: 'center',
                    // custom fields
                    type: ACTIONS,
                },
                'campaign.name': {
                    title: 'Campaign Name',
                    dataIndex: 'campaign.name',
                    fixed: 'left',
                    width: 400,
                    render: (text, record) => (
                        <AppLink
                            to={generatePath(getPath(CAMPAIGN_PAGE), {
                                campaignId: record.campaign.id,
                            })}
                        >
                            {text}
                        </AppLink>
                    ),
                },
                'campaign.targeting_type': {
                    title: 'Targeting Type',
                    dataIndex: 'campaign.targeting_type',
                    align: 'right',
                    render: value =>
                        isNull(value) || isUndefined(value)
                            ? UNDEFINED_VALUE
                            : titleCase(value),
                },
                'campaign.state': {
                    title: 'State',
                    dataIndex: 'campaign.state',
                    align: 'right',
                    render: value => titleCase(value),
                    // custom fields
                    type: SELECT_INPUT,
                    fieldId: 'state',
                    options: [
                        { value: PAUSED, label: 'Paused' },
                        { value: ENABLED, label: 'Enabled' },
                    ],
                },
                'campaign.budget': {
                    title: 'Budget',
                    dataIndex: 'campaign.budget',
                    align: 'right',
                    render: value => formatCurrency(value, { currencyCode }),
                    // custom fields
                    type: NUMBER_INPUT,
                    fieldId: 'budget',
                    min: CAMPAIGN_BUDGET_MIN,
                    max: CAMPAIGN_BUDGET_MAX,
                    step: CAMPAIGN_BUDGET_STEP,
                    precision: CAMPAIGN_BUDGET_PRECISION,
                },
                'campaign.start_date': {
                    title: 'Start Date',
                    dataIndex: 'campaign.start_date',
                    align: 'right',
                    render: value => formatDate(value),
                },
                'campaign.end_date': {
                    title: 'End Date',
                    dataIndex: 'campaign.end_date',
                    align: 'right',
                    render: value => formatDate(value),
                    // custom fields
                    type: DATE_INPUT,
                    fieldId: 'end_date',
                    format: DAY_FORMAT,
                },
                ...METRIC_COLUMNS,
            },
        }
    }

    @autobind
    saveRecord({ id: campaignId, values: data }) {
        this.props.updateCampaignRequest({ campaignId, data })
    }

    @autobind
    deleteRecord({ id: campaignId }) {
        this.props.deleteCampaignRequest({ campaignId })
    }

    @autobind
    handleToggleSettingsModal() {
        this.setState({
            settingsModalVisible: !this.state.settingsModalVisible,
        })
    }

    @autobind
    handleUpdateColumnSettings(settings) {
        const { updateColumnSettings, reloadData } = this.props

        updateColumnSettings(settings)

        // Reload data
        reloadData()

        // Toggle settings modal
        this.handleToggleSettingsModal()
    }

    render() {
        const {
            downloading,
            tableData,
            updatePagination,
            updateSorter,
            reloadData,
            downloadData,
        } = this.props
        const { settingsModalVisible } = this.state
        const tableOptions = this.getTableOptions()

        return (
            <ContentCard
                title="Campaigns Table"
                subTitle="Manage and view tabular data for all Campaigns"
                actions={[
                    <SettingsButton
                        onClick={this.handleToggleSettingsModal}
                        tooltipTitle="Customize table columns"
                    />,
                    <DownloadButton
                        loading={downloading}
                        onClick={downloadData}
                    />,
                ]}
            >
                <PaginatedTable
                    tableOptions={tableOptions}
                    data={tableData.data}
                    loading={tableData.loading}
                    updating={tableData.updating}
                    deleting={tableData.deleting}
                    pagination={{
                        ...getTablePaginationOptions('Campaigns'),
                        ...tableData.pagination,
                    }}
                    sorter={tableData.sorter}
                    columnSettings={tableData.columnSettings}
                    updatePagination={updatePagination}
                    updateSorter={updateSorter}
                    reloadData={reloadData}
                    saveRecord={this.saveRecord}
                    deleteRecord={this.deleteRecord}
                    disableRowActions={CampaignsTable.disableRowActions}
                    actionsToolTip={CampaignsTable.actionsToolTip}
                />
                <SettingsModal
                    droppableId="paginatedTableSettingsModal"
                    modalTitle="Customize Table Columns"
                    visible={settingsModalVisible}
                    handleCancel={this.handleToggleSettingsModal}
                    handleOk={this.handleUpdateColumnSettings}
                    settings={tableData.columnSettings}
                    settingTitles={tableOptions.columns}
                />
            </ContentCard>
        )
    }
}

export default CampaignsTable
