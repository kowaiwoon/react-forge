import { connect } from 'react-redux'

import { CAMPAIGNS_SUMMARY_PAGE } from 'constants/pages'
import {
    selectDomainValue as selectUiDomainValue,
    selectPageDownloading,
    selectCurrencyCode,
} from 'selectors/ui'
import {
    updateCampaignsSummaryPageTablePagination,
    updateCampaignsSummaryPageTableSorter,
    updateCampaignsSummaryPageTableSettingsRequest,
    fetchCampaignsSummaryPageTableRequest,
    downloadCampaignsSummaryPageTableRequest,
    updateCampaignsSummaryPageTableRequest,
    deleteCampaignsSummaryPageTableRequest,
} from 'actions/ui'

import CampaignsTable from './CampaignsTable'

const mapStateToProps = state => ({
    tableData: selectUiDomainValue(state, [CAMPAIGNS_SUMMARY_PAGE, 'table']),
    downloading: selectPageDownloading(state, CAMPAIGNS_SUMMARY_PAGE),
    currencyCode: selectCurrencyCode(state),
})

const mapDispatchToProps = {
    updatePagination: updateCampaignsSummaryPageTablePagination,
    updateSorter: updateCampaignsSummaryPageTableSorter,
    updateColumnSettings: updateCampaignsSummaryPageTableSettingsRequest,
    reloadData: fetchCampaignsSummaryPageTableRequest,
    downloadData: downloadCampaignsSummaryPageTableRequest,
    updateCampaignRequest: updateCampaignsSummaryPageTableRequest,
    deleteCampaignRequest: deleteCampaignsSummaryPageTableRequest,
}

const CampaignsTableContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(CampaignsTable)

export default CampaignsTableContainer
