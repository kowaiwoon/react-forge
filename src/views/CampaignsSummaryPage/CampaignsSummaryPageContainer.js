import { connect } from 'react-redux'

import { selectDomainValue as selectUiDomainValue } from 'selectors/ui'
import {
    mountCampaignsSummaryPageRequest,
    unmountCampaignsSummaryPage,
} from 'actions/ui'
import { CAMPAIGNS_SUMMARY_PAGE } from 'constants/pages'
import { withTabState } from 'components/HigherOrderComponents'

import CampaignsSummaryPage from './CampaignsSummaryPage'

const mapStateToProps = state => ({
    mounting: selectUiDomainValue(state, [CAMPAIGNS_SUMMARY_PAGE, 'mounting']),
})

const mapDispatchToProps = {
    mountCampaignsSummary: mountCampaignsSummaryPageRequest,
    unmountCampaignsSummaryPage,
}

const CampaignsSummaryPageContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(CampaignsSummaryPage)

export default withTabState(CampaignsSummaryPageContainer, 'treemap')
