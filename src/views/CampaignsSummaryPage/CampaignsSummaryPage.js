import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Tabs } from 'antd'

import { PageHeader } from 'components/PageHeader'
import { LoadingIndicator } from 'components/LoadingIndicator'

import { CampaignsChartContainer } from './Charts'
import { CampaignsTableContainer } from './Tables'
import { FiltersContainer } from './Filters'
import styles from './styles.scss'

class CampaignsSummaryPage extends Component {
    static propTypes = {
        // Redux state
        mounting: PropTypes.bool.isRequired,

        // Redux actions
        mountCampaignsSummary: PropTypes.func.isRequired,
        unmountCampaignsSummaryPage: PropTypes.func.isRequired,

        // tab state
        tab: PropTypes.oneOf(['treemap', 'table']).isRequired,
        handleTabChange: PropTypes.func.isRequired,
    }

    componentDidMount() {
        this.props.mountCampaignsSummary()
    }

    componentWillUnmount() {
        this.props.unmountCampaignsSummaryPage()
    }

    render() {
        const { mounting, tab, handleTabChange } = this.props

        if (mounting) {
            return (
                <div className={styles['empty-content']}>
                    <LoadingIndicator size="small" />
                </div>
            )
        }

        return (
            <React.Fragment>
                <PageHeader
                    breadcrumbs={[{ name: 'All Campaigns' }]}
                    filterGroupComponent={<FiltersContainer />}
                />

                <Tabs
                    activeKey={tab}
                    onChange={handleTabChange}
                    tabPosition="top"
                    size="default"
                    tabBarStyle={{
                        fontWeight: '500', // same font-weight as selected tab
                    }}
                >
                    <Tabs.TabPane
                        tab="Campaigns Tree Chart"
                        key="treemap"
                        forceRender
                    >
                        <CampaignsChartContainer />
                    </Tabs.TabPane>

                    <Tabs.TabPane tab="Campaigns Table" key="table" forceRender>
                        <CampaignsTableContainer />
                    </Tabs.TabPane>
                </Tabs>
            </React.Fragment>
        )
    }
}

export default CampaignsSummaryPage
