import { connect } from 'react-redux'

import { SOV_KEYWORD_SEARCH_RESULT_PAGE } from 'constants/pages'
import { selectSovKeyword } from 'selectors/entities'
import { selectDomainValue as selectUiDomainValue } from 'selectors/ui'
import {
    mountSovKeywordSearchResultPageRequest,
    unmountSovKeywordSearchResultPage,
} from 'actions/ui'
import { withTabState } from 'components/HigherOrderComponents'

import SovKeywordSearchResultPage from './SovKeywordSearchResultPage'

const mapStateToProps = (state, ownProps) => {
    const { sovKeywordId, scheduledDate } = ownProps.match.params

    return {
        sovKeywordId,
        sovKeyword: selectSovKeyword(state, sovKeywordId),
        scheduledDate,
        screenshotUrl: selectUiDomainValue(state, [
            SOV_KEYWORD_SEARCH_RESULT_PAGE,
            'screenshotUrl',
        ]),
        mounting: selectUiDomainValue(state, [
            SOV_KEYWORD_SEARCH_RESULT_PAGE,
            'mounting',
        ]),
    }
}

const mapDispatchToProps = {
    mountSovKeywordSearchResultPage: mountSovKeywordSearchResultPageRequest,
    unmountSovKeywordSearchResultPage,
}

const SovKeywordSearchResultPageContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(SovKeywordSearchResultPage)

export default withTabState(SovKeywordSearchResultPageContainer, 'table')
