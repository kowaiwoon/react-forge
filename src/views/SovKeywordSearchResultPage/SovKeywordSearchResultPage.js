import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { generatePath } from 'react-router-dom'
import { Tabs, Button } from 'antd'
import isNull from 'lodash/isNull'

import { SOV_KEYWORDS_SUMMARY_PAGE, SOV_KEYWORD_PAGE } from 'constants/pages'
import { getPath } from 'helpers/pages'
import { formatCerebroDateTime } from 'helpers/formatting'
import { PageHeader } from 'components/PageHeader'
import { LoadingIndicator } from 'components/LoadingIndicator'
import { ContentCard } from 'components/ContentCard'

import { SearchPageResultsTableContainer } from './Tables'
import styles from './styles.scss'

class SovKeywordSearchResultPage extends Component {
    static propTypes = {
        sovKeywordId: PropTypes.string.isRequired,
        sovKeyword: PropTypes.shape({
            text: PropTypes.string,
        }),
        scheduledDate: PropTypes.string.isRequired,
        mounting: PropTypes.bool.isRequired,
        screenshotUrl: PropTypes.string,

        // actions
        mountSovKeywordSearchResultPage: PropTypes.func.isRequired,
        unmountSovKeywordSearchResultPage: PropTypes.func.isRequired,

        // tab state
        tab: PropTypes.oneOf(['table', 'screenshot']).isRequired,
        handleTabChange: PropTypes.func.isRequired,
    }

    static defaultProps = {
        sovKeyword: {},
        screenshotUrl: null,
    }

    componentDidMount() {
        const { sovKeywordId, scheduledDate } = this.props

        this.props.mountSovKeywordSearchResultPage({
            sovKeywordId,
            scheduledDate,
        })
    }

    componentWillUnmount() {
        this.props.unmountSovKeywordSearchResultPage()
    }

    render() {
        const {
            mounting,
            sovKeywordId,
            sovKeyword,
            scheduledDate,
            screenshotUrl,
            tab,
            handleTabChange,
        } = this.props

        if (mounting) {
            return (
                <div className={styles['empty-content']}>
                    <LoadingIndicator size="small" />
                </div>
            )
        }

        return (
            <React.Fragment>
                <PageHeader
                    breadcrumbs={[
                        {
                            name: 'Share of Voice',
                            url: getPath(SOV_KEYWORDS_SUMMARY_PAGE),
                        },
                        {
                            name: sovKeyword.text,
                            url: generatePath(getPath(SOV_KEYWORD_PAGE), {
                                sovKeywordId,
                            }),
                            icon: 'pie-chart',
                        },
                        { name: formatCerebroDateTime(scheduledDate) },
                    ]}
                />

                <Tabs
                    activeKey={tab}
                    onChange={handleTabChange}
                    tabPosition="top"
                    size="default"
                    tabBarStyle={{
                        fontWeight: '500', // same font-weight as selected tab
                    }}
                >
                    <Tabs.TabPane
                        tab="Search Page Results"
                        key="table"
                        forceRender
                    >
                        <SearchPageResultsTableContainer />
                    </Tabs.TabPane>
                    <Tabs.TabPane
                        tab="Full Page Screenshot"
                        key="screenshot"
                        forceRender
                    >
                        <ContentCard
                            title="Full Page Screenshot"
                            subTitle="View screenshot of the entire search results page."
                            bodyStyle={{
                                textAlign: 'center',
                            }}
                            actions={[
                                <Button
                                    icon="download"
                                    href={screenshotUrl}
                                    download={`sov_${sovKeywordId}_${scheduledDate}_screenshot.jpg`}
                                >
                                    Download
                                </Button>,
                            ]}
                        >
                            {isNull(screenshotUrl) ? (
                                'No screenshot available'
                            ) : (
                                <img
                                    src={screenshotUrl}
                                    alt="Full Page Screenshot"
                                    className={styles.screenshot}
                                />
                            )}
                        </ContentCard>
                    </Tabs.TabPane>
                </Tabs>
            </React.Fragment>
        )
    }
}

export default SovKeywordSearchResultPage
