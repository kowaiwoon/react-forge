import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import { SOV_KEYWORD_SEARCH_RESULT_PAGE } from 'constants/pages'
import {
    selectDomainValue as selectUiDomainValue,
    selectPageDownloading,
} from 'selectors/ui'
import {
    updateSovKeywordSearchResultPageTablePagination,
    updateSovKeywordSearchResultPageTableSorter,
    updateSovKeywordSearchResultPageTableSettings,
    fetchSovKeywordSearchResultPageTableRequest,
    downloadSovKeywordSearchResultPageTableRequest,
} from 'actions/ui'
import { selectSovKeyword } from 'selectors/entities'

import SearchPageResultsTable from './SearchPageResultsTable'

const mapStateToProps = (state, ownProps) => {
    const { sovKeywordId } = ownProps.match.params
    return {
        sovKeyword: selectSovKeyword(state, sovKeywordId),
        tableData: selectUiDomainValue(state, [
            SOV_KEYWORD_SEARCH_RESULT_PAGE,
            'table',
        ]),
        downloading: selectPageDownloading(
            state,
            SOV_KEYWORD_SEARCH_RESULT_PAGE
        ),
    }
}

const mapDispatchToProps = {
    updatePagination: updateSovKeywordSearchResultPageTablePagination,
    updateSorter: updateSovKeywordSearchResultPageTableSorter,
    updateColumnSettings: updateSovKeywordSearchResultPageTableSettings,
    reloadData: fetchSovKeywordSearchResultPageTableRequest,
    downloadData: downloadSovKeywordSearchResultPageTableRequest,
}

const SearchPageResultsTableContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(SearchPageResultsTable)

export default withRouter(SearchPageResultsTableContainer)
