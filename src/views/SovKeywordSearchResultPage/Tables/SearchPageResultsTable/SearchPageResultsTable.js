/* global Image */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import truncate from 'lodash/truncate'
import { Modal, Icon } from 'antd'
import autobind from 'autobind-decorator'

import { PaginatedTable, ActionIcon } from 'components/PaginatedTable'
import { ToolTip } from 'components/ToolTip'
import { LoadingIndicator } from 'components/LoadingIndicator'
import { asinUrl } from 'helpers/urls'
import { getTablePaginationOptions } from 'helpers/pagination'
import { titleCase } from 'helpers/formatting'
import { UNDEFINED_VALUE } from 'constants/formatting'
import { ContentCard } from 'components/ContentCard'
import { SettingsButton, DownloadButton } from 'components/Buttons'
import { SettingsModal } from 'components/SettingsModal'

import styles from './styles.scss'

const MIN_MODAL_WIDTH = 500
const HEADLINE_SECTION_MODAL_WIDTH = 1083
const ORGANIC_MODAL_WIDTH = 1079

class SearchPageResultsTable extends Component {
    static propTypes = {
        sovKeyword: PropTypes.shape({
            country_code: PropTypes.string.isRequired,
        }),
        downloading: PropTypes.bool.isRequired,
        tableData: PropTypes.shape({
            data: PropTypes.arrayOf(PropTypes.shape()),
            loading: PropTypes.bool,
            updating: PropTypes.bool,
            deleting: PropTypes.bool,
            pagination: PropTypes.shape({
                pageSize: PropTypes.number,
                current: PropTypes.number,
                total: PropTypes.number,
            }),
            sorter: PropTypes.shape({
                field: PropTypes.string,
                order: PropTypes.oneOf(['descend', 'ascend']),
            }),
            columnSettings: PropTypes.shape({
                actionColumns: PropTypes.array,
                order: PropTypes.array,
                displayState: PropTypes.shape(),
            }),
        }).isRequired,

        // actions
        updatePagination: PropTypes.func.isRequired,
        updateSorter: PropTypes.func.isRequired,
        updateColumnSettings: PropTypes.func.isRequired,
        reloadData: PropTypes.func.isRequired,
        downloadData: PropTypes.func.isRequired,
    }

    static defaultProps = {
        sovKeyword: {},
    }

    state = {
        modalVisible: false,
        selectedImage: {},
        selectedResultType: null,
        imageLoading: false,
        settingsModalVisible: false,
    }

    getTableOptions() {
        const {
            sovKeyword: { country_code: countryCode },
        } = this.props
        return {
            name: 'results',
            rowKey: 'id',
            columns: {
                rank: {
                    title: 'Rank',
                    dataIndex: 'rank',
                    fixed: 'left',
                    sorter: true,
                    align: 'center',
                    width: 100,
                    render: rank => rank + 1,
                },
                result_type: {
                    title: 'Result Type',
                    dataIndex: 'result_type',
                    sorter: true,
                    align: 'left',
                    render: value => titleCase(value),
                },
                text: {
                    title: 'Keyword',
                    dataIndex: 'text',
                    sorter: true,
                    align: 'left',
                },
                'product_metadata.brand': {
                    title: 'Brand',
                    dataIndex: 'product_metadata.brand',
                    align: 'left',
                    render: value => value || UNDEFINED_VALUE,
                },
                asin: {
                    title: 'ASIN',
                    dataIndex: 'asin',
                    sorter: true,
                    align: 'left',
                    render: value =>
                        value ? (
                            <a
                                href={asinUrl(value, countryCode)}
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                {value} <Icon type="amazon" theme="outlined" />
                            </a>
                        ) : (
                            UNDEFINED_VALUE
                        ),
                },
                'product_metadata.title': {
                    title: 'Title',
                    dataIndex: 'product_metadata.title',
                    align: 'left',
                    render: text =>
                        text ? (
                            <ToolTip title={text}>{truncate(text)}</ToolTip>
                        ) : (
                            UNDEFINED_VALUE
                        ),
                },
                actions: {
                    title: 'Actions',
                    dataIndex: 'id',
                    fixed: 'right',
                    sorter: false,
                    align: 'center',
                    render: (value, record) => (
                        <div className={styles.container}>
                            <ActionIcon
                                type="picture"
                                onClick={() => this.handleOpenModal(record)}
                            />
                        </div>
                    ),
                },
            },
        }
    }

    getModalWidth() {
        const { imageLoading, selectedImage, selectedResultType } = this.state

        // Empty modal is set to the correct width to avoid resize jank
        // when image is rendered
        if (imageLoading) {
            if (selectedResultType === 'headline_section') {
                return HEADLINE_SECTION_MODAL_WIDTH
            } else if (selectedResultType === 'organic') {
                return ORGANIC_MODAL_WIDTH
            }
            return MIN_MODAL_WIDTH
        }

        // Image has loaded so use its width plus padding to
        // calculate the modal width.
        const modalWidth = selectedImage.width + 48
        if (modalWidth < MIN_MODAL_WIDTH) {
            return MIN_MODAL_WIDTH
        }
        return modalWidth
    }

    handleOpenModal(record) {
        // Set modal to "loading" state
        this.setState({
            modalVisible: true,
            imageLoading: true,
            selectedImage: {},
            selectedResultType: record.result_type,
        })

        // Download the image and set up onload handler
        const img = new Image()
        img.onload = () => {
            this.setState({ selectedImage: img, imageLoading: false })
        }
        img.src = record.image_url
    }

    @autobind
    handleCloseModal() {
        this.setState({ modalVisible: false })
    }

    @autobind
    handleToggleSettingsModal() {
        this.setState({
            settingsModalVisible: !this.state.settingsModalVisible,
        })
    }

    @autobind
    handleUpdateColumnSettings(settings) {
        const { updateColumnSettings, reloadData } = this.props

        updateColumnSettings(settings)

        // Reload data
        reloadData()

        // Toggle settings modal
        this.handleToggleSettingsModal()
    }

    render() {
        const {
            downloading,
            tableData,
            updatePagination,
            updateSorter,
            reloadData,
            downloadData,
        } = this.props
        const {
            selectedImage,
            modalVisible,
            imageLoading,
            settingsModalVisible,
        } = this.state
        const tableOptions = this.getTableOptions()

        return (
            <ContentCard
                title="Search Results Table"
                subTitle="View tabular data for all Search Results"
                actions={[
                    <SettingsButton
                        onClick={this.handleToggleSettingsModal}
                        tooltipTitle="Customize table columns"
                    />,
                    <DownloadButton
                        loading={downloading}
                        onClick={downloadData}
                    />,
                ]}
            >
                <PaginatedTable
                    tableOptions={tableOptions}
                    data={tableData.data}
                    loading={tableData.loading}
                    updating={tableData.updating}
                    deleting={tableData.deleting}
                    pagination={{
                        ...getTablePaginationOptions('Search Results'),
                        ...tableData.pagination,
                    }}
                    sorter={tableData.sorter}
                    columnSettings={tableData.columnSettings}
                    updatePagination={updatePagination}
                    updateSorter={updateSorter}
                    reloadData={reloadData}
                    downloadData={downloadData}
                />
                <SettingsModal
                    droppableId="paginatedTableSettingsModal"
                    modalTitle="Customize Table Columns"
                    visible={settingsModalVisible}
                    handleCancel={this.handleToggleSettingsModal}
                    handleOk={this.handleUpdateColumnSettings}
                    settings={tableData.columnSettings}
                    settingTitles={tableOptions.columns}
                />
                <Modal
                    visible={modalVisible}
                    footer={null}
                    width={this.getModalWidth()}
                    title="Search Page Result Image"
                    onCancel={this.handleCloseModal}
                >
                    {imageLoading ? (
                        <div style={{ height: '233px' }}>
                            <LoadingIndicator size="small" />
                        </div>
                    ) : (
                        <img
                            className={styles['modal-img']}
                            src={selectedImage.src}
                            alt="Search Result"
                        />
                    )}
                </Modal>
            </ContentCard>
        )
    }
}

export default SearchPageResultsTable
