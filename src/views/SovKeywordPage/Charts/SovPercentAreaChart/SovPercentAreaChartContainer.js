import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import { SOV_KEYWORD_PAGE } from 'constants/pages'
import { SOV_AGGREGATION } from 'constants/filters'
import { updateSovKeywordPageSovChartWeight } from 'actions/ui'
import {
    selectLoadingForSovChart,
    selectDataForSovChart,
    selectPageFilters,
    selectPageSovChartWeight,
} from 'selectors/ui'

import SovPercentAreaChart from './SovPercentAreaChart'

const mapStateToProps = state => {
    const { series, maxDataPoints } = selectDataForSovChart(
        state,
        SOV_KEYWORD_PAGE
    )
    return {
        loading: selectLoadingForSovChart(state, SOV_KEYWORD_PAGE),
        series,
        maxDataPoints,
        aggregate: selectPageFilters(state, SOV_KEYWORD_PAGE)[SOV_AGGREGATION],
        weight: selectPageSovChartWeight(state, SOV_KEYWORD_PAGE),
    }
}

const mapDispatchToProps = {
    updateChartWeight: updateSovKeywordPageSovChartWeight,
}

const SovPercentAreaChartContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(SovPercentAreaChart)

export default withRouter(SovPercentAreaChartContainer)
