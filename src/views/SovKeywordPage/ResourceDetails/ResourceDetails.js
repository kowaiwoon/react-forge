import React from 'react'
import PropTypes from 'prop-types'
import autobind from 'autobind-decorator'

import { ResourceDetails } from 'components/ResourceDetails'
import { titleCase, formatFrequency } from 'helpers/formatting'
import { SELECT_INPUT, NUMBER_INPUT } from 'constants/inputTypes'
import { PAUSED, ENABLED, ARCHIVED } from 'constants/resourceStates'
import { SOV_WRITE } from 'constants/featurePermissions'
import { sovKeywordAttributeTooltips } from 'configuration/attributes'
import { hasPermissions } from 'helpers/featurePermissions'
import {
    SOV_KEYWORD_FREQUENCY_MAX,
    SOV_KEYWORD_FREQUENCY_MIN,
    SOV_KEYWORD_FREQUENCY_PRECISION,
    SOV_KEYWORD_FREQUENCY_STEP,
} from 'constants/sovKeywords'

import styles from './styles.scss'

const renderTooltipWithHelptext = attribute => {
    const info = sovKeywordAttributeTooltips[attribute]

    return info.map(item => {
        const { title, description } = item

        return (
            <div className={styles.tooltip} key={description}>
                {title && <span className={styles.title}>{title}</span>}
                <span className={styles.description}>{description}</span>
            </div>
        )
    })
}

class KeywordResourceDetails extends React.Component {
    static propTypes = {
        sovKeyword: PropTypes.shape({
            text: PropTypes.string,
            state: PropTypes.string,
        }),
        featurePermissions: PropTypes.arrayOf(PropTypes.string).isRequired,
        sovKeywordUpdating: PropTypes.bool.isRequired,
        showDetails: PropTypes.bool.isRequired,

        // actions
        updateSovKeywordRequest: PropTypes.func.isRequired,
        toggleSovKeywordPageDetails: PropTypes.func.isRequired,
    }

    static defaultProps = {
        sovKeyword: {},
    }

    @autobind
    handleUpdateKeywordDetails(values) {
        const { updateSovKeywordRequest } = this.props
        updateSovKeywordRequest(values)
    }

    editToolTip() {
        if (this.props.sovKeyword.state === ARCHIVED) {
            return 'Archived Keywords cannot be modified.'
        }
        if (!hasPermissions(this.props.featurePermissions, SOV_WRITE)) {
            return 'Must have write access to modify Share of Voice Keywords'
        }
        return null
    }

    render() {
        const {
            sovKeyword,
            showDetails,
            featurePermissions,
            sovKeywordUpdating,
            toggleSovKeywordPageDetails,
        } = this.props
        return (
            <ResourceDetails
                name={sovKeyword.text}
                showDetails={showDetails}
                allowEditing={
                    hasPermissions(featurePermissions, SOV_WRITE) &&
                    sovKeyword.state !== ARCHIVED
                }
                editToolTip={this.editToolTip()}
                updating={sovKeywordUpdating}
                onShowDetailsClick={toggleSovKeywordPageDetails}
                onSave={this.handleUpdateKeywordDetails}
                details={[
                    {
                        label: 'State',
                        value: sovKeyword.state,
                        toolTip: renderTooltipWithHelptext('state'),
                        formatValue: titleCase,
                        // input fields
                        fieldId: 'state',
                        type: SELECT_INPUT,
                        options: [
                            { value: PAUSED, label: 'Paused' },
                            { value: ENABLED, label: 'Enabled' },
                        ],
                    },
                    {
                        label: 'Frequency',
                        value: sovKeyword.frequency_in_hours,
                        toolTip: renderTooltipWithHelptext(
                            'frequency_in_hours'
                        ),
                        formatValue: value => formatFrequency(value),
                        // input fields
                        type: NUMBER_INPUT,
                        fieldId: 'frequency_in_hours',
                        min: SOV_KEYWORD_FREQUENCY_MIN,
                        max: SOV_KEYWORD_FREQUENCY_MAX,
                        step: SOV_KEYWORD_FREQUENCY_STEP,
                        precision: SOV_KEYWORD_FREQUENCY_PRECISION,
                    },
                ]}
            />
        )
    }
}

export default KeywordResourceDetails
