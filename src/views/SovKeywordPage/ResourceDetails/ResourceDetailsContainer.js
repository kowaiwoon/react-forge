import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import { selectDomainValue as selectAuthDomainValue } from 'selectors/auth'
import { selectDomainValue as selectUiDomainValue } from 'selectors/ui'
import { selectSovKeyword } from 'selectors/entities'
import { SOV_KEYWORD_PAGE } from 'constants/pages'
import {
    toggleSovKeywordPageDetails,
    updateSovKeywordPageKeywordDetailsRequest,
} from 'actions/ui'

import ResourceDetails from './ResourceDetails'

const mapStateToProps = (state, ownProps) => {
    const { sovKeywordId } = ownProps.match.params

    return {
        sovKeyword: selectSovKeyword(state, sovKeywordId),
        featurePermissions: selectAuthDomainValue(state, 'featurePermissions'),
        sovKeywordUpdating: selectUiDomainValue(state, [
            SOV_KEYWORD_PAGE,
            'keywordUpdating',
        ]),
        showDetails: selectUiDomainValue(state, [
            SOV_KEYWORD_PAGE,
            'showDetails',
        ]),
    }
}

const mapDispatchToProps = {
    updateSovKeywordRequest: updateSovKeywordPageKeywordDetailsRequest,
    toggleSovKeywordPageDetails,
}

const ResourceDetailsContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ResourceDetails)

export default withRouter(ResourceDetailsContainer)
