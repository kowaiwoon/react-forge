import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import { SOV_KEYWORD_PAGE } from 'constants/pages'
import { selectSovKeyword } from 'selectors/entities'
import {
    selectDomainValue as selectUiDomainValue,
    selectPageDownloading,
} from 'selectors/ui'
import {
    updateSovKeywordPageTablePagination,
    updateSovKeywordPageTableSorter,
    updateSovKeywordPageTableSettings,
    fetchSovKeywordPageTableRequest,
    downloadSovKeywordPageTableRequest,
} from 'actions/ui'

import SerpDataPointsTable from './SerpDataPointsTable'

const mapStateToProps = (state, ownProps) => {
    const { sovKeywordId } = ownProps.match.params

    return {
        sovKeyword: selectSovKeyword(state, sovKeywordId),
        tableData: selectUiDomainValue(state, [SOV_KEYWORD_PAGE, 'table']),
        downloading: selectPageDownloading(state, SOV_KEYWORD_PAGE),
    }
}

const mapDispatchToProps = {
    updatePagination: updateSovKeywordPageTablePagination,
    updateSorter: updateSovKeywordPageTableSorter,
    updateColumnSettings: updateSovKeywordPageTableSettings,
    reloadData: fetchSovKeywordPageTableRequest,
    downloadData: downloadSovKeywordPageTableRequest,
}

const SerpDataPointsTableContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(SerpDataPointsTable)

export default withRouter(SerpDataPointsTableContainer)
