import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { generatePath } from 'react-router-dom'
import autobind from 'autobind-decorator'

import { AppLink } from 'components/AppLink'
import { PaginatedTable } from 'components/PaginatedTable'
import { getTablePaginationOptions } from 'helpers/pagination'
import { formatCerebroDateTime } from 'helpers/formatting'
import { SOV_KEYWORD_SEARCH_RESULT_PAGE } from 'constants/pages'
import { getPath } from 'helpers/pages'
import { ContentCard } from 'components/ContentCard'
import { SettingsButton, DownloadButton } from 'components/Buttons'
import { SettingsModal } from 'components/SettingsModal'

class SerpDataPointsTable extends Component {
    static propTypes = {
        sovKeyword: PropTypes.shape({
            id: PropTypes.string,
            text: PropTypes.string,
            region: PropTypes.string,
            country_code: PropTypes.string,
            language_code: PropTypes.string,
            updated_at: PropTypes.string,
            enabled: PropTypes.bool,
            frequency_in_hours: PropTypes.number,
        }),
        downloading: PropTypes.bool.isRequired,
        tableData: PropTypes.shape({
            data: PropTypes.arrayOf(PropTypes.shape()),
            loading: PropTypes.bool,
            updating: PropTypes.bool,
            deleting: PropTypes.bool,
            pagination: PropTypes.shape({
                pageSize: PropTypes.number,
                current: PropTypes.number,
                total: PropTypes.number,
            }),
            sorter: PropTypes.shape({
                field: PropTypes.string,
                order: PropTypes.oneOf(['descend', 'ascend']),
            }),
            columnSettings: PropTypes.shape({
                actionColumns: PropTypes.array,
                order: PropTypes.array,
                displayState: PropTypes.shape(),
            }),
        }).isRequired,

        // actions
        updatePagination: PropTypes.func.isRequired,
        updateSorter: PropTypes.func.isRequired,
        updateColumnSettings: PropTypes.func.isRequired,
        reloadData: PropTypes.func.isRequired,
        downloadData: PropTypes.func.isRequired,
    }

    static defaultProps = {
        sovKeyword: {},
    }

    state = {
        settingsModalVisible: false,
    }

    getTableOptions() {
        const {
            sovKeyword: { id: sovKeywordId },
        } = this.props

        return {
            name: 'searches',
            rowKey: 'id',
            columns: {
                scheduled_date: {
                    title: 'Scheduled Date',
                    dataIndex: 'scheduled_date',
                    fixed: 'left',
                    sorter: true,
                    align: 'right',
                    width: 200,
                    render: (text, record) => (
                        <AppLink
                            to={generatePath(
                                getPath(SOV_KEYWORD_SEARCH_RESULT_PAGE),
                                {
                                    sovKeywordId,
                                    scheduledDate: record.scheduled_date,
                                }
                            )}
                        >
                            {formatCerebroDateTime(text)}
                        </AppLink>
                    ),
                },
                search_time: {
                    title: 'Search Time',
                    dataIndex: 'search_time',
                    sorter: true,
                    align: 'right',
                    width: 200,
                    render: value => formatCerebroDateTime(value),
                },
                country_code: {
                    title: 'Country',
                    dataIndex: 'country_code',
                    sorter: true,
                    align: 'center',
                },
                language_code: {
                    title: 'Language',
                    dataIndex: 'language_code',
                    sorter: true,
                    align: 'center',
                },
                text: {
                    title: 'Keyword',
                    dataIndex: 'text',
                    sorter: true,
                    align: 'left',
                },
                sponsored_result_count: {
                    title: 'Sponsored Results',
                    dataIndex: 'sponsored_result_count',
                    align: 'right',
                },
                result_count: {
                    title: 'Total Results',
                    dataIndex: 'result_count',
                    align: 'right',
                },
            },
        }
    }

    @autobind
    handleToggleSettingsModal() {
        this.setState({
            settingsModalVisible: !this.state.settingsModalVisible,
        })
    }

    @autobind
    handleUpdateColumnSettings(settings) {
        const { updateColumnSettings, reloadData } = this.props

        updateColumnSettings(settings)

        // Reload data
        reloadData()

        // Toggle settings modal
        this.handleToggleSettingsModal()
    }

    render() {
        const {
            downloading,
            tableData,
            updatePagination,
            updateSorter,
            reloadData,
            downloadData,
        } = this.props
        const { settingsModalVisible } = this.state
        const tableOptions = this.getTableOptions()

        return (
            <ContentCard
                title="Searches Table"
                subTitle="View tabular data for all Searches"
                actions={[
                    <SettingsButton
                        onClick={this.handleToggleSettingsModal}
                        tooltipTitle="Customize table columns"
                    />,
                    <DownloadButton
                        loading={downloading}
                        onClick={downloadData}
                    />,
                ]}
            >
                <PaginatedTable
                    tableOptions={tableOptions}
                    data={tableData.data}
                    loading={tableData.loading}
                    updating={tableData.updating}
                    deleting={tableData.deleting}
                    pagination={{
                        ...getTablePaginationOptions('Searches'),
                        ...tableData.pagination,
                    }}
                    sorter={tableData.sorter}
                    columnSettings={tableData.columnSettings}
                    updatePagination={updatePagination}
                    updateSorter={updateSorter}
                    reloadData={reloadData}
                    downloadData={downloadData}
                />
                <SettingsModal
                    droppableId="paginatedTableSettingsModal"
                    modalTitle="Customize Table Columns"
                    visible={settingsModalVisible}
                    handleCancel={this.handleToggleSettingsModal}
                    handleOk={this.handleUpdateColumnSettings}
                    settings={tableData.columnSettings}
                    settingTitles={tableOptions.columns}
                />
            </ContentCard>
        )
    }
}

export default SerpDataPointsTable
