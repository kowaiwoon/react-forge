import { connect } from 'react-redux'

import { SOV_KEYWORD_PAGE } from 'constants/pages'
import { selectSovKeyword } from 'selectors/entities'
import { selectDomainValue as selectUiDomainValue } from 'selectors/ui'
import { mountSovKeywordPageRequest, unmountSovKeywordPage } from 'actions/ui'
import { withTabState } from 'components/HigherOrderComponents'

import SovKeywordPage from './SovKeywordPage'

const mapStateToProps = (state, ownProps) => {
    const { sovKeywordId } = ownProps.match.params

    return {
        sovKeywordId,
        sovKeyword: selectSovKeyword(state, sovKeywordId),
        mounting: selectUiDomainValue(state, [SOV_KEYWORD_PAGE, 'mounting']),
    }
}

const mapDispatchToProps = {
    mountSovKeywordPage: mountSovKeywordPageRequest,
    unmountSovKeywordPage,
}

const SovKeywordPageContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(SovKeywordPage)

export default withTabState(SovKeywordPageContainer, 'timeline')
