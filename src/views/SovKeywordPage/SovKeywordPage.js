import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Tabs } from 'antd'

import { SOV_KEYWORDS_SUMMARY_PAGE } from 'constants/pages'
import { getPath } from 'helpers/pages'
import { PageHeader } from 'components/PageHeader'
import { LoadingIndicator } from 'components/LoadingIndicator'

import { SerpDataPointsTableContainer } from './Tables'
import { SovPercentAreaChartContainer } from './Charts'
import { ResourceDetailsContainer } from './ResourceDetails'
import { FiltersContainer } from './Filters'
import styles from './styles.scss'

class SovKeywordPage extends Component {
    static propTypes = {
        sovKeywordId: PropTypes.string.isRequired,
        sovKeyword: PropTypes.shape({
            text: PropTypes.string,
            state: PropTypes.string,
        }),
        mounting: PropTypes.bool.isRequired,

        // actions
        mountSovKeywordPage: PropTypes.func.isRequired,
        unmountSovKeywordPage: PropTypes.func.isRequired,

        // tab state
        tab: PropTypes.oneOf(['timeline', 'table']).isRequired,
        handleTabChange: PropTypes.func.isRequired,
    }

    static defaultProps = {
        sovKeyword: {},
    }

    componentDidMount() {
        const { sovKeywordId } = this.props
        this.props.mountSovKeywordPage({ sovKeywordId })
    }

    componentWillUnmount() {
        this.props.unmountSovKeywordPage()
    }

    render() {
        const { sovKeyword, mounting, tab, handleTabChange } = this.props

        if (mounting) {
            return (
                <div className={styles['empty-content']}>
                    <LoadingIndicator size="small" />
                </div>
            )
        }

        return (
            <React.Fragment>
                <PageHeader
                    breadcrumbs={[
                        {
                            name: 'Share of Voice',
                            url: getPath(SOV_KEYWORDS_SUMMARY_PAGE),
                        },
                        { name: sovKeyword.text, icon: 'pie-chart' },
                    ]}
                    titleComponent={<ResourceDetailsContainer />}
                    filterGroupComponent={<FiltersContainer />}
                />

                <Tabs
                    activeKey={tab}
                    onChange={handleTabChange}
                    tabPosition="top"
                    size="default"
                    tabBarStyle={{
                        fontWeight: '500', // same font-weight as selected tab
                    }}
                >
                    <Tabs.TabPane
                        tab="Share of Voice"
                        key="timeline"
                        forceRender
                    >
                        <SovPercentAreaChartContainer />
                    </Tabs.TabPane>
                    <Tabs.TabPane
                        tab="SERP Data Points"
                        key="table"
                        forceRender
                    >
                        <SerpDataPointsTableContainer />
                    </Tabs.TabPane>
                </Tabs>
            </React.Fragment>
        )
    }
}

export default SovKeywordPage
