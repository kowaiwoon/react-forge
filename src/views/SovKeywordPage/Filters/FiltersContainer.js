import { connect } from 'react-redux'

import { selectPageFilters, selectPageFilterSettings } from 'selectors/ui'
import {
    updatePageFilterForPage,
    resetPageFiltersForPage,
    updatePageFilterSettingsForPage,
} from 'actions/ui'
import { SOV_KEYWORD_PAGE } from 'constants/pages'

import Filters from './Filters'

const mapStateToProps = state => ({
    filterSettings: selectPageFilterSettings(state, SOV_KEYWORD_PAGE),
    selectedFilterValues: selectPageFilters(state, SOV_KEYWORD_PAGE),
})

const mapDispatchToProps = {
    updatePageFilter: updatePageFilterForPage(SOV_KEYWORD_PAGE),
    resetFilters: resetPageFiltersForPage(SOV_KEYWORD_PAGE),
    updateFilterSettings: updatePageFilterSettingsForPage(SOV_KEYWORD_PAGE),
}

const FiltersContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Filters)

export default FiltersContainer
