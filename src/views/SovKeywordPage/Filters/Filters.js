import React from 'react'
import PropTypes from 'prop-types'
import autobind from 'autobind-decorator'
import set from 'lodash/fp/set'

import {
    SOV_AGGREGATION,
    SOV_SEARCH_TIMES,
    SOV_FOLDS,
    SOV_RESULT_TYPES,
} from 'constants/filters'
import {
    SovAggregationFilter,
    SearchTimeFilter,
    SovFoldsFilter,
    SovResultTypeFilter,
} from 'components/Filters'
import { FilterGroup } from 'components/FilterGroup'

class Filters extends React.Component {
    static propTypes = {
        filterSettings: PropTypes.shape().isRequired,
        selectedFilterValues: PropTypes.shape().isRequired,

        // actions
        updatePageFilter: PropTypes.func.isRequired,
        resetFilters: PropTypes.func.isRequired,
        updateFilterSettings: PropTypes.func.isRequired,
    }

    @autobind
    handleApply(key, value) {
        this.props.updatePageFilter({
            key,
            value,
        })
    }

    @autobind
    handleClose(filter) {
        const { filterSettings } = this.props
        this.props.updateFilterSettings(
            set(['displayState', filter], false, filterSettings)
        )
    }

    render() {
        const {
            filterSettings,
            selectedFilterValues,
            updateFilterSettings,
            resetFilters,
        } = this.props
        return (
            <FilterGroup
                filterSettings={filterSettings}
                updateFilterSettings={updateFilterSettings}
                resetFilters={resetFilters}
            >
                <SovAggregationFilter
                    value={selectedFilterValues[SOV_AGGREGATION]}
                    onApply={this.handleApply}
                    closable={false}
                />
                <SearchTimeFilter
                    values={selectedFilterValues[SOV_SEARCH_TIMES]}
                    onApply={this.handleApply}
                    closable={false}
                />
                <SovFoldsFilter
                    value={selectedFilterValues[SOV_FOLDS].value}
                    onApply={this.handleApply}
                    onCloseFilter={this.handleClose}
                    closable
                    operator={selectedFilterValues[SOV_FOLDS].operator}
                />
                <SovResultTypeFilter
                    values={selectedFilterValues[SOV_RESULT_TYPES]}
                    onApply={this.handleApply}
                    onCloseFilter={this.handleClose}
                    closable
                />
            </FilterGroup>
        )
    }
}

export default Filters
