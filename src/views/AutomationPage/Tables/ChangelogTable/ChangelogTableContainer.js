import { connect } from 'react-redux'

import { AUTOMATION_PAGE } from 'constants/pages'
import { selectDomainValue as selectUiDomainValue } from 'selectors/ui'
import {
    updateAutomationPageTablePagination,
    updateAutomationPageTableSorter,
    updateAutomationPageTableSettings,
    fetchAutomationPageTableRequest,
} from 'actions/ui'

import ChangelogTable from './ChangelogTable'

const mapStateToProps = state => ({
    tableData: selectUiDomainValue(state, [AUTOMATION_PAGE, 'table']),
})

const mapDispatchToProps = {
    updatePagination: updateAutomationPageTablePagination,
    updateSorter: updateAutomationPageTableSorter,
    updateColumnSettings: updateAutomationPageTableSettings,
    reloadData: fetchAutomationPageTableRequest,
}

const ChangelogTableContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ChangelogTable)

export default ChangelogTableContainer
