import { connect } from 'react-redux'

import { KEYWORDS_SUMMARY_PAGE } from 'constants/pages'
import { selectDomainValue as selectUiDomainValue } from 'selectors/ui'
import {
    mountKeywordsSummaryPageRequest,
    unmountKeywordsPageSummary,
} from 'actions/ui'
import { withTabState } from 'components/HigherOrderComponents'

import KeywordsSummaryPage from './KeywordsSummaryPage'

const mapStateToProps = state => ({
    mounting: selectUiDomainValue(state, [KEYWORDS_SUMMARY_PAGE, 'mounting']),
})

const mapDispatchToProps = {
    mountKeywordsSummary: mountKeywordsSummaryPageRequest,
    unmountKeywordsSummary: unmountKeywordsPageSummary,
}

const KeywordsSummaryPageContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(KeywordsSummaryPage)

export default withTabState(KeywordsSummaryPageContainer, 'treemap')
