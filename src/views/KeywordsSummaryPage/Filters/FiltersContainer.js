import { connect } from 'react-redux'

import {
    selectDomainValue as selectUiDomainValue,
    selectPageFilters,
    selectPageFilterSettings,
} from 'selectors/ui'
import {
    selectBrandsForFilters,
    selectCampaignsForFilters,
    selectKeywordsForFilters,
} from 'selectors/entities'
import {
    updatePageFilterForPage,
    resetPageFiltersForPage,
    updatePageFilterSettingsForPage,
    changeBrandsFilterInput,
    changeCampaignsFilterInputForPage,
    changeKeywordsFilterInput,
} from 'actions/ui'
import { KEYWORDS_SUMMARY_PAGE } from 'constants/pages'

import Filters from './Filters'

const mapStateToProps = state => ({
    filterSettings: selectPageFilterSettings(state, KEYWORDS_SUMMARY_PAGE),
    selectedFilterValues: selectPageFilters(state, KEYWORDS_SUMMARY_PAGE),
    brandsFilterOptions: selectBrandsForFilters(state),
    brandsFilterLoading: selectUiDomainValue(state, [
        'app',
        'brandsFilterLoading',
    ]),
    campaignsFilterOptions: selectCampaignsForFilters(state),
    campaignsFilterLoading: selectUiDomainValue(state, [
        'app',
        'campaignsFilterLoading',
    ]),
    keywordsFilterOptions: selectKeywordsForFilters(state),
    keywordsFilterLoading: selectUiDomainValue(state, [
        KEYWORDS_SUMMARY_PAGE,
        'keywordsFilterLoading',
    ]),
})

const mapDispatchToProps = {
    updatePageFilter: updatePageFilterForPage(KEYWORDS_SUMMARY_PAGE),
    resetFilters: resetPageFiltersForPage(KEYWORDS_SUMMARY_PAGE),
    updateFilterSettings: updatePageFilterSettingsForPage(
        KEYWORDS_SUMMARY_PAGE
    ),
    changeBrandsFilterInput,
    changeCampaignsFilterInput: changeCampaignsFilterInputForPage(
        KEYWORDS_SUMMARY_PAGE
    ),
    changeKeywordsFilterInput,
}

const FiltersContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Filters)

export default FiltersContainer
