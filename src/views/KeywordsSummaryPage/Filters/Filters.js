import React from 'react'
import PropTypes from 'prop-types'
import autobind from 'autobind-decorator'
import set from 'lodash/fp/set'

import {
    FACT_TYPES,
    DATES,
    REGIONS,
    COUNTRIES,
    BRANDS,
    CAMPAIGNS,
    CAMPAIGN_TARGETING_TYPES,
    KEYWORD_MATCH_TYPES,
    KEYWORDS,
} from 'constants/filters'
import {
    DateRangeFilter,
    AdTypeFilter,
    RegionsFilter,
    CountriesFilter,
    BrandsFilter,
    CampaignsFilter,
    CampaignTargetingTypesFilter,
    KeywordsFilter,
    KeywordMatchTypesFilter,
} from 'components/Filters'
import { FilterGroup } from 'components/FilterGroup'

class Filters extends React.Component {
    static propTypes = {
        filterSettings: PropTypes.shape().isRequired,
        selectedFilterValues: PropTypes.shape().isRequired,
        campaignsFilterOptions: PropTypes.array.isRequired,
        campaignsFilterLoading: PropTypes.bool.isRequired,
        brandsFilterOptions: PropTypes.array.isRequired,
        brandsFilterLoading: PropTypes.bool.isRequired,
        keywordsFilterOptions: PropTypes.array.isRequired,
        keywordsFilterLoading: PropTypes.bool.isRequired,

        // actions
        updatePageFilter: PropTypes.func.isRequired,
        resetFilters: PropTypes.func.isRequired,
        updateFilterSettings: PropTypes.func.isRequired,
        changeBrandsFilterInput: PropTypes.func.isRequired,
        changeCampaignsFilterInput: PropTypes.func.isRequired,
        changeKeywordsFilterInput: PropTypes.func.isRequired,
    }

    @autobind
    handleApply(key, value) {
        this.props.updatePageFilter({
            key,
            value,
        })
    }

    @autobind
    handleClose(filter) {
        const { filterSettings } = this.props
        this.props.updateFilterSettings(
            set(['displayState', filter], false, filterSettings)
        )
    }

    render() {
        const {
            filterSettings,
            selectedFilterValues,
            updateFilterSettings,
            resetFilters,
            campaignsFilterOptions,
            campaignsFilterLoading,
            changeCampaignsFilterInput,
            keywordsFilterLoading,
            keywordsFilterOptions,
            changeKeywordsFilterInput,
            brandsFilterLoading,
            brandsFilterOptions,
            changeBrandsFilterInput,
        } = this.props
        return (
            <FilterGroup
                filterSettings={filterSettings}
                updateFilterSettings={updateFilterSettings}
                resetFilters={resetFilters}
            >
                <DateRangeFilter
                    values={selectedFilterValues[DATES]}
                    onApply={this.handleApply}
                    closable={false}
                />
                <AdTypeFilter
                    values={selectedFilterValues[FACT_TYPES]}
                    onApply={this.handleApply}
                    closable={false}
                />
                <RegionsFilter
                    values={selectedFilterValues[REGIONS]}
                    onApply={this.handleApply}
                    onCloseFilter={this.handleClose}
                    closable
                />
                <CountriesFilter
                    values={selectedFilterValues[COUNTRIES]}
                    onApply={this.handleApply}
                    onCloseFilter={this.handleClose}
                    closable
                />
                <BrandsFilter
                    values={selectedFilterValues[BRANDS]}
                    options={brandsFilterOptions}
                    onApply={this.handleApply}
                    onCloseFilter={this.handleClose}
                    closable
                    onChangeInput={changeBrandsFilterInput}
                    loading={brandsFilterLoading}
                />
                <CampaignsFilter
                    values={selectedFilterValues[CAMPAIGNS]}
                    options={campaignsFilterOptions}
                    onApply={this.handleApply}
                    onCloseFilter={this.handleClose}
                    closable
                    onChangeInput={changeCampaignsFilterInput}
                    loading={campaignsFilterLoading}
                />
                <CampaignTargetingTypesFilter
                    values={selectedFilterValues[CAMPAIGN_TARGETING_TYPES]}
                    onApply={this.handleApply}
                    onCloseFilter={this.handleClose}
                    closable
                />
                <KeywordsFilter
                    values={selectedFilterValues[KEYWORDS]}
                    options={keywordsFilterOptions}
                    onApply={this.handleApply}
                    onCloseFilter={this.handleClose}
                    closable
                    onChangeInput={changeKeywordsFilterInput}
                    loading={keywordsFilterLoading}
                />
                <KeywordMatchTypesFilter
                    values={selectedFilterValues[KEYWORD_MATCH_TYPES]}
                    onApply={this.handleApply}
                    onCloseFilter={this.handleClose}
                    closable
                />
            </FilterGroup>
        )
    }
}

export default Filters
