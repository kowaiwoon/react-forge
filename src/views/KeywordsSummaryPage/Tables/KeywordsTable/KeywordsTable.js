import React, { Component } from 'react'
import PropTypes from 'prop-types'
import isUndefined from 'lodash/isUndefined'
import isNull from 'lodash/isNull'
import { generatePath } from 'react-router-dom'
import autobind from 'autobind-decorator'

import { AppLink } from 'components/AppLink'
import { PaginatedTable } from 'components/PaginatedTable'
import { UNDEFINED_VALUE } from 'constants/formatting'
import { FACT_TYPE_LABELS } from 'configuration/factTypes'
import { getTablePaginationOptions } from 'helpers/pagination'
import { METRIC_COLUMNS } from 'configuration/tables'
import { getPath } from 'helpers/pages'
import { KEYWORD_PAGE, BRAND_PAGE, CAMPAIGN_PAGE } from 'constants/pages'
import { formatDate, formatCurrency, titleCase } from 'helpers/formatting'
import { ContentCard } from 'components/ContentCard'
import { SettingsButton, DownloadButton } from 'components/Buttons'
import { SettingsModal } from 'components/SettingsModal'

class KeywordsTable extends Component {
    static propTypes = {
        downloading: PropTypes.bool.isRequired,
        tableData: PropTypes.shape({
            data: PropTypes.arrayOf(PropTypes.shape()),
            updating: PropTypes.bool,
            deleting: PropTypes.bool,
            loading: PropTypes.bool,
            pagination: PropTypes.shape({
                pageSize: PropTypes.number,
                current: PropTypes.number,
                total: PropTypes.number,
            }),
            sorter: PropTypes.shape({
                field: PropTypes.string,
                order: PropTypes.oneOf(['descend', 'ascend']),
            }),
            columnSettings: PropTypes.shape({
                actionColumns: PropTypes.array,
                order: PropTypes.array,
                displayState: PropTypes.shape(),
            }),
        }).isRequired,

        // actions
        updatePagination: PropTypes.func.isRequired,
        updateSorter: PropTypes.func.isRequired,
        updateColumnSettings: PropTypes.func.isRequired,
        reloadData: PropTypes.func.isRequired,
        downloadData: PropTypes.func.isRequired,
    }

    state = {
        settingsModalVisible: false,
    }

    static getTableOptions() {
        return {
            name: 'keyword',
            rowKey: 'keyword.id',
            columns: {
                'keyword.text': {
                    title: 'Keyword',
                    dataIndex: 'keyword.text',
                    fixed: 'left',
                    align: 'left',
                    width: 200,
                    render: (text, record) => (
                        <AppLink
                            to={generatePath(getPath(KEYWORD_PAGE), {
                                keywordId: record.keyword.id,
                            })}
                        >
                            {text}
                        </AppLink>
                    ),
                },
                'keyword.match_type': {
                    title: 'Match Type',
                    dataIndex: 'keyword.match_type',
                    align: 'left',
                    render: text => titleCase(text),
                },
                'keyword.bid': {
                    title: 'Bid',
                    dataIndex: 'keyword.bid',
                    align: 'right',
                    // TODO: format with dayparting settings when applicable
                    render: (value, record) =>
                        formatCurrency(value, {
                            decimal: true,
                            currencyCode: record.profile.currency_code,
                        }),
                },
                'keyword.state': {
                    title: 'State',
                    dataIndex: 'keyword.state',
                    align: 'center',
                    render: value => titleCase(value),
                },
                'profile.brand_name': {
                    title: 'Brand',
                    dataIndex: 'profile.brand_name',
                    align: 'left',
                    render: (text, record) => (
                        <AppLink
                            to={generatePath(getPath(BRAND_PAGE), {
                                brandId: record.profile.id,
                            })}
                        >
                            {text}
                        </AppLink>
                    ),
                },
                'profile.country_code': {
                    title: 'Brand Country',
                    dataIndex: 'profile.country_code',
                    align: 'center',
                },
                'profile.currency_code': {
                    title: 'Brand Currency',
                    dataIndex: 'profile.currency_code',
                    align: 'center',
                },
                'profile.region': {
                    title: 'Brand Region',
                    dataIndex: 'profile.region',
                    align: 'center',
                },
                'profile.timezone': {
                    title: 'Brand Timezone',
                    dataIndex: 'profile.timezone',
                    align: 'center',
                },
                'campaign.name': {
                    title: 'Campaign',
                    dataIndex: 'campaign.name',
                    align: 'left',
                    width: 200,
                    render: (text, record) => (
                        <AppLink
                            to={generatePath(getPath(CAMPAIGN_PAGE), {
                                campaignId: record.campaign.id,
                            })}
                        >
                            {text}
                        </AppLink>
                    ),
                },
                'campaign.budget': {
                    title: 'Campaign Budget',
                    dataIndex: 'campaign.budget',
                    align: 'right',
                    render: (value, record) =>
                        formatCurrency(value, {
                            currencyCode: record.profile.currency_code,
                        }),
                },
                'campaign.budget_type': {
                    title: 'Campaign Budget Type',
                    dataIndex: 'campaign.budget_type',
                    align: 'center',
                    render: value => titleCase(value),
                },
                'campaign.campaign_type': {
                    title: 'Campaign Type',
                    dataIndex: 'campaign.campaign_type',
                    align: 'center',
                    render: value => FACT_TYPE_LABELS[value],
                },
                'campaign.dayparting_enabled': {
                    title: 'Campaign Dayparting Enabled',
                    dataIndex: 'campaign.dayparting_enabled',
                    align: 'center',
                    render: value => (value ? 'Enabled' : 'Disabled'),
                },
                'campaign.start_date': {
                    title: 'Campaign Start Date',
                    dataIndex: 'campaign.start_date',
                    align: 'center',
                    render: value => formatDate(value),
                },
                'campaign.end_date': {
                    title: 'Campaign End Date',
                    dataIndex: 'campaign.end_date',
                    align: 'center',
                    render: value => formatDate(value),
                },
                'campaign.state': {
                    title: 'Campaign State',
                    dataIndex: 'campaign.state',
                    align: 'center',
                    render: value => titleCase(value),
                },
                'campaign.targeting_type': {
                    title: 'Campaign Targeting Type',
                    dataIndex: 'campaign.targeting_type',
                    align: 'center',
                    render: value =>
                        isNull(value) || isUndefined(value)
                            ? UNDEFINED_VALUE
                            : titleCase(value),
                },
                ...METRIC_COLUMNS,
            },
        }
    }

    @autobind
    handleToggleSettingsModal() {
        this.setState({
            settingsModalVisible: !this.state.settingsModalVisible,
        })
    }

    @autobind
    handleUpdateColumnSettings(settings) {
        const { updateColumnSettings, reloadData } = this.props

        updateColumnSettings(settings)

        // Reload data
        reloadData()

        // Toggle settings modal
        this.handleToggleSettingsModal()
    }

    render() {
        const {
            downloading,
            tableData,
            updatePagination,
            updateSorter,
            reloadData,
            downloadData,
        } = this.props
        const { settingsModalVisible } = this.state
        const tableOptions = KeywordsTable.getTableOptions()

        return (
            <ContentCard
                title="Keywords Table"
                subTitle="Manage and view tabular data for all Keywords"
                actions={[
                    <SettingsButton
                        onClick={this.handleToggleSettingsModal}
                        tooltipTitle="Customize table columns"
                    />,
                    <DownloadButton
                        loading={downloading}
                        onClick={downloadData}
                    />,
                ]}
            >
                <PaginatedTable
                    tableOptions={KeywordsTable.getTableOptions()}
                    data={tableData.data}
                    loading={tableData.loading}
                    updating={tableData.updating}
                    deleting={tableData.deleting}
                    pagination={{
                        ...getTablePaginationOptions('Keywords'),
                        ...tableData.pagination,
                    }}
                    sorter={tableData.sorter}
                    columnSettings={tableData.columnSettings}
                    updatePagination={updatePagination}
                    updateSorter={updateSorter}
                    reloadData={reloadData}
                />
                <SettingsModal
                    droppableId="paginatedTableSettingsModal"
                    modalTitle="Customize Table Columns"
                    visible={settingsModalVisible}
                    handleCancel={this.handleToggleSettingsModal}
                    handleOk={this.handleUpdateColumnSettings}
                    settings={tableData.columnSettings}
                    settingTitles={tableOptions.columns}
                />
            </ContentCard>
        )
    }
}

export default KeywordsTable
