import { connect } from 'react-redux'

import { KEYWORDS_SUMMARY_PAGE } from 'constants/pages'
import {
    selectDomainValue as selectUiDomainValue,
    selectPageDownloading,
} from 'selectors/ui'
import {
    updateKeywordsSummaryPageTablePagination,
    updateKeywordsSummaryPageTableSorter,
    updateKeywordsSummaryPageTableSettings,
    fetchKeywordsSummaryPageTableRequest,
    downloadKeywordsSummaryPageTableRequest,
} from 'actions/ui'

import KeywordsTable from './KeywordsTable'

const mapStateToProps = state => ({
    tableData: selectUiDomainValue(state, [KEYWORDS_SUMMARY_PAGE, 'table']),
    downloading: selectPageDownloading(state, KEYWORDS_SUMMARY_PAGE),
})

const mapDispatchToProps = {
    updatePagination: updateKeywordsSummaryPageTablePagination,
    updateSorter: updateKeywordsSummaryPageTableSorter,
    updateColumnSettings: updateKeywordsSummaryPageTableSettings,
    reloadData: fetchKeywordsSummaryPageTableRequest,
    downloadData: downloadKeywordsSummaryPageTableRequest,
}

const KeywordsTableContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(KeywordsTable)

export default KeywordsTableContainer
