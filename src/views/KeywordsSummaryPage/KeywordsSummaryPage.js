import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Tabs } from 'antd'

import { PageHeader } from 'components/PageHeader'
import { LoadingIndicator } from 'components/LoadingIndicator'

import { KeywordsChartContainer } from './Charts'
import { KeywordsTableContainer } from './Tables'
import { FiltersContainer } from './Filters'

import styles from './styles.scss'

class KeywordsSummaryPage extends Component {
    static propTypes = {
        // Redux state
        mounting: PropTypes.bool.isRequired,

        // Redux actions
        mountKeywordsSummary: PropTypes.func.isRequired,
        unmountKeywordsSummary: PropTypes.func.isRequired,

        // tab state
        tab: PropTypes.oneOf(['treemap', 'table']).isRequired,
        handleTabChange: PropTypes.func.isRequired,
    }

    componentDidMount() {
        this.props.mountKeywordsSummary()
    }

    componentWillUnmount() {
        this.props.unmountKeywordsSummary()
    }

    render() {
        const { mounting, tab, handleTabChange } = this.props

        if (mounting) {
            return (
                <div className={styles['empty-content']}>
                    <LoadingIndicator size="small" />
                </div>
            )
        }

        return (
            <React.Fragment>
                <PageHeader
                    breadcrumbs={[{ name: 'All Keywords' }]}
                    filterGroupComponent={<FiltersContainer />}
                />

                <Tabs
                    activeKey={tab}
                    onChange={handleTabChange}
                    tabPosition="top"
                    size="default"
                    tabBarStyle={{
                        fontWeight: '500', // same font-weight as selected tab
                    }}
                >
                    <Tabs.TabPane
                        tab="Keywords Tree Chart"
                        key="treemap"
                        forceRender
                    >
                        <KeywordsChartContainer />
                    </Tabs.TabPane>

                    <Tabs.TabPane tab="Keywords Table" key="table" forceRender>
                        <KeywordsTableContainer />
                    </Tabs.TabPane>
                </Tabs>
            </React.Fragment>
        )
    }
}

export default KeywordsSummaryPage
