import React from 'react'
import PropTypes from 'prop-types'

import { PaginatedTreemap } from 'components/PaginatedTreemap'
import { getTreemapPaginationOptions } from 'helpers/pagination'

const propTypes = {
    // React router
    history: PropTypes.shape({
        push: PropTypes.func,
    }).isRequired,

    // Redux state
    factTypes: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.string,
            label: PropTypes.string,
        })
    ).isRequired,
    data: PropTypes.array.isRequired,
    loading: PropTypes.bool.isRequired,
    pagination: PropTypes.object.isRequired,
    sorter: PropTypes.object.isRequired,

    // actions
    updateKeywordsTreemapPagination: PropTypes.func.isRequired,
    updateKeywordsTreemapSorter: PropTypes.func.isRequired,
    fetchKeywordsTreemapRequest: PropTypes.func.isRequired,
}

const defaultProps = {}

const ProductsChart = ({
    history,
    factTypes,
    data,
    loading,
    pagination,
    sorter,
    updateKeywordsTreemapPagination,
    updateKeywordsTreemapSorter,
    fetchKeywordsTreemapRequest,
}) => (
    <PaginatedTreemap
        treemapOptions={{
            name: 'keyword',
            idAttribute: 'keyword.id',
            titleAttribute: 'keyword.text',
            height: 500,
            colors: {
                minColor: '#c4e1ea',
                maxColor: '#8884d8',
            },
        }}
        defaultPagination={getTreemapPaginationOptions('Keywords')}
        factTypes={factTypes}
        data={data}
        loading={loading}
        pagination={pagination}
        sorter={sorter}
        updatePagination={updateKeywordsTreemapPagination}
        updateSorter={updateKeywordsTreemapSorter}
        reloadData={fetchKeywordsTreemapRequest}
        onLeafClick={leafNodeId => {
            history.push(`/keywords/${leafNodeId}`)
        }}
    />
)

ProductsChart.propTypes = propTypes
ProductsChart.defaultProps = defaultProps

export default ProductsChart
