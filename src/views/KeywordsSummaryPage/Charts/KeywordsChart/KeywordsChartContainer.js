import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import { KEYWORDS_SUMMARY_PAGE } from 'constants/pages'
import {
    selectPageFactTypes,
    selectTreemap,
    selectTreemapChartData,
} from 'selectors/ui'
import {
    updateKeywordsSummaryPageTreemapPagination,
    updateKeywordsSummaryPageTreemapSorter,
    fetchKeywordsSummaryPageTreemapRequest,
} from 'actions/ui'

import KeywordsChart from './KeywordsChart'

const mapStateToProps = state => {
    const { loading, pagination, sorter } = selectTreemap(
        state,
        KEYWORDS_SUMMARY_PAGE,
        'treemap'
    )

    return {
        factTypes: selectPageFactTypes(state, KEYWORDS_SUMMARY_PAGE),
        data: selectTreemapChartData(state, KEYWORDS_SUMMARY_PAGE, 'treemap'),
        loading,
        pagination,
        sorter,
    }
}

const mapDispatchToProps = {
    updateKeywordsTreemapPagination: updateKeywordsSummaryPageTreemapPagination,
    updateKeywordsTreemapSorter: updateKeywordsSummaryPageTreemapSorter,
    fetchKeywordsTreemapRequest: fetchKeywordsSummaryPageTreemapRequest,
}

const KeywordsChartContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(KeywordsChart)

export default withRouter(KeywordsChartContainer)
