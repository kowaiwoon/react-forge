import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { generatePath } from 'react-router-dom'
import autobind from 'autobind-decorator'

import { AppLink } from 'components/AppLink'
import { PaginatedTable } from 'components/PaginatedTable'
import { getPath } from 'helpers/pages'
import { BRAND_PAGE } from 'constants/pages'
import { METRIC_COLUMNS } from 'configuration/tables'
import { getTablePaginationOptions } from 'helpers/pagination'
import { ContentCard } from 'components/ContentCard'
import { SettingsButton, DownloadButton } from 'components/Buttons'
import { SettingsModal } from 'components/SettingsModal'

class BrandsTable extends Component {
    static propTypes = {
        downloading: PropTypes.bool.isRequired,
        tableData: PropTypes.shape({
            data: PropTypes.arrayOf(PropTypes.shape()),
            updating: PropTypes.bool,
            deleting: PropTypes.bool,
            loading: PropTypes.bool,
            pagination: PropTypes.shape({
                pageSize: PropTypes.number,
                current: PropTypes.number,
                total: PropTypes.number,
            }),
            sorter: PropTypes.shape({
                field: PropTypes.string,
                order: PropTypes.oneOf(['descend', 'ascend']),
            }),
            columnSettings: PropTypes.shape({
                actionColumns: PropTypes.array,
                order: PropTypes.array,
                displayState: PropTypes.shape(),
            }),
        }).isRequired,

        // actions
        updatePagination: PropTypes.func.isRequired,
        updateSorter: PropTypes.func.isRequired,
        updateColumnSettings: PropTypes.func.isRequired,
        reloadData: PropTypes.func.isRequired,
        downloadData: PropTypes.func.isRequired,
    }

    state = {
        settingsModalVisible: false,
    }

    static getTableOptions() {
        return {
            name: 'brand',
            rowKey: 'profile.id',
            columns: {
                'profile.brand_name': {
                    title: 'Brand Name',
                    fixed: 'left',
                    dataIndex: 'profile.brand_name',
                    width: 175,
                    render: (text, record) => {
                        const sellerStringId = record.profile.seller_string_id

                        const name = sellerStringId
                            ? `Seller Account ${sellerStringId}`
                            : text

                        return (
                            <AppLink
                                to={generatePath(getPath(BRAND_PAGE), {
                                    brandId: record.profile.id,
                                })}
                            >
                                {name}
                            </AppLink>
                        )
                    },
                },
                'profile.country_code': {
                    title: 'Country Code',
                    dataIndex: 'profile.country_code',
                    align: 'center',
                },
                'profile.currency_code': {
                    title: 'Currency',
                    dataIndex: 'profile.currency_code',
                    align: 'center',
                },
                'profile.region': {
                    title: 'Region',
                    dataIndex: 'profile.region',
                    align: 'center',
                },
                'profile.timezone': {
                    title: 'Timezone',
                    dataIndex: 'profile.timezone',
                    align: 'center',
                },
                ...METRIC_COLUMNS,
            },
        }
    }

    @autobind
    handleToggleSettingsModal() {
        this.setState({
            settingsModalVisible: !this.state.settingsModalVisible,
        })
    }

    @autobind
    handleUpdateColumnSettings(settings) {
        const { updateColumnSettings, reloadData } = this.props

        updateColumnSettings(settings)

        // Reload data
        reloadData()

        // Toggle settings modal
        this.handleToggleSettingsModal()
    }

    render() {
        const {
            downloading,
            tableData,
            updatePagination,
            updateSorter,
            reloadData,
            downloadData,
        } = this.props
        const { settingsModalVisible } = this.state
        const tableOptions = BrandsTable.getTableOptions()

        return (
            <ContentCard
                title="Brands Table"
                subTitle="Manage and view tabular data for all Brands"
                actions={[
                    <SettingsButton
                        onClick={this.handleToggleSettingsModal}
                        tooltipTitle="Customize table columns"
                    />,
                    <DownloadButton
                        loading={downloading}
                        onClick={downloadData}
                    />,
                ]}
            >
                <PaginatedTable
                    tableOptions={tableOptions}
                    data={tableData.data}
                    loading={tableData.loading}
                    updating={tableData.updating}
                    deleting={tableData.deleting}
                    pagination={{
                        ...getTablePaginationOptions('Brands'),
                        ...tableData.pagination,
                    }}
                    sorter={tableData.sorter}
                    columnSettings={tableData.columnSettings}
                    updatePagination={updatePagination}
                    updateSorter={updateSorter}
                    reloadData={reloadData}
                />
                <SettingsModal
                    droppableId="paginatedTableSettingsModal"
                    modalTitle="Customize Table Columns"
                    visible={settingsModalVisible}
                    handleCancel={this.handleToggleSettingsModal}
                    handleOk={this.handleUpdateColumnSettings}
                    settings={tableData.columnSettings}
                    settingTitles={tableOptions.columns}
                />
            </ContentCard>
        )
    }
}

export default BrandsTable
