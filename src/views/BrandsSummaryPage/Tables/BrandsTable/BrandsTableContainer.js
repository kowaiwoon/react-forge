import { connect } from 'react-redux'

import { BRANDS_SUMMARY_PAGE } from 'constants/pages'
import {
    selectDomainValue as selectUiDomainValue,
    selectPageDownloading,
} from 'selectors/ui'
import {
    updateBrandsSummaryPageTablePagination,
    updateBrandsSummaryPageTableSorter,
    updateBrandsSummaryPageTableSettings,
    fetchBrandsSummaryPageTableRequest,
    downloadBrandsSummaryPageTableRequest,
} from 'actions/ui'

import BrandsTable from './BrandsTable'

const mapStateToProps = state => ({
    tableData: selectUiDomainValue(state, [BRANDS_SUMMARY_PAGE, 'table']),
    downloading: selectPageDownloading(state, BRANDS_SUMMARY_PAGE),
})

const mapDispatchToProps = {
    updatePagination: updateBrandsSummaryPageTablePagination,
    updateSorter: updateBrandsSummaryPageTableSorter,
    updateColumnSettings: updateBrandsSummaryPageTableSettings,
    reloadData: fetchBrandsSummaryPageTableRequest,
    downloadData: downloadBrandsSummaryPageTableRequest,
}

const BrandsTableContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(BrandsTable)

export default BrandsTableContainer
