import React from 'react'
import PropTypes from 'prop-types'

import { PaginatedTreemap } from 'components/PaginatedTreemap'
import { getTreemapPaginationOptions } from 'helpers/pagination'

const propTypes = {
    // React router
    history: PropTypes.shape({
        push: PropTypes.func,
    }).isRequired,

    // Redux state
    factTypes: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.string,
            label: PropTypes.string,
        })
    ).isRequired,
    data: PropTypes.array.isRequired,
    loading: PropTypes.bool.isRequired,
    pagination: PropTypes.object.isRequired,
    sorter: PropTypes.object.isRequired,

    // actions
    updateBrandsTreemapPagination: PropTypes.func.isRequired,
    updateBrandsTreemapSorter: PropTypes.func.isRequired,
    fetchBrandsTreemapRequest: PropTypes.func.isRequired,
}

const defaultProps = {}

const BrandsChart = ({
    history,
    factTypes,
    data,
    loading,
    pagination,
    sorter,
    updateBrandsTreemapPagination,
    updateBrandsTreemapSorter,
    fetchBrandsTreemapRequest,
}) => (
    <PaginatedTreemap
        treemapOptions={{
            name: 'brand',
            idAttribute: 'profile.id',
            titleAttribute: 'profile.brand_name',
            sellerStringIdAttribute: 'profile.seller_string_id',
            countryCodeAttribute: 'profile.country_code',
            height: 500,
            colors: {
                minColor: '#e3f9e8',
                maxColor: '#39dce5',
            },
        }}
        defaultPagination={getTreemapPaginationOptions('Brands')}
        factTypes={factTypes}
        data={data}
        loading={loading}
        pagination={pagination}
        sorter={sorter}
        updatePagination={updateBrandsTreemapPagination}
        updateSorter={updateBrandsTreemapSorter}
        reloadData={fetchBrandsTreemapRequest}
        onLeafClick={leafNodeId => {
            history.push(`/brands/${leafNodeId}`)
        }}
    />
)

BrandsChart.propTypes = propTypes
BrandsChart.defaultProps = defaultProps

export default BrandsChart
