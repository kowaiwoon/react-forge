import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import { BRANDS_SUMMARY_PAGE } from 'constants/pages'
import {
    selectPageFactTypes,
    selectTreemap,
    selectTreemapChartData,
} from 'selectors/ui'
import {
    updateBrandsSummaryPageTreemapPagination,
    updateBrandsSummaryPageTreemapSorter,
    fetchBrandsSummaryPageTreemapRequest,
} from 'actions/ui'

import BrandsChart from './BrandsChart'

const mapStateToProps = state => {
    const { loading, pagination, sorter } = selectTreemap(
        state,
        BRANDS_SUMMARY_PAGE,
        'treemap'
    )

    return {
        factTypes: selectPageFactTypes(state, BRANDS_SUMMARY_PAGE),
        data: selectTreemapChartData(state, BRANDS_SUMMARY_PAGE, 'treemap'),
        loading,
        pagination,
        sorter,
    }
}

const mapDispatchToProps = {
    updateBrandsTreemapPagination: updateBrandsSummaryPageTreemapPagination,
    updateBrandsTreemapSorter: updateBrandsSummaryPageTreemapSorter,
    fetchBrandsTreemapRequest: fetchBrandsSummaryPageTreemapRequest,
}

const BrandsChartContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(BrandsChart)

export default withRouter(BrandsChartContainer)
