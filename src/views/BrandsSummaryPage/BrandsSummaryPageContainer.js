import { connect } from 'react-redux'

import { BRANDS_SUMMARY_PAGE } from 'constants/pages'
import { selectDomainValue as selectUiDomainValue } from 'selectors/ui'
import {
    mountBrandsSummaryPageRequest,
    unmountBrandsSummaryPage,
} from 'actions/ui'
import { withTabState } from 'components/HigherOrderComponents'

import BrandsSummaryPage from './BrandsSummaryPage'

const mapStateToProps = state => ({
    mounting: selectUiDomainValue(state, [BRANDS_SUMMARY_PAGE, 'mounting']),
})

const mapDispatchToProps = {
    mountBrandsSummary: mountBrandsSummaryPageRequest,
    unmountBrandsSummary: unmountBrandsSummaryPage,
}

const BrandsSummaryPageContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(BrandsSummaryPage)

export default withTabState(BrandsSummaryPageContainer, 'treemap')
