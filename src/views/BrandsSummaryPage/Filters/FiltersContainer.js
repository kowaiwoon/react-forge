import { connect } from 'react-redux'

import { BRANDS_SUMMARY_PAGE } from 'constants/pages'
import {
    selectDomainValue as selectUiDomainValue,
    selectPageFilters,
    selectPageFilterSettings,
} from 'selectors/ui'
import { selectBrandsForFilters } from 'selectors/entities'
import {
    updatePageFilterForPage,
    resetPageFiltersForPage,
    updatePageFilterSettingsForPage,
    changeBrandsFilterInput,
} from 'actions/ui'

import Filters from './Filters'

const mapStateToProps = state => ({
    filterSettings: selectPageFilterSettings(state, BRANDS_SUMMARY_PAGE),
    selectedFilterValues: selectPageFilters(state, BRANDS_SUMMARY_PAGE),
    brandsFilterOptions: selectBrandsForFilters(state),
    brandsFilterLoading: selectUiDomainValue(state, [
        'app',
        'brandsFilterLoading',
    ]),
})

const mapDispatchToProps = {
    updatePageFilter: updatePageFilterForPage(BRANDS_SUMMARY_PAGE),
    updateFilterSettings: updatePageFilterSettingsForPage(BRANDS_SUMMARY_PAGE),
    resetFilters: resetPageFiltersForPage(BRANDS_SUMMARY_PAGE),
    changeBrandsFilterInput,
}

const FiltersContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Filters)

export default FiltersContainer
