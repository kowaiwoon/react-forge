import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Tabs } from 'antd'

import { PageHeader } from 'components/PageHeader'
import { LoadingIndicator } from 'components/LoadingIndicator'

import { BrandsChartContainer } from './Charts'
import { BrandsTableContainer } from './Tables'
import { FiltersContainer } from './Filters'
import styles from './styles.scss'

class BrandsSummaryPage extends Component {
    static propTypes = {
        // Redux state
        mounting: PropTypes.bool.isRequired,

        // Redux actions
        mountBrandsSummary: PropTypes.func.isRequired,
        unmountBrandsSummary: PropTypes.func.isRequired,

        // tab state
        tab: PropTypes.oneOf(['treemap', 'table']).isRequired,
        handleTabChange: PropTypes.func.isRequired,
    }

    componentDidMount() {
        this.props.mountBrandsSummary()
    }

    componentWillUnmount() {
        this.props.unmountBrandsSummary()
    }

    render() {
        const { mounting, tab, handleTabChange } = this.props

        if (mounting) {
            return (
                <div className={styles['empty-content']}>
                    <LoadingIndicator size="small" />
                </div>
            )
        }

        return (
            <React.Fragment>
                <PageHeader
                    breadcrumbs={[{ name: 'All Brands' }]}
                    filterGroupComponent={<FiltersContainer />}
                />

                <Tabs
                    activeKey={tab}
                    onChange={handleTabChange}
                    tabPosition="top"
                    size="default"
                    tabBarStyle={{
                        fontWeight: '500', // same font-weight as selected tab
                    }}
                >
                    <Tabs.TabPane
                        tab="Brands Tree Chart"
                        key="treemap"
                        forceRender
                    >
                        <BrandsChartContainer />
                    </Tabs.TabPane>

                    <Tabs.TabPane tab="Brands Table" key="table" forceRender>
                        <BrandsTableContainer />
                    </Tabs.TabPane>
                </Tabs>
            </React.Fragment>
        )
    }
}

export default BrandsSummaryPage
