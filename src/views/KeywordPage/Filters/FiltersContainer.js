import { connect } from 'react-redux'

import { selectPageFilters, selectPageFilterSettings } from 'selectors/ui'
import {
    updatePageFilterForPage,
    resetPageFiltersForPage,
    updatePageFilterSettingsForPage,
} from 'actions/ui'
import { KEYWORD_PAGE } from 'constants/pages'

import Filters from './Filters'

const mapStateToProps = state => ({
    filterSettings: selectPageFilterSettings(state, KEYWORD_PAGE),
    selectedFilterValues: selectPageFilters(state, KEYWORD_PAGE),
})

const mapDispatchToProps = {
    updatePageFilter: updatePageFilterForPage(KEYWORD_PAGE),
    resetFilters: resetPageFiltersForPage(KEYWORD_PAGE),
    updateFilterSettings: updatePageFilterSettingsForPage(KEYWORD_PAGE),
}

const FiltersContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Filters)

export default FiltersContainer
