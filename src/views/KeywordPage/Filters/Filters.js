import React from 'react'
import PropTypes from 'prop-types'
import autobind from 'autobind-decorator'
import set from 'lodash/fp/set'

import { AGGREGATION, DATES } from 'constants/filters'
import { AggregationFilter, DateRangeFilter } from 'components/Filters'
import { FilterGroup } from 'components/FilterGroup'

class Filters extends React.Component {
    static propTypes = {
        filterSettings: PropTypes.shape().isRequired,
        selectedFilterValues: PropTypes.shape().isRequired,

        // actions
        updatePageFilter: PropTypes.func.isRequired,
        resetFilters: PropTypes.func.isRequired,
        updateFilterSettings: PropTypes.func.isRequired,
    }

    @autobind
    handleApply(key, value) {
        this.props.updatePageFilter({
            key,
            value,
        })
    }

    @autobind
    handleClose(filter) {
        const { filterSettings } = this.props
        this.props.updateFilterSettings(
            set(['displayState', filter], false, filterSettings)
        )
    }

    render() {
        const {
            filterSettings,
            selectedFilterValues,
            updateFilterSettings,
            resetFilters,
        } = this.props
        return (
            <FilterGroup
                filterSettings={filterSettings}
                updateFilterSettings={updateFilterSettings}
                resetFilters={resetFilters}
            >
                <DateRangeFilter
                    values={selectedFilterValues[DATES]}
                    onApply={this.handleApply}
                    closable={false}
                />
                <AggregationFilter
                    value={selectedFilterValues[AGGREGATION]}
                    onApply={this.handleApply}
                    closable={false}
                />
            </FilterGroup>
        )
    }
}

export default Filters
