import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Row, Col, Tabs } from 'antd'
import isEmpty from 'lodash/isEmpty'

import { PageHeader } from 'components/PageHeader'
import { LoadingIndicator } from 'components/LoadingIndicator'
import { MetricSummaryPanel } from 'components/MetricSummaryPanel'
import { BRANDS_SUMMARY_PAGE, CAMPAIGNS_SUMMARY_PAGE } from 'constants/pages'
import { SPONSORED_PRODUCT, HEADLINE_SEARCH } from 'constants/factTypes'
import {
    getKeywordPageBreadcrumbs,
    getCampaignKeywordPageBreadcrumbs,
    getBrandCampaignKeywordPageBreadcrumbs,
} from 'helpers/breadcrumbs'

import {
    ReachChartContainer,
    ConversionsChartContainer,
    RoiChartContainer,
} from './Charts'
import { ResourceDetailsContainer } from './ResourceDetails'
import { FiltersContainer } from './Filters'
import styles from './styles.scss'

class KeywordPage extends Component {
    static propTypes = {
        topLevelPage: PropTypes.string.isRequired,
        keywordId: PropTypes.string.isRequired,
        keyword: PropTypes.shape({
            keyword_name: PropTypes.string,
            state: PropTypes.string,
        }),
        brand: PropTypes.shape({
            id: PropTypes.string,
            brand_name: PropTypes.string,
            region: PropTypes.string,
            country_code: PropTypes.string,
            currency_code: PropTypes.string,
            seller_string_id: PropTypes.string,
        }),
        campaign: PropTypes.shape({
            id: PropTypes.string,
            name: PropTypes.string,
            campaign_type: PropTypes.oneOf([
                SPONSORED_PRODUCT,
                HEADLINE_SEARCH,
            ]),
            targeting_type: PropTypes.string,
            state: PropTypes.string,
            budget: PropTypes.number,
            budget_type: PropTypes.string,
            start_date: PropTypes.string,
            end_date: PropTypes.string,
        }),
        mounting: PropTypes.bool.isRequired,
        keywordAggregate: PropTypes.shape().isRequired,

        // actions
        mountKeywordPage: PropTypes.func.isRequired,
        unmountKeywordPage: PropTypes.func.isRequired,

        // tab state
        tab: PropTypes.oneOf(['roi', 'reach', 'conversions']).isRequired,
        handleTabChange: PropTypes.func.isRequired,
    }

    static defaultProps = {
        keyword: {},
        brand: {},
        campaign: {},
    }

    componentDidMount() {
        const { keywordId } = this.props
        this.props.mountKeywordPage({ keywordId })
    }

    componentWillUnmount() {
        this.props.unmountKeywordPage()
    }

    getBreadcrumbs() {
        const { topLevelPage, brand, campaign, keyword } = this.props

        // All Brands -> Brand -> Campaign -> Keyword
        if (topLevelPage === BRANDS_SUMMARY_PAGE && !isEmpty(brand)) {
            return getBrandCampaignKeywordPageBreadcrumbs(
                brand,
                campaign,
                keyword
            )
        }

        // All Campaigns -> Campaign -> Keyword
        if (topLevelPage === CAMPAIGNS_SUMMARY_PAGE && !isEmpty(campaign)) {
            return getCampaignKeywordPageBreadcrumbs(campaign, keyword)
        }

        // All Keywords -> Keyword
        return getKeywordPageBreadcrumbs(keyword)
    }

    render() {
        const { mounting, keywordAggregate, tab, handleTabChange } = this.props

        if (mounting) {
            return (
                <div className={styles['empty-content']}>
                    <LoadingIndicator size="small" />
                </div>
            )
        }

        return (
            <React.Fragment>
                <PageHeader
                    breadcrumbs={this.getBreadcrumbs()}
                    titleComponent={<ResourceDetailsContainer />}
                    filterGroupComponent={<FiltersContainer />}
                />

                <Tabs
                    activeKey={tab}
                    onChange={handleTabChange}
                    tabPosition="top"
                    size="default"
                    tabBarStyle={{
                        fontWeight: '500', // same font-weight as selected tab
                    }}
                >
                    <Tabs.TabPane
                        tab="Return on Investment"
                        key="roi"
                        forceRender
                    >
                        <MetricSummaryPanel
                            loading={keywordAggregate.loading}
                            data={keywordAggregate.data}
                            category="roi"
                        />
                        <Row>
                            <Col span={24} className={styles['row-spacing']}>
                                <RoiChartContainer />
                            </Col>
                        </Row>
                    </Tabs.TabPane>

                    <Tabs.TabPane tab="Reach" key="reach" forceRender>
                        <MetricSummaryPanel
                            loading={keywordAggregate.loading}
                            data={keywordAggregate.data}
                            category="reach"
                        />
                        <Row className={styles['row-spacing']}>
                            <Col span={24}>
                                <ReachChartContainer />
                            </Col>
                        </Row>
                    </Tabs.TabPane>

                    <Tabs.TabPane
                        tab="Conversions"
                        key="conversions"
                        forceRender
                    >
                        <MetricSummaryPanel
                            loading={keywordAggregate.loading}
                            data={keywordAggregate.data}
                            category="conversions"
                        />
                        <Row className={styles['row-spacing']}>
                            <Col span={24}>
                                <ConversionsChartContainer />
                            </Col>
                        </Row>
                    </Tabs.TabPane>
                </Tabs>
            </React.Fragment>
        )
    }
}

export default KeywordPage
