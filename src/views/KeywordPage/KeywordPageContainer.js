import { connect } from 'react-redux'
import get from 'lodash/get'

import {
    KEYWORD_PAGE,
    BRANDS_SUMMARY_PAGE,
    CAMPAIGNS_SUMMARY_PAGE,
    KEYWORDS_SUMMARY_PAGE,
} from 'constants/pages'
import { selectKeyword, selectBrand, selectCampaign } from 'selectors/entities'
import { selectDomainValue as selectUiDomainValue } from 'selectors/ui'
import { mountKeywordPageRequest, unmountKeywordPage } from 'actions/ui'
import { withTabState } from 'components/HigherOrderComponents'

import KeywordPage from './KeywordPage'

const getTopLevelPage = (brandId, campaignId) => {
    if (brandId) {
        return BRANDS_SUMMARY_PAGE
    } else if (campaignId) {
        return CAMPAIGNS_SUMMARY_PAGE
    }
    return KEYWORDS_SUMMARY_PAGE
}

const mapStateToProps = (state, ownProps) => {
    const { brandId, campaignId, keywordId } = ownProps.match.params
    const keyword = selectKeyword(state, keywordId)
    const brand = keyword ? selectBrand(state, get(keyword, 'profile')) : {}
    const campaign = keyword
        ? selectCampaign(state, get(keyword, 'campaign'))
        : {}

    return {
        topLevelPage: getTopLevelPage(brandId, campaignId),
        keywordId,
        keyword,
        brand,
        campaign,
        mounting: selectUiDomainValue(state, [KEYWORD_PAGE, 'mounting']),
        keywordAggregate: selectUiDomainValue(state, [
            KEYWORD_PAGE,
            'aggregate',
        ]),
    }
}

const mapDispatchToProps = {
    mountKeywordPage: mountKeywordPageRequest,
    unmountKeywordPage,
}

const KeywordPageContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(KeywordPage)

export default withTabState(KeywordPageContainer, 'roi')
