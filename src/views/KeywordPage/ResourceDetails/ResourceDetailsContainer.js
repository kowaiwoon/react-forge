import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import get from 'lodash/get'

import { selectDomainValue as selectUiDomainValue } from 'selectors/ui'
import { selectBrand, selectCampaign, selectKeyword } from 'selectors/entities'
import { KEYWORD_PAGE } from 'constants/pages'
import {
    toggleKeywordPageDetails,
    updateKeywordPageKeywordDetailsRequest,
} from 'actions/ui'

import ResourceDetails from './ResourceDetails'

const mapStateToProps = (state, ownProps) => {
    const { keywordId } = ownProps.match.params
    const keyword = selectKeyword(state, keywordId)
    const brand = keyword ? selectBrand(state, get(keyword, 'profile')) : {}
    const campaign = keyword
        ? selectCampaign(state, get(keyword, 'campaign'))
        : {}

    return {
        keyword,
        brand,
        campaign,
        keywordUpdating: selectUiDomainValue(state, [
            KEYWORD_PAGE,
            'keywordUpdating',
        ]),
        showDetails: selectUiDomainValue(state, [KEYWORD_PAGE, 'showDetails']),
    }
}

const mapDispatchToProps = {
    updateKeywordRequest: updateKeywordPageKeywordDetailsRequest,
    toggleKeywordPageDetails,
}

const ResourceDetailsContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ResourceDetails)

export default withRouter(ResourceDetailsContainer)
