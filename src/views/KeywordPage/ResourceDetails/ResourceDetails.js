import React from 'react'
import PropTypes from 'prop-types'
import autobind from 'autobind-decorator'
import { generatePath } from 'react-router-dom'
import truncate from 'lodash/truncate'

import { ResourceDetails } from 'components/ResourceDetails'
import { FACT_TYPE_LABELS } from 'configuration/factTypes'
import { formatCurrency, titleCase } from 'helpers/formatting'
import { SELECT_INPUT, NUMBER_INPUT } from 'constants/inputTypes'
import { PAUSED, ENABLED, ARCHIVED } from 'constants/resourceStates'
import { SPONSORED_PRODUCT, HEADLINE_SEARCH } from 'constants/factTypes'
import { keywordAttributeTooltips } from 'configuration/attributes'
import {
    KEYWORD_BID_MIN,
    KEYWORD_BID_MAX,
    KEYWORD_BID_STEP,
    KEYWORD_BID_PRECISION,
} from 'constants/keywords'
import { getPath } from 'helpers/pages'
import { CAMPAIGN_PAGE } from 'constants/pages'
import { AppLink } from 'components/AppLink'

import styles from './styles.scss'

const renderTooltipWithHelptext = attribute => {
    const info = keywordAttributeTooltips[attribute]

    return info.map(item => {
        const { title, description } = item

        return (
            <div className={styles.tooltip} key={description}>
                {title && <span className={styles.title}>{title}</span>}
                <span className={styles.description}>{description}</span>
            </div>
        )
    })
}

class KeywordResourceDetails extends React.Component {
    static propTypes = {
        keyword: PropTypes.shape({
            keyword_name: PropTypes.string,
            state: PropTypes.string,
        }),
        brand: PropTypes.shape({
            id: PropTypes.string,
            brand_name: PropTypes.string,
            region: PropTypes.string,
            country_code: PropTypes.string,
            currency_code: PropTypes.string,
            seller_string_id: PropTypes.string,
        }),
        campaign: PropTypes.shape({
            id: PropTypes.string,
            name: PropTypes.string,
            campaign_type: PropTypes.oneOf([
                SPONSORED_PRODUCT,
                HEADLINE_SEARCH,
            ]),
            targeting_type: PropTypes.string,
            state: PropTypes.string,
            budget: PropTypes.number,
            budget_type: PropTypes.string,
            start_date: PropTypes.string,
            end_date: PropTypes.string,
        }),
        keywordUpdating: PropTypes.bool.isRequired,
        showDetails: PropTypes.bool.isRequired,

        // actions
        updateKeywordRequest: PropTypes.func.isRequired,
        toggleKeywordPageDetails: PropTypes.func.isRequired,
    }

    static defaultProps = {
        keyword: {},
        brand: {},
        campaign: {},
    }

    @autobind
    handleUpdateKeywordDetails(values) {
        const { updateKeywordRequest } = this.props
        updateKeywordRequest(values)
    }

    isSponsoredProductKeyword() {
        const { campaign } = this.props
        return campaign.campaign_type === SPONSORED_PRODUCT
    }

    editToolTip() {
        if (this.props.keyword.state === ARCHIVED) {
            return 'Archived Keywords cannot be modified.'
        }
        if (!this.isSponsoredProductKeyword()) {
            return 'Modifying Headline Search Keywords is not yet supported.'
        }
        return null
    }

    render() {
        const {
            keyword,
            campaign,
            brand,
            showDetails,
            keywordUpdating,
            toggleKeywordPageDetails,
        } = this.props
        return (
            <ResourceDetails
                name={keyword.text}
                showDetails={showDetails}
                allowEditing={
                    this.isSponsoredProductKeyword() &&
                    keyword.state !== ARCHIVED
                }
                editToolTip={this.editToolTip()}
                updating={keywordUpdating}
                onShowDetailsClick={toggleKeywordPageDetails}
                onSave={this.handleUpdateKeywordDetails}
                details={[
                    {
                        label: 'Campaign',
                        value: campaign.name,
                        formatValue: value => (
                            <AppLink
                                to={generatePath(getPath(CAMPAIGN_PAGE), {
                                    campaignId: campaign.id,
                                })}
                            >
                                {truncate(value, { length: 60 })}
                            </AppLink>
                        ),
                    },
                    {
                        label: 'Campaign Type',
                        value: campaign.campaign_type,
                        toolTip: renderTooltipWithHelptext('campaign_type'),
                        formatValue: value => FACT_TYPE_LABELS[value],
                    },
                    {
                        label: 'State',
                        value: keyword.state,
                        toolTip: renderTooltipWithHelptext('state'),
                        formatValue: titleCase,
                        // input fields
                        fieldId: 'state',
                        type: SELECT_INPUT,
                        options: [
                            { value: PAUSED, label: 'Paused' },
                            { value: ENABLED, label: 'Enabled' },
                        ],
                    },
                    {
                        label: 'Match Type',
                        value: keyword.match_type,
                        toolTip: renderTooltipWithHelptext('match_type'),
                        formatValue: titleCase,
                    },
                    {
                        label: 'Bid',
                        value: keyword.bid,
                        toolTip: renderTooltipWithHelptext('bid'),
                        formatValue: value =>
                            formatCurrency(value, {
                                decimal: true,
                                currencyCode: brand.currency_code,
                            }),
                        // input fields
                        fieldId: 'bid',
                        type: NUMBER_INPUT,
                        min: KEYWORD_BID_MIN,
                        max: KEYWORD_BID_MAX,
                        step: KEYWORD_BID_STEP,
                        precision: KEYWORD_BID_PRECISION,
                    },
                ]}
            />
        )
    }
}

export default KeywordResourceDetails
