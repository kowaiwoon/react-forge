import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import get from 'lodash/get'

import {
    selectChartMetrics,
    selectDataForStackedBarChart,
    selectLoadingForStackedBarChart,
    selectPageDownloading,
} from 'selectors/ui'
import { selectKeyword, selectCampaign } from 'selectors/entities'
import {
    updateChartMetrics,
    downloadKeywordPageTimeseriesRequest,
} from 'actions/ui'
import { KEYWORD_PAGE } from 'constants/pages'
import { CONVERSIONS } from 'constants/charts'
import { getFactTypeObject } from 'helpers/factTypes'

import ConversionsChart from './ConversionsChart'

const mapStateToProps = (state, ownProps) => {
    const { keywordId } = ownProps.match.params
    const keyword = selectKeyword(state, keywordId)
    const campaign = keyword
        ? selectCampaign(state, get(keyword, 'campaign'))
        : {}

    // convert the factType for a single campaign to an object
    // that's compatible with the chart component
    const factTypes = [getFactTypeObject(campaign.campaign_type)]

    const chartMetrics = selectChartMetrics(state, KEYWORD_PAGE, CONVERSIONS)
    const loading = selectLoadingForStackedBarChart(state, KEYWORD_PAGE)
    const downloading = selectPageDownloading(state, KEYWORD_PAGE)
    const { axes, series } = selectDataForStackedBarChart(
        state,
        KEYWORD_PAGE,
        CONVERSIONS
    )

    return {
        chartMetrics,
        factTypes,
        axes,
        series,
        loading,
        downloading,
    }
}

const mapDispatchToProps = {
    updateChartMetrics,
    downloadData: downloadKeywordPageTimeseriesRequest,
}

const ConversionsChartContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ConversionsChart)

export default withRouter(ConversionsChartContainer)
