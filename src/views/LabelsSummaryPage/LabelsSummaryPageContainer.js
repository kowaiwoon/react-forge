import { connect } from 'react-redux'

import { LABELS_SUMMARY_PAGE } from 'constants/pages'
import { selectDomainValue as selectUiDomainValue } from 'selectors/ui'
import {
    mountLabelsSummaryPageRequest,
    unmountLabelsSummaryPage,
} from 'actions/ui'
import { withTabState } from 'components/HigherOrderComponents'

import LabelsSummaryPage from './LabelsSummaryPage'

const mapStateToProps = state => ({
    mounting: selectUiDomainValue(state, [LABELS_SUMMARY_PAGE, 'mounting']),
})

const mapDispatchToProps = {
    mountPage: mountLabelsSummaryPageRequest,
    unmountPage: unmountLabelsSummaryPage,
}

const LabelsSummaryPageContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(LabelsSummaryPage)

export default withTabState(LabelsSummaryPageContainer, 'table')
