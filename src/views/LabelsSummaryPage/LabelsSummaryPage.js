import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Tabs } from 'antd'

import { PageHeader } from 'components/PageHeader'
import { LoadingIndicator } from 'components/LoadingIndicator'

import { LabelsTableContainer } from './Tables'
import styles from './styles.scss'

class LabelsSummaryPage extends Component {
    static propTypes = {
        // Redux state
        mounting: PropTypes.bool.isRequired,

        // tab state
        tab: PropTypes.oneOf(['table']).isRequired,
        handleTabChange: PropTypes.func.isRequired,

        // Redux actions
        mountPage: PropTypes.func.isRequired,
        unmountPage: PropTypes.func.isRequired,
    }

    componentDidMount() {
        this.props.mountPage()
    }

    componentWillUnmount() {
        this.props.unmountPage()
    }

    render() {
        const { mounting, tab, handleTabChange } = this.props

        if (mounting) {
            return (
                <div className={styles['empty-content']}>
                    <LoadingIndicator size="small" />
                </div>
            )
        }

        return (
            <React.Fragment>
                <PageHeader breadcrumbs={[{ name: 'Labels' }]} />

                <Tabs
                    activeKey={tab}
                    tabPosition="top"
                    size="default"
                    tabBarStyle={{
                        fontWeight: '500', // same font-weight as selected tab
                    }}
                    onChange={handleTabChange}
                >
                    <Tabs.TabPane tab="Labels Table" key="table" forceRender>
                        <LabelsTableContainer />
                    </Tabs.TabPane>
                </Tabs>
            </React.Fragment>
        )
    }
}

export default LabelsSummaryPage
