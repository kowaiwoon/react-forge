import { connect } from 'react-redux'

import { LABELS_SUMMARY_PAGE } from 'constants/pages'
import { selectDomainValue as selectUiDomainValue } from 'selectors/ui'
import { selectDomainValue as selectAuthDomainValue } from 'selectors/auth'
import {
    updateLabelsSummaryPageTablePagination,
    updateLabelsSummaryPageTableSorter,
    updateLabelsSummaryPageTableSettings,
    fetchLabelsSummaryPageTableRequest,
    createLabelsSummaryPageLabelRequest,
} from 'actions/ui'

import LabelsTable from './LabelsTable'

const mapStateToProps = state => ({
    tableData: selectUiDomainValue(state, [LABELS_SUMMARY_PAGE, 'table']),
    creating: selectUiDomainValue(state, [LABELS_SUMMARY_PAGE, 'creating']),
    featurePermissions: selectAuthDomainValue(state, 'featurePermissions'),
})

const mapDispatchToProps = {
    updatePagination: updateLabelsSummaryPageTablePagination,
    updateSorter: updateLabelsSummaryPageTableSorter,
    updateColumnSettings: updateLabelsSummaryPageTableSettings,
    reloadData: fetchLabelsSummaryPageTableRequest,
    createLabelRequest: createLabelsSummaryPageLabelRequest,
}

const LabelsTableContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(LabelsTable)

export default LabelsTableContainer
