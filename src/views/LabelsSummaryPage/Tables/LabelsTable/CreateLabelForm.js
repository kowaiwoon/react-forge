import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form, Input, Button, Row, Col } from 'antd'
import autobind from 'autobind-decorator'
import { CirclePicker } from 'react-color'

import { JustSaved } from 'components/JustSaved'
import { OptionalFormLabel } from 'components/OptionalFormLabel'

class CreateLabelForm extends Component {
    static propTypes = {
        creating: PropTypes.bool.isRequired,
        createLabelRequest: PropTypes.func.isRequired,
        onCancel: PropTypes.func.isRequired,

        // Antd form
        form: PropTypes.object.isRequired,
    }

    @autobind
    handleSubmit(e) {
        e.preventDefault()
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.createLabelRequest({
                    name: values.name.trim(),
                    description: values.description,
                    color: values.color.hex,
                })
            }
        })
    }

    @autobind
    handleCancel(e) {
        this.props.form.resetFields()
        this.props.onCancel(e)
    }

    render() {
        const {
            creating,
            form: { getFieldDecorator, resetFields, getFieldValue },
        } = this.props

        return (
            <Form
                layout="vertical"
                onSubmit={this.handleSubmit}
                hideRequiredMark
            >
                <Row gutter={{ md: 16, xl: 32 }}>
                    <Col md={12} xl={4}>
                        <Form.Item label="Label Name">
                            {getFieldDecorator('name', {
                                rules: [
                                    {
                                        required: true,
                                        message: 'Label Name is required',
                                    },
                                ],
                                validateTrigger: 'onSubmit',
                            })(<Input />)}
                        </Form.Item>
                    </Col>
                    <Col md={12} xl={8}>
                        <Form.Item
                            label={
                                <OptionalFormLabel fieldName="Description" />
                            }
                        >
                            {getFieldDecorator('description')(<Input />)}
                        </Form.Item>
                    </Col>
                    <Col md={12} xl={6}>
                        <Form.Item label="Color">
                            {getFieldDecorator('color', {
                                rules: [
                                    {
                                        required: true,
                                        message: 'Color is required',
                                    },
                                ],
                                validateTrigger: 'onSubmit',
                            })(
                                <CirclePicker
                                    color={getFieldValue('color')}
                                    circleSize={24}
                                    circleSpacing={12}
                                />
                            )}
                        </Form.Item>
                    </Col>
                    <Col md={12} xl={6}>
                        <JustSaved
                            saving={creating}
                            justSavedCallback={resetFields}
                        >
                            {({ justSaved }) => (
                                <div style={{ paddingTop: '29px' }}>
                                    <Button
                                        type="primary"
                                        htmlType="submit"
                                        icon={justSaved ? 'check' : 'plus'}
                                        disabled={creating}
                                        loading={creating}
                                        style={{ marginRight: '15px' }}
                                    >
                                        Add Label
                                    </Button>
                                    <Button
                                        onClick={this.handleCancel}
                                        disabled={creating}
                                    >
                                        Cancel
                                    </Button>
                                </div>
                            )}
                        </JustSaved>
                    </Col>
                </Row>
            </Form>
        )
    }
}

export default Form.create()(CreateLabelForm)
