import React, { Component } from 'react'
import PropTypes from 'prop-types'
import autobind from 'autobind-decorator'

import { PaginatedTable } from 'components/PaginatedTable'
import { getTablePaginationOptions } from 'helpers/pagination'
import { ContentCard } from 'components/ContentCard'
import { SettingsButton, CreateResourceButton } from 'components/Buttons'
import { SettingsModal } from 'components/SettingsModal'
import { ColorCircle } from 'components/ColorCircle'

import CreateLabelForm from './CreateLabelForm'

const TABLE_OPTIONS = {
    name: 'label',
    rowKey: 'id',
    columns: {
        name: {
            title: 'Label Name',
            dataIndex: 'name',
            align: 'left',
            sorter: true,
        },
        description: {
            title: 'Description',
            dataIndex: 'description',
            align: 'left',
            sorter: true,
        },
        color: {
            title: 'Color',
            dataIndex: 'color',
            align: 'left',
            sorter: true,
            render: value => <ColorCircle color={value} />,
        },
    },
}

class LabelsTable extends Component {
    static propTypes = {
        tableData: PropTypes.shape({
            data: PropTypes.arrayOf(PropTypes.shape()),
            updating: PropTypes.bool,
            deleting: PropTypes.bool,
            loading: PropTypes.bool,
            pagination: PropTypes.shape({
                pageSize: PropTypes.number,
                current: PropTypes.number,
                total: PropTypes.number,
            }),
            sorter: PropTypes.shape({
                field: PropTypes.string,
                order: PropTypes.oneOf(['descend', 'ascend']),
            }),
            columnSettings: PropTypes.shape({
                actionColumns: PropTypes.array,
                order: PropTypes.array,
                displayState: PropTypes.shape(),
            }),
        }).isRequired,
        creating: PropTypes.bool.isRequired,

        // actions
        updatePagination: PropTypes.func.isRequired,
        updateSorter: PropTypes.func.isRequired,
        updateColumnSettings: PropTypes.func.isRequired,
        reloadData: PropTypes.func.isRequired,
        createLabelRequest: PropTypes.func.isRequired,
    }

    state = {
        settingsModalVisible: false,
        collapseOpen: false,
    }

    @autobind
    handleToggleSettingsModal() {
        this.setState({
            settingsModalVisible: !this.state.settingsModalVisible,
        })
    }

    @autobind
    handleUpdateColumnSettings(settings) {
        const { updateColumnSettings, reloadData } = this.props

        updateColumnSettings(settings)

        // Reload data
        reloadData()

        // Toggle settings modal
        this.handleToggleSettingsModal()
    }

    @autobind
    handleToggleCollapse() {
        this.setState({
            collapseOpen: !this.state.collapseOpen,
        })
    }

    render() {
        const {
            tableData,
            updatePagination,
            updateSorter,
            reloadData,
            creating,
            createLabelRequest,
        } = this.props
        const { settingsModalVisible, collapseOpen } = this.state

        return (
            <ContentCard
                title="Labels"
                subTitle="Manage and create new labels for your brand's needs."
                actions={[
                    <CreateResourceButton onClick={this.handleToggleCollapse}>
                        New Label
                    </CreateResourceButton>,
                    <SettingsButton
                        onClick={this.handleToggleSettingsModal}
                        tooltipTitle="Customize table columns"
                    />,
                ]}
                collapseOpen={collapseOpen}
                collapseContent={
                    <CreateLabelForm
                        creating={creating}
                        createLabelRequest={createLabelRequest}
                        onCancel={this.handleToggleCollapse}
                    />
                }
            >
                <PaginatedTable
                    tableOptions={TABLE_OPTIONS}
                    data={tableData.data}
                    loading={tableData.loading}
                    updating={tableData.updating}
                    deleting={tableData.deleting}
                    pagination={{
                        ...getTablePaginationOptions('Labels'),
                        ...tableData.pagination,
                    }}
                    sorter={tableData.sorter}
                    columnSettings={tableData.columnSettings}
                    updatePagination={updatePagination}
                    updateSorter={updateSorter}
                    reloadData={reloadData}
                />
                <SettingsModal
                    droppableId="paginatedTableSettingsModal"
                    modalTitle="Customize Table Columns"
                    visible={settingsModalVisible}
                    handleCancel={this.handleToggleSettingsModal}
                    handleOk={this.handleUpdateColumnSettings}
                    settings={tableData.columnSettings}
                    settingTitles={TABLE_OPTIONS.columns}
                />
            </ContentCard>
        )
    }
}

export default LabelsTable
