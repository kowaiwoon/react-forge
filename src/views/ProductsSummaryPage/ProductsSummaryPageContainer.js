import { connect } from 'react-redux'

import { PRODUCTS_SUMMARY_PAGE } from 'constants/pages'
import { selectDomainValue as selectUiDomainValue } from 'selectors/ui'
import {
    mountProductsSummaryPageRequest,
    unmountProductsSummary,
} from 'actions/ui'
import { withTabState } from 'components/HigherOrderComponents'

import ProductsSummaryPage from './ProductsSummaryPage'

const mapStateToProps = state => ({
    mounting: selectUiDomainValue(state, [PRODUCTS_SUMMARY_PAGE, 'mounting']),
})

const mapDispatchToProps = {
    mountProductsSummary: mountProductsSummaryPageRequest,
    unmountProductsSummary,
}

const ProductsSummaryPageContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ProductsSummaryPage)

export default withTabState(ProductsSummaryPageContainer, 'treemap')
