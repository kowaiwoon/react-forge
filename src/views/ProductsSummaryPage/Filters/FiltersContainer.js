import { connect } from 'react-redux'

import {
    selectDomainValue as selectUiDomainValue,
    selectPageFilters,
    selectPageFilterSettings,
} from 'selectors/ui'
import {
    selectBrandsForFilters,
    selectCampaignsForFilters,
    selectProductsForAsinFilter,
    selectProductMetadataForTitleFilter,
} from 'selectors/entities'
import {
    updatePageFilterForPage,
    resetPageFiltersForPage,
    updatePageFilterSettingsForPage,
    changeBrandsFilterInput,
    changeCampaignsFilterInputForPage,
    changeProductAsinsFilterInput,
    changeProductTitlesFilterInput,
} from 'actions/ui'
import { PRODUCTS_SUMMARY_PAGE } from 'constants/pages'

import Filters from './Filters'

const mapStateToProps = state => ({
    filterSettings: selectPageFilterSettings(state, PRODUCTS_SUMMARY_PAGE),
    selectedFilterValues: selectPageFilters(state, PRODUCTS_SUMMARY_PAGE),
    brandsFilterOptions: selectBrandsForFilters(state),
    brandsFilterLoading: selectUiDomainValue(state, [
        'app',
        'brandsFilterLoading',
    ]),
    campaignsFilterOptions: selectCampaignsForFilters(state),
    campaignsFilterLoading: selectUiDomainValue(state, [
        'app',
        'campaignsFilterLoading',
    ]),
    productAsinsFilterOptions: selectProductsForAsinFilter(state),
    productAsinsFilterLoading: selectUiDomainValue(state, [
        PRODUCTS_SUMMARY_PAGE,
        'productAsinsFilterLoading',
    ]),
    productTitlesFilterOptions: selectProductMetadataForTitleFilter(state),
    productTitlesFilterLoading: selectUiDomainValue(state, [
        PRODUCTS_SUMMARY_PAGE,
        'productTitlesFilterLoading',
    ]),
})

const mapDispatchToProps = {
    updatePageFilter: updatePageFilterForPage(PRODUCTS_SUMMARY_PAGE),
    resetFilters: resetPageFiltersForPage(PRODUCTS_SUMMARY_PAGE),
    updateFilterSettings: updatePageFilterSettingsForPage(
        PRODUCTS_SUMMARY_PAGE
    ),
    changeBrandsFilterInput,
    changeCampaignsFilterInput: changeCampaignsFilterInputForPage(
        PRODUCTS_SUMMARY_PAGE
    ),
    changeProductAsinsFilterInput,
    changeProductTitlesFilterInput,
}

const FiltersContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Filters)

export default FiltersContainer
