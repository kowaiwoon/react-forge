import React from 'react'
import PropTypes from 'prop-types'
import autobind from 'autobind-decorator'
import set from 'lodash/fp/set'

import {
    FACT_TYPES,
    DATES,
    REGIONS,
    COUNTRIES,
    BRANDS,
    CAMPAIGNS,
    CAMPAIGN_TARGETING_TYPES,
    PRODUCT_ASINS,
    PRODUCT_TITLES,
} from 'constants/filters'
import {
    DateRangeFilter,
    AdTypeFilter,
    RegionsFilter,
    CountriesFilter,
    BrandsFilter,
    CampaignsFilter,
    CampaignTargetingTypesFilter,
    ProductAsinsFilter,
    ProductTitlesFilter,
} from 'components/Filters'
import { FilterGroup } from 'components/FilterGroup'

class Filters extends React.Component {
    static propTypes = {
        filterSettings: PropTypes.shape().isRequired,
        selectedFilterValues: PropTypes.shape().isRequired,
        brandsFilterOptions: PropTypes.array.isRequired,
        brandsFilterLoading: PropTypes.bool.isRequired,
        campaignsFilterOptions: PropTypes.array.isRequired,
        campaignsFilterLoading: PropTypes.bool.isRequired,
        productAsinsFilterOptions: PropTypes.array.isRequired,
        productAsinsFilterLoading: PropTypes.bool.isRequired,
        productTitlesFilterOptions: PropTypes.array.isRequired,
        productTitlesFilterLoading: PropTypes.bool.isRequired,

        // actions
        updatePageFilter: PropTypes.func.isRequired,
        resetFilters: PropTypes.func.isRequired,
        updateFilterSettings: PropTypes.func.isRequired,
        changeBrandsFilterInput: PropTypes.func.isRequired,
        changeCampaignsFilterInput: PropTypes.func.isRequired,
        changeProductAsinsFilterInput: PropTypes.func.isRequired,
        changeProductTitlesFilterInput: PropTypes.func.isRequired,
    }

    @autobind
    handleApply(key, value) {
        this.props.updatePageFilter({
            key,
            value,
        })
    }

    @autobind
    handleClose(filter) {
        const { filterSettings } = this.props
        this.props.updateFilterSettings(
            set(['displayState', filter], false, filterSettings)
        )
    }

    render() {
        const {
            filterSettings,
            selectedFilterValues,
            updateFilterSettings,
            resetFilters,
            campaignsFilterOptions,
            campaignsFilterLoading,
            changeCampaignsFilterInput,
            brandsFilterLoading,
            brandsFilterOptions,
            changeBrandsFilterInput,
            productAsinsFilterLoading,
            productAsinsFilterOptions,
            changeProductAsinsFilterInput,
            productTitlesFilterLoading,
            productTitlesFilterOptions,
            changeProductTitlesFilterInput,
        } = this.props
        return (
            <FilterGroup
                filterSettings={filterSettings}
                updateFilterSettings={updateFilterSettings}
                resetFilters={resetFilters}
            >
                <DateRangeFilter
                    values={selectedFilterValues[DATES]}
                    onApply={this.handleApply}
                    closable={false}
                />
                <AdTypeFilter
                    values={selectedFilterValues[FACT_TYPES]}
                    onApply={this.handleApply}
                    closable={false}
                    helpTextFunc={() =>
                        'Product views do not yet support Headline Search Campaigns'
                    }
                    disabled
                />
                <RegionsFilter
                    values={selectedFilterValues[REGIONS]}
                    onApply={this.handleApply}
                    onCloseFilter={this.handleClose}
                    closable
                />
                <CountriesFilter
                    values={selectedFilterValues[COUNTRIES]}
                    onApply={this.handleApply}
                    onCloseFilter={this.handleClose}
                    closable
                />
                <BrandsFilter
                    values={selectedFilterValues[BRANDS]}
                    options={brandsFilterOptions}
                    onApply={this.handleApply}
                    onCloseFilter={this.handleClose}
                    closable
                    onChangeInput={changeBrandsFilterInput}
                    loading={brandsFilterLoading}
                />
                <CampaignsFilter
                    values={selectedFilterValues[CAMPAIGNS]}
                    options={campaignsFilterOptions}
                    onApply={this.handleApply}
                    onCloseFilter={this.handleClose}
                    closable
                    onChangeInput={changeCampaignsFilterInput}
                    loading={campaignsFilterLoading}
                />
                <CampaignTargetingTypesFilter
                    values={selectedFilterValues[CAMPAIGN_TARGETING_TYPES]}
                    onApply={this.handleApply}
                    onCloseFilter={this.handleClose}
                    closable
                />
                <ProductAsinsFilter
                    values={selectedFilterValues[PRODUCT_ASINS]}
                    options={productAsinsFilterOptions}
                    onApply={this.handleApply}
                    onCloseFilter={this.handleClose}
                    closable
                    onChangeInput={changeProductAsinsFilterInput}
                    loading={productAsinsFilterLoading}
                />
                <ProductTitlesFilter
                    values={selectedFilterValues[PRODUCT_TITLES]}
                    options={productTitlesFilterOptions}
                    onApply={this.handleApply}
                    onCloseFilter={this.handleClose}
                    closable
                    onChangeInput={changeProductTitlesFilterInput}
                    loading={productTitlesFilterLoading}
                />
            </FilterGroup>
        )
    }
}

export default Filters
