import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Tabs } from 'antd'

import { PageHeader } from 'components/PageHeader'
import { LoadingIndicator } from 'components/LoadingIndicator'

import { FiltersContainer } from './Filters'
import { ProductChartContainer } from './Charts'
import { ProductsTableContainer } from './Tables'
import styles from './styles.scss'

class ProductsSummaryPage extends Component {
    static propTypes = {
        // Redux state
        mounting: PropTypes.bool.isRequired,

        // Redux actions
        mountProductsSummary: PropTypes.func.isRequired,
        unmountProductsSummary: PropTypes.func.isRequired,

        // tab state
        tab: PropTypes.oneOf(['treemap', 'table']).isRequired,
        handleTabChange: PropTypes.func.isRequired,
    }

    componentDidMount() {
        this.props.mountProductsSummary()
    }

    componentWillUnmount() {
        this.props.unmountProductsSummary()
    }

    render() {
        const { mounting, tab, handleTabChange } = this.props

        if (mounting) {
            return (
                <div className={styles['empty-content']}>
                    <LoadingIndicator size="small" />
                </div>
            )
        }

        return (
            <React.Fragment>
                <PageHeader
                    breadcrumbs={[{ name: 'All Products' }]}
                    filterGroupComponent={<FiltersContainer />}
                />

                <Tabs
                    activeKey={tab}
                    onChange={handleTabChange}
                    tabPosition="top"
                    size="default"
                    tabBarStyle={{
                        fontWeight: '500', // same font-weight as selected tab
                    }}
                >
                    <Tabs.TabPane
                        tab="Products Tree Chart"
                        key="treemap"
                        forceRender
                    >
                        <ProductChartContainer />
                    </Tabs.TabPane>

                    <Tabs.TabPane tab="Products Table" key="table" forceRender>
                        <ProductsTableContainer />
                    </Tabs.TabPane>
                </Tabs>
            </React.Fragment>
        )
    }
}

export default ProductsSummaryPage
