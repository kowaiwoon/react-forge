import React, { Component } from 'react'
import PropTypes from 'prop-types'
import get from 'lodash/get'
import isUndefined from 'lodash/isUndefined'
import isNull from 'lodash/isNull'
import { generatePath } from 'react-router-dom'
import truncate from 'lodash/truncate'
import { Icon } from 'antd'
import autobind from 'autobind-decorator'

import { PaginatedTable } from 'components/PaginatedTable'
import { getTablePaginationOptions } from 'helpers/pagination'
import { METRIC_COLUMNS } from 'configuration/tables'
import { formatDate, titleCase, formatCurrency } from 'helpers/formatting'
import { UNDEFINED_VALUE } from 'constants/formatting'
import { FACT_TYPE_LABELS } from 'configuration/factTypes'
import { PRODUCT_PAGE, BRAND_PAGE, CAMPAIGN_PAGE } from 'constants/pages'
import { getPath } from 'helpers/pages'
import { AppLink } from 'components/AppLink'
import { asinUrl } from 'helpers/urls'
import { ContentCard } from 'components/ContentCard'
import { SettingsButton, DownloadButton } from 'components/Buttons'
import { SettingsModal } from 'components/SettingsModal'

import styles from './styles.scss'

class ProductsTable extends Component {
    static propTypes = {
        downloading: PropTypes.bool.isRequired,
        tableData: PropTypes.shape({
            data: PropTypes.arrayOf(PropTypes.shape()),
            updating: PropTypes.bool,
            deleting: PropTypes.bool,
            loading: PropTypes.bool,
            pagination: PropTypes.shape({
                pageSize: PropTypes.number,
                current: PropTypes.number,
                total: PropTypes.number,
            }),
            sorter: PropTypes.shape({
                field: PropTypes.string,
                order: PropTypes.oneOf(['descend', 'ascend']),
            }),
            columnSettings: PropTypes.shape({
                actionColumns: PropTypes.array,
                order: PropTypes.array,
                displayState: PropTypes.shape(),
            }),
        }).isRequired,

        // actions
        updatePagination: PropTypes.func.isRequired,
        updateSorter: PropTypes.func.isRequired,
        updateColumnSettings: PropTypes.func.isRequired,
        reloadData: PropTypes.func.isRequired,
        downloadData: PropTypes.func.isRequired,
    }

    state = {
        settingsModalVisible: false,
    }

    static getTableOptions() {
        return {
            name: 'product',
            rowKey: 'product_ad.id',
            columns: {
                'product_metadata.title': {
                    title: 'Product Title',
                    dataIndex: 'product_ad.product_metadata.title',
                    fixed: 'left',
                    align: 'left',
                    width: 300,
                    render: (value, record) =>
                        value ? (
                            <div className={styles.container}>
                                <img
                                    src={get(
                                        record,
                                        'product_ad.product_metadata.small_image_url'
                                    )}
                                    alt={value}
                                />
                                <AppLink
                                    to={generatePath(getPath(PRODUCT_PAGE), {
                                        productId: get(record, 'product_ad.id'),
                                    })}
                                >
                                    {truncate(value, { length: 45 })}
                                </AppLink>
                            </div>
                        ) : (
                            <div className={styles.container}>
                                <Icon
                                    type="picture"
                                    theme="outlined"
                                    className={styles['missing-img-icon']}
                                />
                                <AppLink
                                    to={generatePath(getPath(PRODUCT_PAGE), {
                                        productId: get(record, 'product_ad.id'),
                                    })}
                                >
                                    {get(record, 'product_ad.asin')}
                                </AppLink>
                            </div>
                        ),
                },
                'product_metadata.price': {
                    title: 'Product Price',
                    dataIndex: 'product_ad.product_metadata.price',
                    align: 'left',
                    render: (value, record) =>
                        value
                            ? formatCurrency(value, {
                                  decimal: true,
                                  currencyCode: get(
                                      record,
                                      'product_ad.product_metadata.currency_code'
                                  ),
                              })
                            : UNDEFINED_VALUE,
                },
                'product_ad.asin': {
                    title: 'ASIN',
                    dataIndex: 'product_ad.asin',
                    align: 'left',
                    width: 150,
                    render: (value, record) =>
                        value ? (
                            <a
                                href={asinUrl(
                                    value,
                                    get(record, 'profile.country_code')
                                )}
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                {value} <Icon type="amazon" theme="outlined" />
                            </a>
                        ) : (
                            UNDEFINED_VALUE
                        ),
                },
                'product_ad.sku': {
                    title: 'SKU',
                    dataIndex: 'product_ad.sku',
                    align: 'right',
                    render: value =>
                        value === null || value === undefined
                            ? UNDEFINED_VALUE
                            : value,
                },
                'product_ad.state': {
                    title: 'State',
                    dataIndex: 'product_ad.state',
                    align: 'center',
                    render: value => titleCase(value),
                },
                'profile.brand_name': {
                    title: 'Brand',
                    dataIndex: 'profile.brand_name',
                    align: 'left',
                    render: (text, record) => (
                        <AppLink
                            to={generatePath(getPath(BRAND_PAGE), {
                                brandId: record.profile.id,
                            })}
                        >
                            {text}
                        </AppLink>
                    ),
                },
                'profile.country_code': {
                    title: 'Brand Country',
                    dataIndex: 'profile.country_code',
                    align: 'center',
                },
                'profile.currency_code': {
                    title: 'Brand Currency',
                    dataIndex: 'profile.currency_code',
                    align: 'center',
                },
                'profile.region': {
                    title: 'Brand Region',
                    dataIndex: 'profile.region',
                    align: 'center',
                },
                'profile.timezone': {
                    title: 'Brand Timezone',
                    dataIndex: 'profile.timezone',
                    align: 'center',
                },
                'campaign.name': {
                    title: 'Campaign',
                    dataIndex: 'campaign.name',
                    align: 'left',
                    width: 200,
                    render: (text, record) => (
                        <AppLink
                            to={generatePath(getPath(CAMPAIGN_PAGE), {
                                campaignId: record.campaign.id,
                            })}
                        >
                            {text}
                        </AppLink>
                    ),
                },
                'campaign.budget': {
                    title: 'Campaign Budget',
                    dataIndex: 'campaign.budget',
                    align: 'right',
                    render: (value, record) =>
                        formatCurrency(value, {
                            currencyCode: record.profile.currency_code,
                        }),
                },
                'campaign.budget_type': {
                    title: 'Campaign Budget Type',
                    dataIndex: 'campaign.budget_type',
                    align: 'center',
                    render: value => titleCase(value),
                },
                'campaign.campaign_type': {
                    title: 'Campaign Type',
                    dataIndex: 'campaign.campaign_type',
                    align: 'center',
                    render: value => FACT_TYPE_LABELS[value],
                },
                'campaign.dayparting_enabled': {
                    title: 'Campaign Dayparting Enabled',
                    dataIndex: 'campaign.dayparting_enabled',
                    align: 'center',
                    render: value => (value ? 'Enabled' : 'Disabled'),
                },
                'campaign.start_date': {
                    title: 'Campaign Start Date',
                    dataIndex: 'campaign.start_date',
                    align: 'center',
                    render: value => formatDate(value),
                },
                'campaign.end_date': {
                    title: 'Campaign End Date',
                    dataIndex: 'campaign.end_date',
                    align: 'center',
                    render: value => formatDate(value),
                },
                'campaign.state': {
                    title: 'Campaign State',
                    dataIndex: 'campaign.state',
                    align: 'center',
                    render: value => titleCase(value),
                },
                'campaign.targeting_type': {
                    title: 'Campaign Targeting Type',
                    dataIndex: 'campaign.targeting_type',
                    align: 'center',
                    render: value =>
                        isNull(value) || isUndefined(value)
                            ? UNDEFINED_VALUE
                            : titleCase(value),
                },
                ...METRIC_COLUMNS,
            },
        }
    }

    @autobind
    handleToggleSettingsModal() {
        this.setState({
            settingsModalVisible: !this.state.settingsModalVisible,
        })
    }

    @autobind
    handleUpdateColumnSettings(settings) {
        const { updateColumnSettings, reloadData } = this.props

        updateColumnSettings(settings)

        // Reload data
        reloadData()

        // Toggle settings modal
        this.handleToggleSettingsModal()
    }

    render() {
        const {
            downloading,
            tableData,
            updatePagination,
            updateSorter,
            reloadData,
            downloadData,
        } = this.props
        const { settingsModalVisible } = this.state
        const tableOptions = ProductsTable.getTableOptions()

        return (
            <ContentCard
                title="Products Table"
                subTitle="View tabular data for all Products"
                actions={[
                    <SettingsButton
                        onClick={this.handleToggleSettingsModal}
                        tooltipTitle="Customize table columns"
                    />,
                    <DownloadButton
                        loading={downloading}
                        onClick={downloadData}
                    />,
                ]}
            >
                <PaginatedTable
                    tableOptions={tableOptions}
                    data={tableData.data}
                    loading={tableData.loading}
                    updating={tableData.updating}
                    deleting={tableData.deleting}
                    pagination={{
                        ...getTablePaginationOptions('Products'),
                        ...tableData.pagination,
                    }}
                    sorter={tableData.sorter}
                    columnSettings={tableData.columnSettings}
                    updatePagination={updatePagination}
                    updateSorter={updateSorter}
                    reloadData={reloadData}
                    downloadData={downloadData}
                />
                <SettingsModal
                    droppableId="paginatedTableSettingsModal"
                    modalTitle="Customize Table Columns"
                    visible={settingsModalVisible}
                    handleCancel={this.handleToggleSettingsModal}
                    handleOk={this.handleUpdateColumnSettings}
                    settings={tableData.columnSettings}
                    settingTitles={tableOptions.columns}
                />
            </ContentCard>
        )
    }
}

export default ProductsTable
