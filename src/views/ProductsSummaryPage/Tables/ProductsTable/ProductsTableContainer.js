import { connect } from 'react-redux'

import { PRODUCTS_SUMMARY_PAGE } from 'constants/pages'
import {
    selectDomainValue as selectUiDomainValue,
    selectPageDownloading,
} from 'selectors/ui'
import {
    updateProductsSummaryPageTablePagination,
    updateProductsSummaryPageTableSorter,
    updateProductsSummaryPageTableSettingsRequest,
    fetchProductsSummaryPageTableRequest,
    downloadProductsSummaryPageTableRequest,
} from 'actions/ui'

import ProductsTable from './ProductsTable'

const mapStateToProps = state => ({
    tableData: selectUiDomainValue(state, [PRODUCTS_SUMMARY_PAGE, 'table']),
    downloading: selectPageDownloading(state, PRODUCTS_SUMMARY_PAGE),
})

const mapDispatchToProps = {
    updatePagination: updateProductsSummaryPageTablePagination,
    updateSorter: updateProductsSummaryPageTableSorter,
    updateColumnSettings: updateProductsSummaryPageTableSettingsRequest,
    reloadData: fetchProductsSummaryPageTableRequest,
    downloadData: downloadProductsSummaryPageTableRequest,
}

const ProductsTableContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ProductsTable)

export default ProductsTableContainer
