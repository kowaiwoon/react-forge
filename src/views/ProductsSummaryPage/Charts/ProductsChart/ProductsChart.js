import React from 'react'
import PropTypes from 'prop-types'
import { Icon } from 'antd'
import truncate from 'lodash/truncate'
import isNull from 'lodash/isNull'
import get from 'lodash/get'

import { PaginatedTreemap } from 'components/PaginatedTreemap'
import { getTreemapPaginationOptions } from 'helpers/pagination'

import styles from './styles.scss'

const propTypes = {
    // React router
    history: PropTypes.shape({
        push: PropTypes.func,
    }).isRequired,

    // Redux state
    factTypes: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.string,
            label: PropTypes.string,
        })
    ).isRequired,
    data: PropTypes.array.isRequired,
    loading: PropTypes.bool.isRequired,
    pagination: PropTypes.object.isRequired,
    sorter: PropTypes.object.isRequired,

    // actions
    updateProductsTreemapPagination: PropTypes.func.isRequired,
    updateProductsTreemapSorter: PropTypes.func.isRequired,
    fetchProductsTreemapRequest: PropTypes.func.isRequired,
}

const defaultProps = {}

const ProductsChart = ({
    history,
    factTypes,
    data,
    loading,
    pagination,
    sorter,
    updateProductsTreemapPagination,
    updateProductsTreemapSorter,
    fetchProductsTreemapRequest,
}) => (
    <PaginatedTreemap
        treemapOptions={{
            name: 'product',
            idAttribute: 'product_ad.id',
            titleAttribute: 'product_ad.asin',
            height: 500,
            colors: {
                minColor: '#c4e1ea',
                maxColor: '#8884d8',
            },
        }}
        defaultPagination={getTreemapPaginationOptions('Products')}
        factTypes={factTypes}
        data={data}
        loading={loading}
        pagination={pagination}
        sorter={sorter}
        updatePagination={updateProductsTreemapPagination}
        updateSorter={updateProductsTreemapSorter}
        reloadData={fetchProductsTreemapRequest}
        onLeafClick={leafNodeId => {
            history.push(`/products/${leafNodeId}`)
        }}
        getTitle={({ resourceData }) => {
            const metadata = get(
                resourceData,
                'product_ad.product_metadata',
                null
            )
            return !isNull(metadata)
                ? truncate(metadata.title, { length: 60 })
                : resourceData.product_ad.asin
        }}
        getTooltipContent={({ resourceData }) => {
            const metadata = get(
                resourceData,
                'product_ad.product_metadata',
                null
            )
            return (
                <div className={styles.container}>
                    {!isNull(metadata) ? (
                        <div className={styles['image-box']}>
                            <img
                                src={metadata.small_image_url}
                                alt={metadata.title}
                            />
                        </div>
                    ) : (
                        <Icon
                            type="picture"
                            theme="outlined"
                            className={styles['missing-img-icon']}
                        />
                    )}
                    <ul>
                        {!isNull(metadata) && (
                            <li>
                                <div>Price</div>
                                <div>{metadata.price}</div>
                            </li>
                        )}
                        <li>
                            <div>ASIN</div>
                            <div>{resourceData.product_ad.asin}</div>
                        </li>
                        <li>
                            <div>Brand</div>
                            <div>{resourceData.profile.brand_name}</div>
                        </li>
                    </ul>
                </div>
            )
        }}
    />
)

ProductsChart.propTypes = propTypes
ProductsChart.defaultProps = defaultProps

export default ProductsChart
