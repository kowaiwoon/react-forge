import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import { PRODUCTS_SUMMARY_PAGE } from 'constants/pages'
import {
    selectPageFactTypes,
    selectTreemap,
    selectTreemapChartData,
} from 'selectors/ui'
import {
    updateProductsSummaryPageTreemapPagination,
    updateProductsSummaryPageTreemapSorter,
    fetchProductsSummaryPageTreemapRequest,
} from 'actions/ui'

import ProductsChart from './ProductsChart'

const mapStateToProps = state => {
    const { loading, pagination, sorter } = selectTreemap(
        state,
        PRODUCTS_SUMMARY_PAGE,
        'treemap'
    )
    return {
        factTypes: selectPageFactTypes(state, PRODUCTS_SUMMARY_PAGE),
        data: selectTreemapChartData(state, PRODUCTS_SUMMARY_PAGE, 'treemap'),
        loading,
        pagination,
        sorter,
    }
}

const mapDispatchToProps = {
    updateProductsTreemapPagination: updateProductsSummaryPageTreemapPagination,
    updateProductsTreemapSorter: updateProductsSummaryPageTreemapSorter,
    fetchProductsTreemapRequest: fetchProductsSummaryPageTreemapRequest,
}

const ProductsChartContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ProductsChart)

export default withRouter(ProductsChartContainer)
