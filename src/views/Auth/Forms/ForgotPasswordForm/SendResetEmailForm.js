import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Button, Row, Form, Input, Alert } from 'antd'
import autobind from 'autobind-decorator'
import set from 'lodash/set'
import isEmpty from 'lodash/isEmpty'
import isNull from 'lodash/isNull'

const propTypes = {
    // Antd Form
    form: PropTypes.shape({
        getFieldDecorator: PropTypes.func,
        validateFields: PropTypes.func,
    }).isRequired,

    auth: PropTypes.shape({
        passwordResetEmailSent: PropTypes.bool,
        error: PropTypes.string,
        isFetching: PropTypes.bool,
    }).isRequired,

    // actions
    sendResetEmailRequest: PropTypes.func.isRequired,
    resetAuthErrors: PropTypes.func.isRequired,
}
const defaultProps = {}

class SendResetEmailForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            emailSentErrorAlertVisible: false,
        }
    }

    static getDerivedStateFromProps(nextProps) {
        const nextState = {}

        if (nextProps.auth.passwordResetEmailSent) {
            set(nextState, 'emailSentSuccessAlertVisible', true)
        }

        if (!isNull(nextProps.auth.error)) {
            set(nextState, 'emailSentErrorAlertVisible', true)
        }

        return isEmpty(nextState) ? null : nextState
    }

    @autobind
    handleEmailSentAlertClose() {
        this.setState({
            emailSentErrorAlertVisible: false,
        })
        this.props.resetAuthErrors()
    }

    @autobind
    handleSendResetEmailSubmit(e) {
        e.preventDefault()
        this.props.form.validateFields((err, values) => {
            const { email } = values

            if (!err) {
                this.props.sendResetEmailRequest({ email })
            }
        })
    }

    emailFieldDecorator(component) {
        return this.props.form.getFieldDecorator('email', {
            rules: [
                {
                    required: true,
                    message: 'Email is required',
                },
            ],
        })(component)
    }

    render() {
        return (
            <Form onSubmit={this.handleSendResetEmailSubmit}>
                <Form.Item hasFeedback>
                    {this.emailFieldDecorator(<Input placeholder="Email" />)}
                </Form.Item>

                {this.props.auth.passwordResetEmailSent ? (
                    <Row>
                        <Button
                            type="primary"
                            htmlType="submit"
                            loading={this.props.auth.isFetching}
                        >
                            Send Password Reset Email Again
                        </Button>
                        <br />
                        <br />
                        <Alert
                            message="Check your email for a reset code"
                            type="success"
                            closable
                        />
                    </Row>
                ) : (
                    <Row>
                        <Button
                            type="primary"
                            htmlType="submit"
                            loading={this.props.auth.isFetching}
                        >
                            Send Password Reset Email
                        </Button>
                    </Row>
                )}

                {this.state.emailSentErrorAlertVisible && (
                    <div>
                        <br />
                        <Alert
                            message={this.props.auth.error}
                            type="error"
                            closable
                            afterClose={this.handleEmailSentAlertClose}
                        />
                    </div>
                )}
            </Form>
        )
    }
}

SendResetEmailForm.propTypes = propTypes
SendResetEmailForm.defaultProps = defaultProps

export default Form.create()(SendResetEmailForm)
