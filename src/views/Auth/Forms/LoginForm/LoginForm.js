import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Button, Row, Form, Input, Alert } from 'antd'
import autobind from 'autobind-decorator'
import isNull from 'lodash/isNull'

import { getPath } from 'helpers/pages'
import { AUTH_SIGNUP_PAGE, AUTH_FORGOT_PASSWORD_PAGE } from 'constants/pages'
import { AppLink } from 'components/AppLink'

class LoginForm extends Component {
    static propTypes = {
        // Form
        form: PropTypes.shape({
            getFieldDecorator: PropTypes.func,
            validateFields: PropTypes.func,
        }).isRequired,

        // Redux state
        auth: PropTypes.shape({
            isFetching: PropTypes.bool,
            error: PropTypes.string,
        }).isRequired,

        // Redux actions
        resetAuth: PropTypes.func.isRequired,
        signInRequest: PropTypes.func.isRequired,
    }

    static defaultProps = {}

    constructor(props) {
        super(props)
        this.state = {
            loginAlertVisible: false,
        }
    }

    static getDerivedStateFromProps(nextProps) {
        if (!isNull(nextProps.auth.error)) {
            return { loginAlertVisible: true }
        }
        return null
    }

    @autobind
    handleLoginAlertClose() {
        this.setState({
            loginAlertVisible: false,
        })
        this.props.resetAuth()
    }

    @autobind
    handleSubmit(e) {
        e.preventDefault()
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.signInRequest(values)
            }
        })
    }

    emailFieldDecorator(component) {
        return this.props.form.getFieldDecorator('email', {
            rules: [
                {
                    required: true,
                    message: 'Email is required.',
                },
                {
                    type: 'email',
                    message: 'The input is not valid email.',
                },
            ],
            validateTrigger: '',
        })(component)
    }

    passwordFieldDecorator(component) {
        return this.props.form.getFieldDecorator('password', {
            rules: [
                {
                    required: true,
                    message: 'Password is required.',
                },
            ],
            validateTrigger: '',
        })(component)
    }

    render() {
        return (
            <Form onSubmit={this.handleSubmit}>
                <Form.Item hasFeedback>
                    {this.emailFieldDecorator(<Input placeholder="Email" />)}
                </Form.Item>

                <Form.Item hasFeedback>
                    {this.passwordFieldDecorator(
                        <Input type="password" placeholder="Password" />
                    )}
                </Form.Item>

                <Row>
                    <Button
                        type="primary"
                        htmlType="submit"
                        loading={this.props.auth.isFetching}
                    >
                        Sign in
                    </Button>
                    <p>
                        <AppLink to={getPath(AUTH_SIGNUP_PAGE)}>
                            <span>Sign Up</span>
                        </AppLink>
                        <AppLink to={getPath(AUTH_FORGOT_PASSWORD_PAGE)}>
                            <span>Forgot Password</span>
                        </AppLink>
                    </p>
                </Row>

                {this.state.loginAlertVisible && (
                    <Alert
                        message={this.props.auth.error}
                        type="error"
                        closable
                        afterClose={this.handleLoginAlertClose}
                    />
                )}
            </Form>
        )
    }
}

export default Form.create()(LoginForm)
