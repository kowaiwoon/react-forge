import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Tabs } from 'antd'
import ReactJoyride from 'react-joyride'
import autobind from 'autobind-decorator'
import isNull from 'lodash/isNull'

import { PageHeader } from 'components/PageHeader'
import { ResourceDetails } from 'components/ResourceDetails'
import { UNDEFINED_VALUE } from 'constants/formatting'
import { LoadingIndicator } from 'components/LoadingIndicator'

import OrganizationsPaneContainer from './OrganizationsPane/OrganizationsPaneContainer'
import PermissionsPaneContainer from './PermissionsPane/PermissionsPaneContainer'
import styles from './styles.scss'

class ProfilePage extends Component {
    static propTypes = {
        // tab state
        tab: PropTypes.oneOf(['organizations', 'permissions']).isRequired,
        handleTabChange: PropTypes.func.isRequired,

        // redux state
        mounting: PropTypes.bool.isRequired,
        showDetails: PropTypes.bool.isRequired,
        auth: PropTypes.shape({
            username: PropTypes.string,
            email: PropTypes.string,
            phone_number: PropTypes.string,
            name: PropTypes.string,
            company: PropTypes.string,
            organizationId: PropTypes.string,
        }).isRequired,
        hasIntegration: PropTypes.bool.isRequired,

        // redux actions
        mountProfilePageRequest: PropTypes.func.isRequired,
        unmountProfilePage: PropTypes.func.isRequired,
        toggleProfilePageDetails: PropTypes.func.isRequired,
    }

    state = {
        run: false,
        stepIndex: 0,
        steps: [
            {
                title: 'Open Organizations Tab',
                content: (
                    <p>
                        Go to the <strong>Organizations</strong> tab to complete
                        account setup.
                    </p>
                ),
                placement: 'bottom',
                target: '.organizations-tab',
                spotlightClicks: true,
            },
        ],
    }

    componentDidMount() {
        this.props.mountProfilePageRequest()
        this.setInitialWalkThroughState()
    }

    componentWillUnmount() {
        this.props.unmountProfilePage()
    }

    setInitialWalkThroughState() {
        const {
            tab,
            hasIntegration,
            auth: { organizationId },
        } = this.props

        if (
            tab !== 'organizations' &&
            (!hasIntegration || isNull(organizationId))
        ) {
            this.setState({ run: true })
        }
    }

    @autobind
    handleTabChangeWithWalkThrough(tabName) {
        const {
            hasIntegration,
            auth: { organizationId },
            handleTabChange,
        } = this.props

        handleTabChange(tabName)

        if (
            tabName !== 'organizations' &&
            (!hasIntegration || isNull(organizationId))
        ) {
            this.setState({ run: true })
        } else if (this.state.run) {
            this.setState({ run: false })
        }
    }

    render() {
        const {
            tab,
            showDetails,
            toggleProfilePageDetails,
            auth: { username, email, phone_number, name, company },
            mounting,
        } = this.props
        const { run, stepIndex, steps } = this.state

        if (mounting) {
            return (
                <div className={styles['empty-content']}>
                    <LoadingIndicator size="small" />
                </div>
            )
        }

        return (
            <React.Fragment>
                <ReactJoyride
                    hideBackButton
                    disableScrolling
                    disableScrollParentFix // prevents overflow=initial from being set in parent div
                    run={run}
                    stepIndex={stepIndex}
                    steps={steps}
                    styles={{
                        options: {
                            zIndex: 10000,
                        },
                    }}
                />

                <PageHeader
                    breadcrumbs={[{ name: 'Profile' }]}
                    titleComponent={
                        <ResourceDetails
                            name={name}
                            allowEditing={false}
                            editToolTip="Editing profile information is not yet supported."
                            showDetails={showDetails}
                            onShowDetailsClick={toggleProfilePageDetails}
                            details={[
                                { label: 'User ID', value: username },
                                { label: 'Email', value: email },
                                {
                                    label: 'Phone Number',
                                    value: isNull(phone_number)
                                        ? UNDEFINED_VALUE
                                        : phone_number,
                                },
                                { label: 'Name', value: name },
                                { label: 'Company', value: company },
                            ]}
                        />
                    }
                />

                <Tabs
                    activeKey={tab}
                    onChange={this.handleTabChangeWithWalkThrough}
                    tabPosition="top"
                    size="default"
                    tabBarStyle={
                        { fontWeight: '500' } // same font-weight as selected tab
                    }
                >
                    <Tabs.TabPane
                        tab={
                            <span className="organizations-tab">
                                Organizations
                            </span>
                        }
                        key="organizations"
                        forceRender
                    >
                        <OrganizationsPaneContainer tab={tab} />
                    </Tabs.TabPane>

                    <Tabs.TabPane
                        tab="Permissions"
                        key="permissions"
                        forceRender
                    >
                        <PermissionsPaneContainer />
                    </Tabs.TabPane>
                </Tabs>
            </React.Fragment>
        )
    }
}

export default ProfilePage
