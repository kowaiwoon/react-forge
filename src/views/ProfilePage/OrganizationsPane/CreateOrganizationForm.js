import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Modal, Form, Input, Button } from 'antd'

import { OptionalFormLabel } from 'components/OptionalFormLabel'
import { showDriftSidebar } from 'helpers/drift'

import styles from './styles.scss'

// Antd Form.create() requires an instance of React.Component
// but eslint wants us to use a stateless function component here
// eslint-disable-next-line
class CreateOrganizationForm extends Component {
    static propTypes = {
        visible: PropTypes.bool.isRequired,
        onCancel: PropTypes.func.isRequired,
        onOk: PropTypes.func.isRequired,
        creating: PropTypes.bool.isRequired,
        form: PropTypes.object.isRequired,
    }

    render() {
        const {
            visible,
            onCancel,
            onOk,
            creating,
            form: { getFieldDecorator },
        } = this.props
        return (
            <Modal
                visible={visible}
                onCancel={onCancel}
                onOk={onOk}
                okButtonProps={{ disabled: creating, loading: creating }}
                cancelButtonProps={{ disabled: creating }}
                title="Create a New Organization"
                okText="Create Organization"
            >
                <p>
                    <strong>Creating an Organization</strong> allows you to
                    securely provide multiple users with access to data. With
                    Organizations, you define User Groups with explict
                    permissions, then invite your colleagues to be members of
                    these groups.
                </p>
                <p>
                    <strong>Is your company already using Downstream?</strong>{' '}
                    If so, your probably don&apos;t need to create a new
                    organization. Instead, contact your company&apos;s
                    administrator to get added to an existing User Group.
                </p>
                <p>
                    <strong>Question or concerns?</strong> Contact us{' '}
                    <Button
                        className={styles['link-button']}
                        onClick={showDriftSidebar}
                    >
                        on chat
                    </Button>
                    . We&apos;re here to help!
                </p>
                <Form layout="vertical" hideRequiredMark>
                    <Form.Item label="Organization Name">
                        {getFieldDecorator('name', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Organization name is required.',
                                },
                            ],
                            validateTrigger: 'onSubmit',
                        })(<Input />)}
                    </Form.Item>
                    <Form.Item label="Website">
                        {getFieldDecorator('website', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Website is required.',
                                },
                            ],
                            validateTrigger: 'onSubmit',
                        })(<Input placeholder="https://www.my-website.com" />)}
                    </Form.Item>
                    <Form.Item
                        label={<OptionalFormLabel fieldName="Logo URL" />}
                    >
                        {getFieldDecorator('image_url')(
                            <Input placeholder="https://www.my-website.com/my-logo.png" />
                        )}
                    </Form.Item>
                </Form>
            </Modal>
        )
    }
}

export default Form.create()(CreateOrganizationForm)
