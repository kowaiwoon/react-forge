import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import { selectDomainValue as selectUiDomainValue } from 'selectors/ui'
import { selectHasIntegration } from 'selectors/auth'
import { selectOrganizations } from 'selectors/orgs'
import { PROFILE_PAGE } from 'constants/pages'
import {
    openProfilePageOrganizationForm,
    closeProfilePageOrganizationForm,
    createProfilePageOrganizationRequest,
    acceptInvitationProfilePageRequest,
} from 'actions/ui'

import OrganizationsPane from './OrganizationsPane'

const mapStateToProps = state => ({
    organizations: selectOrganizations(state),
    formVisible: selectUiDomainValue(state, [
        PROFILE_PAGE,
        'organizationFormVisible',
    ]),
    creating: selectUiDomainValue(state, [
        PROFILE_PAGE,
        'creatingOrganization',
    ]),
    invitations: selectUiDomainValue(state, [PROFILE_PAGE, 'invitations']),
    acceptingInvite: selectUiDomainValue(state, [
        PROFILE_PAGE,
        'acceptingInvite',
    ]),
    auth: state.auth,
    hasIntegration: selectHasIntegration(state),
})

const mapDispatchToProps = {
    openForm: openProfilePageOrganizationForm,
    closeForm: closeProfilePageOrganizationForm,
    createOrganizationRequest: createProfilePageOrganizationRequest,
    acceptInvitationRequest: acceptInvitationProfilePageRequest,
}

const OrganizationsPaneContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(OrganizationsPane)

export default withRouter(OrganizationsPaneContainer)
