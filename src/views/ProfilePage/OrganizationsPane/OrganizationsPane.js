import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Row, Col, Card, Button } from 'antd'
import ReactJoyride, { EVENTS, STATUS } from 'react-joyride'
import { generatePath } from 'react-router-dom'
import autobind from 'autobind-decorator'
import classNames from 'classnames'
import get from 'lodash/get'
import isNull from 'lodash/isNull'

import { getPath } from 'helpers/pages'
import { ORGANIZATION_PAGE } from 'constants/pages'
import { AppLink } from 'components/AppLink'
import { OrganizationCard } from 'components/OrganizationCard'
import { formatUrl } from 'helpers/formatting'

import CreateOrganizationForm from './CreateOrganizationForm'
import styles from './styles.scss'

class OrganizationsPane extends Component {
    static propTypes = {
        tab: PropTypes.string.isRequired,

        // redux state
        organizations: PropTypes.arrayOf(
            PropTypes.shape({
                id: PropTypes.string.isRequired,
                name: PropTypes.string.isRequired,
                group_count: PropTypes.number.isRequired,
                member_count: PropTypes.number.isRequired,
            })
        ).isRequired,
        auth: PropTypes.shape({
            organizationId: PropTypes.string,
        }).isRequired,
        hasIntegration: PropTypes.bool.isRequired,
        formVisible: PropTypes.bool.isRequired,
        creating: PropTypes.bool.isRequired,
        invitations: PropTypes.array.isRequired,
        acceptingInvite: PropTypes.bool.isRequired,

        // actions
        openForm: PropTypes.func.isRequired,
        closeForm: PropTypes.func.isRequired,
        createOrganizationRequest: PropTypes.func.isRequired,
        acceptInvitationRequest: PropTypes.func.isRequired,

        // react-router
        history: PropTypes.object.isRequired,
    }

    state = {
        cardHeight: 250,
        run: false,
        stepIndex: 0,
        steps: [
            {
                title: 'Welcome to Downstream!',
                content: (
                    <p>
                        This guide will walk you through the steps to setup your
                        account.
                    </p>
                ),
                placement: 'center',
                target: 'body',
                locale: { close: 'Get Started' },
            },
            {
                title: 'Create an Organization',
                content: (
                    <p>
                        Click here to create an Organization for your company.
                    </p>
                ),
                placement: 'bottom',
                target: '.create-new-organization-btn',
                spotlightClicks: true,
            },
            {
                title: 'Configure Your Organization',
                content: (
                    <p>
                        Now that you&apos;ve created an Organization, it&apos;s
                        time to configure it. Click the organization to get
                        started.
                    </p>
                ),
                placement: 'right',
                target: '.new-organization',
                spotlightClicks: true,
            },
        ],
    }

    componentDidMount() {
        this.setInitialWalkThroughState()
    }

    componentDidUpdate(prevProps, prevState) {
        this.displayConfigureOrganizationWalkThrough(prevProps, prevState)
    }

    componentWillUnmount() {
        if (!isNull(this.timer)) {
            clearTimeout(this.timer)
        }
    }

    setInitialWalkThroughState() {
        const {
            tab,
            auth: { organizationId },
            hasIntegration,
        } = this.props

        if (tab === 'organizations') {
            if (isNull(organizationId)) {
                this.setState({ run: true, stepIndex: 0 })
            } else if (!hasIntegration) {
                this.setState({ run: true, stepIndex: 2 })
            }
            return
        }

        if (isNull(organizationId)) {
            this.setState({ stepIndex: 1 })
        } else if (!hasIntegration) {
            this.setState({ stepIndex: 2 })
        }
    }

    displayConfigureOrganizationWalkThrough(prevProps, prevState) {
        const {
            tab,
            auth: { organizationId },
            hasIntegration,
        } = this.props
        const { stepIndex } = this.state

        // new organization is created
        if (
            isNull(prevProps.auth.organizationId) &&
            !isNull(organizationId) &&
            !hasIntegration
        ) {
            this.setState({
                run: true,
                stepIndex: 2,
            })
            return
        }

        // handle tab changes
        if (prevState.run && tab !== 'organizations') {
            this.setState({ run: false })
        } else if (
            !prevState.run &&
            tab === 'organizations' &&
            isNull(organizationId) &&
            stepIndex !== 0
        ) {
            // delay the state change so page can switch to tab
            this.timer = setTimeout(() => {
                this.setState({ run: true, stepIndex: 1 })
            }, 500)
        } else if (
            !prevState.run &&
            tab === 'organizations' &&
            !hasIntegration &&
            stepIndex !== 0
        ) {
            // delay the state change so page can switch to tab
            this.timer = setTimeout(() => {
                this.setState({ run: true, stepIndex: 2 })
            }, 500)
        }
    }

    timer = null

    @autobind
    handleCreate() {
        const {
            props: { form },
        } = this.formRef
        form.validateFields((err, values) => {
            if (!err) {
                this.props.createOrganizationRequest({
                    ...values,
                    website: formatUrl(values.website),
                })
                form.resetFields()
            }
        })
    }

    @autobind
    handleCancel() {
        const {
            props: { form },
        } = this.formRef
        this.props.closeForm()
        form.resetFields()
    }

    @autobind
    handleJoyrideCallback(data) {
        const { index, type, status } = data

        if (status === STATUS.FINISHED) {
            this.setState({ run: false, stepIndex: 0 })
        } else if (type === EVENTS.STEP_AFTER && index < 2) {
            // Update state to advance the tour
            const stepIndex = index + 1
            this.setState({
                stepIndex,
            })
        }
    }

    @autobind
    handleCreateOrganizationClick() {
        this.setState({ run: false })
    }

    handleAcceptInvite(invitation) {
        this.props.acceptInvitationRequest({
            invitationId: invitation.id,
            organizationId: get(invitation, ['organization', 'id']),
            history: this.props.history,
        })
    }

    render() {
        const {
            organizations,
            invitations,
            formVisible,
            openForm,
            creating,
            acceptingInvite,
        } = this.props
        const { cardHeight, run, stepIndex, steps } = this.state

        return (
            <React.Fragment>
                <ReactJoyride
                    hideBackButton
                    disableScrolling
                    disableScrollParentFix // prevents overflow=initial from being set in parent div
                    callback={this.handleJoyrideCallback}
                    run={run}
                    stepIndex={stepIndex}
                    steps={steps}
                    styles={{
                        options: {
                            zIndex: 10000,
                        },
                    }}
                />
                <Row gutter={16}>
                    {invitations.map((invitation, index) => (
                        <Col
                            key={invitation.organization.id}
                            span={6}
                            style={{ marginTop: index > 3 ? '16px' : 0 }}
                        >
                            <OrganizationCard
                                organization={invitation.organization}
                                onSize={size =>
                                    this.setState({
                                        cardHeight: size.height,
                                    })
                                }
                                pendingInvite
                                handleAcceptInvite={() =>
                                    this.handleAcceptInvite(invitation)
                                }
                                acceptingInvite={acceptingInvite}
                            />
                        </Col>
                    ))}
                    {organizations.map((org, index) => (
                        <Col
                            key={org.id}
                            span={6}
                            style={{
                                marginTop:
                                    invitations.length + index > 3 ? '16px' : 0,
                            }}
                        >
                            <AppLink
                                to={generatePath(getPath(ORGANIZATION_PAGE), {
                                    organizationId: org.id,
                                })}
                            >
                                <OrganizationCard
                                    className={
                                        index === 0 ? 'new-organization' : ''
                                    }
                                    organization={org}
                                    onSize={size =>
                                        this.setState({
                                            cardHeight: size.height,
                                        })
                                    }
                                />
                            </AppLink>
                        </Col>
                    ))}
                    <Col
                        span={6}
                        style={{
                            marginTop:
                                invitations.length + organizations.length > 3
                                    ? '16px'
                                    : 0,
                        }}
                    >
                        <Card
                            className={styles['new-card']}
                            style={{ height: cardHeight }}
                            onClick={openForm}
                        >
                            <div className={styles['new-card-container']}>
                                <Button
                                    type="primary"
                                    shape="circle"
                                    icon="plus"
                                    className={classNames(
                                        'create-new-organization-btn'
                                    )}
                                    onClick={this.handleCreateOrganizationClick}
                                />
                                <div>Create New Organization</div>
                            </div>
                        </Card>
                        <CreateOrganizationForm
                            wrappedComponentRef={formRef => {
                                this.formRef = formRef
                            }}
                            visible={formVisible}
                            onCancel={this.handleCancel}
                            onOk={this.handleCreate}
                            creating={creating}
                        />
                    </Col>
                </Row>
            </React.Fragment>
        )
    }
}

export default OrganizationsPane
