import { connect } from 'react-redux'

import { selectDomainValue } from 'selectors/ui'
import { selectHasIntegration } from 'selectors/auth'
import { PROFILE_PAGE } from 'constants/pages'
import {
    toggleProfilePageDetails,
    mountProfilePageRequest,
    unmountProfilePage,
} from 'actions/ui'
import { withTabState } from 'components/HigherOrderComponents'

import ProfilePage from './ProfilePage'

const mapStateToProps = state => ({
    mounting: selectDomainValue(state, [PROFILE_PAGE, 'mounting']),
    showDetails: selectDomainValue(state, [PROFILE_PAGE, 'showDetails']),
    auth: state.auth,
    hasIntegration: selectHasIntegration(state),
})

const mapDispatchToProps = {
    toggleProfilePageDetails,
    mountProfilePageRequest,
    unmountProfilePage,
}

const ProfilePageContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ProfilePage)

export default withTabState(ProfilePageContainer, 'organizations')
