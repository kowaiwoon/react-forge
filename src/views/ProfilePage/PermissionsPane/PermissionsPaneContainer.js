import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import PermissionsPane from './PermissionsPane'

const mapStateToProps = state => ({
    auth: state.auth,
})

const PermissionsPaneContainer = connect(mapStateToProps)(PermissionsPane)

export default withRouter(PermissionsPaneContainer)
