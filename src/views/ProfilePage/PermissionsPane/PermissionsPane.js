import React from 'react'
import PropTypes from 'prop-types'
import { List, Avatar } from 'antd'

import { ContentCard } from 'components/ContentCard'
import { getFeaturePermissionsData } from 'helpers/featurePermissions'

import styles from './styles.scss'

const propTypes = {
    // Redux state
    auth: PropTypes.shape({
        featurePermissions: PropTypes.arrayOf(PropTypes.string),
    }).isRequired,
}
const defaultProps = {}

const HasPermissionsAvatar = () => (
    <Avatar style={{ backgroundColor: '#87d068' }} icon="check" />
)

const NoPermissionsAvatar = () => (
    <Avatar style={{ backgroundColor: '#f43a49' }} icon="close" />
)

const PermissionsPane = ({ auth: { featurePermissions } }) => (
    <React.Fragment>
        <ContentCard title="Feature Permissions" rowSpacing>
            <List
                className={styles.list}
                grid={{
                    size: 'small',
                    column: 1,
                }}
                dataSource={getFeaturePermissionsData(featurePermissions)}
                renderItem={item => (
                    <List.Item>
                        <List.Item.Meta
                            avatar={
                                item.hasPermissions ? (
                                    <HasPermissionsAvatar />
                                ) : (
                                    <NoPermissionsAvatar />
                                )
                            }
                            title={item.title}
                            description={item.description}
                        />
                    </List.Item>
                )}
            />
        </ContentCard>
    </React.Fragment>
)

PermissionsPane.propTypes = propTypes
PermissionsPane.defaultProps = defaultProps

export default PermissionsPane
