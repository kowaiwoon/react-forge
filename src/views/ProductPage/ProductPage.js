import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Tabs, Row, Col } from 'antd'
import isEmpty from 'lodash/isEmpty'

import { BRANDS_SUMMARY_PAGE, CAMPAIGNS_SUMMARY_PAGE } from 'constants/pages'
import { SPONSORED_PRODUCT, HEADLINE_SEARCH } from 'constants/factTypes'
import { PageHeader } from 'components/PageHeader'
import { LoadingIndicator } from 'components/LoadingIndicator'
import { MetricSummaryPanel } from 'components/MetricSummaryPanel'
import {
    getBrandCampaignProductPageBreadcrumbs,
    getCampaignProductPageBreadcrumbs,
    getProductPageBreadcrumbs,
} from 'helpers/breadcrumbs'

import { FiltersContainer } from './Filters'
import { ResourceDetailsContainer } from './ResourceDetails'
import {
    ConversionsChartContainer,
    ReachChartContainer,
    RoiChartContainer,
} from './Charts'
import styles from './styles.scss'

class ProductPage extends Component {
    static propTypes = {
        productId: PropTypes.string.isRequired,
        product: PropTypes.shape({
            ad_group_id: PropTypes.string,
            asin: PropTypes.string,
            campaign_id: PropTypes.string,
            created_date: PropTypes.string,
            id: PropTypes.string,
            sku: PropTypes.string,
            state: PropTypes.string,
            updated_date: PropTypes.string,
        }),
        campaign: PropTypes.shape({
            budget: PropTypes.number,
            budget_type: PropTypes.string,
            campaign_type: PropTypes.oneOf([
                SPONSORED_PRODUCT,
                HEADLINE_SEARCH,
            ]),
            end_date: PropTypes.string,
            id: PropTypes.string,
            name: PropTypes.string,
            premium_bid_adjustment: PropTypes.bool,
            start_date: PropTypes.string,
            state: PropTypes.string,
            targeting_type: PropTypes.string,
        }),
        brand: PropTypes.shape({
            id: PropTypes.string,
            brand_name: PropTypes.string,
            region: PropTypes.string,
            country_code: PropTypes.string,
            currency_code: PropTypes.string,
            seller_string_id: PropTypes.string,
        }),
        metadata: PropTypes.shape({
            title: PropTypes.string,
            price: PropTypes.number,
            small_image_url: PropTypes.string,
            brand: PropTypes.string,
        }),
        topLevelPage: PropTypes.string.isRequired,
        mounting: PropTypes.bool.isRequired,
        productAggregate: PropTypes.shape().isRequired,

        // actions
        mountProductPage: PropTypes.func.isRequired,
        unmountProductPage: PropTypes.func.isRequired,

        // tab state
        tab: PropTypes.oneOf(['roi', 'reach', 'conversions']).isRequired,
        handleTabChange: PropTypes.func.isRequired,
    }

    static defaultProps = {
        product: {},
        campaign: {},
        brand: {},
        metadata: {},
    }

    componentDidMount() {
        const { productId } = this.props
        this.props.mountProductPage({ productId })
    }

    componentWillUnmount() {
        this.props.unmountProductPage()
    }

    getBreadcrumbs() {
        const { topLevelPage, brand, campaign, metadata, product } = this.props
        const productName = isEmpty(metadata) ? product.asin : metadata.title

        // All Brands -> Brand -> Campaign -> Product
        if (topLevelPage === BRANDS_SUMMARY_PAGE && !isEmpty(brand)) {
            return getBrandCampaignProductPageBreadcrumbs(
                brand,
                campaign,
                productName
            )
        }

        // All Campaigns -> Campaign -> Product
        if (topLevelPage === CAMPAIGNS_SUMMARY_PAGE && !isEmpty(campaign)) {
            return getCampaignProductPageBreadcrumbs(campaign, productName)
        }

        // All Products -> Product
        return getProductPageBreadcrumbs(productName)
    }

    render() {
        const { productAggregate, mounting, tab, handleTabChange } = this.props

        if (mounting) {
            return (
                <div className={styles['empty-content']}>
                    <LoadingIndicator size="small" />
                </div>
            )
        }

        return (
            <React.Fragment>
                <PageHeader
                    breadcrumbs={this.getBreadcrumbs()}
                    titleComponent={<ResourceDetailsContainer />}
                    filterGroupComponent={<FiltersContainer />}
                />

                <Tabs
                    activeKey={tab}
                    onChange={handleTabChange}
                    tabPosition="top"
                    size="default"
                    tabBarStyle={{
                        fontWeight: '500', // same font-weight as selected tab
                    }}
                >
                    <Tabs.TabPane
                        tab="Return on Investment"
                        key="roi"
                        forceRender
                    >
                        <MetricSummaryPanel
                            loading={productAggregate.loading}
                            data={productAggregate.data}
                            category="roi"
                        />
                        <Row>
                            <Col span={24} className={styles['row-spacing']}>
                                <RoiChartContainer />
                            </Col>
                        </Row>
                    </Tabs.TabPane>

                    <Tabs.TabPane tab="Reach" key="reach" forceRender>
                        <MetricSummaryPanel
                            loading={productAggregate.loading}
                            data={productAggregate.data}
                            category="reach"
                        />
                        <Row className={styles['row-spacing']}>
                            <Col span={24}>
                                <ReachChartContainer />
                            </Col>
                        </Row>
                    </Tabs.TabPane>

                    <Tabs.TabPane
                        tab="Conversions"
                        key="conversions"
                        forceRender
                    >
                        <MetricSummaryPanel
                            loading={productAggregate.loading}
                            data={productAggregate.data}
                            category="conversions"
                        />
                        <Row className={styles['row-spacing']}>
                            <Col span={24}>
                                <ConversionsChartContainer />
                            </Col>
                        </Row>
                    </Tabs.TabPane>
                </Tabs>
            </React.Fragment>
        )
    }
}

export default ProductPage
