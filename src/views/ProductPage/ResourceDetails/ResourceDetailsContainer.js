import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import get from 'lodash/get'

import { selectDomainValue as selectUiDomainValue } from 'selectors/ui'
import {
    selectBrand,
    selectCampaign,
    selectProduct,
    selectMetadata,
} from 'selectors/entities'
import { PRODUCT_PAGE } from 'constants/pages'
import {
    toggleProductPageDetails,
    updateProductPageProductDetailsRequest,
} from 'actions/ui'

import ResourceDetails from './ResourceDetails'

const mapStateToProps = (state, ownProps) => {
    const { productId } = ownProps.match.params
    const product = selectProduct(state, productId)
    const campaign = product
        ? selectCampaign(state, get(product, 'campaign'))
        : {}
    const brand = product ? selectBrand(state, get(product, 'profile')) : {}
    const metadata =
        product && get(product, 'product_metadata')
            ? selectMetadata(state, get(product, 'product_metadata'))
            : {}

    return {
        product,
        campaign,
        brand,
        metadata,
        productUpdating: selectUiDomainValue(state, [
            PRODUCT_PAGE,
            'productUpdating',
        ]),
        showDetails: selectUiDomainValue(state, [PRODUCT_PAGE, 'showDetails']),
    }
}

const mapDispatchToProps = {
    updateProductRequest: updateProductPageProductDetailsRequest,
    toggleProductPageDetails,
}

const ResourceDetailsContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ResourceDetails)

export default withRouter(ResourceDetailsContainer)
