import React from 'react'
import PropTypes from 'prop-types'
import { Icon } from 'antd'
import autobind from 'autobind-decorator'
import isEmpty from 'lodash/isEmpty'
import truncate from 'lodash/truncate'

import { UNDEFINED_VALUE } from 'constants/formatting'
import { ResourceDetails } from 'components/ResourceDetails'
import { ToolTip } from 'components/ToolTip'
import { FACT_TYPE_LABELS } from 'configuration/factTypes'
import { formatCurrency, titleCase } from 'helpers/formatting'
import { SELECT_INPUT } from 'constants/inputTypes'
import { PAUSED, ENABLED, ARCHIVED } from 'constants/resourceStates'
import { SPONSORED_PRODUCT, HEADLINE_SEARCH } from 'constants/factTypes'
import { productAttributeTooltips } from 'configuration/attributes'
import { asinUrl } from 'helpers/urls'

import styles from './styles.scss'

const renderTooltipWithHelptext = attribute => {
    const info = productAttributeTooltips[attribute]

    return info.map(item => {
        const { title, description } = item

        return (
            <div className={styles.tooltip} key={description}>
                {title && <span className={styles.title}>{title}</span>}
                <span className={styles.description}>{description}</span>
            </div>
        )
    })
}

class KeywordResourceDetails extends React.Component {
    static propTypes = {
        product: PropTypes.shape({
            ad_group_id: PropTypes.string,
            asin: PropTypes.string,
            campaign_id: PropTypes.string,
            created_date: PropTypes.string,
            id: PropTypes.string,
            sku: PropTypes.string,
            state: PropTypes.string,
            updated_date: PropTypes.string,
        }),
        campaign: PropTypes.shape({
            budget: PropTypes.number,
            budget_type: PropTypes.string,
            campaign_type: PropTypes.oneOf([
                SPONSORED_PRODUCT,
                HEADLINE_SEARCH,
            ]),
            end_date: PropTypes.string,
            id: PropTypes.string,
            name: PropTypes.string,
            premium_bid_adjustment: PropTypes.bool,
            start_date: PropTypes.string,
            state: PropTypes.string,
            targeting_type: PropTypes.string,
        }),
        brand: PropTypes.shape({
            id: PropTypes.string,
            brand_name: PropTypes.string,
            region: PropTypes.string,
            country_code: PropTypes.string,
            currency_code: PropTypes.string,
            seller_string_id: PropTypes.string,
        }),
        metadata: PropTypes.shape({
            title: PropTypes.string,
            price: PropTypes.number,
            small_image_url: PropTypes.string,
            brand: PropTypes.string,
        }),
        productUpdating: PropTypes.bool.isRequired,
        showDetails: PropTypes.bool.isRequired,

        // actions
        updateProductRequest: PropTypes.func.isRequired,
        toggleProductPageDetails: PropTypes.func.isRequired,
    }

    static defaultProps = {
        product: {},
        campaign: {},
        brand: {},
        metadata: {},
    }

    @autobind
    handleUpdateProductDetails(values) {
        const { updateProductRequest } = this.props
        updateProductRequest(values)
    }

    isSponsoredProductProduct() {
        const { campaign } = this.props
        return campaign.campaign_type === SPONSORED_PRODUCT
    }

    editToolTip() {
        if (this.props.product.state === ARCHIVED) {
            return 'Archived Products cannot be modified.'
        }
        if (!this.isSponsoredProductProduct()) {
            return 'Modifying Headline Search Products is not yet supported.'
        }
        return null
    }

    render() {
        const {
            metadata,
            brand,
            campaign,
            product,
            productUpdating,
            showDetails,
            toggleProductPageDetails,
        } = this.props
        return (
            <ResourceDetails
                name={
                    isEmpty(metadata) ? (
                        product.asin
                    ) : (
                        <ToolTip
                            mouseEnterDelay={0.3}
                            placement="topLeft"
                            title={metadata.title}
                        >
                            {truncate(metadata.title, {
                                length: 100,
                            })}
                        </ToolTip>
                    )
                }
                showDetails={showDetails}
                allowEditing={
                    this.isSponsoredProductProduct() &&
                    product.state !== ARCHIVED
                }
                editToolTip={this.editToolTip()}
                updating={productUpdating}
                onShowDetailsClick={toggleProductPageDetails}
                onSave={this.handleUpdateProductDetails}
                details={[
                    {
                        value: metadata.small_image_url,
                        formatValue: value =>
                            value ? (
                                <img
                                    src={value}
                                    alt={metadata.title}
                                    className={styles['product-img']}
                                />
                            ) : (
                                <Icon
                                    type="picture"
                                    theme="outlined"
                                    className={styles['missing-img-icon']}
                                />
                            ),
                    },
                    {
                        label: 'Price',
                        value: metadata.price,
                        toolTip: renderTooltipWithHelptext('price'),
                        formatValue: value =>
                            value
                                ? formatCurrency(value, {
                                      decimal: true,
                                      currencyCode: metadata.currency_code,
                                  })
                                : UNDEFINED_VALUE,
                    },
                    {
                        label: 'ASIN',
                        value: product.asin,
                        toolTip: renderTooltipWithHelptext('asin'),
                        formatValue: value =>
                            value ? (
                                <a
                                    href={asinUrl(value, brand.country_code)}
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    {value}{' '}
                                    <Icon type="amazon" theme="outlined" />
                                </a>
                            ) : (
                                UNDEFINED_VALUE
                            ),
                    },
                    {
                        label: 'Brand',
                        value: brand.brand_name,
                        toolTip: renderTooltipWithHelptext('brand'),
                        formatValue: value => value || UNDEFINED_VALUE,
                    },
                    {
                        label: 'State',
                        value: product.state,
                        toolTip: renderTooltipWithHelptext('state'),
                        formatValue: titleCase,
                        // input fields
                        fieldId: 'state',
                        type: SELECT_INPUT,
                        options: [
                            { value: PAUSED, label: 'Paused' },
                            { value: ENABLED, label: 'Enabled' },
                        ],
                    },
                    {
                        label: 'Campaign Type',
                        value: campaign.campaign_type,
                        toolTip: renderTooltipWithHelptext('campaign_type'),
                        formatValue: value => FACT_TYPE_LABELS[value],
                    },
                ]}
            />
        )
    }
}

export default KeywordResourceDetails
