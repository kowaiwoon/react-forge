import { connect } from 'react-redux'
import get from 'lodash/get'

import {
    PRODUCT_PAGE,
    BRANDS_SUMMARY_PAGE,
    CAMPAIGNS_SUMMARY_PAGE,
    PRODUCTS_SUMMARY_PAGE,
} from 'constants/pages'
import {
    selectProduct,
    selectCampaign,
    selectBrand,
    selectMetadata,
} from 'selectors/entities'
import { selectDomainValue as selectUiDomainValue } from 'selectors/ui'
import { mountProductPageRequest, unmountProductPage } from 'actions/ui'
import { withTabState } from 'components/HigherOrderComponents'

import ProductPage from './ProductPage'

const getTopLevelPage = (brandId, campaignId) => {
    if (brandId) {
        return BRANDS_SUMMARY_PAGE
    } else if (campaignId) {
        return CAMPAIGNS_SUMMARY_PAGE
    }
    return PRODUCTS_SUMMARY_PAGE
}

const mapStateToProps = (state, ownProps) => {
    const { brandId, campaignId, productId } = ownProps.match.params
    const product = selectProduct(state, productId)
    const campaign = product
        ? selectCampaign(state, get(product, 'campaign'))
        : {}
    const brand = product ? selectBrand(state, get(product, 'profile')) : {}
    const metadata =
        product && get(product, 'product_metadata')
            ? selectMetadata(state, get(product, 'product_metadata'))
            : {}

    return {
        productId,
        product,
        campaign,
        brand,
        metadata,
        topLevelPage: getTopLevelPage(brandId, campaignId),
        mounting: selectUiDomainValue(state, [PRODUCT_PAGE, 'mounting']),
        productAggregate: selectUiDomainValue(state, [
            PRODUCT_PAGE,
            'aggregate',
        ]),
    }
}

const mapDispatchToProps = {
    mountProductPage: mountProductPageRequest,
    unmountProductPage,
}

const ProductPageContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ProductPage)

export default withTabState(ProductPageContainer, 'roi')
