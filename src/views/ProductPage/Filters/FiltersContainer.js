import { connect } from 'react-redux'

import { selectPageFilters, selectPageFilterSettings } from 'selectors/ui'
import {
    updatePageFilterForPage,
    resetPageFiltersForPage,
    updatePageFilterSettingsForPage,
} from 'actions/ui'
import { PRODUCT_PAGE } from 'constants/pages'

import Filters from './Filters'

const mapStateToProps = state => ({
    filterSettings: selectPageFilterSettings(state, PRODUCT_PAGE),
    selectedFilterValues: selectPageFilters(state, PRODUCT_PAGE),
})

const mapDispatchToProps = {
    updatePageFilter: updatePageFilterForPage(PRODUCT_PAGE),
    resetFilters: resetPageFiltersForPage(PRODUCT_PAGE),
    updateFilterSettings: updatePageFilterSettingsForPage(PRODUCT_PAGE),
}

const FiltersContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Filters)

export default FiltersContainer
