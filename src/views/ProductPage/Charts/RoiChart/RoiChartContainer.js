import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import get from 'lodash/get'

import {
    selectChartMetrics,
    selectDataForStackedBarChart,
    selectLoadingForStackedBarChart,
    selectPageDownloading,
} from 'selectors/ui'
import { selectCampaign, selectProduct } from 'selectors/entities'
import {
    updateChartMetrics,
    downloadProductPageTimeseriesRequest,
} from 'actions/ui'
import { PRODUCT_PAGE } from 'constants/pages'
import { ROI } from 'constants/charts'
import { getFactTypeObject } from 'helpers/factTypes'

import RoiChart from './RoiChart'

const mapStateToProps = (state, ownProps) => {
    const { productId } = ownProps.match.params
    const product = selectProduct(state, productId)
    const campaign = product
        ? selectCampaign(state, get(product, 'campaign'))
        : {}

    // convert the factType for a single campaign to an object
    // that's compatible with the chart component
    const factTypes = [getFactTypeObject(campaign.campaign_type)]

    const chartMetrics = selectChartMetrics(state, PRODUCT_PAGE, ROI)
    const loading = selectLoadingForStackedBarChart(state, PRODUCT_PAGE)
    const downloading = selectPageDownloading(state, PRODUCT_PAGE)
    const { axes, series } = selectDataForStackedBarChart(
        state,
        PRODUCT_PAGE,
        ROI
    )

    return {
        chartMetrics,
        factTypes,
        axes,
        series,
        loading,
        downloading,
    }
}

const mapDispatchToProps = {
    updateChartMetrics,
    downloadData: downloadProductPageTimeseriesRequest,
}

const RoiChartContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(RoiChart)

export default withRouter(RoiChartContainer)
