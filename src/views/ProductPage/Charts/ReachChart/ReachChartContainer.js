import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import get from 'lodash/get'

import {
    selectChartMetrics,
    selectDataForStackedBarChart,
    selectLoadingForStackedBarChart,
    selectPageDownloading,
} from 'selectors/ui'
import { selectCampaign, selectProduct } from 'selectors/entities'
import {
    updateChartMetrics,
    downloadProductPageTimeseriesRequest,
} from 'actions/ui'
import { PRODUCT_PAGE } from 'constants/pages'
import { REACH } from 'constants/charts'
import { getFactTypeObject } from 'helpers/factTypes'

import ReachChart from './ReachChart'

const mapStateToProps = (state, ownProps) => {
    const { productId } = ownProps.match.params
    const product = selectProduct(state, productId)
    const campaign = product
        ? selectCampaign(state, get(product, 'campaign'))
        : {}

    // convert the factType for a single campaign to an object
    // that's compatible with the chart component
    const factTypes = [getFactTypeObject(campaign.campaign_type)]

    const chartMetrics = selectChartMetrics(state, PRODUCT_PAGE, REACH)
    const loading = selectLoadingForStackedBarChart(state, PRODUCT_PAGE)
    const downloading = selectPageDownloading(state, PRODUCT_PAGE)
    const { axes, series } = selectDataForStackedBarChart(
        state,
        PRODUCT_PAGE,
        REACH
    )

    return {
        chartMetrics,
        factTypes,
        axes,
        series,
        loading,
        downloading,
    }
}

const mapDispatchToProps = {
    updateChartMetrics,
    downloadData: downloadProductPageTimeseriesRequest,
}

const ReachChartContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ReachChart)

export default withRouter(ReachChartContainer)
