import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import get from 'lodash/get'

import {
    selectChartMetrics,
    selectDataForStackedBarChart,
    selectLoadingForStackedBarChart,
    selectPageDownloading,
} from 'selectors/ui'
import { selectCampaign, selectProduct } from 'selectors/entities'
import {
    updateChartMetrics,
    downloadProductPageTimeseriesRequest,
} from 'actions/ui'
import { PRODUCT_PAGE } from 'constants/pages'
import { CONVERSIONS } from 'constants/charts'
import { getFactTypeObject } from 'helpers/factTypes'

import ConversionsChart from './ConversionsChart'

const mapStateToProps = (state, ownProps) => {
    const { productId } = ownProps.match.params
    const product = selectProduct(state, productId)
    const campaign = product
        ? selectCampaign(state, get(product, 'campaign'))
        : {}

    // convert the factType for a single campaign to an object
    // that's compatible with the chart component
    const factTypes = [getFactTypeObject(campaign.campaign_type)]

    const chartMetrics = selectChartMetrics(state, PRODUCT_PAGE, CONVERSIONS)
    const loading = selectLoadingForStackedBarChart(state, PRODUCT_PAGE)
    const downloading = selectPageDownloading(state, PRODUCT_PAGE)
    const { axes, series } = selectDataForStackedBarChart(
        state,
        PRODUCT_PAGE,
        CONVERSIONS
    )

    return {
        chartMetrics,
        factTypes,
        axes,
        series,
        loading,
        downloading,
    }
}

const mapDispatchToProps = {
    updateChartMetrics,
    downloadData: downloadProductPageTimeseriesRequest,
}

const ConversionsChartContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ConversionsChart)

export default withRouter(ConversionsChartContainer)
