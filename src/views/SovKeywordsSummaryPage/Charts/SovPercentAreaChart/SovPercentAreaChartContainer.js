import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import { SOV_KEYWORDS_SUMMARY_PAGE } from 'constants/pages'
import { SOV_AGGREGATION } from 'constants/filters'
import { updateSovKeywordsSummaryPageSovChartWeight } from 'actions/ui'
import {
    selectLoadingForSovChart,
    selectDataForSovChart,
    selectPageFilters,
    selectPageSovChartWeight,
} from 'selectors/ui'

import SovPercentAreaChart from './SovPercentAreaChart'

const mapStateToProps = state => {
    const { series, maxDataPoints } = selectDataForSovChart(
        state,
        SOV_KEYWORDS_SUMMARY_PAGE
    )
    return {
        loading: selectLoadingForSovChart(state, SOV_KEYWORDS_SUMMARY_PAGE),
        series,
        maxDataPoints,
        aggregate: selectPageFilters(state, SOV_KEYWORDS_SUMMARY_PAGE)[
            SOV_AGGREGATION
        ],
        weight: selectPageSovChartWeight(state, SOV_KEYWORDS_SUMMARY_PAGE),
    }
}

const mapDispatchToProps = {
    updateChartWeight: updateSovKeywordsSummaryPageSovChartWeight,
}

const SovPercentAreaChartContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(SovPercentAreaChart)

export default withRouter(SovPercentAreaChartContainer)
