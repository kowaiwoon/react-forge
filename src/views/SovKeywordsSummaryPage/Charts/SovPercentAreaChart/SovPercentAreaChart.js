import React from 'react'
import PropTypes from 'prop-types'

import { ALL_SOV_WEIGHTS } from 'configuration/sovWeights'
import { PercentageAreaChartCard } from 'components/PercentageAreaChartCard'
import { SOV_AGGREGATION_UNIT } from 'constants/reducerKeys'

const propTypes = {
    loading: PropTypes.bool.isRequired,
    series: PropTypes.array.isRequired,
    aggregate: PropTypes.oneOf([
        SOV_AGGREGATION_UNIT.DAY,
        SOV_AGGREGATION_UNIT.WEEK,
        SOV_AGGREGATION_UNIT.MONTH,
    ]).isRequired,
    maxDataPoints: PropTypes.number.isRequired,
    weight: PropTypes.oneOf(ALL_SOV_WEIGHTS).isRequired,

    // actions
    updateChartWeight: PropTypes.func.isRequired,
}

const defaultProps = {}

const SovPercentAreaChart = ({
    loading,
    series,
    aggregate,
    maxDataPoints,
    weight,
    updateChartWeight,
}) => (
    <PercentageAreaChartCard
        title="Share of Voice Timeseries"
        excerpt="The percent of the Amazon search engine "
        loading={loading}
        series={series}
        aggregate={aggregate}
        maxDataPoints={maxDataPoints}
        showDownload={false}
        weight={weight}
        updateChartWeight={updateChartWeight}
    />
)

SovPercentAreaChart.propTypes = propTypes
SovPercentAreaChart.defaultProps = defaultProps

export default SovPercentAreaChart
