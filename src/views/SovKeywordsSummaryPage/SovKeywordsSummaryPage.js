import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Tabs } from 'antd'

import { PageHeader } from 'components/PageHeader'
import { LoadingIndicator } from 'components/LoadingIndicator'

import { KeywordsTableContainer } from './Tables'
import { SovPercentAreaChartContainer } from './Charts'
import { FiltersContainer } from './Filters'
import styles from './styles.scss'

class SovKeywordsSummaryPage extends Component {
    static propTypes = {
        // Redux state
        mounting: PropTypes.bool.isRequired,

        // Redux actions
        mountSovKeywordsSummary: PropTypes.func.isRequired,
        unmountSovKeywordsSummary: PropTypes.func.isRequired,

        // tab state
        tab: PropTypes.oneOf(['timeline', 'table']).isRequired,
        handleTabChange: PropTypes.func.isRequired,
    }

    componentDidMount() {
        this.props.mountSovKeywordsSummary()
    }

    componentWillUnmount() {
        this.props.unmountSovKeywordsSummary()
    }

    render() {
        const { mounting, tab, handleTabChange } = this.props

        if (mounting) {
            return (
                <div className={styles['empty-content']}>
                    <LoadingIndicator size="small" />
                </div>
            )
        }

        return (
            <React.Fragment>
                <PageHeader
                    breadcrumbs={[{ name: 'Share of Voice' }]}
                    filterGroupComponent={<FiltersContainer />}
                />

                <Tabs
                    activeKey={tab}
                    onChange={handleTabChange}
                    tabPosition="top"
                    size="default"
                    tabBarStyle={{
                        fontWeight: '500', // same font-weight as selected tab
                    }}
                >
                    <Tabs.TabPane
                        tab="Share of Voice"
                        key="timeline"
                        forceRender
                    >
                        <SovPercentAreaChartContainer />
                    </Tabs.TabPane>
                    <Tabs.TabPane tab="Keywords" key="table" forceRender>
                        <KeywordsTableContainer />
                    </Tabs.TabPane>
                </Tabs>
            </React.Fragment>
        )
    }
}

export default SovKeywordsSummaryPage
