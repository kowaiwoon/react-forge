import { connect } from 'react-redux'

import {
    selectDomainValue as selectUiDomainValue,
    selectPageFilters,
    selectPageFilterSettings,
} from 'selectors/ui'
import {
    selectSovBrandsForFilters,
    selectSovKeywordsForFilters,
    selectSovKeywordCategoriesForFilter,
} from 'selectors/entities'
import {
    updatePageFilterForPage,
    resetPageFiltersForPage,
    updatePageFilterSettingsForPage,
    changeSovBrandsFilterInput,
    changeSovKeywordsFilterInput,
    changeSovKeywordCategoriesFilterInput,
} from 'actions/ui'
import { SOV_KEYWORDS_SUMMARY_PAGE } from 'constants/pages'

import Filters from './Filters'

const mapStateToProps = state => ({
    filterSettings: selectPageFilterSettings(state, SOV_KEYWORDS_SUMMARY_PAGE),
    selectedFilterValues: selectPageFilters(state, SOV_KEYWORDS_SUMMARY_PAGE),
    sovBrandsFilterOptions: selectSovBrandsForFilters(state),
    sovBrandsFilterLoading: selectUiDomainValue(state, [
        SOV_KEYWORDS_SUMMARY_PAGE,
        'sovBrandsFilterLoading',
    ]),
    sovKeywordsFilterOptions: selectSovKeywordsForFilters(state),
    sovKeywordsFilterLoading: selectUiDomainValue(state, [
        SOV_KEYWORDS_SUMMARY_PAGE,
        'sovKeywordsFilterLoading',
    ]),
    sovKeywordCategoriesFilterOptions: selectSovKeywordCategoriesForFilter(
        state
    ),
    sovKeywordCategoriesFilterLoading: selectUiDomainValue(state, [
        SOV_KEYWORDS_SUMMARY_PAGE,
        'sovKeywordCategoriesFilterLoading',
    ]),
})

const mapDispatchToProps = {
    updatePageFilter: updatePageFilterForPage(SOV_KEYWORDS_SUMMARY_PAGE),
    resetFilters: resetPageFiltersForPage(SOV_KEYWORDS_SUMMARY_PAGE),
    updateFilterSettings: updatePageFilterSettingsForPage(
        SOV_KEYWORDS_SUMMARY_PAGE
    ),
    changeSovBrandsFilterInput,
    changeSovKeywordsFilterInput,
    changeSovKeywordCategoriesFilterInput,
}

const FiltersContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Filters)

export default FiltersContainer
