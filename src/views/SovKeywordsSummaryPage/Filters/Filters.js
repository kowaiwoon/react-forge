import React from 'react'
import PropTypes from 'prop-types'
import autobind from 'autobind-decorator'
import get from 'lodash/get'
import set from 'lodash/fp/set'

import {
    SOV_AGGREGATION,
    SOV_SEARCH_TIMES,
    SOV_COUNTRIES,
    // SOV_BRANDS,
    SOV_KEYWORDS,
    SOV_KEYWORD_CATEGORIES,
    SOV_LANGUAGES,
    SOV_STATES,
    SOV_FOLDS,
    SOV_RESULT_TYPES,
} from 'constants/filters'
import {
    SovAggregationFilter,
    SearchTimeFilter,
    SovCountriesFilter,
    // SovBrandsFilter,
    SovKeywordsFilter,
    SovKeywordCategoriesFilter,
    SovLanguagesFilter,
    SovStatesFilter,
    SovFoldsFilter,
    SovResultTypeFilter,
} from 'components/Filters'
import { FilterGroup } from 'components/FilterGroup'

class Filters extends React.Component {
    static propTypes = {
        filterSettings: PropTypes.shape().isRequired,
        selectedFilterValues: PropTypes.shape().isRequired,

        // sovBrandsFilterOptions: PropTypes.array.isRequired,
        // sovBrandsFilterLoading: PropTypes.bool.isRequired,

        sovKeywordsFilterOptions: PropTypes.array.isRequired,
        sovKeywordsFilterLoading: PropTypes.bool.isRequired,
        sovKeywordCategoriesFilterOptions: PropTypes.array.isRequired,
        sovKeywordCategoriesFilterLoading: PropTypes.bool.isRequired,

        // actions
        updatePageFilter: PropTypes.func.isRequired,
        resetFilters: PropTypes.func.isRequired,
        updateFilterSettings: PropTypes.func.isRequired,
        // changeSovBrandsFilterInput: PropTypes.func.isRequired,
        changeSovKeywordsFilterInput: PropTypes.func.isRequired,
        changeSovKeywordCategoriesFilterInput: PropTypes.func.isRequired,
    }

    @autobind
    handleApply(key, value) {
        this.props.updatePageFilter({
            key,
            value,
        })
    }

    @autobind
    handleClose(filter) {
        const { filterSettings } = this.props
        this.props.updateFilterSettings(
            set(['displayState', filter], false, filterSettings)
        )
    }

    render() {
        const {
            filterSettings,
            updateFilterSettings,
            resetFilters,
            selectedFilterValues,
            // sovBrandsFilterOptions,
            // sovBrandsFilterLoading,
            sovKeywordsFilterOptions,
            sovKeywordsFilterLoading,
            sovKeywordCategoriesFilterOptions,
            sovKeywordCategoriesFilterLoading,
            // changeSovBrandsFilterInput,
            changeSovKeywordsFilterInput,
            changeSovKeywordCategoriesFilterInput,
        } = this.props

        return (
            <FilterGroup
                filterSettings={filterSettings}
                updateFilterSettings={updateFilterSettings}
                resetFilters={resetFilters}
            >
                <SovAggregationFilter
                    onApply={this.handleApply}
                    value={selectedFilterValues[SOV_AGGREGATION]}
                    closable={false}
                />
                <SearchTimeFilter
                    values={selectedFilterValues[SOV_SEARCH_TIMES]}
                    onApply={this.handleApply}
                    closable={false}
                />
                <SovCountriesFilter
                    values={selectedFilterValues[SOV_COUNTRIES]}
                    onApply={this.handleApply}
                    onCloseFilter={this.handleClose}
                    closable
                />
                {/* TODO: Uncomment when chart and table are in sync with brands selected */}
                {/* <SovBrandsFilter
                    values={selectedFilterValues[SOV_BRANDS]}
                    options={sovBrandsFilterOptions}
                    onApply={this.handleApply}
                    onCloseFilter={this.handleClose}
                    closable
                    onChangeInput={changeSovBrandsFilterInput}
                    loading={sovBrandsFilterLoading}
                /> */}
                <SovKeywordsFilter
                    values={selectedFilterValues[SOV_KEYWORDS]}
                    options={sovKeywordsFilterOptions}
                    onApply={this.handleApply}
                    onCloseFilter={this.handleClose}
                    closable
                    onChangeInput={changeSovKeywordsFilterInput}
                    loading={sovKeywordsFilterLoading}
                />
                <SovKeywordCategoriesFilter
                    values={selectedFilterValues[SOV_KEYWORD_CATEGORIES]}
                    options={sovKeywordCategoriesFilterOptions}
                    onApply={this.handleApply}
                    onCloseFilter={this.handleClose}
                    closable
                    onChangeInput={changeSovKeywordCategoriesFilterInput}
                    loading={sovKeywordCategoriesFilterLoading}
                />
                <SovLanguagesFilter
                    values={selectedFilterValues[SOV_LANGUAGES]}
                    onApply={this.handleApply}
                    onCloseFilter={this.handleClose}
                    closable
                />
                <SovStatesFilter
                    values={selectedFilterValues[SOV_STATES]}
                    onApply={this.handleApply}
                    onCloseFilter={this.handleClose}
                    closable
                />
                <SovFoldsFilter
                    operator={get(
                        selectedFilterValues[SOV_FOLDS],
                        ['operator'],
                        null
                    )}
                    value={get(
                        selectedFilterValues[SOV_FOLDS],
                        ['value'],
                        null
                    )}
                    onApply={this.handleApply}
                    onCloseFilter={this.handleClose}
                    closable
                />
                <SovResultTypeFilter
                    values={selectedFilterValues[SOV_RESULT_TYPES]}
                    onApply={this.handleApply}
                    onCloseFilter={this.handleClose}
                    closable
                />
            </FilterGroup>
        )
    }
}

export default Filters
