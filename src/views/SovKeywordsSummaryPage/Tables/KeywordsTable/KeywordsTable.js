import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { generatePath } from 'react-router-dom'
import autobind from 'autobind-decorator'

import { AppLink } from 'components/AppLink'
import { PaginatedTable } from 'components/PaginatedTable'
import { getTablePaginationOptions } from 'helpers/pagination'
import { getPath } from 'helpers/pages'
import { SOV_KEYWORD_PAGE } from 'constants/pages'
import {
    titleCase,
    formatCerebroDateTime,
    formatFrequency,
} from 'helpers/formatting'
import { PAUSED, ARCHIVED, ENABLED } from 'constants/resourceStates'
import { ACTIONS, NUMBER_INPUT, SELECT_INPUT } from 'constants/inputTypes'
import {
    SOV_KEYWORD_FREQUENCY_PRECISION,
    SOV_KEYWORD_FREQUENCY_STEP,
    SOV_KEYWORD_FREQUENCY_MAX,
    SOV_KEYWORD_FREQUENCY_MIN,
} from 'constants/sovKeywords'
import { hasPermissions } from 'helpers/featurePermissions'
import { SOV_WRITE } from 'constants/featurePermissions'
import { ContentCard } from 'components/ContentCard'
import {
    SettingsButton,
    DownloadButton,
    CreateResourceButton,
} from 'components/Buttons'
import { SettingsModal } from 'components/SettingsModal'

import { AttachingDetails } from './AttachingDetails'

class KeywordsTable extends Component {
    static propTypes = {
        downloading: PropTypes.bool.isRequired,
        tableData: PropTypes.shape({
            data: PropTypes.arrayOf(PropTypes.shape()),
            loading: PropTypes.bool,
            attaching: PropTypes.bool,
            updating: PropTypes.bool,
            deleting: PropTypes.bool,
            pagination: PropTypes.shape({
                pageSize: PropTypes.number,
                current: PropTypes.number,
                total: PropTypes.number,
            }),
            sorter: PropTypes.shape({
                field: PropTypes.string,
                order: PropTypes.oneOf(['descend', 'ascend']),
            }),
            columnSettings: PropTypes.shape({
                actionColumns: PropTypes.array,
                order: PropTypes.array,
                displayState: PropTypes.shape(),
            }),
        }).isRequired,
        featurePermissions: PropTypes.arrayOf(PropTypes.string).isRequired,

        // actions
        updatePagination: PropTypes.func.isRequired,
        updateSorter: PropTypes.func.isRequired,
        updateColumnSettings: PropTypes.func.isRequired,
        reloadData: PropTypes.func.isRequired,
        downloadData: PropTypes.func.isRequired,
        attachKeywordsRequest: PropTypes.func.isRequired,
        updateKeywordRequest: PropTypes.func.isRequired,
        deleteKeywordRequest: PropTypes.func.isRequired,
    }

    state = {
        settingsModalVisible: false,
        collapseOpen: false,
    }

    static getTableOptions() {
        return {
            name: 'keyword',
            rowKey: 'id',
            columns: {
                actions: {
                    title: 'Actions',
                    dataIndex: 'id',
                    fixed: 'right',
                    sorter: false,
                    align: 'center',
                    // custom fields
                    type: ACTIONS,
                },
                text: {
                    title: 'Keyword',
                    dataIndex: 'text',
                    fixed: 'left',
                    sorter: true,
                    align: 'left',
                    width: 300,
                    render: (text, record) => (
                        <AppLink
                            to={generatePath(getPath(SOV_KEYWORD_PAGE), {
                                sovKeywordId: record.id,
                            })}
                        >
                            {text}
                        </AppLink>
                    ),
                },
                country_code: {
                    title: 'Country',
                    dataIndex: 'country_code',
                    sorter: true,
                    align: 'center',
                },
                language_code: {
                    title: 'Language',
                    dataIndex: 'language_code',
                    sorter: true,
                    align: 'center',
                },
                frequency_in_hours: {
                    title: 'Frequency',
                    dataIndex: 'frequency_in_hours',
                    sorter: true,
                    align: 'left',
                    render: value => formatFrequency(value),
                    // custom fields
                    type: NUMBER_INPUT,
                    fieldId: 'frequency_in_hours',
                    min: SOV_KEYWORD_FREQUENCY_MIN,
                    max: SOV_KEYWORD_FREQUENCY_MAX,
                    step: SOV_KEYWORD_FREQUENCY_STEP,
                    precision: SOV_KEYWORD_FREQUENCY_PRECISION,
                },
                created_at: {
                    title: 'Created',
                    dataIndex: 'created_at',
                    sorter: true,
                    align: 'right',
                    width: 200,
                    render: value => formatCerebroDateTime(value),
                },
                category: {
                    title: 'Category',
                    dataIndex: 'category',
                    sorter: true,
                    align: 'left',
                },
                state: {
                    title: 'Status',
                    dataIndex: 'state',
                    sorter: true,
                    align: 'center',
                    render: value => titleCase(value),
                    // custom fields
                    type: SELECT_INPUT,
                    fieldId: 'state',
                    options: [
                        { value: PAUSED, label: 'Paused' },
                        { value: ENABLED, label: 'Enabled' },
                    ],
                },
            },
        }
    }

    @autobind
    disableRowActions(record) {
        const { featurePermissions } = this.props
        return (
            !hasPermissions(featurePermissions, SOV_WRITE) ||
            record.state === ARCHIVED
        )
    }

    @autobind
    actionsToolTip(record) {
        const { featurePermissions } = this.props
        if (!hasPermissions(featurePermissions, SOV_WRITE)) {
            return 'Must have write access to modify Share of Voice Keywords'
        }
        if (record.state === ARCHIVED) {
            return 'Archived Keywords cannot be modified.'
        }
        return null
    }

    @autobind
    attachRecords(values) {
        this.props.attachKeywordsRequest({ keywords: values })
    }

    @autobind
    saveRecord({ id: keywordId, values: data }) {
        this.props.updateKeywordRequest({ keywordId, data })
    }

    @autobind
    deleteRecord({ id: keywordId }) {
        this.props.deleteKeywordRequest({ keywordId })
    }

    @autobind
    handleToggleSettingsModal() {
        this.setState({
            settingsModalVisible: !this.state.settingsModalVisible,
        })
    }

    @autobind
    handleUpdateColumnSettings(settings) {
        const { updateColumnSettings, reloadData } = this.props

        updateColumnSettings(settings)

        // Reload data
        reloadData()

        // Toggle settings modal
        this.handleToggleSettingsModal()
    }

    @autobind
    handleToggleCollapse() {
        this.setState({
            collapseOpen: !this.state.collapseOpen,
        })
    }

    render() {
        const {
            downloading,
            tableData,
            updatePagination,
            updateSorter,
            reloadData,
            downloadData,
        } = this.props
        const { settingsModalVisible, collapseOpen } = this.state
        const tableOptions = KeywordsTable.getTableOptions()

        return (
            <ContentCard
                title="SOV Keywords Table"
                subTitle="Manage and view tabular data for all SOV Keywords"
                actions={[
                    <CreateResourceButton onClick={this.handleToggleCollapse}>
                        Add New Keywords
                    </CreateResourceButton>,
                    <SettingsButton
                        onClick={this.handleToggleSettingsModal}
                        tooltipTitle="Customize table columns"
                    />,
                    <DownloadButton
                        loading={downloading}
                        onClick={downloadData}
                    />,
                ]}
                collapseOpen={collapseOpen}
                collapseContent={
                    <AttachingDetails
                        attaching={tableData.attaching}
                        onAttach={this.attachRecords}
                        onCancel={this.handleToggleCollapse}
                    />
                }
            >
                <PaginatedTable
                    tableOptions={tableOptions}
                    data={tableData.data}
                    loading={tableData.loading}
                    updating={tableData.updating}
                    deleting={tableData.deleting}
                    pagination={{
                        ...getTablePaginationOptions('SOV Keywords'),
                        ...tableData.pagination,
                    }}
                    sorter={tableData.sorter}
                    columnSettings={tableData.columnSettings}
                    updatePagination={updatePagination}
                    updateSorter={updateSorter}
                    reloadData={reloadData}
                    downloadData={downloadData}
                    saveRecord={this.saveRecord}
                    deleteRecord={this.deleteRecord}
                    disableRowActions={this.disableRowActions}
                    actionsToolTip={this.actionsToolTip}
                />
                <SettingsModal
                    droppableId="paginatedTableSettingsModal"
                    modalTitle="Customize Table Columns"
                    visible={settingsModalVisible}
                    handleCancel={this.handleToggleSettingsModal}
                    handleOk={this.handleUpdateColumnSettings}
                    settings={tableData.columnSettings}
                    settingTitles={tableOptions.columns}
                />
            </ContentCard>
        )
    }
}

export default KeywordsTable
