import React, { Component } from 'react'
import PropTypes from 'prop-types'
import autobind from 'autobind-decorator'
import has from 'lodash/has'
import get from 'lodash/get'
import noop from 'lodash/noop'
import map from 'lodash/map'

import { ScrollableTable } from 'components/PaginatedTable'
import { formatFrequency } from 'helpers/formatting'
import { getSovRegionCountryMap, getSovCountryLanguageMap } from 'helpers/codes'
import { PAUSED, ENABLED } from 'constants/resourceStates'
import {
    ACTIONS,
    NUMBER_INPUT,
    SELECT_INPUT,
    CASCADER_INPUT,
} from 'constants/inputTypes'
import {
    SOV_KEYWORD_FREQUENCY_MIN,
    SOV_KEYWORD_FREQUENCY_MAX,
    SOV_KEYWORD_FREQUENCY_STEP,
    SOV_KEYWORD_FREQUENCY_PRECISION,
} from 'constants/sovKeywords'
import { isActions } from 'helpers/inputTypes'

import AttachingTableEditableCell from './AttachingTableEditableCell'

const REGION_COUNTRY_LANGUAGE_CASCADER_OPTIONS = map(
    getSovRegionCountryMap(),
    (countries, region) => ({
        value: region,
        label: region,
        children: countries.map(country => ({
            value: country,
            label: country,
            children: getSovCountryLanguageMap()[country].map(language => ({
                value: language,
                label: language,
            })),
        })),
    })
)

class AttachingTable extends Component {
    static propTypes = {
        rowKey: PropTypes.string.isRequired,
        data: PropTypes.arrayOf(PropTypes.shape()),
        attaching: PropTypes.bool.isRequired,

        // actions
        changeRecord: PropTypes.func.isRequired,
        deleteRecord: PropTypes.func.isRequired,
    }

    static defaultProps = {
        data: [],
    }

    getColumns() {
        const { rowKey, changeRecord } = this.props
        const columns = [
            {
                title: 'Added Keywords',
                dataIndex: 'text',
                fixed: 'left',
                align: 'left',
                width: 200,
            },
            {
                title: 'Region / Country / Language',
                dataIndex: 'region_country_lang',
                align: 'left',
                // custom fields
                type: CASCADER_INPUT,
                fieldId: 'region_country_lang',
                options: REGION_COUNTRY_LANGUAGE_CASCADER_OPTIONS,
            },
            {
                title: 'Status',
                dataIndex: 'state',
                align: 'left',
                // custom fields
                type: SELECT_INPUT,
                fieldId: 'state',
                options: [
                    { value: PAUSED, label: 'Paused' },
                    { value: ENABLED, label: 'Enabled' },
                ],
            },
            {
                title: 'Frequency',
                dataIndex: 'frequency_in_hours',
                sorter: true,
                align: 'left',
                render: value => formatFrequency(value),
                // custom fields
                type: NUMBER_INPUT,
                fieldId: 'frequency_in_hours',
                min: SOV_KEYWORD_FREQUENCY_MIN,
                max: SOV_KEYWORD_FREQUENCY_MAX,
                step: SOV_KEYWORD_FREQUENCY_STEP,
                precision: SOV_KEYWORD_FREQUENCY_PRECISION,
            },
            {
                title: 'Actions',
                dataIndex: rowKey,
                fixed: 'right',
                align: 'center',
                // custom fields
                type: ACTIONS,
            },
        ]
        return columns
            .map(column => ({
                width: isActions(column.type)
                    ? 70
                    : column.title.length * 6 + 70,
                ...column,
            }))
            .map(column => {
                if (has(column, 'type')) {
                    return {
                        ...column,
                        onCell: record => ({
                            record,
                            rowKey,
                            cellType: column.type,
                            fieldId: column.fieldId,
                            dataIndex: column.dataIndex,
                            options: column.options,
                            parser: column.parser,
                            min: column.min,
                            max: column.max,
                            step: column.step,
                            precision: column.precision,
                            onChange: changeRecord,
                            onDelete: this.delete,
                        }),
                    }
                }
                return column
            })
    }

    @autobind
    delete(record) {
        const { rowKey, deleteRecord } = this.props
        deleteRecord({ id: get(record, rowKey) })
    }

    render() {
        const components = {
            body: {
                cell: AttachingTableEditableCell,
            },
        }
        const { rowKey, data, attaching } = this.props
        const columns = this.getColumns()

        return (
            <ScrollableTable
                components={components}
                dataSource={data}
                columns={columns}
                rowClassName="editable-row"
                rowKey={rowKey}
                pagination={false}
                loading={attaching}
                onChange={noop}
                onRow={noop}
                tableSize="middle"
                maxHeight={null}
                maxScrollY={({ height }) => {
                    if (height > 350) {
                        return 350
                    }
                    return 0
                }}
                locale={{
                    emptyText:
                        "Keywords entered will appear here. Once added, review each keywords' region code, country code, language code, status, and frequency.",
                }}
            />
        )
    }
}

export default AttachingTable
