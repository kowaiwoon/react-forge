import { connect } from 'react-redux'

import { SOV_KEYWORDS_SUMMARY_PAGE } from 'constants/pages'
import {
    selectDomainValue as selectUiDomainValue,
    selectPageDownloading,
} from 'selectors/ui'
import { selectDomainValue as selectAuthDomainValue } from 'selectors/auth'
import {
    updateSovKeywordsSummaryPageTablePagination,
    updateSovKeywordsSummaryPageTableSorter,
    updateSovKeywordsSummaryPageTableSettings,
    fetchSovKeywordsSummaryPageTableRequest,
    downloadSovKeywordsSummaryPageTableRequest,
    attachSovKeywordsSummaryPageTableKeywordsRequest,
    updateSovKeywordsSummaryPageTableKeywordRequest,
    deleteSovKeywordsSummaryPageTableKeywordRequest,
} from 'actions/ui'

import KeywordsTable from './KeywordsTable'

const mapStateToProps = state => ({
    tableData: selectUiDomainValue(state, [SOV_KEYWORDS_SUMMARY_PAGE, 'table']),
    downloading: selectPageDownloading(state, SOV_KEYWORDS_SUMMARY_PAGE),
    featurePermissions: selectAuthDomainValue(state, 'featurePermissions'),
})

const mapDispatchToProps = {
    updatePagination: updateSovKeywordsSummaryPageTablePagination,
    updateSorter: updateSovKeywordsSummaryPageTableSorter,
    updateColumnSettings: updateSovKeywordsSummaryPageTableSettings,
    reloadData: fetchSovKeywordsSummaryPageTableRequest,
    downloadData: downloadSovKeywordsSummaryPageTableRequest,
    attachKeywordsRequest: attachSovKeywordsSummaryPageTableKeywordsRequest,
    updateKeywordRequest: updateSovKeywordsSummaryPageTableKeywordRequest,
    deleteKeywordRequest: deleteSovKeywordsSummaryPageTableKeywordRequest,
}

const KeywordsTableContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(KeywordsTable)

export default KeywordsTableContainer
