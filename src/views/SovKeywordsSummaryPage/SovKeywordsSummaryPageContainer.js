import { connect } from 'react-redux'

import { SOV_KEYWORDS_SUMMARY_PAGE } from 'constants/pages'
import { selectDomainValue as selectUiDomainValue } from 'selectors/ui'
import {
    mountSovKeywordsSummaryPageRequest,
    unmountSovKeywordsSummaryPage,
} from 'actions/ui'
import { withTabState } from 'components/HigherOrderComponents'

import SovKeywordsSummaryPage from './SovKeywordsSummaryPage'

const mapStateToProps = state => ({
    mounting: selectUiDomainValue(state, [
        SOV_KEYWORDS_SUMMARY_PAGE,
        'mounting',
    ]),
})

const mapDispatchToProps = {
    mountSovKeywordsSummary: mountSovKeywordsSummaryPageRequest,
    unmountSovKeywordsSummary: unmountSovKeywordsSummaryPage,
}

const SovKeywordsSummaryPageContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(SovKeywordsSummaryPage)

export default withTabState(SovKeywordsSummaryPageContainer, 'timeline')
