import { normalize } from 'normalizr'

import { keywordSchema, productSchema } from '../index'

export const keyword = {
    id: '244301367846641',
    text: 'drums and percussion',
    match_type: 'broad',
    state: 'enabled',
    bid: 0.5,
    campaign: {
        id: '182729361125729',
        name: 'Campaign1',
        campaign_type: 'sponsoredProducts',
        targeting_type: 'manual',
        state: 'enabled',
        budget: 10,
        budget_type: 'daily',
        start_date: '2016-05-09T00:00:00',
        end_date: '2016-09-22T00:00:00',
        premium_bid_adjustment: false,
    },
    ad_group: {
        id: '205354846279036',
        name: 'AdGroup1',
        default_bid: 0.5,
        state: 'enabled',
    },
    profile: {
        id: '3931123856677272',
        brand_name: 'Profile1',
        seller_string_id: null,
        region: 'NA',
        country_code: 'US',
        currency_code: 'USD',
    },
}

export const product = {
    id: '26705868846260',
    campaign: {
        id: '135381972560285',
        name: 'Marking SP Manual June 2016 (1)',
        campaign_type: 'sponsoredProducts',
        targeting_type: 'manual',
        state: 'paused',
        budget: 15.0,
        budget_type: 'daily',
        start_date: '2016-06-20T00:00:00Z',
        end_date: null,
        premium_bid_adjustment: true,
    },
    campaign_id: '135381972560285',
    ad_group_id: '149460025322222',
    sku: null,
    asin: 'B000OZMSYC',
    state: 'paused',
    created_date: '2018-10-10T23:43:13.187930Z',
    updated_date: '2018-10-19T02:41:27.646774Z',
}

describe('[Schemas]', () => {
    describe('keywordSchema', () => {
        it('normalizes data', () => {
            const normalizedData = normalize(keyword, keywordSchema)
            expect(normalizedData).toEqual({
                entities: {
                    adGroups: {
                        '205354846279036': {
                            default_bid: 0.5,
                            id: '205354846279036',
                            name: 'AdGroup1',
                            state: 'enabled',
                        },
                    },
                    brands: {
                        '3931123856677272': {
                            brand_name: 'Profile1',
                            country_code: 'US',
                            currency_code: 'USD',
                            id: '3931123856677272',
                            region: 'NA',
                            seller_string_id: null,
                        },
                    },
                    campaigns: {
                        '182729361125729': {
                            budget: 10,
                            budget_type: 'daily',
                            campaign_type: 'sponsoredProducts',
                            end_date: '2016-09-22T00:00:00',
                            id: '182729361125729',
                            name: 'Campaign1',
                            premium_bid_adjustment: false,
                            start_date: '2016-05-09T00:00:00',
                            state: 'enabled',
                            targeting_type: 'manual',
                        },
                    },
                    keywords: {
                        '244301367846641': {
                            ad_group: '205354846279036',
                            bid: 0.5,
                            campaign: '182729361125729',
                            id: '244301367846641',
                            match_type: 'broad',
                            profile: '3931123856677272',
                            state: 'enabled',
                            text: 'drums and percussion',
                        },
                    },
                },
                result: '244301367846641',
            })
        })
    })

    describe('productsSchema', () => {
        it('normalizes data', () => {
            const normalizedData = normalize(product, productSchema)
            expect(normalizedData).toEqual({
                entities: {
                    campaigns: {
                        '135381972560285': {
                            budget: 15,
                            budget_type: 'daily',
                            campaign_type: 'sponsoredProducts',
                            end_date: null,
                            id: '135381972560285',
                            name: 'Marking SP Manual June 2016 (1)',
                            premium_bid_adjustment: true,
                            start_date: '2016-06-20T00:00:00Z',
                            state: 'paused',
                            targeting_type: 'manual',
                        },
                    },
                    products: {
                        '26705868846260': {
                            ad_group_id: '149460025322222',
                            asin: 'B000OZMSYC',
                            campaign: '135381972560285',
                            campaign_id: '135381972560285',
                            created_date: '2018-10-10T23:43:13.187930Z',
                            id: '26705868846260',
                            sku: null,
                            state: 'paused',
                            updated_date: '2018-10-19T02:41:27.646774Z',
                        },
                    },
                },
                result: '26705868846260',
            })
        })
    })
})
