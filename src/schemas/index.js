import { schema } from 'normalizr'

// brands
export const brandSchema = new schema.Entity('brands')
export const brandListSchema = new schema.Array(brandSchema)

// campaigns
export const campaignSchema = new schema.Entity('campaigns', {
    profile: brandSchema,
})
export const campaignListSchema = new schema.Array(campaignSchema)

// ad groups
export const adGroupSchema = new schema.Entity('adGroups')

// keywords
export const keywordSchema = new schema.Entity('keywords', {
    campaign: campaignSchema,
    profile: brandSchema,
    ad_group: adGroupSchema,
})
export const keywordListSchema = new schema.Array(keywordSchema)

// product metadata
export const productMetadataSchema = new schema.Entity('product_metadata')

// products
export const productSchema = new schema.Entity('products', {
    campaign: campaignSchema,
    profile: brandSchema,
    product_metadata: productMetadataSchema,
})
export const productListSchema = new schema.Array(productSchema)

// sov keywords
export const sovKeywordSchema = new schema.Entity('sovKeywords')
export const sovKeywordListSchema = new schema.Array(sovKeywordSchema)

// users
export const userSchema = new schema.Entity('users')
export const userListSchema = new schema.Array(userSchema)

// organizations
export const organizationSchema = new schema.Entity('organizations', {
    owner: userSchema,
})
export const organizationListSchema = new schema.Array(organizationSchema)

// org groups
export const organizationGroupSchema = new schema.Entity('groups', {
    organization: organizationSchema,
    members: [userSchema],
})
export const organizationGroupListSchema = new schema.Array(
    organizationGroupSchema
)

// invitations
export const invitationSchema = new schema.Entity('invitations', {
    organization: organizationSchema,
    user: userSchema,
    inviter: userSchema,
})
export const invitationListSchema = new schema.Array(invitationSchema)
