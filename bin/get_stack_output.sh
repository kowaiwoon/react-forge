#!/usr/bin/env bash

STACK_NAME=ForgeInfrastructure-${STAGE}

aws --region ${AWS_DEFAULT_REGION} \
    --query "Stacks[0].Outputs[?OutputKey=='${1}'].OutputValue | [0]" \
    --output text \
    cloudformation describe-stacks \
    --stack-name ${STACK_NAME}
