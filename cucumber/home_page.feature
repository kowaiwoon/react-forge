Feature: Home Page
  The Downstream Home Page provides users with a high-level overview of all their AMS accounts, integrated into
  a set of unified visualizations.  This page is valuable for customers because they can view their entire AMS
  marketing investment in a single page, as opposed to having to login to many separate AMS accounts across
  multiple marketplaces (e.g., US, UK, FR, ES, etc.).

  Background:
    Given the user is logged in
    And the user has integrated data

  Scenario: User clicks on Return on Investment tab with default filters
    Given default filters are selected
    When the user clicks on the Return on Investment tab
    Then the Return on Investment chart should be shown
    And the Return on Investment chart should have data

  Scenario: User clicks on Reach tab with default filters
    Given default filters are selected
    When the user clicks on the Reach tab
    Then the Reach chart should be shown
    And the Reach chart should have data

  Scenario: User clicks on Conversions tab with default filters
    Given default filters are selected
    When the user clicks on the Reach tab
    Then the Reach chart should be shown
    And the Reach chart should have data

  Scenario: User changes date range filter, with Return on Investment tab selected
    Given the Return on Investment tab is shown
    When the user changes the Date Range filter to 2018-05-15 - 2018-08-13
    Then the Return on Investment chart should be shown
    And the Return on Investment chart should have 90 days of data
