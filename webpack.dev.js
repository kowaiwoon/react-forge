const path = require('path')

const webpack = require('webpack')
const merge = require('webpack-merge')

const commonConfigPromise = require('./webpack.common.js')
const ANT_THEME_OVERRIDES = require('./ant-theme')

const devConfigPromise = () =>
    commonConfigPromise().then(commonConfig =>
        merge(commonConfig, {
            mode: 'development',
            devtool: 'eval-source-map',
            devServer: {
                contentBase: path.resolve(__dirname, './dist'),
                hot: true,
                historyApiFallback: true,
                host: '0.0.0.0',
                headers: {
                    'Access-Control-Allow-Origin': '*',
                },
            },
            module: {
                // Don't run postCSS for development
                // It boosts the hot-reloading performance of style changes and developer experience
                rules: [
                    {
                        test: /react-date-range\/dist\/styles\.css$/,
                        use: ['style-loader', 'css-loader'],
                    },

                    {
                        test: /react-date-range\/dist\/theme\/default\.css$/,
                        use: ['style-loader', 'css-loader'],
                    },

                    {
                        test: /\.less$/,
                        use: [
                            'style-loader',
                            'css-loader',
                            {
                                loader: 'less-loader',
                                options: {
                                    javascriptEnabled: true,
                                    modifyVars: ANT_THEME_OVERRIDES,
                                },
                            },
                        ],
                    },

                    {
                        test: /\.scss$/,
                        exclude: /node_modules/,
                        use: [
                            'style-loader',
                            {
                                loader: 'css-loader',
                                options: {
                                    sourceMap: true,
                                    modules: true,
                                    localIdentName: '[local]___[hash:base64:5]',
                                    importLoaders: 1,
                                },
                            },
                            'fast-sass-loader',
                        ],
                    },
                ],
            },

            plugins: [new webpack.HotModuleReplacementPlugin()],
        })
    )

module.exports = devConfigPromise
