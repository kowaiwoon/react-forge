const mockReactGA = jest.genMockFromModule('react-ga')

mockReactGA.create = jest.fn(() => mockReactGA)

export default mockReactGA
