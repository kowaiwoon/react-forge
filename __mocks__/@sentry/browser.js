const mockSentry = jest.genMockFromModule('@sentry/browser')

export const configureScope = jest.fn(() => mockSentry)
export const captureException = jest.fn(() => mockSentry)
